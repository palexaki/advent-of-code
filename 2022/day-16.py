import functools
import re
import time


class Valve:
    def __init__(self, name, flowrate, sub_valve_names):
        self.name = name
        self.flowrate = flowrate
        self._sub_names = sub_valve_names
        self.children = []

    def add_child(self, valve):
        self.children.append(valve)

    @staticmethod
    def parse_valve(line):
        match = re.match("Valve ([A-Z]{2}) has flow rate=(\\d+); tunnels? leads? to valves? ((?:[A-Z]{2}(?:, )?)+)",
                         line)
        return Valve(match.group(1), int(match.group(2)), match.group(3).split(', '))

    def __repr__(self):
        return f"Valve(name={self.name}, flowrate={self.flowrate}, sub_names={self._sub_names or [v.name for v in self.children]})"


@functools.cache
def find_max_flow_rate(valve: Valve, time_left=30, opened=frozenset()) -> int:
    if time_left <= 0:
        return 0
    max_flowed = 0
    if valve.flowrate and valve not in opened:
        max_flowed = find_max_flow_rate(valve, time_left - 1, opened | frozenset([valve]))
        max_flowed += valve.flowrate * (time_left - 1)
    return max(max_flowed, max(find_max_flow_rate(child, time_left - 1, opened) for child in valve.children))


def task1(valves):
    return find_max_flow_rate(valves['AA'])


@functools.cache
def find_max_flow_rate_together(valve: Valve, elephant: bool, start: Valve, time_left=26, opened=frozenset()) -> int:
    if time_left <= 0:
        return 0
    max_flowed = 0
    if not elephant:
        max_flowed = find_max_flow_rate_together(start, True, start, 26, opened)
    time_left -= 1
    if valve.flowrate and valve not in opened:
        flowed = find_max_flow_rate_together(valve, elephant, start, time_left,  opened | frozenset([valve]))
        flowed += valve.flowrate * time_left
        max_flowed = max(max_flowed, flowed)
    return max(max_flowed,
               max(find_max_flow_rate_together(child, elephant, start, time_left, opened)
                   for child in valve.children))
    # if me.flowrate and me.name not in opened and elephant.flowrate and elephant.name not in opened and me != elephant:
    #     max_flowed = find_max_flow_rate_together(me,
    #                                              elephant,
    #                                              flowing_valves,
    #                                              time_left,
    #                                              opened | frozenset([me.name, elephant.name]))
    #     max_flowed += (me.flowrate + elephant.flowrate) * time_left
    #
    # if me.flowrate and me.name not in opened:
    #     my_flow = me.flowrate * time_left
    #     for el_child in elephant.children:
    #         me_opened_flow = find_max_flow_rate_together(me,
    #                                                      el_child,
    #                                                      flowing_valves,
    #                                                      time_left,
    #                                                      opened | frozenset([me.name])) + my_flow
    #         max_flowed = max(max_flowed, me_opened_flow)
    #
    # if elephant.flowrate and elephant.name not in opened:
    #     el_flow = elephant.flowrate * time_left
    #     for my_child in me.children:
    #         el_opened_flow = find_max_flow_rate_together(my_child,
    #                                                      elephant,
    #                                                      flowing_valves,
    #                                                      time_left,
    #                                                      opened | frozenset([elephant.name])) + el_flow
    #         max_flowed = max(max_flowed, el_opened_flow)
    # for my_child in me.children:
    #     for el_child in elephant.children:
    #         max_flowed = max(max_flowed,
    #                          find_max_flow_rate_together(my_child, el_child, flowing_valves, time_left, opened))
    # return max_flowed


def task2(valves):
    return find_max_flow_rate_together(valves['AA'], False, valves['AA'])


def main():
    with open('../inputs/day-16') as f:
        valves = {(a := Valve.parse_valve(line)).name: a for line in f}
    for v in valves:
        for sub_name in valves[v]._sub_names:
            valves[v].add_child(valves[sub_name])
        valves[v]._sub_names = []
    start = time.time()
    # flowing_valves = frozenset(valves[v].name for v in valves if valves[v].flowrate)
    print("Result for task 1:", task1(valves))
    print("Result for task 2:", task2(valves))
    end = time.time() - start
    print(end)


if __name__ == "__main__":
    main()
