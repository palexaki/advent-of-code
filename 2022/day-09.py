def sign(n):
    if n == 0:
        return 0
    elif n < 0:
        return -1
    elif n > 0:
        return 1


def move_head(head, dir):
    match dir:
        case 'U':
            return head[0], head[1] + 1
        case 'D':
            return head[0], head[1] - 1
        case 'R':
            return head[0] + 1, head[1]
        case 'L':
            return head[0] - 1, head[1]


def move_tail(head: (int, int), tail: (int, int)):
    difference = [t - h for h, t in zip(head, tail)]
    signs = [sign(i) for i in difference]
    if max(abs(diff) for diff in difference) <= 1:
        return tail
    elif difference[0] == 0 and abs(difference[1]) == 2:
        return tail[0], head[1] + signs[1]
    elif difference[1] == 0 and abs(difference[0]) == 2:
        return head[0] + signs[0], tail[1]
    else:
        return tuple(t-s for (t,s) in zip(tail, signs))


def task1(instructions):
    head = tail = (0, 0)
    visited = set()
    for [direction, amount] in instructions:
        for _ in range(amount):
            head = move_head(head, direction)
            tail = move_tail(head, tail)
            visited.add(tail)
    return len(visited)


def task2(instructions):
    rope = [(0, 0) for _ in range(10)]
    visited = set()
    for [direction, amount] in instructions:
        for _ in range(amount):
            rope[0] = move_head(rope[0], direction)
            for i in range(1, 10):
                rope[i] = move_tail(rope[i - 1], rope[i])
            visited.add(rope[9])
    return len(visited)


if __name__ == '__main__':
    with open('../inputs/day-9') as f:
        in_instructions = [[direction, int(amount)]
                           for [direction, amount]
                           in (line.strip().split(' ') for line in f.readlines())]
    print("Result for task 1:", task1(in_instructions))
    print("Result for task 2:", task2(in_instructions))
