def parse_monkey_job(line: str):
    name: str
    job: str
    name, job = line.split(": ")
    if job[0].isnumeric():
        return name, int(job)
    else:
        return name, job.split(' ')


def evaluate_monkey(monkeys, current_monkey='root'):
    match monkeys[current_monkey]:
        case int() as num:
            return num
        case [l, '+', r]:
            return evaluate_monkey(monkeys, l) + evaluate_monkey(monkeys, r)
        case [l, '-', r]:
            return evaluate_monkey(monkeys, l) - evaluate_monkey(monkeys, r)
        case [l, '*', r]:
            return evaluate_monkey(monkeys, l) * evaluate_monkey(monkeys, r)
        case [l, '/', r]:
            return evaluate_monkey(monkeys, l) // evaluate_monkey(monkeys, r)


def task1(monkeys):
    return evaluate_monkey({name: job for name, job in monkeys})


def memoize_eval_monkeys(monkeys, current_monkey='root'):
    if current_monkey == 'humn':
        return
    job = monkeys[current_monkey]
    if isinstance(job, int):
        return job
    left, op, right = job
    ev_left = memoize_eval_monkeys(monkeys, left)
    ev_right = memoize_eval_monkeys(monkeys, right)
    if not (isinstance(ev_left, int) and isinstance(ev_right, int)):
        return
    val = None
    match op:
        case '+':
            val = ev_left + ev_right
        case '-':
            val = ev_left - ev_right
        case '*':
            val = ev_left * ev_right
        case '/':
            val = ev_left // ev_right
    if val is not None:
        monkeys[current_monkey] = val
    return val


def calculate_sub_must_equal(left, op, right, must_equal):
    match (left, op, right):
        case (_, '+', int()):
            return must_equal - right
        case (int(), '+', _):
            return must_equal - left

        case (_, '-', int()):
            return must_equal + right
        case (int(), '-', _):
            return left - must_equal

        case (_, '*', int()):
            return must_equal // right
        case (int(), '*', _):
            return must_equal // left

        case (_, '/', int()):
            return must_equal * right
        case (int(), '/', _):
            return left // must_equal


def find_humn_val(monkeys, current_monkey='root', must_equal=None):
    if current_monkey == 'humn':
        return must_equal
    ln, op, rn = monkeys[current_monkey]
    lv = monkeys[ln]
    rv = monkeys[rn]
    num = lv if isinstance(lv, int) else rv
    name = ln if num != lv else rn
    if op != '=' and must_equal is None:
        raise ValueError("must_equal cannot be none if we are doing a normal operation")
    match op:
        case '=':
            return find_humn_val(monkeys, name, num)
        case _:
            return find_humn_val(monkeys, name, calculate_sub_must_equal(lv if isinstance(lv, int) else ln,
                                                                         op,
                                                                         rv if isinstance(rv, int) else rn, must_equal))


def task2(monkeys):
    monkeys = {name: job for name, job in monkeys}
    monkeys['root'][1] = '='
    monkeys['humn'] = None
    memoize_eval_monkeys(monkeys)
    return find_humn_val(monkeys)


def main():
    with open("../inputs/day-21") as f:
        monkey_jobs = [parse_monkey_job(line.strip()) for line in f]
    print("Result for task 1:", task1(monkey_jobs))
    print("Result for task 2:", task2(monkey_jobs))


if __name__ == "__main__":
    main()
