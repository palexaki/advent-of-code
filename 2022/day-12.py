from dataclasses import dataclass, field
from queue import PriorityQueue


@dataclass(order=True)
class QueueItem:
    weight: int
    position: (int, int) = field(compare=False)


def find_start(height_map):
    for i in range(len(height_map)):
        for j in range(len(height_map[0])):
            if height_map[i][j] == 'S':
                return i, j


def in_box(pos, w, h):
    return 0 <= pos[0] < w and 0 <= pos[1] < h


def add_ps(pos, diff):
    return tuple(p + d for p, d in zip(pos, diff))


def neighbors(position, w, h):
    ns = [add_ps(position, diff) for diff in ((1, 0), (-1, 0), (0, 1), (0, -1))]
    return [n for n in ns if in_box(n, w, h)]


def height(v):
    if v == 'S':
        return ord('a')
    elif v == 'E':
        return ord('z')
    else:
        return ord(v)


def task1(height_map, start=None):
    start = start or find_start(height_map)
    queue = PriorityQueue()
    queue.put(QueueItem(weight=0, position=start))
    distance = {}
    w = len(height_map)
    h = len(height_map[0])
    while not queue.empty():
        node: QueueItem = queue.get()
        height_val = height_map[node.position[0]][node.position[1]]
        if height_val == 'E':
            return node.weight
        if node.position in distance:
            continue
        distance[node.position] = node.weight
        for n in neighbors(node.position, w, h):
            n_val = height_map[n[0]][n[1]]
            if n not in distance and (height(n_val) - height(height_val) <= 1):
                queue.put(QueueItem(weight=node.weight+1, position=n))

def task2(height_map):
    shortest = None
    for i in range(len(height_map)):
        for j in range(len(height_map[0])):
            if height_map[i][j] == 'a':
                distance = task1(height_map, start=(i, j))
                if distance is not None:
                    shortest = distance if shortest is None else min(shortest, distance)
    return shortest


def main():
    with open('../inputs/day-12') as f:
        height_map = [[x for x in line.strip()] for line in f]
    print("Result for task 1:", task1(height_map))
    print("Result for task 2:", task2(height_map))


if __name__ == '__main__':
    main()
