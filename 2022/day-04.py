def parse(line: str):
    pair = line.split(',')
    return [[int(i) for i in assignment.split('-')] for assignment in pair]


def iscontained(pair: [[int, int], [int, int]]):
    [[l1, r1], [l2, r2]] = pair
    return (l1 <= l2 and r1 >= r2) or (l2 <= l1 and r2 >= r1)


def isoverlap(pair: [[int, int], [int, int]]):
    [[l1, r1], [l2, r2]] = pair
    return (r1 >= l2 and l1 <= r2) or (r2 >= l1 and l2 <= r1)


def task1(lines):
    pairs = [parse(line) for line in lines]
    return sum(iscontained(pair) for pair in pairs)


def task2(lines):
    pairs = [parse(line) for line in lines]
    return sum(isoverlap(pair) for pair in pairs)

if __name__ == '__main__':
    with open('../inputs/day-4') as file:
        inputs = [line.strip() for line in file.readlines()]
    print("Result of task 1:", task1(inputs))
    print("Result of task 2:", task2(inputs))


