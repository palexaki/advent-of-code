import re


def parse_crates(crates: list[str], stack_id_line: str):
    num_stacks = int(stack_id_line[-3])
    stacks = [[] for _ in range(num_stacks)]
    for crate_line in crates:
        for stack_id in range(num_stacks):
            val = crate_line[stack_id*4+1]
            if val != ' ':
                stacks[stack_id].append(val)
    return stacks


def parse_move(line):
    match = re.match('move (\\d+) from (\\d) to (\\d)', line)
    return [int(m) for m in match.groups()]


def parse(lines: list[str]):
    crates = []
    stacks = []
    moves = []
    for line in lines:
        if line[1].isdigit():
            stacks.extend(parse_crates(crates, line))
            break
        crates.append(line)
    for line in lines:
        if line and line[0] != 'm':
            continue
        moves.append(parse_move(line))
    return [stacks, moves]


def task(stacks: list[list[str]], moves: list[[int, int, int]], reverse: bool):
    stacks = [[i for i in stack] for stack in stacks]
    for [amount, from_id, to_id] in moves:
        from_stack = stacks[from_id-1]
        stacks[from_id-1] = from_stack[amount:]
        inter = from_stack[:amount]
        if reverse:
            inter.reverse()
        stacks[to_id-1] = inter + stacks[to_id-1]

    return ''.join(stack[0] for stack in stacks)


if __name__ == '__main__':
    with open('../inputs/day-5') as f:
        lines = [line for line in f.readlines()]

    [stcks, mvs] = parse(lines)
    print("Result of task 1:", task(stcks, mvs, True))
    print("Result of task 2:", task(stcks, mvs, False))
