def task1(program: list[list[str]], want_register: bool = False):
    pc = 0
    clock_cycle = 0
    register = 1
    first_add = True
    score = 0
    registers = []
    while pc < len(program):
        instruction = program[pc]
        clock_cycle += 1
        registers.append(register)
        if (clock_cycle - 20) % 40 == 0:
            score += register * clock_cycle
        match instruction:
            case ['noop']:
                pc += 1
            case ['addx', _] if first_add:
                first_add = False
            case ['addx', n] if not first_add:
                register += int(n)
                pc += 1
                first_add = True
    return score if not want_register else registers


def task2(program: list[list[str]]):
    registers = task1(program, True)
    screen = [[' ' for _ in range(40)] for _ in range(6)]
    for i in range(240):
        if abs(registers[i] - (i % 40)) <= 1:
            screen[i // 40][i % 40] = '#'
    return '\n'.join(''.join(row) for row in screen)


if __name__ == '__main__':
    with open('../inputs/day-10') as f:
        in_program = [line.strip().split(' ') for line in f.readlines()]
    print("Result of task 1:", task1(in_program))
    print("Result of task 2:", task2(in_program), sep='\n')
