from json import loads
from itertools import zip_longest
from functools import cmp_to_key


def in_order(left, right):
    for l, r in zip_longest(left, right, fillvalue=None):
        match (l, r):
            case (None, _):
                return True
            case (_, None):
                return False
            case (int(), int()) if l != r:
                return l < r
            case (int(), list()) if isinstance(o := in_order([l], r), bool):
                return o
            case (list(), int()) if isinstance(o := in_order(l, [r]), bool):
                return o
            case (list(), list()) if isinstance(o := in_order(l, r), bool):
                return o
    return None


def in_order_cmp(left, right):
    match in_order(left, right):
        case None:
            return 0
        case True:
            return -1
        case False:
            return 1
    raise NotImplementedError


def task1(pairs):
    return sum(map(lambda p: p[0], filter(lambda p: in_order(*p[1]), enumerate(pairs, start=1))))


def task2(packets):
    dividers = [
        [[2]],
        [[6]]
    ]
    packets = packets[:] + dividers
    ed = sorted(packets, key=cmp_to_key(in_order_cmp))
    indices = [1+ed.index(d) for d in dividers]
    return indices[0] * indices[1]


def main():
    with open('../inputs/day-13') as f:
        lines = [loads(line.strip()) for line in f if line != '\n']
        pairs = [(lines[i], lines[i + 1]) for i in range(0, len(lines), 2)]
    # for i, pair in enumerate(pairs):
        # print(f"Pair {i} is {'' if in_order(*pair) else 'not '}in order")
    print("Result of task 1:", task1(pairs))
    print("Result of task 2:", task2(lines))


if __name__ == '__main__':
    main()
