from queue import LifoQueue


def neighbors(point):
    for i in range(len(point)):
        for diff in [-1, 1]:
            yield tuple(n + diff if i == j else n for j, n in enumerate(point))


def goal(point, min_point, max_point):
    for i, min_i, max_i in zip(point, min_point, max_point):
        if i < min_i or i > max_i:
            return True
    return False


def trapped_island(point, points, min_point, max_point):
    queue = LifoQueue()
    queue.put(point)
    visited = set()
    while not queue.empty():
        p = queue.get()
        if goal(p, min_point, max_point):
            return set()
        if p in visited:
            continue
        visited.add(p)
        for n in neighbors(p):
            if n not in points and n not in visited:
                queue.put(n)
    return visited


def min_max_points(points):
    min_p = max_p = points[0]
    for p in points:
        min_p = tuple(min(x, y) for x, y in zip(min_p, p))
        max_p = tuple(max(x, y) for x, y in zip(max_p, p))
    return min_p, max_p


def task1(points):
    points_set = set(points)
    sides = 0
    for point in points:
        sides += 6 - len(points_set & set(neighbors(point)))
    return sides


def task2(points):
    air_points = set()
    points_set = set(points)
    min_point, max_point = min_max_points(points)
    for x in range(min_point[0], max_point[0]):
        for y in range(min_point[1], max_point[1]):
            for z in range(min_point[2], max_point[2]):
                if (p := (x, y, z)) not in points_set:
                    air_points.add(p)
    trapped_air = set()
    for air in air_points:
        if air in trapped_air:
            continue
        trapped_air |= trapped_island(air, points_set, min_point, max_point)
    removable_points = trapped_air | points_set
    sides = 0
    for point in points:
        sides += 6 - len(removable_points & set(neighbors(point)))
    return sides


def main():
    with open('../inputs/day-18') as f:
        points = [tuple(int(i) for i in point.split(',')) for point in f]
    # print(points)
    # print(list(neighbors((0, 0, 0))))
    print("Result of task 1:", task1(points))
    print("Result of task 2:", task2(points))
    min_point, max_point = min_max_points(points)


if __name__ == '__main__':
    main()
