from functools import reduce

with open('../inputs/day-3') as file:
    lines = [i.strip() for i in file.readlines()]


def score_letter(letter: str):
    if letter.isupper():
        return ord(letter) - 38
    else:
        return ord(letter) - 96


def task1():
    score = 0
    for line in lines:
        left, right = (set(line[:len(line)//2]), set(line[len(line)//2:]))
        intersection = left.intersection(right)
        score += score_letter(intersection.pop())
    return score


def task2():
    # groups = [lines[i*3:i*3+3] for i in range(len(lines)//3)]
    # groups = [[set(line) for line in group] for group in groups]
    # common = [reduce(lambda a, b: a.intersection(b), (set(line) for line in group)) for group in groups]
    return sum(score_letter(reduce(lambda a, b: a.intersection(b), (set(line) for line in lines[i:i+3])).pop()) for i in range(0, len(lines), 3))


if __name__ == "__main__":
    print("Result for task 1:", task1())
    print("Result for task 2:", task2())