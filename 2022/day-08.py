def visibility_line(line):
    visibility = [False for _ in line]
    highest = line[0]
    visibility[0] = True
    for i in range(len(line)):
        if line[i] > highest:
            highest = line[i]
            visibility[i] = True
    return visibility


def task1(grid: list[list[int]]):
    visibility_grid = [[False for _ in row] for row in grid]
    for i in range(len(grid)):
        row = grid[i]
        visibility_grid[i] = [old_v or new_v for (old_v, new_v) in zip(visibility_grid[i], visibility_line(row))]
    for i in range(len(grid)):
        row = grid[i][::-1]
        visibility_grid[i] = [old_v or new_v
                              for (old_v, new_v)
                              in zip(visibility_grid[i][::-1], visibility_line(row))][::-1]
    for j in range(len(grid[0])):
        column = [row[j] for row in grid]
        visibility_column = visibility_line(column)
        for i in range(len(grid)):
            visibility_grid[i][j] |= visibility_column[i]
    for j in range(len(grid[0])):
        column = [row[j] for row in grid[::-1]]
        visibility_column = visibility_line(column)
        for i in range(len(grid)):
            visibility_grid[len(grid) - i - 1][j] |= visibility_column[i]
    return sum(sum(row) for row in visibility_grid)


def scenic_score(grid, row, column) -> int | float:
    height = grid[row][column]
    score = 1
    visible = 0
    for i in range(row+1, len(grid)):
        visible += 1
        if grid[i][column] >= height:
            break
    score *= visible
    visible = 0
    for i in range(row-1, -1, -1):
        visible += 1
        if grid[i][column] >= height:
            break
    score *= visible
    visible = 0
    for j in range(column-1, -1, -1):
        visible += 1
        if grid[row][j] >= height:
            break
    score *= visible
    visible = 0
    for j in range(column+1, len(grid[0])):
        visible += 1
        if grid[row][j] >= height:
            break
    score *= visible
    return score


def task2(grid):
    scores = [[scenic_score(grid, i, j) for j in range(1, len(grid[0])-1)] for i in range(1, len(grid)-1)]
    return max(max(row) for row in scores)


def print_visibility(grid):
    for row in grid:
        for visible in row:
            print('x' if visible else '.', end='')
        print()


if __name__ == '__main__':
    with open('../inputs/day-8') as f:
        input_grid = [[int(i) for i in line.strip()] for line in f.readlines()]
    print("Result for task 1", task1(input_grid))
    print("Result for task 2", task2(input_grid))
