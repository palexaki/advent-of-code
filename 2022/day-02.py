with open('../inputs/day-2') as f:
    inputs = [l.strip().split(' ') for l in f.readlines()]

wins = {
    'X': 'C',
    'Y': 'A',
    'Z': 'B',
}

draws = {
    'X': 'A',
    'Y': 'B',
    'Z': 'C',
}

points = {
    'X': 1,
    'Y': 2,
    'Z': 3,
}

rotate = {
    'A': 'B',
    'B': 'C',
    'C': 'A'
}

scored = {
    'A': 1,
    'B': 2,
    'C': 3
}


def round(opponent, me):
    if draws[me] == opponent:
        score = 3
    elif wins[me] == opponent:
        score = 6
    else:
        score = 0
    return score + points[me]


def round2(opponent, finish):
    if finish == 'X':
        return scored[rotate[rotate[opponent]]]
    elif finish == 'Y':
        return 3 + scored[opponent]
    elif finish == 'Z':
        return 6 + scored[rotate[opponent]]


print("Result for task 1:", sum(round(*choices) for choices in inputs))
print("Result for task 2:", sum(round2(*choices) for choices in inputs))
