import re
from typing import Callable


class MyNum:
    def __init__(self, primes: list[int] | tuple[int], *args, number=None, mods: tuple[int] = None):
        self.primes = tuple(primes)
        if number is None and mods is None:
            raise ValueError("One of number and mods should be set")
        if mods is not None:
            self.mods = mods
        else:
            self.mods = tuple(number % p for p in primes)

    def __add__(self, other):
        if not isinstance(other, MyNum):
            raise NotImplementedError("Can only add MyNum to MyNum for now")
        elif other.primes != self.primes:
            raise ValueError("Both primes tuples must be equal")

        mods: tuple[int] = tuple((m + o) % p for m, o, p in zip(self.mods, other.mods, self.primes))
        return MyNum(self.primes,
                     mods=mods)

    def __mul__(self, other):
        if not isinstance(other, MyNum):
            raise NotImplementedError("Can only multiply MyNum to MyNum for now")
        elif other.primes != self.primes:
            raise ValueError("Both primes tuples must be equal")

        mods: tuple[int] = tuple((m * o) % p for m, o, p in zip(self.mods, other.mods, self.primes))
        return MyNum(self.primes,
                     mods=mods)

    def __pow__(self, power, modulo=None):
        if modulo is not None:
            raise ValueError("Don't think this is possible")

        return MyNum(self.primes, mods=tuple((m ** power) % p for m, p in zip(self.mods, self.primes)))

    def __mod__(self, other):
        if not isinstance(other, int):
            raise NotImplementedError("Not implemented yet, only modding with ints for now")
        index = self.primes.index(other)
        if not (0 <= index < len(self.primes)):
            raise ValueError("Can only find the mod for numbers in the primes list")
        return self.mods[index]

    def __str__(self):
        return f"<MyNum primes={self.primes}, mods={self.mods}>"

    def __repr__(self):
        return self.__str__()


class Monkey:
    def __init__(self, items: list[int], operation: Callable[[int], int], divisible_test: int, true: int, false: int,
                 op: (str, int), monkey: int):
        self.items = items
        self.operation = operation
        self.divisible_test = divisible_test
        self.false_throw = false
        self.true_throw = true
        self.inspected_items = 0
        self.op = op
        self.monkey = monkey

    @classmethod
    def parse(cls, monkey_description: list[str]):
        monkey = int(re.findall('\\d+', monkey_description[0])[0])
        items = [int(i) for i in re.findall('\\d+', monkey_description[1])]
        op_match = re.match("Operation: new = old ([*+]) (\\d+|old)", monkey_description[2])
        op_int = op_match.group(2)
        if op_int == 'old':
            operation = lambda n: n ** 2
        else:
            op_int = int(op_int)
        if op_int != 'old' and op_match.group(1) == '*':
            operation = lambda n: op_int * n
        elif op_int != 'old':
            operation = lambda n: op_int + n
        test_num = int(re.findall('\\d+', monkey_description[3])[0])
        true = int(re.findall('\\d+', monkey_description[4])[0])
        false = int(re.findall('\\d+', monkey_description[5])[0])
        return Monkey(items, operation, test_num, true, false, (op_match.group(1), op_int), monkey)

    def do_test(self, item):
        if (item % self.divisible_test) == 0:
            return self.true_throw
        else:
            return self.false_throw

    def inspect_item(self):
        if len(self.items) == 0:
            return
        item = self.operation(self.items[0])  # //3
        self.inspected_items += 1
        self.items = self.items[1:]
        return item, self.do_test(item)

    def add_item(self, item):
        self.items.append(item)

    def __str__(self):
        return f"""Monkey {self.monkey}
  Items: {', '.join(str(i) for i in self.items)}
  Operation: new = old {self.op[0]} {self.op[1]}
  Test: divisible by {self.divisible_test}
    If true: throw to monkey {self.true_throw}
    If false: throw to monkey {self.false_throw}"""


def task1(monkeys: list[Monkey]):
    for _ in range(20):
        for monkey in monkeys:
            while monkey.items:
                i, to = monkey.inspect_item()
                monkeys[to].add_item(i)
    monkeys.sort(key=lambda m: m.inspected_items, reverse=True)
    return monkeys[0].inspected_items * monkeys[1].inspected_items


def task2(monkeys: list[Monkey]):
    divisibles = tuple(m.divisible_test for m in monkeys)
    for monkey in monkeys:
        monkey.items = [MyNum(divisibles, number=i) for i in monkey.items]
        if isinstance(monkey.op[1], int):
            num = MyNum(divisibles, number=monkey.op[1])
            monkey.operation = num.__mul__ if monkey.op[0] == '*' else num.__add__
    for i in range(10000):
        if i == 1:
            print([m.inspected_items for m in monkeys])
        if i == 20:
            print([m.inspected_items for m in monkeys])
        elif i % 1000 == 0 and i > 0:
            print([m.inspected_items for m in monkeys])
        for monkey in monkeys:
            while monkey.items:
                i, to = monkey.inspect_item()
                monkeys[to].add_item(i)
    monkeys.sort(key=lambda m: m.inspected_items, reverse=True)
    return monkeys[0].inspected_items * monkeys[1].inspected_items


def split_list(lst: list, delim):
    splits = []
    while delim in lst:
        index = lst.index(delim)
        splits.append(lst[:index])
        lst = lst[index + 1:]
    splits.append(lst)
    return splits


if __name__ == '__main__':
    with open('../inputs/day-11') as f:
        lines = [line.strip() for line in f]
    monkeys_in = [Monkey.parse(m) for m in split_list(lines, '')]
    print("Result of task 1:", task1([Monkey.parse(m) for m in split_list(lines, '')]))
    print("Result of task 2:", task2([Monkey.parse(m) for m in split_list(lines, '')]))
