from pprint import pprint

strings = ['$ ls', '$ cd ..', '$ cd ab', '180 fil', 'dir a']


def print_dir(dir, depth=0):
    prefix = '  '*depth+'-'
    for item in dir:
        match dir[item]:
            case dict():
                print(prefix, item, '(dir)')
                print_dir(dir[item], depth+1)
            case int():
                print(prefix, item, f'(file, size={dir[item]})')




def build_structure(history, i=0):
    current_directory = {}
    while i < len(history):
        match history[i].split(' '):
            case ['$', 'ls']:
                pass
            case ['$', 'cd', '..']:
                i += 1
                break
            case ['$', 'cd', into]:
                (nested, i) = build_structure(history, i+1)
                current_directory[into] = nested
                continue
            case ['dir', name]:
                current_directory[name] = {}
            case [size, name]:
                current_directory[name] = int(size)
        i += 1
    return current_directory, i


def calculate_total_sizes(dir):
    size = 0
    total = 0
    for item in dir:
        match dir[item]:
            case dict():
                (sub_size, sub_total) = calculate_total_sizes(dir[item])
                size += sub_size
                total += sub_total
            case int():
                size += dir[item]
    if size <= 100000:
        total += size
    return size, total


def task1(history):
    directory_structure = build_structure(history)[0]
    return calculate_total_sizes(directory_structure)[1]


def deletable_directory(dir, necessary_space):
    size = 0
    smallest_dir = None
    for item in dir:
        match dir[item]:
            case dict():
                (sub_size, sub_smallest_dir) = deletable_directory(dir[item], necessary_space)
                size += sub_size
                if smallest_dir is None:
                    smallest_dir = sub_smallest_dir
                if sub_smallest_dir is not None and sub_smallest_dir < smallest_dir:
                    smallest_dir = sub_smallest_dir
            case int():
                size += dir[item]
    if smallest_dir is None and size >= necessary_space:
        smallest_dir = size
    elif necessary_space <= size <= smallest_dir:
        smallest_dir = size
    return size, smallest_dir


def task2(history):
    directory_structure = build_structure(history)[0]
    total = calculate_total_sizes(directory_structure)[0]
    needed_space = 30000000 - (70000000 - total)
    return deletable_directory(directory_structure, needed_space)[1]


if __name__ == '__main__':
    with open('../inputs/day-7') as f:
        commands = [line.rstrip() for line in f.readlines()]
    print("result of task 1", task1(commands))
    print("result of task 2", task2(commands))
