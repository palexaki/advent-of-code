def decrypt_file(file: list[int], rounds=1):
    file = file[:]
    size = len(file)
    positions = list(range(size))
    for _ in range(rounds):
        for i in range(size):
            index = positions.index(i)
            val = file[index]
            new_pos = (index + val) % (size - 1)
            positions.pop(index)
            positions.insert(new_pos, i)
            file.pop(index)
            file.insert(new_pos, val)
    return file


def task1(file):
    file = decrypt_file(file)
    size = len(file)
    zero_index = file.index(0)
    l = [file[(i + zero_index) % size] for i in [1000, 2000, 3000]]
    return sum(l)


def task2(file):
    key = 811589153
    file = decrypt_file([n*key for n in file], 10)
    size = len(file)
    zero_index = file.index(0)
    return sum(file[(i + zero_index) % size] for i in [1000, 2000, 3000])


def main():
    with open('../inputs/day-20') as f:
        file = [int(n) for n in f]
    print("Result of task 1:", task1(file))
    print("Result of task 2:", task2(file))


if __name__ == '__main__':
    main()
