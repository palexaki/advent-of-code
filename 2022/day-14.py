def sign(n):
    if n == 0:
        return 0
    elif n > 0:
        return 1
    elif n < 0:
        return -1


def add_pos(a, b):
    return tuple(i + j for i, j in zip(a, b))


def min_pos(a, b):
    return tuple(i - j for i, j in zip(a, b))


def position_segments(a, b):
    diff = tuple(sign(i) for i in min_pos(b, a))
    while a != b:
        yield a
        a = add_pos(a, diff)
    yield b


def path_to_positions(path):
    parsed = [tuple(int(j) for j in i.split(',')) for i in path.split('->')]
    scan = dict()
    for a, b in zip(parsed, parsed[1:]):
        for p in position_segments(a, b):
            scan[p] = 'rock'
    return scan


def parse_scan(paths):
    scan = dict()
    for path in paths:
        scan |= path_to_positions(path)
    return scan


def min_max_of_scan(scan):
    min_p = max_p = (500, 0)
    for p in scan:
        min_p = tuple(min(x, y) for x, y in zip(min_p, p))
        max_p = tuple(max(x, y) for x, y in zip(max_p, p))
    return min_p, max_p


def print_scan(scan: dict):
    (min_p, max_p) = min_max_of_scan(scan)
    for y in range(min_p[1], max_p[1]+1):
        for x in range(min_p[0], max_p[0]+1):
            val = scan.get((x, y), None)
            if x == 500 and y == 0:
                print('+', end='')
            elif val == 'rock':
                print('#', end='')
            elif val == 'sand':
                print('O', end='')
            elif val == 'floor':
                print('-', end='')
            else:
                print('.', end='')
        print()


def trickle_grain(scan, min_max=None):
    (min_p, max_p) = min_max or min_max_of_scan(scan)
    grain = (500, 0)
    while min_p[0] <= grain[0] <= max_p[0] and grain[1] <= max_p[1]:
        for move in [(0, 1), (-1, 1), (1, 1)]:
            if scan.get(p := add_pos(grain, move)) is None:
                grain = p
                break
        else:
            return grain


def task1(scan: dict):
    scan = scan.copy()
    grains = 0
    while (p := trickle_grain(scan)) is not None:
        scan[p] = 'sand'
        grains += 1
    return grains


def task2(scan: dict):
    scan = scan.copy()
    (min_p, max_p) = min_max_of_scan(scan)
    min_p = (min_p[0]-max_p[1]-50, min_p[1])
    max_p = (max_p[0]+max_p[1]+50, max_p[1]+2)
    for x in range(499-max_p[1], 501+max_p[1]):
        scan[(x, max_p[1])] = 'floor'
    grains = 0
    while (p := trickle_grain(scan, (min_p, max_p))) != (500, 0):
        if p is None:
            return "ERROR"
        scan[p] = 'sand'
        grains += 1
    return grains+1


def main():
    test1 = ["498, 4 -> 498, 6 -> 496, 6", "503, 4 -> 502, 4 -> 502, 9 -> 494, 9"]
    test_scan = parse_scan(test1)
    with open('../inputs/day-14') as f:
        scan = parse_scan(f)
    assert task1(test_scan) == 24
    assert task2(test_scan) == 93
    print("Result of task 1:", task1(scan))
    print("Result of task 2:", task2(scan))


if __name__ == '__main__':
    main()
