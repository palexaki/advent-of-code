def task(signal, run):
    for i in range(run-1, len(signal)):
        chars = {signal[i-j] for j in range(run)}
        if len(chars) == run:
            return i+1


if __name__ == '__main__':
    tests = {
        'mjqjpqmgbljsphdztnvjfqwrcgsmlb': (7, 19),
        'bvwbjplbgvbhsrlpgdmjqwftvncz': (5, 23),
        'nppdvjthqldpwncqszvftbrmjlhg': (6, 23),
        'nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg': (10, 29),
        'zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw': (11, 26)
    }
    for signal in tests:
        assert task(signal, 4) == tests[signal][0], f"oops {task(signal, 4)} != {tests[signal][0]}"
        run14 = task(signal, 14)
        assert run14 == tests[signal][1], f"oops {run14} != {tests[signal][1]}"

    with open('../inputs/day-6') as f:
        signal = f.readline().strip()

    print("Result for task 1:", task(signal, 4))
    print("Result for task 2:", task(signal, 14))
