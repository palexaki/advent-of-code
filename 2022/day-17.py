from collections import deque

SHAPES = [
    [(2, 4), (3, 4), (4, 4), (5, 4)],
    [(3, 4), (2, 5), (3, 5), (4, 5), (3, 6)],
    [(2, 4), (3, 4), (4, 4), (4, 5), (4, 6)],
    [(2, 4), (2, 5), (2, 6), (2, 7)],
    [(2, 4), (3, 4), (2, 5), (3, 5)]
]


def make_shape(highest_points, shape):
    max_y = max(highest_points)
    return [(x, y + max_y) for x, y in shape]


def push_shape(direction, shape, points):
    min_x, max_x = min(x for x, _ in shape), max(x for x, _ in shape)
    if (direction == '<' and min_x == 0) or (direction == '>' and max_x == 6):
        return shape
    diff = 1 if direction == '>' else -1
    new_shape = [(x + diff, y) for x, y in shape]
    if not (set(new_shape) & points):
        return new_shape
    else:
        return shape


def fall_shape(points, shape):
    fallen = [(x, y - 1) for x, y in shape]
    if not (set(fallen) & points):
        return fallen, True
    else:
        return shape, False


def extract_state(highest_points, points):
    lowest_point = find_lowest_reachable_point(points, highest_points)
    highest_point = max(highest_points)
    state = []
    for y in range(lowest_point, highest_point + 1):
        for x in range(7):
            if (x, y) in points:
                state.append((x, y - lowest_point))
    return frozenset(state)


def neighbours(p, top):
    NS = [(0, 1), (0, -1), (1, 0), (-1, 0)]
    for n in NS:
        m = (p[0] + n[0], p[1] + n[1])
        if (0 <= m[0] < 7) and (0 <= m[1] <= top):
            yield m


def find_lowest_reachable_point(points, highest_points):
    top = max(highest_points) + 1
    q = deque([(0, top)])
    lowest_y = top
    visited = set()
    while len(q) > 0:
        p = q.popleft()
        for n in neighbours(p, top):
            if n in visited or n in points:
                continue
            visited.add(n)
            lowest_y = min(lowest_y, n[1])
            q.append(n)
    return lowest_y


def drop_shape(shape, pattern, jet_index, points, highest_points):
    moved = True
    while moved:
        shape = push_shape(pattern[jet_index], shape, points)
        shape, moved = fall_shape(points, shape)
        jet_index = (jet_index + 1) % len(pattern)
    for x, y in shape:
        highest_points[x] = max(highest_points[x], y)

    return highest_points, points | set(shape), jet_index


def do_tetris(pattern, rocks=2022):
    highest_points = [0 for i in range(7)]
    jet_index = 0
    points = set((i, 0) for i in range(7))
    states = dict()
    found = False
    lowest_reachable = 0
    shape_amount = 0
    while shape_amount < rocks:
        shape_index = shape_amount % 5
        highest_points, points, jet_index = drop_shape(make_shape(highest_points, SHAPES[shape_index]),
                                                       pattern, jet_index, points, highest_points)
        shape_amount += 1
        if found:
            continue
        viewable_part = extract_state(highest_points, points)
        state = (viewable_part, shape_index, jet_index)
        if state in states:
            lowest_reachable = max(lowest_reachable, find_lowest_reachable_point(points, highest_points))
            found = True
            (prev_highest, prev_shapes) = states[state]
            cycle = shape_amount - prev_shapes
            leftover_cycles = (rocks - shape_amount) // cycle
            leftover_height = (highest_points[0] - prev_highest[0]) * leftover_cycles
            for x in range(7):
                for y in range(lowest_reachable, max(highest_points) + 1):
                    if (x, y) in points:
                        points.add((x, y + leftover_height))
            highest_points = [p + leftover_height for p in highest_points]
            shape_amount += (cycle * leftover_cycles)
        else:
            states[state] = (tuple(highest_points), shape_amount)
    return max(highest_points)


def task1(pattern):
    return do_tetris(pattern, 2022)


def task2(pattern):
    return do_tetris(pattern, 1000000000000)


def main():
    with open('../inputs/day-17') as f:
        line = f.readline()
    print("Result for task 1:", task1(line.strip()))
    print("Result for task 1:", task2(line.strip()))


if __name__ == "__main__":
    main()
