import re

from tqdm import tqdm


def parse_sensor(line):
    match = re.match("^Sensor at x=(-?\\d+), y=(-?\\d+): closest beacon is at x=(-?\\d+), y=(-?\\d+)$",
                     line)
    return tuple(int(i) for i in match.groups())


def manhattan_distance(a, b):
    return sum(abs(i - j) for i, j in zip(a, b))


def detection_interval_on_y(sensor, y):
    (sx, sy, bx, by) = sensor
    distance = manhattan_distance((sx, sy), (bx, by))
    perpend_distance = abs(sy - y)
    if perpend_distance > distance:
        return None
    leftover_for_x = distance - perpend_distance
    return sx - leftover_for_x, sx + leftover_for_x


def merge_intervals(a, b):
    if (b[1] < a[0] or a[1] < b[0]) and b[1] + 1 != a[0] and a[1] + 1 != b[0]:
        return None

    return min(a[0], b[0]), max(a[1], b[1])


def merge_interval_list(intervals: list):
    new_intervals = []
    for a in intervals:
        for b in filter(lambda x: x != a, intervals):
            if overlap := merge_intervals(a, b):
                if not new_intervals:
                    new_intervals.append(overlap)
                    break
                for i in range(len(new_intervals)):
                    if next_overlap := merge_intervals(overlap, new_intervals[i]):
                        new_intervals[i] = next_overlap
                        break
                else:
                    new_intervals.append(overlap)

                break
        else:
            new_intervals.append(a)
    return new_intervals


def blonki(intervals: list):
    new_intervals = merge_interval_list(intervals)
    if len(new_intervals) == 1:
        return new_intervals
    newest_intervals = merge_interval_list(new_intervals)
    while len(newest_intervals) < len(new_intervals):
        new_intervals, newest_intervals = newest_intervals, merge_interval_list(newest_intervals)
    return newest_intervals


def task1(sensors, y=2000000):
    intervals = [a for sensor in sensors if (a := detection_interval_on_y(sensor, y))]
    new_intervals = blonki(intervals)
    if len(new_intervals) == 1:
        return abs(new_intervals[0][0] - new_intervals[0][1])


def task2(sensors, maxi=4000000):
    lines = []
    for i in tqdm(range(maxi + 1)):
        intervals = blonki([a for sensor in sensors if (a := detection_interval_on_y(sensor, i))])
        if len(intervals) == 1:
            continue
        else:
            lines.append((intervals, i))

    if len(lines) != 1:
        print("stop")
        return
    (ies, y) = lines[0]
    ies = [i for i in ies if 0 <= i[0] <= maxi or 0 <= i[1] <= maxi]
    if len(ies) != 2:
        print("incorrect")
        return
    return (4000000 * (ies[0][0]-1 if 0 <= ies[0][0]-1 <= maxi else ies[0][1]+1)) + y


def main():
    with open('../inputs/day-15') as f:
        sensor_data = [parse_sensor(line) for line in f]
    print("Result for task 1:", task1(sensor_data, 10))
    print("Result for task 2:", task2(sensor_data))
    # for s in sensor_data:
    #     print(s)


if __name__ == '__main__':
    main()
