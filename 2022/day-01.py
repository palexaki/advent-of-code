from pprint import pprint

with open("../inputs/day-1") as file:
    inputs = file.readlines()

inputs = [i.strip() for i in inputs]

elves = []
current_elf = []

for calories in inputs:
    if not calories:
        elves.append(sum(current_elf))
        current_elf = []
    else:
        current_elf.append(int(calories))

elves.append(sum(current_elf))

elves.sort(reverse=True)


print("Solution to puzzle 1:", elves[0])
print("Solution to puzzle 2:", sum(elves[0:3]))
