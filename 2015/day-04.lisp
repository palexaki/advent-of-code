(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :md5)
  (ql:quickload :advent-util))
(defpackage #:day-4
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-4)

(defun hash-to-string (hash)
  (apply #'concatenate 'string
         (map 'list (lambda (x)
                      (format nil "~2,'0x" x))
              hash)))

(defun hash-starts-0 (hash)
  (string= "000000" (hash-to-string hash) :end2 6))

(defun hash (n &optional (string "bgvyzdsv"))
  (md5:md5sum-string (format nil "~a~a" string n)))

(defun solve-1 ()
  (loop :for x :from 0
        :when (hash-starts-0 (hash x))
          :return x))

(defun solve-2 ())
