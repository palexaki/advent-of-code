(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-1
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-1)

(defun solve-1 (&optional (input (first (day-input-lines 1 2015))))
  (- (count #\( input)
     (count #\) input)))

(defun solve-2 (&optional (input (first (day-input-lines 1 2015))))
  (loop :for char :across input
        :for position :from 1
        :summing (if (char= char #\() 1 -1) :into level
        :when (= level -1)
          :return position))
