(ql:quickload :gsll)
(ql:quickload :queues)
(ql:quickload :queues.priority-queue)
(defpackage #:day-22
  (:use #:cl)
  (:export solve-2))

(in-package #:day-22)

(defparameter *depth* 11817)
(defparameter *target* #C(9 751))

(defun maref (array index)
  (aref array (imagpart index) (realpart index)))

(defun (setf maref) (value array index)
  (setf (aref array (imagpart index) (realpart index)) value))

(defun marray-in-bounds-p (array index)
  (array-in-bounds-p array (imagpart index) (realpart index)))

(defun geologic-index (map index target)
  (cond ((zerop index) 0)
        ((= index target) 0)
        ((zerop (realpart index)) (* (imagpart index) 48271))
        ((zerop (imagpart index)) (* (realpart index) 16807))
        (t (* (maref map (- index #C(1 0)))
              (maref map (- index #C(0 1)))))))

(defun erosion-level (map index target depth)
  (mod (+ (geologic-index map index target) depth) 20183))

(defun create-erosion-map (size target depth)
  (let ((map (make-array (list (imagpart size)
                               (realpart size))
                         :adjustable t)))
    (dotimes (x (realpart size))
      (setf (maref map (complex x 0)) (erosion-level map (complex x 0) target depth)))
    (dotimes (y (imagpart size))
      (setf (maref map (complex 0 y)) (erosion-level map (complex 0 y) target depth)))
    (loop :for y :from 1 :below (imagpart size) :do
      (loop :for x :from 1 :below (realpart size) :do
        (setf (maref map (complex x y)) (erosion-level map (complex x y) target depth))))
    map))

(defun erosion-map->type-map (erosion-map)
  (let ((sum 0)
        (type-map (make-array (array-dimensions erosion-map))))
    (dotimes (y (array-dimension erosion-map 0))
      (dotimes (x (array-dimension erosion-map 1))
        (incf sum
              (setf (aref type-map y x) (mod (aref erosion-map y x) 3)))))
    (values type-map
            sum)))

(defun type->type-sym (type)
  (ccase type
    (0 'rok)
    (1 'wet)
    (2 'nar)))

(defun type-map->type-map-sym (type-map)
  (let ((type-map-sym (make-array (array-dimensions type-map))))
    (dotimes (y (array-dimension type-map 0))
      (dotimes (x (array-dimension type-map 1))
        (setf (aref type-map-sym y x) (type->type-sym (aref type-map y x)))))
    type-map-sym))


(defun solve-1 ()
  (erosion-map->type-map (create-erosion-map (+ *target* #C(1 1)) *target* *depth*)))

(defun possiblep (map)
  (lambda (pos)
    (and (not (or (minusp (realpart pos))
                  (minusp (imagpart pos))))
         (marray-in-bounds-p map pos))))

(defun neighbours (position map)
  (remove-if-not (possiblep map)
                 (mapcar (lambda (diff)
                           (+ position diff))
                         '(#C(1 0) #C(-1 0) #C(0 1) #C(0 -1)))))

(defun equippable (type equipment)
  (ccase equipment
    (climb (or (eq type 'rok) (eq type 'wet)))
    (torch (or (eq type 'rok) (eq type 'nar)))
    (neith (or (eq type 'wet) (eq type 'nar)))))

(defun switch-equipment (type-origin type-destination equipment)
  (if (equippable type-destination equipment)
      equipment
      (ccase type-origin
        (rok (ccase type-destination
               (wet 'climb)
               (nar 'torch)))
        (wet (ccase type-destination
               (rok 'climb)
               (nar 'neith)))
        (nar (ccase type-destination
               (rok 'torch)
               (wet 'neith))))))

(defun cost (map origin destination equipment)
  (when (/= 1 (abs (- origin destination)))
    (error "Distance between positions must be 1 else cost cannot be calculated"))
  (let ((type-origin (maref map origin))
        (type-destination (maref map destination)))
    (unless (equippable type-origin equipment)
      (error "Equipment must be equippable in the origin location"))
    (cond
      ((eq type-origin type-destination) (list 1 equipment))
      ((equippable type-destination equipment) (list 1 equipment))
      (t (list 8 (switch-equipment type-origin type-destination equipment))))))

(defun mcompare (a b)
  (< (first a) (first b)))

(defun solve-2 ()
  (let ((map (type-map->type-map-sym (erosion-map->type-map (create-erosion-map (+ *target* #C(1 1))
                                                                                *target* *depth*)))))
    (loop :with visited := (make-hash-table :test 'equal)
          :and queue := (queues:make-queue :priority-queue :compare #'mcompare)
          :initially (queues:qpush queue (list 0 0 'torch))
          :until (gethash *target* visited)
          :do (let* ((current (queues:qpop queue))
                     (current-cost (first current))
                     (current-position (second current))
                     (current-equipment (third current))
                     (neighbours (neighbours current-position map)))
                (when (< current-cost (gethash current-position visited most-positive-fixnum))
                  (setf (gethash current-position visited) current-cost)
                  (dolist (neighbour neighbours)
                    (unless (gethash neighbour visited)
                      (destructuring-bind (n-cost n-equipment) (cost map
                                                                     current-position
                                                                     neighbour
                                                                     current-equipment)
                        (assert (equippable (maref map neighbour) n-equipment))
                        (unless (gethash neighbour visited)
                          (queues:qpush queue (list (+ n-cost current-cost) neighbour n-equipment))))))))
          :finally (return visited))))
