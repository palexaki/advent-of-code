(defpackage #:day-18
  (:use #:cl))

(in-package #:day-18)


(defun state (map index)
  (aref map (imagpart index) (realpart index)))


(defun (setf state) (value map index)
  (setf (aref map (imagpart index) (realpart index)) value))

(defun get-neighbours (map index)
  (remove nil
          (loop :for y :from -1 :upto 1
                :appending (loop :for x :from -1 :upto 1
                                 :unless (and (= x 0) (= y 0))
                                   :collect (handler-case (state map (+ index (complex x y)))
                                              (error () nil))))))

(defun read-input (file)
  (let ((input (with-open-file (stream file)
                 (loop :for line := (read-line stream nil)
                       :while line
                       :collect line))))
    (make-array (list (length input) (length (first input)))
                :initial-contents input)))

(defun next-state (map index)
  (let ((neighbours (get-neighbours map index))
        (current (state map index)))
    (cond ((char= current #\.) (if (>= (count #\| neighbours) 3)
                                   #\|
                                   #\.))
          ((char= current #\|) (if (>= (count #\# neighbours) 3)
                                   #\#
                                   #\|))
          ((char= current #\#) (if (and (>= (count #\# neighbours) 1)
                                        (>= (count #\| neighbours) 1))
                                   #\#
                                   #\.)))))


(defun evolve (map)
  (let* ((y-size (array-dimension map 0))
         (x-size (array-dimension map 1))
         (next (make-array (list y-size x-size))))
    (dotimes (y y-size)
      (dotimes (x x-size)
        (setf (state next (complex x y)) (next-state map (complex x y)))))
    next))


(defun print-state (map)
  (let* ((y-size (array-dimension map 0))
         (x-size (array-dimension map 1)))
    (dotimes (y y-size)
      (dotimes (x x-size)
        (format t "~a" (aref map y x)))
      (terpri))))

(defun count-type (map type)
  (let ((y-size (array-dimension map 0))
        (x-size (array-dimension map 1))
        (count 0))
    (dotimes (y y-size)
      (dotimes (x x-size)
        (when (char= (aref map y x) type)
          (incf count))))
    count))

(defun resource-value (map)
  (let ((y-size (array-dimension map 0))
        (x-size (array-dimension map 1))
        (count-wood 0)
        (count-lumber 0))
    (dotimes (y y-size)
      (dotimes (x x-size)
        (let ((current (aref map y x)))
          (when (char= current #\|)
            (incf count-wood))
          (when (char= current #\#)
            (incf count-lumber)))))
    (* count-wood count-lumber)))

(defun solve-1 (&optional (input #P"./18.in"))
  (let ((map (read-input input)))
    (dotimes (i 10)
      (setf map (evolve map)))
    (resource-value map)))


(defun solve-2 (&optional (input #P"./18.in"))
  (let ((end 1000000000)
        (map (read-input input))
        previous)
    (dotimes (i end)
      (let* ((next (evolve map))
             (prev-pos (position next previous :test #'equalp)))
        (when prev-pos
          (loop :for ind :from (1+ i)
                :and stat := next :then (evolve stat)
                :when (= (mod ind (1+ prev-pos)) (mod end (1+ prev-pos)))
                  :do (return-from solve-2 (resource-value stat))))
        (push next previous)
        (setf map next)))
    (resource-value map)))


(defun map-eq (x y)
  (assert (equal (array-dimensions x) (array-dimensions y)))
  (let ((x-size (array-dimension x 1))
        (y-size (array-dimension x 0)))
    (dotimes (i y-size)
      (dotimes (j x-size)
        (when (char/= (aref x i j) (aref y i j))
          (return-from map-eq nil)))))
  t)
