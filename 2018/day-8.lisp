(defpackage #:day-8
  (:use #:cl))

(in-package #:day-8)

(defparameter *input-file* #P"~/code/lisp/advent2018/day-8-input.txt")

(defstruct node
  (child-nodes nil)
  (metadata nil))


(defun read-node (stream)
  (let ((child-nodes (read stream))
        (metadata-entries (read stream))
        (node (make-node)))
    (dotimes (i child-nodes)
      (push (read-node stream) (node-child-nodes node)))
    (setf (node-child-nodes node) (nreverse (node-child-nodes node)))
    (dotimes (i metadata-entries)
      (push (read stream) (node-metadata node)))
    node))

(defun read-input-file ()
  (with-open-file (stream *input-file*)
    (read-node stream)))

(defun sum-metadata (node)
  (apply #'+ (loop :for child :in (node-child-nodes node)
                   :summing (sum-metadata child))
         (node-metadata node)))

(defun get-child (node index)
  (let ((index (1- index))
        (child-nodes (length (node-child-nodes node))))
    (when (< -1 index child-nodes)
      (nth index (node-child-nodes node)))))

(defun value-node (node)
  (cond ((not node) 0)
    	  ((null (node-child-nodes node)) (apply #'+ (node-metadata node)))
        (t (loop :for index :in (node-metadata node)
                 :summing (value-node (get-child node index))))))
