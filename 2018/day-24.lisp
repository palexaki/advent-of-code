(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :parseq)
  (ql:quickload :advent-util))
(defpackage #:day-24
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:Alexandria
        ))

(in-package #:day-24)


(defstruct group
  army
  units
  hit-points
  immunities
  weaknesses
  attack-type
  attack-damage
  initiative)

(parseq:defrule armies ()
    (and (army "Immune System:") (army "Infection:")))

(parseq:defrule army (name)
    (and name (+ (string group)) (? ""))
  (:choose 1)
  (:lambda (&rest groups) (mapcar (lambda (group)
                                    (setf (group-army (first group))
                                          (if (equal "Immune System:" name)
                                              :immune-system
                                              :infection))
                                    (first group))
                                  groups)))

(parseq:defrule group ()
    (and my-int " units each with " my-int " hit points " (? specialties)
         "with an attack that does " my-int " " wort " damage at initiative " my-int)
  (:choose 0 2 4 6 8 10)
  (:lambda (units hp specialties attack-damage attack-type initiative)
    (make-group :units units :hit-points hp :attack-damage attack-damage :attack-type attack-type
                :initiative initiative :weaknesses (cdr (assoc :weak specialties))
                :immunities (cdr (assoc :immune specialties)))))

(parseq:defrule specialties ()
    (and "(" (rep (2) specialty) ") ")
  (:choose 1)
  (:lambda (&rest ties)
    (remove-if-not #'identity ties)))


(parseq:defrule specialty ()
    (and (or "weak" "immune") " to " (+ (and wort (? ", "))) (? "; "))
  (:choose 0 2)
  (:lambda (kind specialy)
    (list* (intern (string-upcase kind) :keyword)
           (mapcar #'first
                   specialy))))

(parseq:defrule wort ()
    (+ (char "a-z"))
  (:string)
  (:lambda (word)
    (intern (string-upcase word) :keyword)))

(parseq:defrule my-int ()
    (+ digit)
  (:string)
  (:lambda (number) (parse-integer number)))

(defun solve-1 (&optional (input (day-input-lines 24 2018)))
  (declare (ignore input)))

(defun solve-2 (&optional (input (day-input-lines 24 2018)))
  (declare (ignore input)))
