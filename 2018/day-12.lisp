(defpackage #:day-12
  (:use #:cl #:cl-ppcre))

(in-package #:day-12)

(defvar *transitions*)
(defparameter *input-file* #P"~/code/lisp/advent/2018/day-12-input.txt")

(defun get-neighbours (state index)
  (loop :for index :from (- index 2) :upto (+ index 2)
        :collecting (gethash index state)))

(defun new-state (state index)
  (gethash (get-neighbours state index) *transitions*))


(defun lowest-existing (state)
  (loop :for key :being :each :hash-key :of state
        :minimizing key))

(defun max-existing (state)
  (loop :for key :being :each :hash-key :of state
        :maximizing key))

(defun evolve (state)
  (let* ((next (make-hash-table)))
    (loop :for i :from (-  (lowest-existing state) 3) :upto (+ (max-existing state) 3)
          :when (new-state state i)
          :do (setf (gethash i next) t))
    next))

(defconstant +initial-regex+ "\([.#]+\)")

(defun initial-state-string (string)
  (aref (nth-value 1 (scan-to-strings +initial-regex+ string))
        0))


(defun parse-initial-state (string)
  (let ((initial-state-string (initial-state-string string))
        (state (make-hash-table)))
    (loop :for index :from 0 :below (length initial-state-string)
          :for char :across initial-state-string
          :when (char= char #\#)
            :do (setf (gethash index state) t))
    state))

(defconstant +transition-regex+ "\([.#]+\) => #")

(defun parse-transition (transition transitions)
  (let ((initial-state (nth-value 1 (scan-to-strings +transition-regex+ transition))))
    (when initial-state
      (setf (gethash (loop :for char :across (aref initial-state 0)
                           :collect (char= char #\#))
                     transitions)
            t))))

(defun parse-transitions (transitions)
  (let ((transitions-table (make-hash-table :test 'equal)))
    (mapcar (lambda (transition)
              (parse-transition transition transitions-table))
            transitions)
    transitions-table))

(defun read-input (input)
  (with-open-file (stream input)
    (loop :for line := (read-line stream nil)
          :while line
          :collect line)))

(defun parse-input (&optional (input *input-file*))
  (let* ((lines (read-input input))
         (initial-state (parse-initial-state (first lines)))
         (transitions (parse-transitions (rest lines))))
    (values initial-state
            transitions)))

(defun value-state (state)
  (let ((sum 0))
    (maphash (lambda (key value)
               (when value
                 (incf sum key)))
             state)
    sum))

(defun solve-1 (&optional (input *input-file*))
  (let (next)
    (multiple-value-bind (state *transitions*) (parse-input input)
      (setf next (evolve state))
      (dotimes (i 10000)
        (setf next (evolve next)
              state (evolve state)))
      (values 10000 next state))))

(defun print-gen-tabulated (generation padding)
  (loop repeat (- padding (length generation))
        do (format t " "))
  (loop for cell across generation
        do (format t "~[ ~;*~]" cell))
  (format t "~%"))

(defun print-iters (iterations)
  (let ((longest (length (first (last iterations)))))
    (dolist (generation iterations)
      (print-gen-tabulated generation longest))))

(defun gen-iters (seed n)
  (loop for generation = seed then (evolve generation)
        repeat n
        collecting generation))

(defun draw-generation (generation image row)
  (loop with width = (png:image-width image)
        with length = (length generation)
        for i from (1- length) downto 0
        do (setf (aref image row (- width (- length i)) 0)
                 (* 255 (aref generation i)))))

(defun generations->image (generations)
  (let* ((width (length (first (last generations))))
         (height (length generations))
         (image (png:make-image height width 1)))
    (loop for row from 0
          for generation in generations
          do (draw-generation generation image row))
    image))

(defun write-generations (generations file)
  (let ((image (generations->image generations)))
    (with-open-file (output file :direction :output :element-type '(unsigned-byte 8)
                                 :if-exists :supersede)
      (png:encode image output))))
