(defpackage #:day-19
  (:use #:cl #:cl-ppcre))

(in-package #:day-19)

(defconstant +instructions+ '(addr addi mulr muli banr bani borr bori
                              setr seti gtir gtri gtrr eqir eqri eqrr))

(defun reg-with-reg (func reg-a reg-b registers)
  (let ((a (aref registers reg-a))
        (b (aref registers reg-b)))
    (funcall func a b)))

(defun reg-with-val (func reg-a val-b registers)
  (let ((a (aref registers reg-a)))
    (funcall func a val-b)))

(defun val-with-reg (func val-a reg-b registers)
  (let ((b (aref registers reg-b)))
    (funcall func val-a b)))


(defun addr (reg-a reg-b registers)
  (reg-with-reg #'+ reg-a reg-b registers))

(defun addi (reg-a val-b registers)
  (reg-with-val #'+ reg-a val-b registers))

(defun mulr (reg-a reg-b registers)
  (reg-with-reg #'* reg-a reg-b registers))

(defun muli (reg-a val-b registers)
  (reg-with-val #'* reg-a val-b registers))

(defun banr (reg-a reg-b registers)
  (reg-with-reg #'logand reg-a reg-b registers))

(defun bani (reg-a val-b registers)
  (reg-with-val #'logand reg-a val-b registers))

(defun borr (reg-a reg-b registers)
  (reg-with-reg #'logior reg-a reg-b registers))

(defun bori (reg-a val-b registers)
  (reg-with-val #'logior reg-a val-b registers))

(defun setr (reg-a reg-b registers)
  (declare (ignore reg-b))
  (aref registers reg-a))

(defun seti (val-a ign-b registers)
  (declare (ignore ign-b registers))
  val-a)

(defun gtir (val-a reg-b registers)
  (val-with-reg (lambda (a b)
                  (if (> a b)
                      1
                      0))
                val-a reg-b registers))

(defun gtri (reg-a val-b registers)
  (reg-with-val (lambda (a b)
                  (if (> a b)
                      1
                      0))
                reg-a val-b registers))

(defun gtrr (reg-a reg-b registers)
  (reg-with-reg (lambda (a b)
                  (if (> a b)
                      1
                      0))
                reg-a reg-b registers))

(defun eqir (val-a reg-b registers)
  (val-with-reg (lambda (a b)
                  (if (= a b)
                      1
                      0))
                val-a reg-b registers))

(defun eqri (reg-a val-b registers)
  (reg-with-val (lambda (a b)
                  (if (= a b)
                      1
                      0))
                reg-a val-b registers))

(defun eqrr (reg-a reg-b registers)
  (reg-with-reg (lambda (a b)
                  (if (= a b)
                      1
                      0))
                reg-a reg-b registers))

(defun exec-instruction (instruction a b c registers)
  (let ((res (funcall instruction a b registers)))
    (setf (aref registers c) res)))

(defun scan-to-array (regex target)
  (nth-value 1 (scan-to-strings regex target)))

(defun read-input (file)
  (with-open-file (stream file)
    (loop :for line := (read-line stream nil)
          :while line
          :collect line)))

(defparameter *instruction-name* (create-scanner "(\\D+) "))
(defparameter *instruction-args* (create-scanner "(\\d+) (\\d+) (\\d+)"))

(defun read-instruction (line)
  (list :instruction (intern (string-upcase (aref (scan-to-array *instruction-name* line) 0)))
        :arguments (map 'list #'parse-integer (scan-to-array *instruction-args* line))))

(defparameter *instruction-pointer* (create-scanner "#ip (\\d+)"))

(defun read-ip (line)
  (parse-integer (aref (scan-to-array *instruction-pointer* line) 0)))

(defun parse-program (file)
  (let ((input (read-input file)))
    (list :ip-register (read-ip (first input))
          :instructions (map 'vector #'read-instruction
                              (rest input)))))

(defun execute (program)
  (let ((ip (getf program :ip-register))
        (instructions (getf program :instructions))
        (registers (make-array 6)))
    (setf (aref registers 0) 1)
    (loop :with instructions-length := (length instructions)
          :while (< -1 (aref registers ip) instructions-length)
          :for ip-value := (aref registers ip)
          :for instruction-list := (aref instructions ip-value)
          :for (a b c) := (getf instruction-list :arguments)
          :do ;(print ip-value)
              (exec-instruction (getf instruction-list :instruction) a b c registers)
              (incf (aref registers ip)))
    registers))

(defun solve-1 ()
  (execute (parse-program #P"./19.in")))

(defun remove-single (possible true)
  (let (singles)
    (loop :for opcode :being :each :hash-key :in possible :using (hash-value instructions)
          :when (= (length instructions) 1)
            :do (push (cons opcode instructions) singles))
    (unless singles
      (error "There are no singles in this thing what to do?"))
    (loop :for (opcode instruction) :in singles
          :do
             (setf (gethash opcode true) instruction)
             (maphash (lambda (key value)
                        (setf (gethash key possible) (remove instruction value)))
                      possible))))

(defun work-out-opcodes (samples)
  (let ((possible (make-hash-table))
        (true (make-hash-table)))
    (dolist (sample samples)
      (loop :with (opcode a b c) := (getf sample :instruction)
            :for instruction :in +instructions+
            :for new := (copy-seq (getf sample :before))
            :do (exec-instruction instruction a b c new)
            :when (equalp new (getf sample :after))
              :do (push instruction (gethash opcode possible))))
    (loop :for hash-key :being :each :hash-key :in possible
          :do (setf (gethash hash-key possible)
                    (remove-duplicates (gethash hash-key possible))))
    (loop :until (= (hash-table-count true) (length +instructions+))
          :do (remove-single possible true))
    true))

(defun execute-program (opcode-mapping instructions)
  (let ((registers (vector 0 0 0 0)))
    (dolist (instruction instructions)
      (destructuring-bind (opcode a b c) instruction
        (exec-instruction (gethash opcode opcode-mapping)
                          a b c registers)))
    registers))

(defun solve-2 (file)
  (let* ((opcodes (work-out-opcodes (parse-samples-of file)))
         (instructions (parse-program file)))
    (execute-program opcodes instructions)))
