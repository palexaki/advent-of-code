(defpackage #:day-4
  (:use #:cl #:cl-ppcre))
(in-package #:day-4)

(defparameter *date-scanner* (create-scanner "\\[1518-([0-9]{2})-([0-9]{2}) (00|23):([0-9]{2})\\]"))
(defparameter *id-scanner* (create-scanner "Guard #([0-9]+)"))
(defparameter *falls-scanner* (create-scanner "falls asleep"))
(defparameter *wakes-scanner* (create-scanner "wakes up"))

(defun read-stream (stream)
  (loop for entry = (read-line stream nil)
        while entry
        collect entry))

(defun entry-date (entry)
  (let* ((date-values (nth-value 1 (scan-to-strings *date-scanner* entry)))
         (month (parse-integer (aref date-values 0)))
         (day (parse-integer (aref date-values 1)))
         (hour (parse-integer (aref date-values 2)))
         (minute (parse-integer (aref date-values 3))))
     (encode-universal-time 0 minute hour day month 1900)))



(defun sort-entries (entries)
  (sort entries #'< :key (lambda (entry)
                           (getf entry :key))))

(defun date-minutes (date)
  (multiple-value-bind (seconds minutes hours day month)
      (decode-universal-time date)
    (declare (ignore seconds hours day month))
    minutes))
(defun date-hours (date)
  (multiple-value-bind (seconds minutes hours)
      (decode-universal-time date)
    (declare (ignore seconds minutes))
    hours))
(defun date-day (date)
  (multiple-value-bind (seconds minutes hours day month)
      (decode-universal-time date)
    (declare (ignore seconds minutes hours month))
    day))
(defun date-month (date)
  (multiple-value-bind (seconds minutes hours day month)
      (decode-universal-time date)
    (declare (ignore seconds minutes hours day))
    month))

(defun date-month-day-combi (date)
  (+ (* (date-month date) 100) (date-day date)))

(defun read-input-file ()
  (with-open-file (stream "./day-4-input.txt")
    (read-stream stream )))

(defun entry-action (entry current-guard)
  (let ((date (entry-date entry)))
    `(:key ,date :date ,(date-month-day-combi date) :minute ,(date-minutes date)
            ,@(cond ((scan *id-scanner* entry) `(:guard ,(parse-integer
                                                          (aref (nth-value 1
                                                                           (scan-to-strings *id-scanner* entry))
                                                                0))
                                                 :action :starts))
                    ((scan *falls-scanner* entry) `(:guard ,current-guard :action :sleep))
                    ((scan *wakes-scanner* entry) `(:guard ,current-guard :action :wakes))))))

(defun parse-entries (entries)
  (let (current-guard
        (entries (sort entries #'< :key #'entry-date)))
    (loop :for entry :in entries
          :for action := (entry-action entry current-guard)
          :do (setf current-guard (getf action :guard))
          :collect action)))


(defun solve-1 ()
  (let* ((entries (group-by:group-by (sort-entries (parse-entries (read-input-file)))
                                     :key (lambda (entry)
                                            (getf entry :guard))
                                     :value #'identity))
         (guards (make-hash-table :size (length entries))))
    (dolist (entry entries)
      (setf (gethash (car entry) guards) (make-array 60 :element-type 'fixnum :initial-element 0)))
    (loop :for guard :in entries
          :for  guard-id := (first guard)
          :and guard-log := (rest guard) :do
            (loop :for sub-log :on guard-log
		              :as  entry := (first sub-log)
                  :and next  := (second sub-log) :do
                    (when (eq (getf entry :action) :sleep)
                      (loop :for minute :from (identity (getf entry :minute)) :below (identity (getf next :minute))
                            :do (incf (aref (gethash guard-id guards) minute))))))
    (sort (loop :for guard :being :each :hash-key :of guards :using (hash-value sleep-log)
                :collect (cons guard sleep-log))
          #'> :key (lambda (entry)
                     (loop :for i :across (cdr entry)
                           :summing i)))))
