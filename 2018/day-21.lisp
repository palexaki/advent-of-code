(defpackage #:day-21
  (:use #:cl #:cl-ppcre))

(in-package #:day-21)

(defun reg-with-reg (func reg-a reg-b registers)
  (let ((a (aref registers reg-a))
        (b (aref registers reg-b)))
    (funcall func a b)))

(defun reg-with-val (func reg-a val-b registers)
  (let ((a (aref registers reg-a)))
    (funcall func a val-b)))

(defun val-with-reg (func val-a reg-b registers)
  (let ((b (aref registers reg-b)))
    (funcall func val-a b)))


(defun addr (reg-a reg-b registers)
  (reg-with-reg #'+ reg-a reg-b registers))

(defun addi (reg-a val-b registers)
  (reg-with-val #'+ reg-a val-b registers))

(defun mulr (reg-a reg-b registers)
  (reg-with-reg #'* reg-a reg-b registers))

(defun muli (reg-a val-b registers)
  (reg-with-val #'* reg-a val-b registers))

(defun banr (reg-a reg-b registers)
  (reg-with-reg #'logand reg-a reg-b registers))

(defun bani (reg-a val-b registers)
  (reg-with-val #'logand reg-a val-b registers))

(defun borr (reg-a reg-b registers)
  (reg-with-reg #'logior reg-a reg-b registers))

(defun bori (reg-a val-b registers)
  (reg-with-val #'logior reg-a val-b registers))

(defun setr (reg-a reg-b registers)
  (declare (ignore reg-b))
  (aref registers reg-a))

(defun seti (val-a ign-b registers)
  (declare (ignore ign-b registers))
  val-a)

(defun gtir (val-a reg-b registers)
  (val-with-reg (lambda (a b)
                  (if (> a b)
                      1
                      0))
                val-a reg-b registers))

(defun gtri (reg-a val-b registers)
  (reg-with-val (lambda (a b)
                  (if (> a b)
                      1
                      0))
                reg-a val-b registers))

(defun gtrr (reg-a reg-b registers)
  (reg-with-reg (lambda (a b)
                  (if (> a b)
                      1
                      0))
                reg-a reg-b registers))

(defun eqir (val-a reg-b registers)
  (val-with-reg (lambda (a b)
                  (if (= a b)
                      1
                      0))
                val-a reg-b registers))

(defun eqri (reg-a val-b registers)
  (reg-with-val (lambda (a b)
                  (if (= a b)
                      1
                      0))
                reg-a val-b registers))

(defun eqrr (reg-a reg-b registers)
  (reg-with-reg (lambda (a b)
                  (if (= a b)
                      1
                      0))
                reg-a reg-b registers))

(defun exec-instruction (instruction a b c registers)
  (let ((res (funcall instruction a b registers)))
    (setf (aref registers c) res)))

(defun scan-to-array (regex target)
  (nth-value 1 (scan-to-strings regex target)))

(defun read-input (file)
  (with-open-file (stream file)
    (loop :for line := (read-line stream nil)
          :while line
          :collect line)))

(defparameter *instruction-name* (create-scanner "(\\D+) "))
(defparameter *instruction-args* (create-scanner "(\\d+) (\\d+) (\\d+)"))

(defun read-instruction (line)
  (list :instruction (intern (string-upcase (aref (scan-to-array *instruction-name* line) 0)))
        :arguments (map 'list #'parse-integer (scan-to-array *instruction-args* line))))

(defparameter *instruction-pointer* (create-scanner "#ip (\\d+)"))

(defun read-ip (line)
  (parse-integer (aref (scan-to-array *instruction-pointer* line) 0)))

(defun parse-program (file)
  (let ((input (read-input file)))
    (list :ip-register (read-ip (first input))
          :instructions (map 'vector #'read-instruction
                              (rest input)))))

(defun execute (program &optional (zero-start 0))
  (let ((ip (getf program :ip-register))
        (instructions (getf program :instructions))
        (registers (make-array 6)))
    (setf (aref registers 0) zero-start)
    (loop :for executed-num :from 1
          :with instructions-length := (length instructions)
          :while (< -1 (aref registers ip) instructions-length)
          :for ip-value := (aref registers ip)
          :for instruction-list := (aref instructions ip-value)
          :for (a b c) := (getf instruction-list :arguments)
          :do ;(print ip-value)
              (exec-instruction (getf instruction-list :instruction) a b c registers)
              (incf (aref registers ip))
          :finally (return (values registers executed-num)))))
