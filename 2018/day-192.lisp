(defun main ()
  (let ((limit 10551293)
        (total 0))
    (loop :for i :from 1 :upto limit
          :do (when (zerop (mod limit i))
                (incf total i))
          :finally (return total))))
