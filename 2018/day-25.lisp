(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :fset)
  (ql:quickload :advent-util))

(defpackage #:day-25
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))


(in-package #:day-25)

(defstruct (union-find (:conc-name uf-))
  parent
  rank)

(defun initialize-union-find (size)
  (make-union-find :parent (make-array size :initial-contents (loop :for x :from 0 :below size
                                                                    :collect x))
                   :rank (make-array size :initial-element 0)))


(defun uf-find (union-find x)
  (let* ((parents (uf-parent union-find))
         (parent (aref parents x)))
    (if (= parent x)
        x
        (setf (aref parents x)
              (uf-find union-find parent)))))

(defun uf-union (union-find x y)
  (let ((xrt (uf-find union-find x))
        (yrt (uf-find union-find y))
        (ranks (uf-rank union-find))
        (parents (uf-parent union-find)))
    (cond
      ((< (aref ranks yrt) (aref ranks xrt)) (setf (aref parents yrt) xrt))
      ((< (aref ranks xrt) (aref ranks yrt)) (setf (aref parents xrt) yrt))
      ((/= xrt yrt) (setf (aref parents yrt) xrt) (incf (aref ranks xrt))))
    (/= xrt yrt)))


(defun components (union-find &key (start 0) end (components (fset:empty-set)))
  (if (>= start (or end (length (uf-rank union-find))))
      components
      (components union-find :start (1+ start)
                             :end (or end (length (uf-rank union-find)))
                             :components (fset:with components
                                                    (uf-find union-find
                                                             (aref (uf-parent union-find) start))))))

(defun distance (a b)
  (reduce #'+
          (mapcar (alexandria:compose #'abs #'-) a b)))

(defparameter *example* '("0,0,0,0"
                          "3,0,0,0"
                          "0,3,0,0"
                          "0,0,3,0"
                          "0,0,0,3"
                          "0,0,0,6"
                          "9,0,0,0"
                          "12,0,0,0"))

(defparameter *example-2* '("-1,2,2,0"
                            "0,0,2,-2"
                            "0,0,0,-2"
                            "-1,2,0,0"
                            "-2,-2,-2,2"
                            "3,0,2,-1"
                            "-1,3,2,2"
                            "-1,0,-1,0"
                            "0,2,1,-2"
                            "3,0,0,0"))

(defparameter *example-3* '("1,-1,0,1"
                            "2,0,-1,0"
                            "3,2,-1,0"
                            "0,0,3,1"
                            "0,0,-1,-1"
                            "2,3,-2,0"
                            "-2,2,0,0"
                            "2,-2,0,-1"
                            "1,-1,0,-1"
                            "3,2,0,2"))


(defparameter *example-4* '("1,-1,-1,-2"
                            "-2,-2,0,1"
                            "0,2,1,3"
                            "-2,3,-2,1"
                            "0,2,3,-2"
                            "-1,-1,1,-2"
                            "0,-2,-1,0"
                            "-2,2,3,-1"
                            "1,2,2,0"
                            "-1,-2,0,-2"))

(defun solve-1 (&optional (input (day-input-lines 25 2018)))
  (let ((points (map 'vector #'ints input))
        (uf (initialize-union-find (length input))))
    (dotimes (i (length points) (components uf))
      (dotimes (j (- (length points) i 1))
        (let ((k (+ i j 1)))
          (when (<= (distance (aref points i)
                              (aref points k))
                    3)
            ;(print (list i k))
            (uf-union uf i k)))))))

(defun solve-2 ())
