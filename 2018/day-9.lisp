(defpackage #:day-9
  (:use #:cl))
(in-package #:day-9)

(defun insert (list index new-element)
  (let (retlist)
    (dotimes (i (length list))
      (when (= i index)
        (push new-element retlist))
      (push (nth i list) retlist))
    (when (>= index (length list))
      (push new-element retlist))
    (nreverse retlist)))

(defun delete-ind (list index)
  (delete-if (lambda (num)
               t)
             list
             :start index
             :count 1))

(defun circ-index (ind list)
  (mod ind (length list)))

(defun insert (list index new)
  (cond ((zerop index) (list* new list))
        (t (push new (cdr (nthcdr (1- index) list)))
           list)))

(defun print-marbles (circle current)
  (loop :for marble :in circle
        :for i :from 0
        :do (if (= i current)
                (format t " (~a)" marble)
                (format t " ~a" marble)))
  (terpri))

(defun play-game (max players)
  (let ((scores (make-array players)))
    (loop :with current := 0
          :and  circle  := '(0)
          :for marble :from 1 :upto max
          :for player := 0 :then (mod (1+ player) players)
          :do (cond ((zerop (mod marble 23))
                     (incf (aref scores player) marble)
                     (setf current (circ-index (- current 7) circle))
                     (incf (aref scores player) (nth current circle))
                     (setf circle (delete-ind circle current)))
                    (t (setf current (circ-index (+ current 2) circle))
                       (setf circle (insert circle current marble))))
          )
    scores))
