(defpackage #:day-16
  (:use #:cl #:cl-ppcre))

(in-package #:day-16)

(defconstant +instructions+ '(addr addi mulr muli banr bani borr bori
                              setr seti gtir gtri gtrr eqir eqri eqrr))

(defun reg-with-reg (func reg-a reg-b registers)
  (let ((a (aref registers reg-a))
        (b (aref registers reg-b)))
    (funcall func a b)))

(defun reg-with-val (func reg-a val-b registers)
  (let ((a (aref registers reg-a)))
    (funcall func a val-b)))

(defun val-with-reg (func val-a reg-b registers)
  (let ((b (aref registers reg-b)))
    (funcall func val-a b)))


(defun addr (reg-a reg-b registers)
  (reg-with-reg #'+ reg-a reg-b registers))

(defun addi (reg-a val-b registers)
  (reg-with-val #'+ reg-a val-b registers))

(defun mulr (reg-a reg-b registers)
  (reg-with-reg #'* reg-a reg-b registers))

(defun muli (reg-a val-b registers)
  (reg-with-val #'* reg-a val-b registers))

(defun banr (reg-a reg-b registers)
  (reg-with-reg #'logand reg-a reg-b registers))

(defun bani (reg-a val-b registers)
  (reg-with-val #'logand reg-a val-b registers))

(defun borr (reg-a reg-b registers)
  (reg-with-reg #'logior reg-a reg-b registers))

(defun bori (reg-a val-b registers)
  (reg-with-val #'logior reg-a val-b registers))

(defun setr (reg-a reg-b registers)
  (declare (ignore reg-b))
  (aref registers reg-a))

(defun seti (val-a ign-b registers)
  (declare (ignore ign-b registers))
  val-a)

(defun gtir (val-a reg-b registers)
  (val-with-reg (lambda (a b)
                  (if (> a b)
                      1
                      0))
                val-a reg-b registers))

(defun gtri (reg-a val-b registers)
  (reg-with-val (lambda (a b)
                  (if (> a b)
                      1
                      0))
                reg-a val-b registers))

(defun gtrr (reg-a reg-b registers)
  (reg-with-reg (lambda (a b)
                  (if (> a b)
                      1
                      0))
                reg-a reg-b registers))

(defun eqir (val-a reg-b registers)
  (val-with-reg (lambda (a b)
                  (if (= a b)
                      1
                      0))
                val-a reg-b registers))

(defun eqri (reg-a val-b registers)
  (reg-with-val (lambda (a b)
                  (if (= a b)
                      1
                      0))
                reg-a val-b registers))

(defun eqrr (reg-a reg-b registers)
  (reg-with-reg (lambda (a b)
                  (if (= a b)
                      1
                      0))
                reg-a reg-b registers))

(defun exec-instruction (instruction a b c registers)
  (let ((res (funcall instruction a b registers)))
    (setf (aref registers c) res)))

(defparameter *before* (create-scanner "Before:\\s+\\[(\\d+), (\\d+), (\\d+), (\\d+)\\]"))
(defparameter *after* (create-scanner "After:\\s+\\[(\\d+), (\\d+), (\\d+), (\\d+)\\]"))
(defparameter *instruction* (create-scanner "\\] (\\d+) (\\d+) (\\d+) (\\d+) A"))


(defun scan-to-array (regex target)
  (nth-value 1 (scan-to-strings regex target)))

(defun read-sample (string)
  (let ((before (map 'vector #'parse-integer
                     (scan-to-array *before* string)))
        (after (map 'vector #'parse-integer
                    (scan-to-array *after* string)))
        (instruction (map 'list #'parse-integer
                          (scan-to-array *instruction* string))))
    (when (and (plusp (length before))
               (plusp (length after))
               (plusp (length instruction)))
      (list :before before :after after :instruction instruction))))


(defun count-sample (sample)
  (if sample
    (loop :with (nil a b c) := (getf sample :instruction)
          :for instruction :in +instructions+
          :for new := (copy-seq (getf sample :before))
          :do (exec-instruction instruction a b c new)
          :count (equalp new (getf sample :after)))
    0))

(defun read-input (file)
  (with-open-file (stream file)
    (loop :for line := (read-line stream nil)
          :while line
          :collect line)))

(defun parse-samples-of (file)
  (remove nil (mapcar #'read-sample (read-input file))))

(defparameter *only-instruction* (create-scanner "(\\d+) (\\d+) (\\d+) (\\d+)"))

(defun read-instruction (line)
  (map 'list #'parse-integer (scan-to-array *only-instruction* line)))

(defun parse-program (file)
  (remove nil (mapcar #'read-instruction (read-input file))))

(defun solve-1 (file)
  (let ((input (parse-samples-of file)))
    (count-if (lambda (count)
                (>= count 3))
              (mapcar #'count-sample input))))

(defun remove-single (possible true)
  (let (singles)
    (loop :for opcode :being :each :hash-key :in possible :using (hash-value instructions)
          :when (= (length instructions) 1)
            :do (push (cons opcode instructions) singles))
    (unless singles
      (error "There are no singles in this thing what to do?"))
    (loop :for (opcode instruction) :in singles
          :do
             (setf (gethash opcode true) instruction)
             (maphash (lambda (key value)
                        (setf (gethash key possible) (remove instruction value)))
                      possible))))

(defun work-out-opcodes (samples)
  (let ((possible (make-hash-table))
        (true (make-hash-table)))
    (dolist (sample samples)
      (loop :with (opcode a b c) := (getf sample :instruction)
            :for instruction :in +instructions+
            :for new := (copy-seq (getf sample :before))
            :do (exec-instruction instruction a b c new)
            :when (equalp new (getf sample :after))
              :do (push instruction (gethash opcode possible))))
    (loop :for hash-key :being :each :hash-key :in possible
          :do (setf (gethash hash-key possible)
                    (remove-duplicates (gethash hash-key possible))))
    (loop :until (= (hash-table-count true) (length +instructions+))
          :do (remove-single possible true))
    true))

(defun execute-program (opcode-mapping instructions)
  (let ((registers (vector 0 0 0 0)))
    (dolist (instruction instructions)
      (destructuring-bind (opcode a b c) instruction
        (exec-instruction (gethash opcode opcode-mapping)
                          a b c registers)))
    registers))

(defun solve-2 (file)
  (let* ((opcodes (work-out-opcodes (parse-samples-of file)))
         (instructions (parse-program file)))
    (execute-program opcodes instructions)))
