(defpackage #:day-7
  (:use #:cl #:alexandria))

(in-package #:day-7)
(defparameter *input-file* #P"~/code/lisp/advent/2018/day-7-input.txt")

(defun remove-topological (dag &key (predicate #'<) (test #'char=))
  (let* ((removable (remove-if-not (lambda (node)
                                     (= (length node) 1))
                                   dag))
         (removable (sort removable predicate))
         (removable (first (first removable)))
         (dag (mapcar (lambda (node)
                        (list* (first node) (remove removable (rest node))))
                      dag)))
    (when removable
      (values (remove-if (lambda (node)
                           (funcall test removable (first node)))
                         dag)
              removable))))


(defun node< (node-a node-b)
  (char< (first node-a) (first node-b)))

(defun topological-sort (dag)
  (loop :for (sub-dag removed)
          := (multiple-value-list (remove-topological dag :predicate #'node<))
          :then (multiple-value-list (remove-topological sub-dag :predicate #'node<))
        :collecting removed
        :while sub-dag))

(defun read-input ()
  (let* ((all-items (make-hash-table))
         (dag-input (with-open-file (stream *input-file*)
                      (loop :for input := (read-line stream nil)
                            :while input
                            :do (setf (gethash (char input 0) all-items) t
                                      (gethash (char input 2) all-items) t)
                            :collect (list (char input 2) (char input 0)))))
         (dag (group-by:group-by dag-input :value #'second)))
    (maphash (lambda (key value)
               (declare (ignore value))
               (when (not (assoc key dag :test #'char=))
                 (push (list key) dag)))
             all-items)
    dag))

(defun work-unit (dag)
  (nth-value 1 (remove-topological dag :predicate #'node<)))

(defun work-unit-effort (work-unit)
  (- (char-code work-unit) 4))

(defun remove-temporary (dag work-unit)
  (remove (list work-unit) dag :test #'equal))

(defun remove-fully (dag work-unit)
  (mapcar (lambda (node)
            (remove work-unit node))
          dag))

(defun work (dag workers-amount)
  (let ((workers (make-array workers-amount :initial-element nil))
        (workers-units (make-array workers-amount :initial-element nil)))
    (loop :for second :from 0
          :while (or dag (position-if #'identity workers))
          :do (when-let (pos (position 0 workers))
                (setf (aref workers pos) nil
                      dag (remove-fully dag (aref workers-units pos))))
              (loop :while (and (work-unit dag)
                                (position nil workers)) :do
                (when-let ((pos (position nil workers))
                           (work-unit (work-unit dag)))
                  (setf (aref workers pos) (work-unit-effort work-unit)
                        dag (remove-temporary dag work-unit)
                        (aref workers-units pos) work-unit)))
              (setf workers (map (list 'simple-vector workers-amount) (lambda (amount)
                                                                        (when amount
                                                                          (1- amount)))
                                workers))
          :finally (return second))))
