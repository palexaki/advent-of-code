(defpackage #:day-2
  (:use #:cl))

(in-package #:day-2)

(defvar *input* #P"./day-2-input.txt")

(defun ddd (word)
  (let ((count (make-hash-table))
        two
        three)
    (loop for char across word do
      (incf (gethash char count 0)))
    (maphash (lambda (key value)
               (declare (ignore key))
               (when (= 2 value)
                 (setf two t))
               (when (= 3  value)
                 (setf three t)))
             count)
    (values two three)))

(defun read-file (filename)
  (with-open-file (stream filename)
    (loop :for word := (read-line stream nil)
          :while word
          :collect word)))

(defun solve-1 ()
  (let ((words (read-file *input*))
        (twos 0)
        (threes 0))
    (dolist (word words)
      (multiple-value-bind (two three)
          (ddd word)
        (when two
          (incf twos))
        (when three
          (incf threes))))
    (* twos threes)))


(defun hamming-distance (id1 id2)
  (let ((difference 0))
    (loop :for letter1 :across id1
          :and letter2 :across id2 :do
            (when (char/= letter1 letter2)
              (incf difference)))
    difference))


(defun has-one-distance-p (id ids)
  (loop :for candidate-id :in ids
        :when (= (hamming-distance id candidate-id) 1)
          :return candidate-id))

(defun one-distance-pair (ids)
  (loop :for candidate-id :in ids
        :and candidate-ids :on (rest ids)
        :for has-one-distance-p := (has-one-distance-p candidate-id candidate-ids)
        :when has-one-distance-p
          :return (values candidate-id has-one-distance-p)))

(defun solve-2 (&optional your-ids)
  (let* ((ids (or your-ids
                  (read-file *input*)))
         (pair (multiple-value-list (one-distance-pair ids)))
         (pair (mapcar (lambda (id)
                         (loop :for char :across id
                               :collect char))
                       pair)))
    (values
     (intersection (first pair) (second pair) :test #'char=)
     pair)))
