(defpackage #:day-3
  (:use #:cl))

(in-package #:day-3)

(defvar *input* #P"./day-3-input.txt")

(defun make-claim (id left-margin top-margin width height)
  `(:id ,id :left-margin ,left-margin :top-margin ,top-margin
    :width ,width :height ,height))

(defun claim-area (claim fabric)
  (destructuring-bind (&key left-margin top-margin width height)
      claim
    (declare (type integer top-margin left-margin width height))
    (loop :repeat height
          :as y :from top-margin :do
            (loop :repeat width
                  :as x :from left-margin :do
                    (incf (aref fabric y x))))))

(defun read-claim (claim-string)
  (nsubstitute #\Space #\# claim-string)
  (nsubstitute #\Space #\@ claim-string)
  (nsubstitute #\Space #\, claim-string)
  (nsubstitute #\Space #\x claim-string)
  (nsubstitute #\Space #\: claim-string)
  (with-input-from-string (stream claim-string)
    (make-claim (read stream) (read stream) (read stream) (read stream) (read stream))))


(defun read-claims (stream)
  (mapcar #'read-claim (loop :for claim := (read-line stream nil)
                             :while claim
                             :collect claim)))

(defun claims-overlap-p (claim-1 claim-2)
  (destructuring-bind ((&key
                          ((:left-margin left-margin-1)) ((:top-margin top-margin-1))
                          ((:width width-1)) ((:height height-1)) &allow-other-keys)
                       (&key
                          ((:left-margin left-margin-2)) ((:top-margin top-margin-2))
                          ((:width width-2)) ((:height height-2)) &allow-other-keys))
      (list claim-1 claim-2)
    (let ((right-1 (+ left-margin-1 width-1 ))
          (right-2 (+ left-margin-2 width-2 ))
          (down-1 (+ top-margin-1 height-1 ))
          (down-2 (+ top-margin-2 height-2 )))
      (incf left-margin-1)
      (incf top-margin-1)
      (incf left-margin-2)
      (incf top-margin-2)

      (not (or (> left-margin-2 right-1)
               (< right-2 left-margin-1)
               (> top-margin-2 down-1)
               (< down-2 top-margin-1))))))

(defun solve-1 (&optional claims)
  (let ((claims (or claims (with-open-file (stream *input*)
                             (read-claims stream))))
        (fabric (make-array '(1500 1500))))
    (mapcar (lambda (claim)
              (claim-area claim fabric))
            claims)
    (loop :for i :below (array-total-size fabric)
          :count (> (row-major-aref fabric i) 1))))


(defun solve-2 (&optional claims)
  (let ((claims (or claims (with-open-file (stream *input*)
                             (read-claims stream))))
        derp)
    (dolist (candidate claims)
      (print candidate)
      (when (notany (lambda (claim)
                      (claims-overlap-p candidate claim))
                    (remove candidate claims :test #'equal))
        (setf derp candidate)))
    derp))
