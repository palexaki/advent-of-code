(defpackage #:day-13
  (:use #:cl))

(in-package #:day-13)

(defparameter *input-file* #P"~/code/lisp/advent/2018/day-13-input.txt")

(defparameter *char-to-dir* '((#\> . right) (#\< . left)
                              (#\^ . up) (#\v . down)))

(defun read-to-line (stream)
  (loop :for line := (read-line stream nil)
        :while line
        :collect line))

(defun read-input (file)
  (let (res)
    (with-open-file (stream file)
      (setf res (read-to-line stream)))
    (make-array (list (length res) (length (first res)))
                :initial-contents res)))

(defun make-cart (direction-char position)
  (make-instance 'cart :position position :direction (cdr (assoc direction-char *char-to-dir*
                                                                 :test #'char=))))

(defun char-to-line (char)
  (cond ((or (char= char #\>)
             (char= char #\<)) #\-)
        ((or (char= char #\^)
             (char= char #\v)) #\|)
        (t char)))

(defun is-cart-p (char)
  (cond ((char= #\v char) '(down))
        ((char= #\^ char) '(up))
        ((char= #\> char) '(right))
        ((char= #\< char) '(left))))


(defun action (char cart)
  (let ((cart-direction (direction cart))
        (turned (turned cart))
        (last-turn (last-turn cart)))
    (cond (turned (list  cart-direction))
          ((char= #\| char) (list cart-direction))
          ((char= #\- char) (list cart-direction))
          ((char= #\/ char) (curve-right cart-direction turned))
          ((char= #\\ char) (curve-left cart-direction turned))
          ((char= #\+ char) (intersection-action cart-direction last-turn turned))
          (t (error "what happend")))))

(defun curve-right (direction has-turned)
  (if has-turned
      (list direction)
      (case direction
        (up '(turn-right))
        (down '(turn-right))
        (left '(turn-left))
        (right '(turn-left)))))


(defun curve-left (direction has-turned)
  (if has-turned
      (list direction)
      (case direction
        (down '(turn-left))
        (up '(turn-left))
        (right '(turn-right))
        (left '(turn-right)))))

(defun intersection-action (direction last-turn turned)
  (if turned
      (list direction)
      (cdr (assoc last-turn `((turn-right . (turn-left inter))
                              (turn-left . (,direction inter))
                              (straight . (turn-right inter)))))))

(defun turn-left (direction)
  (case direction
    (up 'left)
    (down 'right)
    (left 'down)
    (right 'up)))

(defun turn-right (direction)
  (case direction
    (up 'right)
    (down 'left)
    (left 'up)
    (right 'down)))

(defclass cart ()
  ((pos :accessor pos :initarg :position)
   (direction :accessor direction :initarg :direction)
   (turned :accessor turned :initform nil)
   (last-turn :accessor last-turn :initform 'turn-right)))

(defmethod print-object ((obj cart) stream)
  (print-unreadable-object (obj stream :type t)
    (format stream "position: ~a, direction: ~a" (pos obj) (direction obj)))) 

(defun carts (map)
  (let* ((dimensions (array-dimensions map))
         (y-size (first dimensions))
         (x-size (second dimensions))
         carts)
    (dotimes (y y-size)
      (dotimes (x x-size)
        (let ((char (aref map y x)))
          (when (is-cart-p char)
            (push (make-cart char (list x y)) carts)
            (setf (aref map y x) (char-to-line char))))))
    carts))

(defun sort-carts (carts map)
  (sort (copy-list carts) #'< :key (lambda (cart)
                                     (apply #'array-row-major-index map (reverse (pos cart))))))

(defun apply-action (cart action-l)
  (print cart)
  (destructuring-bind (action &optional intersection) action-l
    (with-slots (pos direction last-turn turned) cart
      (when (eq direction 'collided)
        (return-from apply-action))
      (setf turned (find action '(turn-left turn-right)))
      (when intersection
        (setf last-turn (or (find action '(turn-left turn-right))
                            'straight)))
      (when (eq action 'turn-left)
        (setf direction (turn-left direction)))
      (when (eq action 'turn-right)
        (setf direction (turn-right direction)))
      (when (eq action 'right)
        (incf (first pos)))
      (when (eq action 'left)
        (decf (first pos)))
      (when (eq action 'up)
        (decf (second pos)))
      (when (eq action 'down)
        (incf (second pos))))))


(defun check-collision (cart carts)
  (loop :with positio := (pos cart)
        :for other :in carts
        :do (when (and (not (eq other cart)) (equal (pos other) positio))
              (setf (direction cart) 'collided
                    (direction other) 'collided))))

(defun solve-1 (&optional (input *input-file*))
  (let* ((map (read-input input))
         (carts (sort-carts (carts map) map)))
    (print map)
    (print carts)
    (loop :repeat 10 :while
          (every (lambda (cart)
                   (not (eq (direction cart) 'collided)))
                 carts)
          :do (mapcar (lambda (cart)
                        (let* ((pos (pos cart))
                               (char (apply #'aref map (reverse pos))))
                          (apply-action cart (action char cart))
                          (check-collision cart carts)))
                      carts)
          (setf carts (sort-carts carts map)))
    carts))
