(defpackage #:day-15
  (:use #:cl #:queues #:alexandria))

(in-package #:day-15)

(defparameter *input-file* "~/code/lisp/advent/2018/15.in")


(defclass creature ()
  ((health-points :initform 200 :accessor hp)
   (pos :initarg :position :accessor pos)))

(defclass goblin (creature)
  ())

(defclass elf (creature)
  ())


(defgeneric enemy-p (me other))

(defmethod enemy-p ((me t) (other t))
  nil)

(defmethod enemy-p ((me goblin) (other elf))
  (< 0 (hp other)))

(defmethod enemy-p ((me elf) (other goblin))
  (< 0 (hp other)))

(defun read-input (file)
  (let* ((contents (with-open-file (stream file)
                     (loop :for line := (read-line stream nil)
                           :while line
                           :collect line)))
         (line-length (length (first contents)))
         (total (length contents)))
    (make-array (list total line-length) :initial-contents contents)))

(defmethod print-object ((object creature) stream)
  (print-unreadable-object (object stream :type t)
    (format stream "HP: ~A, POS: (~A ~A)"
            (hp object)
            (realpart (pos object))
            (imagpart (pos object)))))


(defun make-creature (char position)
  (cond ((char= char #\G) (make-instance 'goblin :position position))
        ((char= char #\E) (make-instance 'elf :position position))))

(defun parse-input (input)
  (let* ((x-size (array-dimension input 1))
         (y-size (array-dimension input 0))
         creatures)
    (dotimes (y y-size)
      (dotimes (x x-size)
        (when-let (creature (make-creature (aref input y x) (complex x y)))
          (setf (aref input y x) creature)
          (push creature creatures))))
    (values input
            creatures)))


(defun free (point map)
  (eql #\. (aref map (imagpart point) (realpart point))))

(defun possible (point map)
  (let* ((y-size (array-dimension map 0))
         (x-size (array-dimension map 1)))
    (when (and (< -1 (realpart point) x-size)
               (< -1 (imagpart point) y-size))
      t)))


(defun neighbours-no-free (point map)
  (let* ((neighbour-tings '(#C(0 -1) #C(-1 0)
                            #C(1 0) #C(0 1)))
         (neighbours (mapcar (lambda (d)
                               (+ point d))
                             neighbour-tings)))
    (remove-if-not (lambda (point)
                     (possible point map))
                   neighbours)))

(defun neighbours (point map)
  (remove-if-not (lambda (point)
                   (free point map))
                 (neighbours-no-free point map)))


(defun bfs (start end map)
  (let ((costs (make-hash-table :test 'equal))
        (queue (make-queue :simple-queue))
        (passed (make-hash-table :test 'equal)))
    (setf (gethash start costs) 0)
    (dolist (neighbour (neighbours start map))
      (qpush queue neighbour)
      (setf (gethash neighbour costs) 1))
    (loop :while (and (plusp (qsize queue))
                      (not (gethash end passed)))
          :do (let* ((next (qpop queue))
                     (cost (gethash next costs))
                     (neighbours (neighbours next map)))
                (setf (gethash next passed) t)
                (dolist (neighbour neighbours)
                  (let ((n-cost (gethash neighbour costs gsll:+positive-infinity+)))
                    (when (< (+ cost 1) n-cost)
                      (qpush queue neighbour)
                      (setf (gethash neighbour costs) (+ cost 1)))))))
    (gethash end costs gsll:+positive-infinity+)))


(defun read-pos (map creature)
  (array-row-major-index map (imagpart (pos creature))
                             (realpart (pos creature))))


(defun targets (creature creatures map)
  (remove-if-not (lambda (opponent)
                   (and (enemy-p creature opponent)
                        (neighbours (pos opponent) map)))
                 creatures))


(defun sort-by-read-pos (creatures map)
  (stable-sort (copy-list creatures)
               #'<
               :key (lambda (creature)
                      (read-pos map creature))))


(defun do-step (creature creatures map)
  (when (> 0 (hp creature))
    (return-from do-step))
  (let* ((targets (sort-by-read-pos (targets creature creatures map) map))
         (targets (stable-sort targets #'<
                               :key (lambda (target)
                                      (bfs (pos creature)
                                           (pos target)
                                           map))))
         (target (first targets)))
    (when (and target
               (/= 1 (bfs (pos creature)
                          (pos target)
                          map)))
      (let ((next-pos (first (stable-sort (neighbours (pos creature) map)
                                   #'<
                                     :key (lambda (neighbour)
                                            (bfs (+ (pos creature) neighbour)
                                                 (pos target)
                                                 map))))))
        (when next-pos
          (setf (aref map (imagpart (pos creature)) (realpart (pos creature))) #\.
                (aref map (imagpart next-pos) (realpart next-pos)) creature
                (pos creature) next-pos))))
    target))

(defun do-attack (creature map)
  (let* ((neighbours (neighbours-no-free (pos creature) map))
         (neighbours (mapcar (lambda (point)
                               (aref map (imagpart point) (realpart point)))
                             neighbours))
         (neighbours (remove-if-not (lambda (opp)
                                      (enemy-p creature opp))
                                    neighbours))
         (neighbours (sort neighbours #'< :key #'hp))
         (neighbours (sort-by-read-pos neighbours map))
         (target (first neighbours)))
    (when target
      (decf (hp target)
          3)
      (when (<= (hp target) 0)
        (setf (aref map (imagpart (pos target))
                    (realpart (pos target))) #\.)))))


(defun solve-1 (&optional (file *input-file*))
  (multiple-value-bind (map creats) (parse-input (read-input file))
    (loop :for round :from 1
          :for creatures := (sort-by-read-pos creats map)
                         :then (sort-by-read-pos creatures map)
          :do (dolist (creature creatures)
                (unless (targets creature creatures map)
                  (return-from solve-1 (values round
                                               creatures
                                               map)))
                (when-let (target (do-step creature creatures map))
                  (do-attack creature map)))
          ;(print-state map)
          )))


(defgeneric e-char (e))

(defmethod e-char ((e character))
    e)

(defmethod e-char ((e elf))
  #\E)
(defmethod e-char ((e goblin))
  #\G)

(defun print-state (map)
  (let ((x-size (array-dimension map 1))
        (y-size (array-dimension map 0)))
    (dotimes (y y-size)
      (dotimes (x x-size)
        (format t "~a" (e-char (aref map y x))))
      (format t "~%"))))
