(defpackage #:day-17
  (:use #:cl #:cl-ppcre :zpng))

(in-package #:day-17)

(defparameter *vein* (create-scanner "([xy])=(\\d+), [xy]=(\\d+)..(\\d+)"))

(defun read-vein (input)
  (let* ((vein (scan-to-array *vein* input))
         (static-dir (intern (string-upcase (aref vein 0)) :keyword))
         (static-val (parse-integer (aref vein 1)))
         (line-vals (map 'list #'parse-integer (subseq vein 2))))
    (list :dir static-dir :static-val static-val :line-vals line-vals)))

(defun scan-to-array (regex target)
  (nth-value 1 (scan-to-strings regex target)))

(defun read-input (file)
  (with-open-file (stream file)
    (loop :for line := (read-line stream nil)
          :while line
          :collect line)))

(defun parse-input (input)
  (mapcar #'read-vein input))


(defun min-max (veins dir)
  (loop :for vein :in veins
        :for val := (if (eq (getf vein :dir) dir)
                        (list (getf vein :static-val))
                        (getf vein :line-vals))
        :minimizing (apply #'min val) :into min-x
        :maximizing (apply #'max val) :into max-x
        :finally (return (values min-x max-x))))

(defun populate-underground-map (vein map min-x)
  (let ((direction (getf vein :dir))
        (static (getf vein :static-val)))
    (loop :with (min max) := (getf vein :line-vals)
          :for i :from min :upto max
          :for index := (if (eq direction :y)
                            (list static (- i min-x -1))
                            (list i (- static min-x -1)))
          :do (setf (apply #'aref map index) :w))))

(defun create-underground-map (veins)
  (multiple-value-bind (min-x max-x) (min-max veins :x)
    (let* ((max-y (nth-value 1 (min-max veins :y)))
           (underground-map (make-array (list (1+ max-y) (- max-x min-x -3))
                                        :initial-element nil)))
      (dolist (vein veins)
        (populate-underground-map vein underground-map min-x))
      underground-map)))

(defun in-bounds (map index)
  (destructuring-bind (y x) (array-dimensions map)
    (let ((y-index (realpart index))
          (x-index (imagpart index)))
      (and (< -1 y-index y)
           (< -1 x-index x)))))

(defun bottomp (underground-map new)
  (>= (1+ (realpart new)) (array-dimension underground-map 0)))

(defun under (underground-map new)
  (aref underground-map (1+ (realpart new)) (imagpart new)))

(defun trickle-stream (underground-map new)
  (cond ((bottomp underground-map new) nil)
        ((not (status underground-map (1+ new)))
         (setf (status underground-map (1+ new)) :s)
         (list (+ new #c(1 0))))
        (t (setf (status underground-map new)
                 :b)
           (list new))))

(defun status (map index)
  (when (in-bounds map index)
    (aref map (realpart index) (imagpart index))))

(defun (setf status) (value map index)
  (when (in-bounds map index)
    (when (= #c(12 11) index)
      (when (eq value :b)
        (break)))
    (setf (aref map (realpart index) (imagpart index)) value)))

(defun move-steady-p (status)
  (or (not status) (eq status :s)))


(defun trickle-steady (underground-map new)
  (when (= new #C(12 11))
    (break))
  (let* ((left (+ new #C(0 -1)))
         (right (+ new #C(0 1)))
         (left-val (status underground-map left))
         (right-val (status underground-map right)))
    (remove nil (list (when (move-steady-p left-val)
                        (setf (status underground-map left) :s)
                        left)
                      (when (move-steady-p right-val)
                        (setf (status underground-map right) :s)
                        right)))))

(defun trickle-new (underground-map new-water)
  (let (next-new-water)
    (dolist (new new-water)
      (let ((type (status underground-map new)))
        (setf next-new-water (append next-new-water
                                     (case type
                                       (:s (trickle-stream underground-map new))
                                       (:b (trickle-steady underground-map new)))))))
    next-new-water))

(defun find-lowest-active-stream (map)
  (let ((x-size (array-dimension map 1))
        (y-size (array-dimension map 0)))
    (loop :for y :from (1- y-size) :downto 0
          :for streams := (loop :for x :from 0 :below x-size
                                :when (eq (aref map y x) :s)
                                  :collect (complex y x))
          :when (and streams (= y (1- y-size)))
            :do (return)
          :when (and streams)
            :do (return streams))))

(defun trickle-water (underground-map min-x)
  (setf (aref underground-map 0 (- 500 min-x -1)) :s)
  (let ((new-water (list (complex 0 (- 500 min-x -1)))))
    (loop :while new-water
          :do (setf new-water (trickle-new underground-map new-water))
              (unless new-water
                (setf new-water (find-lowest-active-stream underground-map)))))
  underground-map)

(defun solve-1 (file)
  (let* ((veins (parse-input (read-input file)))
         (min-x (min-max veins :x))
         (min-y (min-max veins :y))
         (veins-map (create-underground-map veins)))
    (trickle-water veins-map min-x)
    (values veins-map
            (count-water veins-map min-y))))


(defun count-water (map min-y)
  (loop :for y :from min-y :below (array-dimension map 0)
        :summing (loop :for x :from 0 :below (array-dimension map 1)
                       :for val := (aref map y x)
                       :count (or (eq val :s) (eq val :b)))))


(defun print-map (map)
  (destructuring-bind (y-size x-size) (array-dimensions map)
    (dotimes (y y-size)
      (dotimes (x x-size)
        (if (aref map y x)
            (format t "~a " (aref map y x))
            (format t ". ")))
      (terpri))))


(defun draw-map (map file)
  (let* ((x-size (array-dimension map 1))
         (y-size (array-dimension map 0))
         (png (make-instance 'pixel-streamed-png
                             :color-type :truecolor
                             :width x-size
                             :height y-size)))
    (with-open-file (stream file :direction :output :element-type '(unsigned-byte 8)
                                 :if-exists :supersede :if-does-not-exist :create)
      (start-png png stream)
      (dotimes (y y-size)
        (dotimes (x x-size)
          (let ((val (aref map y x)))
            (cond ((null val) (write-pixel (list 0 0 0) png))
                  ((eq val :s) (write-pixel (list 0 255 0) png))
                  ((eq val :b) (write-pixel (list 0 0 255) png))
                  ((eq val :w) (write-pixel (list 255 255 255) png))))))
      (finish-png png))))
