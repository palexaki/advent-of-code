(defpackage #:day-11
  (:use #:cl))

(in-package #:day-11)

(defparameter *input* 2694)

(defun cell-power-level (x y serial)
  (let* ((rack-id (+ x 10))
         (level (* rack-id y))
         (level (+ level serial))
         (level (* level rack-id))
         (level (mod (floor level 100) 10)))
    (- level 5)))


(defun create-grid (x-size y-size serial)
  (let ((grid (make-array (list y-size x-size))))
    (dotimes (y y-size)
      (dotimes (x x-size)
        (setf (aref grid y x) (cell-power-level (1+ x) (1+ y) serial))))
    grid))


(defun sum-square (grid x y size)
  (let ((sum 0))
    (dotimes (i size)
      (dotimes (j size)
        (incf sum (aref grid (+ y i) (+ x j)))))
    sum))

(defun find-largest-square (grid)
  (let ((x-size (array-dimension grid 1))
        (y-size (array-dimension grid 0))
        (max-sum (aref grid 0 0))
        max-coordinate
        max-size)
    (dotimes (size (min x-size y-size))
      (dotimes (y (- y-size size))
        (dotimes (x (- x-size size))
          (let ((sum (sum-square grid x y size)))
            (when (> sum max-sum)
              (setf max-sum sum
                    max-coordinate (list (1+ x) (1+ y))
                    max-size size))))))
    (values max-sum
            max-coordinate
            max-size)))


(defun solve-1 (serial)
  (let ((grid (create-grid 300 300 serial)))
    (find-largest-3-3-square grid)))

(defun solve-2 (serial)
  (let ((grid (create-grid 300 300 serial)))
    (find-largest-square grid)))
