(defpackage #:day-6
  (:use #:cl))

(in-package #:day-6)

(defparameter *input-file* "./day-6-input.txt")

(defun parse (line)
  (with-input-from-string (stream line)
    (list (prog1 (read stream) (read-char stream))
          (read stream))))

(defun read-input-file (stream)
  (loop :for line := (read-line stream nil)
        :while line
        :collect (parse line)))


(defun read-input ()
  (with-open-file (file *input-file*)
    (read-input-file file)))


(defun max-point (points min-x min-y)
  (loop :for point :in points
        :maximizing (first point) :into x
        :maximizing (second point) :into y
        :finally (return (list (+ 1 x (- min-x)) (+ 1 y (- min-y))))))

(defun distance (a b)
  (destructuring-bind ((a-x a-y) (b-x b-y))
      (list a b)
    (+ (abs (- a-x b-x))
       (abs (- a-y b-y)))))


(defun find-closest (point points)
  (let* ((sorted (sort (copy-list points) #'< :key (lambda (candidate)
                                                     (distance candidate point))))
         (first-dist (distance (first sorted) point))
         (second-dist (distance (second sorted) point)))
    (if  (/= first-dist second-dist)
         (copy-list  (first sorted)))))


(defun the-map (points)
  (let* ((min-x (apply #'min (mapcar #'first points)))
         (min-y (apply #'min (mapcar #'second points)))
         (dist-map (make-array (reverse (max-point points -50 -50)) :initial-element nil)))
    (loop :for x :below (array-dimension dist-map 1) :do
      (loop :for y :below (array-dimension dist-map 0) :do
        (setf (aref dist-map y x) (find-closest (list x y) points))))
    dist-map))

(defun total-distance (point points)
  (apply #'+ (mapcar (lambda (current)
                       (distance point current))
                     points)))

(defun closest-map (points max)
  (let* ((max-point (max-point points 0 0))
         (dist-map (make-array (reverse max-point) :initial-element nil)))
    (loop :for y :below (array-dimension dist-map 0) :do
      (loop :for x :below (array-dimension dist-map 1)
            :when (< (total-distance (list x y) points) max)
              :do (setf (aref dist-map y x) t)))
    dist-map))

(defun count-2d-array (item array &key (test #'eq))
  (let ((count 0))
    (dotimes (y (array-dimension array 0))
      (dotimes (x (array-dimension array 1))
        (when (funcall test item (aref array y x))
          (incf count))))
    count))

(defun remove-point (point dist-map)
  (loop :for x :below (array-dimension dist-map 0) :do
    (loop :for y :below (array-dimension dist-map 1) :do
      (when (equal point (aref dist-map x y))
        (setf (aref dist-map x y) nil))))
  dist-map)


(defun remove-edge (map)
  (let ((x-size (array-dimension map 1))
        (y-size (array-dimension map 0))
        (edge-nodes (make-hash-table :test 'equal)))
    (dolist (x (list 0 (1- x-size)))
      (dotimes (y y-size)
        (setf (gethash (aref map y x) edge-nodes) t)))
    (dolist (y (list 0 (1- y-size)))
      (dotimes (x x-size)
        (setf (gethash (aref map y x) edge-nodes) t)))
    (maphash (lambda (key value)
               (declare (ignore value))
               (remove-point key map))
             edge-nodes)))

(defun count-map (dist-map)
  (let ((hash (make-hash-table :test 'equal))
        collect)
    (dotimes (x (array-dimension dist-map 0))
      (dotimes (y (array-dimension dist-map 1))
        (incf (gethash (aref dist-map x y) hash 0))))
    (setf (gethash nil hash) 0)
    (maphash #'(lambda (key value) (push value collect)) hash)
    (apply #'max collect)))
