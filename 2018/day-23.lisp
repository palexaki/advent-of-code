(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :cl-smt-lib)
  (ql:quickload :advent-util))
(defpackage #:day-23
  (:use #:advent-util
        #:cl
        #:cl-smt-lib
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-23)

(named-readtables:in-readtable :cl-smt-lib)

(defparameter *example* '("pos=<10,12,12>,r=2"
                          "pos=<12,14,12>,r=2"
                          "pos=<16,12,12>,r=4"
                          "pos=<14,14,14>,r=6"
                          "pos=<50,50,50>,r=200"
                          "pos=<10,10,10>,r=5"))

(defparameter *z3* (make-smt "z3" "-in" "-smt2"))


(defun distance (a b)
  (reduce #'+
          (mapcar (alexandria:compose #'abs #'-) a b)))


(defun largest-range-nanobot (bots)
  (loop :with max-r := 0
        :and  max-bot := nil
        :for (x y z r) :in bots
        :when (> r max-r)
          :do (setf max-r r
                    max-bot (list x y z))
        :finally
          (return (values max-bot max-r))))

(defun solve-1 (&optional (input (day-input-lines 23 2018)))
  (let ((bots (mapcar #'ints input)))
    (multiple-value-bind (max-bot range)
        (largest-range-nanobot bots)
      (count-if (lambda (bot)
                  (<= (distance max-bot
                                (subseq bot 0 3))
                      range))
                bots))))


(defun box-intersection (box-1 box-2)
  (destructuring-bind ((a b) (s r))
      (list box-1 box-2)
    (list (mapcar #'max a s)
          (mapcar #'min b r))))

(defun volume (box)
  (destructuring-bind (a b)
      box
    (reduce #'* (mapcar (alexandria:compose (alexandria:curry #'abs 0) #'-)
                        b a))))

(defun boxes-intersect-p (box-1 box-2)
  (destructuring-bind (a b)
      (box-intersection box-1 box-2)

    (notany (alexandria:compose #'minusp #'-) b a)))


(defun solve-2 (&optional (input (day-input-lines 23 2018)) test)
  (let ((bots (mapcar #'ints input)))
    (write-to-smt (if test
                      t
                      *z3*)
                  #!`((reset)
                      (define-fun abs ((x Int)) Int
                        (ite (>= x 0) x (- x)))
                      (declare-const x Int)
                      (declare-const y Int)
                      (declare-const z Int)
                      (define-fun in-range ((bx Int) (by Int) (bz Int) (br Int)) Int
                        (ite (<= (+ (abs (- x bx))
                                    (abs (- y by))
                                    (abs (- z bz)))
                                br)
                            1 0))
                      (declare-const sum Int)
                      (declare-const dist Int)
                      (assert (= dist (+ (abs x) (abs y) (abs z))))
                      (assert (= sum (+ ,@(MAPCAR (LAMBDA (bot)
                                                    (CONS 'in-range bot))
                                                  BOTS))))
                      (maximize sum)
                      (minimize dist)
                      (check-sat)
                      (get-objectives)))
    (read *z3*)
    (read *z3*)))
