(defpackage #:day-14
  (:use #:cl))
(in-package #:day-14)

(defun make-start ()
  (make-array 2 :initial-contents '(3 7) :adjustable t :fill-pointer t))

(defparameter *start* (make-start))
(defparameter *times* 147061)

(defun circle-index-vector (array index)
  (let* ((dimension (length array)))
    (mod index dimension)))

(defun digits (num)
  (when (zerop num)
    (return-from digits (list 0)))
  (let (digits)
    (loop :while (not (zerop num))
          :do (push (mod num 10) digits)
              (setf num (floor num 10)))
    digits))

(defun print-scores (array elf1 elf2)
  (loop :for elt :across array
        :and i :from 0
        :do (cond ((= i elf1) (format t "(~a)" elt))
                  ((= i elf2) (format t "[~a]" elt))
                  (t (format t " ~a " elt))))
  (terpri))

(defun get-10-after (array index)
  (loop :repeat 10
        :for i :from index
        :collect (aref array i)))

(defun last-digits-include (array pattern)
  (let* ((dimension (length array))
         (odim (length pattern))
         (start (- dimension odim)))
    (if (minusp start)
        nil
        (loop :for i :from 0
              :for j :from start :below dimension
              :always (= (elt pattern i) (elt array j))))))

(defun solve-1 (times)
  (let ((start (make-start))
        (elf1 0)
        (elf2 1)
        (pattern (digits times)))
    (do ()
        ((= 1 2))
      ;(print-scores start elf1 elf2)
      (let* ((elf1-score (aref start elf1))
             (elf2-score (aref start elf2))
             (new-recipes (digits (+ elf1-score elf2-score))))
        (dolist (recipe new-recipes)
          (vector-push-extend recipe start)
          (when (last-digits-include start pattern)
            (return-from solve-1 (- (length start) (length pattern)))))

        (setf elf1 (circle-index-vector start (+ elf1 elf1-score 1))
              elf2 (circle-index-vector start (+ elf2 elf2-score 1)))))
    (values start
            elf1
            elf2)))
