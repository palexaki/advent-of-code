(defpackage #:day-10
  (:use #:cl #:cl-ppcre #:png #:alexandria
        #:zpng))

(in-package #:day-10)

(defparameter *position-regex* "position=<\\s*(-?\\d+),\\s*(-?\\d+)>")
(defparameter *velocity-regex* "velocity=<\\s*(-?\\d+),\\s*(-?\\d+)>")
(defparameter *input-file* #P"./day-10-input.txt")
(defparameter *example-file* #P"./day-10-ex.txt")

(defclass m-speed ()
  ((x :reader speed-x :initarg :x)
   (y :reader speed-y :initarg :y)))

(defun make-speed (x y)
  (make-instance 'm-speed :x x :y y))

(defmethod print-object ((obj m-speed) stream)
  (print-unreadable-object (obj stream :type t)
    (format stream "X: ~a, Y: ~a" (speed-x obj) (speed-y obj))))

(defun get-position (line)
  (map 'list #'parse-integer (nth-value 1 (scan-to-strings *position-regex* line))))

(defun get-velocity (line)
  (let ((vel (nth-value 1 (scan-to-strings *velocity-regex* line))))
    (make-speed (parse-integer (aref vel 0))
                (parse-integer (aref vel 1)))))

(defun move-lights (lights)
  (let ((new-lights (make-hash-table :test 'equal)))
    (maphash (lambda (pos value)
               (let ((x (first pos))
                     (y (second pos)))
                 (mapcar (lambda (light)
                           (push light
                                 (gethash (list (+ x (speed-x light))
                                                (+ y (speed-y light)))
                                          new-lights)))
                         value)))
             lights)
    new-lights)
  #|(let* ((dims (array-dimensions lights))
         (x-size (second dims))
         (y-size (first dims))
         (new-lights (make-array dims :initial-element nil)))
    (dotimes (y y-size)
      (dotimes (x x-size)
        (dolist (light (aref lights y x))
          (push light (aref new-lights
                            (+ y (speed-y light))
                            (+ x (speed-x light)))))))
    new-lights)|#)

(defun lights->image (lights filename)
  (multiple-value-bind (dimensions mins)
      (lights-dimension lights)
    (let* ((x-size (first dimensions))
           (y-size (second dimensions))
           (min-x (first mins))
           (min-y (second mins))
           (image (make-instance 'pixel-streamed-png
                                 :color-type :grayscale
                                 :height y-size
                                 :width x-size)))
      (with-open-file (stream filename
                              :direction :output
                              :if-exists :supersede
                              :if-does-not-exist :create
                              :element-type '(unsigned-byte 8))
        (start-png image stream)
        (dotimes (y y-size)
          (dotimes (x x-size)
            (if (gethash (list (+ min-x x) (+ min-y y)) lights)
                (write-pixel (list 255) image)
                (write-pixel (list 0) image))))
        (finish-png image)))))


(defun write-lights (lights &optional (second 0))
  (let ((filename (format nil "~a.png" second)))
    (lights->image lights filename)))

(defun lights-dimension (hash)
  (loop :for key :being :each :hash-key :in hash
        :maximizing (first key) :into max-x
        :maximizing (second key) :into max-y
        :minimizing (first key) :into min-x
        :minimizing (second key) :into min-y
        :finally (return (values (list (+ 2 (abs (- max-x min-x)))
                                       (+ 2 (abs (- max-y min-y))))
                                 (list min-x min-y)))))

(defun lines->lights (lines)
  (let ((lights-hash (make-hash-table :test 'equal)))
    (dolist (line lines)
      (push (get-velocity line) (gethash (get-position line) lights-hash)))
    lights-hash
    #|(multiple-value-bind (dimensions mins)
        (lights-dimension lights-hash)
      (let* ((y-size (second dimensions))
             (x-size (first dimensions))
             (min-x (first mins))
             (min-y (second mins))
             (lights (make-array (reverse dimensions) :initial-element nil)))
        (dotimes (y y-size)
          (dotimes (x x-size)
            (when-let (light (gethash (list (+ x min-x)
                                            (+ y min-y))
                                      lights-hash))
              (setf (aref lights y x)
                    (list light)))))
        lights))|#))

(defun read-lines (file)
  (with-open-file (stream file)
    (loop :for line := (read-line stream nil)
          :while line
          :collect line)))

(defun read-input (&optional (file *input-file*))
  (lines->lights (read-lines file)))

(defun lights-size (lights)
  (let* ((dimensions (lights-dimension lights))
         (x (first dimensions))
         (y (second dimensions)))
    (sqrt (+ (* x x) (* y y)))))

(defun solve (max &optional (file *input-file*))
  (loop :for lights := (read-input file) :then (move-lights lights)
        :and second :below max
        :when (< (lights-size lights) 1000)
          :do (write-lights lights second)))
