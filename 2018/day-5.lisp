(defpackage #:day-5
  (:use #:cl))

(in-package #:day-5)

(defvar *input* "./day-5-input.lisp")

(defun collapses-p (poly-a poly-b)
  (and (char/= poly-a poly-b)
       (char-equal poly-a poly-b)))

(defun remove-ind (list index)
  (remove t list :start index :test (lambda (&rest a)
                                      t)
                 :end (1+ index)))

(defun removed (indices list)
  (dolist (ind (reverse indices))
    (setf list (remove-ind list (1+ ind))
          list (remove-ind list ind)))
  list)

(defun process (polymer)
  (setf polymer (map 'list #'identity polymer))
  (let (removelist)
    (loop do
      (setf removelist nil)
      (loop with skip
            for i from 0
            for curr in polymer
            and next in (rest polymer)
            when (and (not skip)
                      (collapses-p curr next))
              do (push i removelist)
                 (setf skip t))
      (setf polymer (removed removelist polymer))
          while removelist))
  polymer)

(defun solve-1 ()
  (let ((input (with-open-file  (stream *input*)
                 (read-line stream))))
    (process input)))

(defun solve-2 ()
  (let ((input (with-open-file (stream *input*)
                 (read-line stream))))
    (loop :with  iterinput
          :for x :from 97 :upto 122 :do (setf iterinput (remove (code-char x) input :test #'char-equal))
          :minimizing (length (process iterinput)))))
