(defpackage #:day-1
  (:use #:cl))

(in-package #:day-1)
(defvar *input-file* #p"./day-1-input.txt")

(defun read-stream (stream)
  (loop for num = (read stream nil)
        while num
        collect num))

(defun solve-1 ()
  (with-open-file (file *input-file*)
    (apply #'+ (read-stream file))))

(defun solve-2 ()
  (let* ((changes (with-open-file (file *input-file*)
                   (read-stream file)))
         (changes-length (length changes))
         (changes (make-array changes-length :initial-contents changes))
         (current-frequency 0)
         (past-frequencies (make-hash-table)))
    (loop :for i := 0 :then (mod (1+ i) changes-length)
          :when (gethash current-frequency past-frequencies) :return current-frequency
            :end :do
          (setf (gethash current-frequency past-frequencies) t)
          (incf current-frequency (aref changes i)))))
