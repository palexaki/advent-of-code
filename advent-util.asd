(asdf/defsystem:defsystem #:advent-util
  :description "My utilities for advent of code"
  :author "Philippos Boon Alexaki"
  :serial t
  :depends-on (#:cl-ppcre #:drakma #:alexandria #:parseq #:iterate)
  :components ((:file "utils")))
