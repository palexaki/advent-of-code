(defpackage #:advent-util
  (:use #:cl #:cl-ppcre #:drakma #:alexandria)
  (:export #:day-input-file-ensured
           #:read-stream-into-lines
           #:day-input-lines
           #:make-pos
           #:pos-add
           #:pos-addf
           #:pos-sub
           #:position=
           #:pos-neighbors
           #:unsafe-pos-neighbors
           #:board-position
           #:in-board-p
           #:pos-x
           #:pos-y
           #:map-position
           #:shortest
           #:direction->delta
           #:next-direction
           #:prev-direction
           #:scan-groups
           #:dist-man
           #:dist-euc
           #:map-hash-collect
           #:lambda+
           #:parse-groups
           #:ints
           #:make-2d-array
           #:execute-process
           #:start-process
           #:parse-program
           #:add-operator
           #:process-input
           #:process-output
           #:process-relative-base
           #:process-pc
           #:process-program
           #:int
           #:safe-board-position))

(in-package :advent-util)

;
;;; Input for a day stuff

(defparameter *advent-folder* (ql:where-is-system :advent-util))
(defparameter *session* (read-file-into-string (merge-pathnames "cookie" *advent-folder*)))


(defun day-input-filename (day &optional (year 2021))
  (merge-pathnames (format nil "~a/input/~a" year day)
                   *advent-folder*))

(defun day-url (day &optional (year 2021))
  (format nil "https://adventofcode.com/~a/day/~a/input"
          year day))

(defun session-cookie-jar ()
  (let* ((cookie (make-instance 'cookie
                                :domain ".adventofcode.com"
                                :name "session"
                                :value *session*
                                :expires (+ 100000000 (get-universal-time))
                                :securep t
                                :http-only-p t)))
    (make-instance 'cookie-jar
                   :cookies (list cookie))))

(defun download-day-input (day &optional (year 2021))
  (http-request (day-url day year) :cookie-jar (session-cookie-jar)))

(defun day-input-file-ensured (day &optional (year 2021))
  (let ((filename (day-input-filename day year)))
    (unless (probe-file filename)
      (write-string-into-file (download-day-input day year)
                              filename))
    filename))

(defun read-stream-into-lines (stream)
  (loop :for line := (read-line stream nil)
        :while line
        :collect line))

(defun day-input-lines (day &optional (year 2021))
  (with-open-file (stream (day-input-file-ensured day year))
    (read-stream-into-lines stream)))


;
;;; position things
(defun make-pos (x y)
  (list x y))

(defun safe-board-position (board position)
  (when (in-board-p board position)
    (board-position board position)))

(defun board-position (board position)
  (apply #'aref board (reverse position)))

(defun (setf board-position) (value board position)
  (setf (apply #'aref board (reverse position)) value))

(defun pos-x (position)
  (first position))

(defun pos-y (position)
  (second position))

(defun board-size (board)
  (array-dimension board 0))

(defun position= (a b)
  (equal a b))

(defun in-board-p (board position)
  (and (< -1 (pos-x position) (array-dimension board 1))
       (< -1 (pos-y position) (array-dimension board 0))))

(defun pos-add (a b)
  (mapcar #'+ a b))

(define-modify-macro pos-addf (delta)
  pos-add)

(defun pos-sub (a b)
  (mapcar #'- a b))

(defun unsafe-pos-neighbors (position)
  (mapcar (lambda (delta) (pos-add position delta))
          '((0 1) (0 -1)
            (1 0) (-1 0))))

(defun pos-neighbors (board position)
  (remove-if-not (lambda (pos)
                   (in-board-p board pos))
                 (unsafe-pos-neighbors position)))


(defun direction->delta (direction)
  (case direction
    (:right '(1 0))
    (:left '(-1 0))
    (:up '(0 -1))
    (:down '(0 1))))

(defun next-direction (direction)
  (case direction
    (:right :down)
    (:down :left)
    (:left :up)
    (:up :right)))

(defun prev-direction (direction)
  (next-direction (next-direction (next-direction direction))))

(defun map-position (map position)
  (gethash position map nil))

(defun (setf map-position) (value map position)
  (setf (gethash position map) value))

(defmacro defdist (name (combiner inside))
  (with-gensyms (a b)
    `(defun ,name (,a &optional (,b (make-pos 0 0)))
       (funcall ,combiner
                (funcall ,inside (- (pos-x ,a) (pos-x ,b)))
                (funcall ,inside (- (pos-y ,a) (pos-y ,b)))))))

(defdist dist-man (#'+ #'abs))

(defdist dist-euc ((lambda (xs ys)
                     (sqrt (+ xs ys)))
                   (lambda (n)
                     (* n n))))

(defun shortest (a b)
  (if (< (dist-man a)
         (dist-man b))
      a
      b))

;
;;; General useful stuff

(defmacro map-hash-collect (collector hash (k v) &body body)
  `(let (,collector)
     (maphash (lambda (,k ,v)
                (declare (ignorable ,k ,v))
                ,@body)
              ,hash)
     ,(if (listp collector) (car collector) collector)))


(defmacro lambda+ ((&rest lambda-lists) &body body)
  (with-gensyms (args)
    `(lambda (&rest ,args)
       (destructuring-bind (,@lambda-lists)
           ,args
         ,@body))))

(defun scan-groups (regex target-string &key
                                          (start 0)
                                          (end (length target-string))
                                          sharedp)
  (nth-value 1 (scan-to-strings regex target-string
                                :start start
                                :end end
                                :sharedp sharedp)))

(defun parse-groups (regex target-string &rest functions)
  (let ((groups (scan-groups regex target-string)))
    (map 'list (lambda (group function)
                 (funcall function group))
         groups
         functions)))

(defun ints (string &optional only-positive)
  (flet ((int-pos (string &optional (start 0))
           (position-if (if only-positive
                            #'digit-char-p
                            (disjoin #'digit-char-p
                                     (lambda (char)
                                       (char= char #\-))))
                        string :start start)))
    (loop :for pos := (int-pos string) :then (int-pos string pos)
          :while (and pos
                      (< pos (length string)))
          :if (multiple-value-bind (integer position)
                  (parse-integer string :start pos :junk-allowed t)
                (setf pos position)
                integer)
            :collect :it
          :while (and pos
                      (< pos (length string))))))

(defun make-2d-array (contents)
  (make-array (list (length contents)
                    (length (first contents)))
              :initial-contents contents))

;
;;; The computer of 2019


(defvar *relative-base* 0)

(defstruct (operation (:conc-name op-))
  function
  args-amount
  result-offset
  move
  jump)

(defun digit (number n &optional (base 10))
  (mod (floor number (expt base n)) base))

(defun parameter-mode (instruction parameter)
  (digit instruction (+ 2 parameter)))

(defun parse-instruction (instruction)
  (let ((opcode (mod instruction 100))
        (c (digit instruction 2))
        (b (digit instruction 3))
        (a (digit instruction 4)))
    (values opcode
            (vector c b a))))

(defun get-opcode (instruction)
  (mod instruction 100))

(defparameter *max-opcode-size* 99)
(declaim (type fixnum *max-opcode-size*))
(defun operator-array (&optional (size *max-opcode-size*))
  (make-array (1+ size) :initial-element nil))
(defparameter *operators* (operator-array))


(defun add-operator (process opcode function args-amount
                     result-offset move &optional jump)
  (setf (aref (process-ops process) opcode) (make-operation :function function
                                                            :args-amount args-amount
                                                            :result-offset result-offset
                                                            :move move
                                                            :jump jump)))

(defstruct process
  program
  (relative-base 0)
  input
  output
  (ops (operator-array))
  (pc 0))

(defun adjustable-program (program-data)
  (make-array (length program-data) :adjustable t :initial-contents program-data))

(defun start-process (program-data &optional input)
  (let ((process (make-process :program (adjustable-program program-data)
                               :input input)))
    (set-to-standard-operators process)
    process))

(defun set-to-standard-operators (process)
  (add-operator process 1 #'+ 2 3 4)
  (add-operator process 2 #'* 2 3 4)
  (add-operator process 3 (lambda ()
                            (pop (process-input process)))
                0 1 2)
  (add-operator process 4 (lambda (out)
                            (push out (process-output process)))
                1 nil 2)
  (add-operator process 5 (complement #'zerop) 2 nil 3 t)
  (add-operator process 6 #'zerop 2 nil 3 t)
  (add-operator process 7 (lambda (a b)
                    (if (< a b) 1 0))
                2 3 4)
  (add-operator process 8 (lambda (a b)
                    (if (= a b) 1 0))
                2 3 4)
  (add-operator process 9 (lambda (a)
                    (incf (process-relative-base process) a))
                1 nil 2)
  (values))

(defun op-info (process opcode)
  (assert (<= 0 opcode *max-opcode-size*))
  (aref (process-ops process) opcode))

(defun ensure-pos (program pos)
  (when (<= (length program) pos)
    (adjust-array program (+ 100 pos))))

(defun adjust-aref (program pos)
  (ensure-pos program pos)
  (aref program pos))

(defun (setf adjust-aref) (value program pos)
  (ensure-pos program pos)
  (setf (aref program pos) value))

(defun argument-values (process arg-amount instruction)
  (with-slots (program pc relative-base) process
      (loop :repeat arg-amount
            :for x :from (1+ pc)
            :for parameter :from 0
            :for parameter-value := (aref program (+ pc parameter 1))
            :collect (case (parameter-mode instruction parameter)
                       (0 (adjust-aref program parameter-value))
                       (1 parameter-value)
                       (2 (adjust-aref program (+ relative-base
                                                  parameter-value)))))))

(defun execute-op (process)
  (with-slots (pc program relative-base) process
    (let* ((instruction (aref program pc))
           (opcode (get-opcode instruction)))
      (with-slots (function args-amount
                   result-offset move
                   jump) (op-info process opcode)
        (let ((vals (argument-values process args-amount instruction)))
          (if jump
              (destructuring-bind (test pos) vals
                (if (funcall function test)
                    (setf pc pos)
                    (incf pc move)))
              (let ((result (apply function vals)))
                (when result-offset
                  (let* ((result-parameter
                           (aref program (+ pc result-offset)))
                         (result-mode (parameter-mode instruction args-amount))
                         (result-position (+ (ecase result-mode
                                               (0 0)
                                               (2 relative-base))
                                             result-parameter)))
                    (setf (adjust-aref program result-position)
                          result)))
                (incf pc move))))))))


(defun execute-process (process)
  (with-slots (pc program) process
    (loop :for opcode := (aref program pc)
          :while (/= opcode *max-opcode-size*)
          :do (execute-op process)
          :finally (return (process-output process)))))

(defun parse-program (input)
  (let ((split (cl-ppcre:split "," input)))
    (map 'vector #'parse-integer split)))

;
;;; Parseq convenience rules

(parseq:defrule int ()
    (+ digit)
  (:string)
  (:lambda (x) (parse-integer x)))
