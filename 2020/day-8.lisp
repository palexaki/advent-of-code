(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :parseq)
  (ql:quickload :advent-util))
(defpackage #:day-8
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-8)


(defparameter *example* '("nop +0"
                          "acc +1"
                          "jmp +4"
                          "acc +3"
                          "jmp -3"
                          "acc -99"
                          "acc +1"
                          "jmp -4"
                          "acc +6"))

(defun parse-instruction (line)
  (list (intern (string-upcase (subseq line 0 3)) :keyword)
        (parse-integer line :start 4)))

(defun parse-input (input)
  (map 'vector #'parse-instruction input))


(defun solve-1 (&optional (input (day-input-lines 8 2020)))
  (loop :with program := (parse-input input)
        :and  pointer := 0
        :and  acc     := 0
        :for  instruction := (aref program pointer)
        ;:do (print (list pointer acc))
        :until (member pointer pointers)
        :collect pointer :into pointers
        ;:collect acc     :into accumulators
        :do (case (first instruction)
              (:acc (incf acc (second instruction)))
              (:jmp (incf pointer (1- (second instruction))))
              (:nop))
            (incf pointer)
        :finally
           (return acc)))

(defun run-normally (program)
  (loop :with pointer := 0
        :and  acc     := 0
        :for  instruction := (aref program pointer)
        :when (member pointer pointers)
          :return nil
        :collect pointer :into pointers
        :do (case (first instruction)
              (:acc (incf acc (second instruction)))
              (:jmp (incf pointer (1- (second instruction))))
              (:nop))
            (incf pointer)
        :until (= (length program) pointer)
        :finally
           (return acc)))

(defun run-replaced (program pos)
  (destructuring-bind (instruction argument)
      (aref program pos)
    (when
        (case instruction
          (:jmp (setf (aref program pos) (list :nop argument)))
          (:nop (setf (aref program pos) (list :jmp argument))))
      (prog1
          (run-normally program)
        (setf (aref program pos) (list instruction argument))))))

(defun solve-2 (&optional (input (day-input-lines 8 2020)))
  (loop :with program := (parse-input input)
        :for x :from 0 :below (length program)
        :when (run-replaced program x)
          :return :it))
