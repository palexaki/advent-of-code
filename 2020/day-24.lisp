(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :fset)
  (ql:quickload :parseq)
  (ql:quickload :advent-util))
(defpackage #:day-24
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-24)

(defparameter *example* '("sesenwnenenewseeswwswswwnenewsewsw"
                          "neeenesenwnwwswnenewnwwsewnenwseswesw"
                          "seswneswswsenwwnwse"
                          "nwnwneseeswswnenewneswwnewseswneseene"
                          "swweswneswnenwsewnwneneseenw"
                          "eesenwseswswnenwswnwnwsewwnwsene"
                          "sewnenenenesenwsewnenwwwse"
                          "wenwwweseeeweswwwnwwe"
                          "wsweesenenewnwwnwsenewsenwwsesesenwne"
                          "neeswseenwwswnwswswnw"
                          "nenwswwsewswnenenewsenwsenwnesesenew"
                          "enewnwewneswsewnwswenweswnenwsenwsw"
                          "sweneswneswneneenwnewenewwneswswnese"
                          "swwesenesewenwneswnwwneseswwne"
                          "enesenwswwswneneswsenwnewswseenwsese"
                          "wnwnesenesenenwwnenwsewesewsesesew"
                          "nenewswnwewswnenesenwnesewesw"
                          "eneswnwswnwsenenwnwnwwseeswneewsenese"
                          "neswnwewnwnwseenwseesewsenwsweewe"
                          "wseweeenwnesenwwwswnew"))


(defparameter *neighbours* '((:e -1 0)
                           (:se -1 1)
                           (:sw 0 1)
                           (:w 1 0)
                           (:nw 1 -1)
                           (:ne 0 -1)))

(defun direction-neighbour (direction)
  (cdr (assoc direction *neighbours*)))

(defun tile-neighbours (tile)
  (mapcar (lambda (neighbour)
            (pos-add tile (cdr neighbour)))
          *neighbours*))

(parseq:defrule tiles ()
    (+ (string steps))
  (:lambda (&rest steps) (mapcar #'first steps)))

(parseq:defrule steps ()
    (+ step))

(parseq:defrule step ()
    (or "e" "se" "sw" "w" "ne" "nw")
  (:lambda (step) (intern (string-upcase step) :keyword)))

(defun tiles-set (tiles)
  (fset:filter-pairs (lambda (item amount)
                       (declare (ignore item))
                       (oddp amount))
                     (fset:convert 'fset:bag (mapcar (lambda (tile)
                                                       (reduce #'pos-add (mapcar #'direction-neighbour tile)))
                                                     tiles))))


(defun solve-1 (&optional (input (day-input-lines 24 2020)))
  (let ((tiles (parseq:parseq 'tiles input)))
    (fset:set-size (tiles-set tiles))))

(defun count-neighbours (tile floor)
  (count-if (lambda (neighbour)
              (gethash neighbour floor))
            (tile-neighbours tile)))

(defun step-flipping (floor)
  (let ((next-floor (make-hash-table :test 'equal)))
    (loop :for tile :being :the :hash-key :of floor :using (:hash-value black)
          :for neighbour-count := (count-neighbours tile floor)
          :when (or (and black (<= 1 neighbour-count 2))
                    (and (not black) (= neighbour-count 2)))
            :do (setf (gethash tile next-floor) t)
                (dolist (neighbour (tile-neighbours tile))
                  (setf (gethash neighbour next-floor) (gethash neighbour next-floor))))
    next-floor))

(defun count-black-tiles (floor)
  (loop :for val :being :the :hash-value :of floor
        :count val))

(defun solve-2 (&optional (input (day-input-lines 24 2020)))
  (let ((map (make-hash-table :test 'equal)))
    (fset:do-bag-pairs (tile ig (tiles-set (parseq:parseq 'tiles input)))
      (declare (ignore ig))
      (setf (gethash tile map) t)
      (dolist (neighbour (tile-neighbours tile))
        (setf (gethash neighbour map) (gethash neighbour map))))
    (loop :repeat 101
          :for floor := map :then (step-flipping floor)
          :for iter :from 0
          :finally (return (count-black-tiles floor)))))
