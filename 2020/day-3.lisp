(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-3
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-3)

(defun circ-board-position (board position)
  (board-position board (make-pos (mod (pos-x position)
                                       (array-dimension board 1))
                                  (pos-y position))))

(defun parse-input (input)
  (make-array (list (length input)
                    (length (first input)))
              :initial-contents input))

(defun traverse-slope-count-trees (map slope)
  (loop :for position := (make-pos 0 0) :then (pos-add position slope)
        :while (< (pos-y position) (array-dimension map 0))
        :counting (char= #\# (circ-board-position map position))))

(defun solve-1 (&optional (input (day-input-lines 3 2020)))
  (traverse-slope-count-trees (parse-input input)
                              '(3 1)))

(defun solve-2 (&optional (input (day-input-lines 3 2020)))
  (let ((map (parse-input input)))
    (reduce #'* (mapcar (alexandria:curry #'traverse-slope-count-trees map)
                        '((1 1) (3 1)
                          (5 1) (7 1)
                          (1 2))))))
