(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :parseq)
  (ql:quickload :advent-util))
(defpackage #:day-7
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-7)

(parseq:defrule rules ()
    (+ (string rule))
  (:let (bag-map (make-hash-table :test 'equal)))
  (:lambda (&rest rules)
    (list
     (mapcar #'first rules)
     bag-map)))

(parseq:defrule rule ()
    (and bag
         " contain"
         (or
          (+ bag-amount)
          no-other)
         #\.)
  (:choose 0 2)
  (:lambda (bag bag-amounts) (list* bag bag-amounts)))

(parseq:defrule no-other ()
    " no other bags"
  (:constant '()))

(parseq:defrule bag-amount ()
    (and " " digit " " bag (? #\,))
  (:choose 1 3)
  (:lambda (amount bag) (list bag (digit-char-p amount))))


(parseq:defrule bag ()
    (and (+ (char "a-z"))
         " "
         (+ (char "a-z"))
         " bag"
         (? #\s))
  (:choose 0 1 2)
  (:string)
   (:external bag-map)
   (:lambda (bag) (if (gethash bag bag-map)
                      (gethash bag bag-map)
                      (setf (gethash bag bag-map) bag))))

(defparameter *example* '("light red bags contain 1 bright white bag, 2 muted yellow bags."
                          "dark orange bags contain 3 bright white bags, 4 muted yellow bags."
                          "bright white bags contain 1 shiny gold bag."
                          "muted yellow bags contain 2 shiny gold bags, 9 faded blue bags."
                          "shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags."
                          "dark olive bags contain 3 faded blue bags, 4 dotted black bags."
                          "vibrant plum bags contain 5 faded blue bags, 6 dotted black bags."
                          "faded blue bags contain no other bags."
                          "dotted black bags contain no other bags."))

(defparameter *example-2* '("shiny gold bags contain 2 dark red bags."
                            "dark red bags contain 2 dark orange bags."
                            "dark orange bags contain 2 dark yellow bags."
                            "dark yellow bags contain 2 dark green bags."
                            "dark green bags contain 2 dark blue bags."
                            "dark blue bags contain 2 dark violet bags."
                            "dark violet bags contain no other bags."))

(defun tree-size (node child-finder)
  (cond ((null node) 0)
        (t (reduce #'+ (mapcar (lambda (child)
                                 (tree-size child child-finder))
                               (funcall child-finder node))
                   :initial-value 1))))

(defun tree-children (node child-finder)
  (let ((children (funcall child-finder node)))
    (cond ((null node) nil)
          ((null children) (list node))
          (t  (reduce #'append (mapcar (lambda (child)
                                         (tree-children child child-finder))
                                       children)
                      :initial-value children)))))


(defun solve-1 (&optional (input (day-input-lines 7 2020)))
  (destructuring-bind (tree hash)
      (parseq:parseq 'rules input)
    (flet ((children (bag)
             (mapcar #'first (remove-if-not (lambda (rule)
                                              (assoc bag (rest rule)))
                                            tree))))
      (length (remove-duplicates (tree-children (gethash "shiny gold" hash) #'children))))))


(defun tree-size-weighted (node rules)
  (reduce #'+ (mapcar (lambda+ ((bag amount))
                        (* amount (tree-size-weighted bag rules)))
                      (rest (assoc node rules)))
          :initial-value 1))

(defun solve-2 (&optional (input (day-input-lines 7 2020)))
  (destructuring-bind (tree hash)
      (parseq:parseq 'rules input)
    (1- (tree-size-weighted (gethash "shiny gold" hash) tree))))
