(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :alexandria)
  (ql:quickload :advent-util))
(defpackage #:day-17
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-17)

(defparameter *example* '(".#."
                          "..#"
                          "###"))

(defun neighbours (point)
  (apply #'alexandria:map-product
         (lambda (&rest delta)
           (mapcar #'+ point delta))
         (loop :for coor :in point
               :collect '(-1 0 1))))

(defun initialize-cubes (input &optional (dimensions 3))
  (loop :with dimension := (make-hash-table :test 'equal)
        :for y :from 0
        :for row :in input
        :do (loop :for x :from 0
                  :for cube :across row
                  :for point := (apply #'list x y (loop :repeat (- dimensions 2)
                                                        :collect 0))
                  :when (char= cube #\#)
                    :do (loop :initially (setf (gethash point dimension) t)
                              :for neighbour :in (neighbours point)
                              :do (setf (gethash neighbour dimension) (gethash neighbour dimension))))
        :finally (return dimension)))

(defun count-neighbours (dimension point)
  (count-if (lambda (neighbour)
              (and (not (equal point neighbour))
                   (gethash neighbour dimension)))
            (neighbours point)))

(defun step-cycle (dimension)
  (loop :with next-dimension := (make-hash-table :test 'equal)
        :for point :being :the :hash-key :of dimension :using (:hash-value active)
        :for count := (count-neighbours dimension point)
        :when (or (and active (<= 2 count 3))
                  (and (not active) (= count 3)))
          :do (loop :initially (setf (gethash point next-dimension) t)
                    :for neighbour :in (neighbours point)
                    :do (setf (gethash neighbour next-dimension) (gethash neighbour next-dimension)))
        :finally (return next-dimension)))

(defun count-active (dimension)
  (loop :for active :being :the :hash-value :of dimension
        :count active))


(defun solve-1 (&optional (input (day-input-lines 17 2020)))
  (loop :repeat 7
        :for dimension := (initialize-cubes input) :then (step-cycle dimension)
        :do (print (count-active dimension))
        :finally (return (count-active dimension))))

(defun solve-2 (&optional (input (day-input-lines 17 2020)))
  (loop :repeat 7
        :for dimension := (initialize-cubes input 4) :then (step-cycle dimension)
        :do (print (count-active dimension))
        :finally (return (count-active dimension))))

(defun bbox-dimension (dimension)
  (loop :for (x y z) :being :the :hash-key :of dimension :using (:hash-value active)
        :when active
          :minimizing x :into min-x
          :and :minimizing y :into min-y
          :and :minimizing z :into min-z
          :and :maximizing x :into max-x
          :and :maximizing y :into max-y
          :and :maximizing z :into max-z
        :finally (return (list min-x min-y min-z max-x max-y max-z))))

(defun print-dimension (dimension &optional (destination t))
  (loop :with (min-x min-y min-z max-x max-y max-z) := (bbox-dimension dimension)
        :for z :from min-z :upto max-z
        :do (format destination "~%z=~a~%" z)
           (loop :for y :from min-y :upto max-y
                  :do (loop :for x :from min-x :upto max-x
                            :if (gethash (list x y z) dimension)
                              :do (format destination "#")
                            :else
                              :do (format destination "."))
                      (format destination "~%")))
  dimension)
