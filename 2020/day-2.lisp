(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))

(defpackage #:day-2
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        #:alexandria
        ))

(in-package #:day-2)

(defparameter *regex* "(\\d+)-(\\d+) (.): (.+)")

(defun parse-password (pass)
  (parse-groups *regex* pass
                #'parse-integer #'parse-integer
                #'character #'identity))

(defun parse-input (input)
  (mapcar (lambda (string)
            (parse-password string))
          input))


(defun valid-password-p (min max char pass)
  (<= min (count char pass) max))

(defun valid-password-2-p (a b char pass)
  (xor (char= char (char pass (1- a)))
       (char= char (char pass (1- b)))))

(defun count-valid-according (records policy)
  (count-if (lambda (record)
              (apply policy record))
            records))

(defun solve-1 (&optional (input (day-input-lines 2 2020)))
  (count-valid-according (parse-input input) #'valid-password-p))

(defun solve-2 (&optional (input (day-input-lines 2 2020)))
  (count-valid-according (parse-input input) #'valid-password-2-p))
