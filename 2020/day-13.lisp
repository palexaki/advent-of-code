(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))

(defpackage #:day-13
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-13)


(defun parse-input (input)
  (list (parse-integer (first input))
        (mapcar (lambda (id)
                  (parse-integer id :junk-allowed t))
                (split-sequence:split-sequence #\, (second input)))))

(defun nearest-departure (start repeat)
  (- (* repeat (1+ (floor start repeat))) start))


(defun find-nearest-departure (start ids &optional (nearest-departure most-positive-fixnum) (nearest-id 0))
  (if (endp ids)
      (values nearest-departure nearest-id)
      (let ((cur-nearest (nearest-departure start (first ids))))
        (find-nearest-departure start (rest ids) (min nearest-departure cur-nearest)
                                (if (< cur-nearest nearest-departure)
                                    (first ids)
                                    nearest-id)))))

(defun solve-1 (&optional (input (day-input-lines 13 2020)))
  (destructuring-bind (start ids)
      (parse-input input)
    (find-nearest-departure start (remove nil ids))))

(defun ids-rems (ids)
  (loop :for id :in ids
        :and index :from 0
        :when id
          :collect (cons id (mod (- id index) id))))

(defun bezout (a b)
  (loop :for quotient := 0 :then (truncate old-r r)
        :for old-s := 1 :then s
        :and s := 0 :then (- old-s (* quotient s))
        :and old-r := a :then r
        :and r := b :then (- old-r (* quotient r))
        :while (/= r 0)
        :finally (return (list old-s (truncate (- old-r (* old-s a))
                                               b)))))


(defun chinese-remainder (pair-1 pair-2)
  (destructuring-bind ((n1 . a1) (n2 . a2))
      (list pair-1 pair-2)
    (destructuring-bind (m1 m2) (bezout n1 n2)
      (let ((N (* n1 n2))
            (x (+ (* n1 m1 a2) (* n2 m2 a1))))
        (cons N (mod x N))))))

(defun solve-2 (&optional (input (day-input-lines 13 2020)))
  (destructuring-bind (start ids)
      (parse-input input)
    (declare (ignore start))
    (cdr (reduce #'chinese-remainder (ids-rems ids)))))
