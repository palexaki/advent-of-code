(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-5
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-5)


(defun decode (sequence)
  (let ((hi 127)
        (lo 0)
        (ri 7)
        (le 0))
    (dotimes (i 7)
      (case (char sequence i)
        (#\F (setf hi (floor (+ hi lo) 2)))
        (#\B (setf lo (1+ (floor (+ hi lo) 2))))))
    (dotimes (i 3)
      (case (char sequence (+ i 7))
        (#\L (setf ri (floor (+ ri le) 2)))
        (#\R (setf le (1+ (floor (+ ri le) 2))))))
    (list hi ri)))

(defun seat-id (seat)
  (+ (* 8 (first seat)) (second seat)))

(defun solve-1 (&optional (input (day-input-lines 5 2020)))
  (reduce #'max
          (mapcar (alexandria:compose #'seat-id #'decode) input)))


(defun find-seat (seat-ids)
  (if (= (- (second seat-ids) (first seat-ids))
         2)
      (1+ (first seat-ids))
      (find-seat (rest seat-ids))))

(defun solve-2 (&optional (input (day-input-lines 5 2020)))
  (let ((list (sort (mapcar (alexandria:compose #'seat-id #'decode) input)
                    #'<)))
    (find-seat list)))
