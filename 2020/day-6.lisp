(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :parseq)
  (ql:quickload :fset)
  (ql:quickload :advent-util))
(defpackage #:day-6
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-6)


(defparameter *alphabet* "abcdefghijklmnopqrstuvwxyz")


(defun solve-1 (&optional (input (day-input-lines 6 2020)))
  (reduce #'+ (mapcar  (alexandria:compose #'length
                                             #'remove-duplicates
                                             (lambda (group)
                                               (mapcar (lambda (str)
                                                         (char str 0))
                                                       (mapcan
                                                        (alexandria:curry #'cl-ppcre:split "\\s*")
                                                        group))))
                      (split-sequence:split-sequence "" input :test #'equal))))

(defun solve-2 (&optional (input (day-input-lines 6 2020)))
  (let ((groups (mapcar (lambda (group)
                          (mapcar (alexandria:compose (lambda (grp) (mapcar (lambda (str) (char str 0))
                                                                            grp))
                                                      (alexandria:curry #'cl-ppcre:split "\\s*"))
                                  group))
                        (split-sequence:split-sequence "" input :test #'equal))))
    (reduce #'+ (mapcar (lambda (group)
                          (count-if (lambda (char)
                                      (every (alexandria:curry #'member char)
                                             group))
                                    *alphabet*))
                        groups))))

(parseq:defrule everyone ()
    (+ (and group (? "")))
  (:lambda (&rest group)
    (mapcar #'first group)))

(parseq:defrule group ()
    (+ (string person))
  (:flatten))

(parseq:defrule person ()
    (+ (char "a-z"))
  (:lambda (&rest chars)
    (fset:convert 'fset:set chars)))


(defun solve-with-parse (&optional (input (day-input-lines 6 2020)))
  (flet ((count-sum (requirement groups)
           (reduce #'+ (mapcar (alexandria:compose #'fset:size (lambda (group)
                                                                 (reduce requirement group)))
                               groups))))
    (let ((parsed (parseq:parseq 'everyone input)))
      (list
       :day-1
       (count-sum #'fset:union parsed)
       :day-2
       (count-sum #'fset:intersection parsed)))))
