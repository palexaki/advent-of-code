(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-25
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-25)

(defparameter +div-value+ 20201227)

(defparameter *example* '("5764801" "17807724"))

(defun encrypt-till (num goal)
  (loop :for i :from 1
        :for n := num :then (mod (* num n) +div-value+)
        :when (= n goal)
          :return i))

(defun encrypt (subject loop-size)
  (loop :repeat loop-size
        :for x := subject :then (mod (* x subject) +div-value+)
        :finally (return x)))


(defun solve-1 (&optional (input (day-input-lines 25 2020)))
  (destructuring-bind (card-key door-key)
      (mapcar #'parse-integer input)
    (let ((card-loop (encrypt-till 7 card-key)))
      (encrypt door-key card-loop))))
