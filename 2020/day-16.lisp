(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :parseq)
  (ql:quickload :advent-util))
(defpackage #:day-16
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-16)



(defparameter *example* '("class: 1-3 or 5-7" "row: 6-11 or 33-44"
                          "seat: 13-40 or 45-50" "" "your ticket:" "7,1,14" ""
                          "nearby tickets:" "7,3,47" "40,4,50" "55,2,20"
                          "38,6,12"))

(defparameter *example-2* '("class: 0-1 or 4-19"
                            "row: 0-5 or 8-19"
                            "seat: 0-13 or 16-19"
                            ""
                            "your ticket:"
                            "11,12,13"
                            ""
                            "nearby tickets:"
                            "3,9,18"
                            "15,1,5"
                            "5,14,9"))

(parseq:defrule reference ()
    (and rules "" my-ticket "" nearby-tickets)
  (:choose 0 2 4))

(parseq:defrule rules ()
    (+ (string rule))
  (:lambda (&rest rules) (mapcar #'first rules)))

(parseq:defrule rule ()
    (and rule-name ": " my-number "-" my-number " or " my-number "-" my-number)
  (:choose 0 2 4 6 8))

(parseq:defrule rule-name ()
    (+ (char "a-z "))
  (:string)
  (:lambda (name) (intern (substitute #\- #\Space (string-upcase name)) :keyword)))

(parseq:defrule my-ticket ()
    (and "your ticket:" (string csv))
  (:choose '(1 0)))

(parseq:defrule nearby-tickets ()
    (and "nearby tickets:" (+ (string csv)))
  (:choose 1)
  (:lambda (&rest tickets) (mapcar #'first tickets)))

(parseq:defrule csv ()
    (+ (and my-number (? (char ","))))
  (:lambda (&rest vals) (mapcar #'first vals)))


(parseq:defrule my-number ()
    (+ digit)
  (:string)
  (:function 'parse-integer))

(defun number-valid (number rule)
  (destructuring-bind (a b c d)
      (rest rule)
    (or
     (<= a number b)
     (<= c number d))))

(defun number-any (number rules)
  (some (lambda (rule)
          (number-valid number rule))
        rules))

(defun ticket-valid (ticket rules)
  (every (lambda (number)
          (number-any number rules))
        ticket))

(defun error-rate (tickets rules &optional (acc 0))
  (if (endp tickets)
      acc
      (error-rate (rest tickets) rules (reduce #'+ (remove-if (lambda (number)
                                                                (number-any number rules))
                                                              (first tickets))
                                               :initial-value acc))))



(defun solve-1 (&optional (input (day-input-lines 16 2020)))
  (let ((document (parseq:parseq 'reference input)))
    (error-rate (third document) (first document))))


(defun reshape-to-columns (rows)
  (labels ((shaper (rows columns)
             (if (endp rows)
                 (mapcar #'reverse columns)
                 (shaper (rest rows)
                         (mapcar (lambda (el column)
                                   (list* el column))
                                 (first rows)
                                 columns)))))
    (shaper rows (make-list (length (first rows))))))

(defun field-order-prelim (columns fields)
  (mapcar (lambda (column)
            (remove-if-not (lambda (rule)
                             (every (lambda (number)
                                      (number-valid number rule))
                                    column))
                           fields))
          columns))

(defun field-order (columns fields)
  (loop :with possible := (field-order-prelim columns fields)
        :with order := (make-array (length possible))
        :while (some #'identity possible)
        :do (let* ((index (position 1 possible :key 'length))
                   (field (first (nth index possible))))
              (setf (aref order index) (first field)
                    possible (mapcar (lambda (fields)
                                       (remove (car field) fields :key #'first))
                                     possible)))
        :finally (return order)))

(defun valid-tickets (tickets rules)
  (remove-if-not (lambda (ticket)
                   (ticket-valid ticket rules))
                 tickets))

(defun solve-2 (&optional (input (day-input-lines 16 2020)))
  (destructuring-bind (rules my-ticket nearby-tickets)
      (parseq:parseq 'reference input)
    (reduce #'* (loop :for val :in my-ticket
                      :for field :across (field-order (reshape-to-columns (valid-tickets nearby-tickets rules))
                                                      rules)
                      :when (string= "DEPARTURE" (string field) :end2 (min (length (string field)) 9))
                        :collect val))))
