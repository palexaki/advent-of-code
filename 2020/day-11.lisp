(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :png)
  (ql:quickload :advent-util))
(defpackage #:day-11
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-11)

(defparameter *example* '("L.LL.LL.LL"
                          "LLLLLLL.LL"
                          "L.L.L..L.."
                          "LLLL.LL.LL"
                          "L.LL.LL.LL"
                          "L.LLLLL.LL"
                          "..L.L....."
                          "LLLLLLLLLL"
                          "L.LLLLLL.L"
                          "L.LLLLL.LL"))


(defparameter *directions* '((0 1)  (-1 1)  (1 1)
                             (0 -1) (-1 -1) (1 -1)
                             (1 0)
                             (-1 0)))


(defun unsafe-pos-adjacent (position)
  (mapcar (lambda (delta) (pos-add position delta))
          '((0 1)  (-1 1)  (1 1)
            (0 -1) (-1 -1) (1 -1)
            (1 0)
            (-1 0))))

(defun adjacent (board position)
  (remove-if-not (lambda (pos)
                   (in-board-p board pos))
                 (unsafe-pos-adjacent position)))

(defparameter *converter* '((#\# . :occupied) (#\. . :floor) (#\L . :empty)))

(defun char->type (char)
  (cdr (assoc char *converter*)))

(defun type->char (type)
  (car (rassoc type *converter*)))

(defun seat-p (type)
  (find type '(:occupied :empty)))

(defun occupied-adjacent (seats position)
  (count-if (lambda (position)
              (eq :occupied (board-position seats position)))
            (adjacent seats position)))

(defun seats-round (seats &optional (occupied-function #'occupied-adjacent) (trigger 4))
  (let ((next (alexandria:copy-array seats)))
    (dotimes (x (array-dimension seats 1))
      (dotimes (y (array-dimension seats 0))
        (let* ((pos (make-pos x y))
               (cur (board-position seats pos)))
          (when (seat-p cur)
            (let ((occupied-adjacent (funcall occupied-function seats pos)))
              (cond ((and (eq cur :occupied)
                          (>= occupied-adjacent trigger))
                     (setf (board-position next pos) :empty))
                    ((and (eq cur :empty)
                          (= occupied-adjacent 0))
                     (setf (board-position next pos) :occupied))))))))
    next))

(defun parse-input (input)
  (make-array (list (length input) (length (first input)))
              :initial-contents (mapcar (alexandria:curry #'map 'list #'char->type)
                                        input)))

(defun count-occupied (seats)
  (count :occupied (make-array (array-total-size seats) :displaced-to seats)))

(defun solve-1 (&optional (input (day-input-lines 11 2020)))
  (loop :for seats := (parse-input input) :then next-seats
        :for next-seats := (seats-round seats)
        :for x :from 0
        :when (evenp x)
          :do (seats-to-pic x seats)
        :until (equalp seats next-seats)
        :finally (return seats)))


(defun move-direction-till-seat (seats position direction)
  (labels ((helper (position)
             (cond ((not (in-board-p seats position)) nil)
                   ((seat-p (board-position seats position)) (values position
                                                                     (board-position seats position)))
                   (t (helper (pos-add position direction))))))
    (helper (pos-add position direction))))


(defun occupied-visible (seats)
  (let ((map (make-hash-table :test 'equal)))
    (dotimes (y (array-dimension seats 0))
      (dotimes (x (array-dimension seats 1))
        (when (seat-p (board-position seats (make-pos x y)))
          (setf (gethash (make-pos x y) map)
                (remove nil
                        (mapcar (lambda (direction)
                                  (move-direction-till-seat seats (make-pos x y) direction))
                                *directions*))))))
    (lambda (seats position)
      (count :occupied (mapcar (alexandria:curry #'board-position seats) (gethash position map))))))

(defun solve-2 (&optional (input (day-input-lines 11 2020)))
  (let ((occupied-func (occupied-visible (parse-input input))))
    (count-occupied
     (loop :for seats := (parse-input input) :then next-seats
           :for next-seats := (seats-round seats occupied-func 5)
           :for x :from 0
           :when (evenp x)
             :do (seats-to-pic x seats)
           :until (equalp seats next-seats)
           :finally (return seats)))))


(defun print-map (seats)
  (dotimes (y (array-dimension seats 0))
    (dotimes (x (array-dimension seats 1))
      (princ (type->char (aref seats y x))))
    (terpri))
  (values))


(defun seats-to-pic (n seats)
  (let* ((image (png:make-image (array-dimension seats 0)
                                (array-dimension seats 1)
                                1)))
    (dotimes (y (array-dimension seats 0))
      (dotimes (x (array-dimension seats 1))
        (setf (aref image y x 0)
              (ccase (aref seats y x)
                (:floor 0)
                (:empty 170)
                (:occupied 255)))))
    (with-open-file (stream (format nil "map/~2,'0d.png" n)
                            :direction :output
                            :element-type '(unsigned-byte 8)
                            :if-exists :supersede)
      (png:encode image stream))))
