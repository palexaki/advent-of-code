(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :split-sequence)
  (ql:quickload :advent-util))
(defpackage #:day-4
  (:use #:advent-util
        #:cl
        #:split-sequence
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-4)


(defparameter *required-fields* '(:ecl :pid :eyr :hcl :byr :iyr :hgt))

(defparameter *fields* (union *required-fields* '(:cid)))

(defparameter *test* '("ecl:gry pid:860033327 eyr:2020 hcl:#fffffd"
                       "byr:1937 iyr:2017 cid:147 hgt:183cm"
                       ""
                       "iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884"
                       "hcl:#cfa07d byr:1929"
                       ""
                       "hcl:#ae17e1 iyr:2013"
                       "eyr:2024"
                       "ecl:brn pid:760753108 byr:1931"
                       "hgt:179cm"
                       ""
                       "hcl:#cfa07d eyr:2025 pid:166559648"
                       "iyr:2011 ecl:brn hgt:59in"))

(defparameter *test-valid* '("pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980"
                            "hcl:#623a2f"
                            ""
                            "eyr:2029 ecl:blu cid:129 byr:1989"
                            "iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm"
                            ""
                            "hcl:#888785"
                            "hgt:164cm byr:2001 iyr:2015 cid:88"
                            "pid:545766238 ecl:hzl"
                            "eyr:2022"
                            ""
                            "iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719"))


(defun parse-entry (entry)
  (mapcar (lambda (field)
            (destructuring-bind (key value)
                (split-sequence #\: field)
              (cons (intern (string-upcase key) "KEYWORD")
                    value)))
          (mapcan (alexandria:curry #'split-sequence #\Space)
                  entry)))

(defun parse-input (input)
  (mapcar (lambda (entry)
            (parse-entry entry))
          (split-sequence "" input :test #'equal)))

(defun valid-entry (entry)
  (null (set-difference *required-fields*
                        (mapcar #'car entry))))

(defparameter *hair-regex* (cl-ppcre:parse-string "#[0-9a-f]{6}"))
(defparameter *height-regex* (cl-ppcre:parse-string "(\\d+)(in|cm)"))

(defun valid-height-p (height)
  (destructuring-bind (&optional height measure)
      (parse-groups *height-regex* height #'parse-integer
                    (lambda (measure)
                      (intern (string-upcase measure) "KEYWORD")))
    ;(print (cons height measure))
    (and height measure
         (case measure
           (:in (<= 59 height 76))
           (:cm (<= 150 height 193))
           (otherwise nil)))))

(defun valid-strict (entry)
  (every (lambda+ ((key . value))
           ;(print (cons key value))
           (ccase key
             (:ecl (find value '("amb" "blu" "brn" "gry" "grn" "hzl" "oth")
                         :test #'equalp))
             (:hcl (cl-ppcre:scan *hair-regex* value))
             (:hgt (valid-height-p value))
             (:byr (and (every #'digit-char-p value)
                        (<= 1920 (parse-integer value) 2002)))
             (:iyr (and (every #'digit-char-p value)
                        (<= 2010 (parse-integer value) 2020)))
             (:eyr (and (every #'digit-char-p value)
                        (<= 2020 (parse-integer value) 2030)))
             (:pid (and (= 9 (length value))
                        (every #'digit-char-p value)))
             (:cid t)))
         entry))

(defun solve-1 (&optional (input (day-input-lines 4 2020)))
  (count-if #'valid-entry (parse-input input)))

(defun solve-2 (&optional (input (day-input-lines 4 2020)))
  (count-if #'valid-strict
            (remove-if-not #'valid-entry (parse-input input))))
