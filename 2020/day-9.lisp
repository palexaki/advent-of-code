(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-9
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-9)

(defparameter *example* '(
                          "35"
                          "20"
                          "15"
                          "25"
                          "47"
                          "40"
                          "62"
                          "55"
                          "65"
                          "95"
                          "102"
                          "117"
                          "150"
                          "182"
                          "127"
                          "219"
                          "299"
                          "277"
                          "309"
                          "576"

                          ))

(defun valid-number (number seen &optional (end 25))
  (let ((considered (first seen)))
    (labels ((sum-member (list end)
               (cond ((= end 0) nil)
                     ((= number (+ (first list) considered)) t)
                     (t (sum-member (rest list) (1- end))))))
      (cond ((= end 1) nil)
            ((sum-member (rest seen) (1- end)) t)
            ((valid-number number (rest seen) (1- end)) t)))))

(defun encoded (message &optional seen (ends 25))
  (cond ((endp message) t)
        ((not (valid-number (first message) seen ends)) (values (first message)
                                                           seen))
        (t (encoded (rest message) (cons (first message) seen) ends))))

(defun solve-1 (&optional (input (day-input-lines 9 2020)))
  (let ((input (mapcar #'parse-integer input)))
    (encoded (nthcdr 25 input) (reverse (subseq input 0 25)))))

(defun encryption-weakness (number list)
  (labels ((sums-to (list &optional (acc 0) numbers)
             (cond ((= acc number) numbers)
                   ((endp list) nil)
                   (t (sums-to (rest list) (+ acc (first list)) (cons (first list) numbers))))))
    (if (endp list)
        nil
        (alexandria:if-let ((nums (sums-to list)))
          (+ (apply #'max nums) (apply #'min nums))
          (encryption-weakness number (rest list))))))

(defun solve-2 (&optional (input (day-input-lines 9 2020)))
  (encryption-weakness (solve-1 input) (mapcar #'parse-integer input)))
