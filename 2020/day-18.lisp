(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-18
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-18)

(defparameter *examples* '((1 + 2 * 3 + 4 * 5 + 6)
                           (2 * 3 + (4 * 5))
                           (5 + (8 * 3 + 9 + 3 * 4 * 3))
                           (5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4)))
                           (((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2)))

(defun parse-line (line)
  (read-from-string (concatenate 'string "(" line ")")))


(defun evaluate-plus (expression)
  (cond ((atom expression) expression)
        ((endp (cdr expression)) (evaluate (car expression)))
        (t
         (destructuring-bind (a operator b &rest rest)
             expression
           (case operator
             (+ (evaluate-plus (cons (+ (evaluate a t) (evaluate b t))
                                     rest)))
             (* (list* (evaluate a t) '* (alexandria:ensure-list (evaluate-plus (cons (evaluate b t) rest))))))))))

(defun evaluate (expression &optional precedence)
  (cond ((atom expression) expression)
        ((endp (cdr expression)) (evaluate (car expression)))
        ((and precedence (member '+ expression)) (evaluate (evaluate-plus expression) t))
        (t
         (destructuring-bind (a operator b &rest rest)
             expression
           (evaluate (cons (funcall operator (evaluate a precedence) (evaluate b precedence))
                           rest)
                     precedence)))))

(defun solve-1 (&optional (input (day-input-lines 18 2020)))
  (reduce #'+ (mapcar (alexandria:compose #'evaluate #'parse-line) input)))

(defun solve-2 (&optional (input (day-input-lines 18 2020)))
  (reduce #'+ (mapcar (alexandria:compose (alexandria:rcurry #'evaluate t)
                                          #'parse-line)
                      input)))
