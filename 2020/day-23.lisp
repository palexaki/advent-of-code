(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-23
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-23)

(defparameter *example* "389125467")

(defun parse-input (input)
  (apply #'alexandria:circular-list (map 'list #'digit-char-p input)))

(defun destination-num (current-cup picked-up max-num)
  (loop :for dest :downfrom (1- current-cup)
        :while (or (= 0 dest)
                   (member (mod dest max-num) picked-up))
        :finally (return (mod dest max-num))))


(defun find-destination (dest-num cups max-num)
  (loop :repeat max-num
        :for subl :on cups
        :when (= (car subl) dest-num)
          :return subl))


(defun perform-move (cups &optional (max-num 10) fast-access)
  (let ((current (first cups))
        (picked-up (subseq cups 1 4)))
    (setf (cdr cups) (nthcdr 4 cups))
    ;; (print picked-up)
    ;; (print cups)
    (let* ((dest-num (destination-num current picked-up max-num))
           (dest-list (if fast-access
                         (gethash dest-num fast-access)
                         (find-destination dest-num cups max-num))))
      ;(print dest-num)
      ;; (unless (eq (gethash dest-num fast-access)
      ;;             (find-destination dest-num cups max-num))
      ;;   (print 'errorr))
      ;; (print current)
      ;; (print (first dest-list))
      (when fast-access
        (loop :for sublis :on picked-up
              :do (setf (gethash (car sublis) fast-access) sublis)))
      ;(print dest-list)
      (setf (cdr (last picked-up)) (cdr dest-list)
            (cdr dest-list) picked-up))
    (cdr cups)))

(defun solve-1 (&optional (input (day-input-lines 23 2020)))
  (loop :repeat 101
        :for cups := (parse-input (alexandria:ensure-car input)) :then (perform-move cups)
        :finally (return (format nil "~{~a~}" (subseq (find-destination 1 cups 10) 1 9)))))

(defun solve-2 (&optional (input (day-input-lines 23 2020)))
  ;(declare (optimize (debug 3)))
  (let* ((cups-first (map 'list #'digit-char-p (alexandria:ensure-car input)))
         (cups-rest (alexandria:iota 999991 :start 10)))
    (setf (cdr (last cups-rest)) cups-first
          (cdr (nthcdr 8 cups-first)) cups-rest)
    ;(print 'test)
    (let ((cups  (loop :repeat 10000001
                       :with fast-access := (make-hash-table)
                         :initially (loop :repeat 1000000
                                          :for sublis :on cups-first
                                          :do (setf (gethash (car sublis) fast-access) sublis))
                       :for cups := cups-first :then (perform-move cups 1000001 fast-access)
                       ;; :when (= 0 (mod iter 100000))
                       ;;   :do (print 'tost)
                       :finally (return cups))))
      (setf cups (find-destination 1 cups 1000001))
      (print (second cups))
      (print (third cups))
      (* (second cups) (third cups))

      )))
