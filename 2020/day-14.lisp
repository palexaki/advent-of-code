(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :fset)
  (ql:quickload :parseq)
  (ql:quickload :advent-util))
(defpackage #:day-14
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-14)

(defun list->mask (input)
  (loop :with seq-size := 1
        :with seq-start := 0
        :with masks
        :for char :across (reverse (concatenate 'string "X" input))
        :and char-seq := char
        :for index :from 0
        :do (if (eq char-seq char)
                (incf seq-size)
                (psetf seq-size 1
                       seq-start index
                       masks (if (or (null char-seq) (eq char-seq #\X))
                                 masks
                                 (cons (list (byte seq-size seq-start) (digit-char-p char-seq))
                                       masks))))
        :finally (return masks)))

(parseq:defrule initialization ()
    (+ (string (or mask memset)))
  (:lambda (&rest instructions)
    (mapcar #'first instructions)))

(parseq:defrule mask ()
    (and "mask = " (+ (char "10X")))
  (:choose 1)
  (:string))

(parseq:defrule memset ()
    (and "mem[" my-number "] = " my-number)
  (:choose 1 3))

(parseq:defrule my-number ()
    (+ digit)
  (:string)
  (:function 'parse-integer))

(defun apply-mask (mask number)
  (reduce (lambda (number mask-item)
            (dpb (- (second mask-item)) (first mask-item) number))
          mask
          :initial-value number))

(defparameter *example* '("mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X"
                          "mem[8] = 11"
                          "mem[7] = 101"
                          "mem[8] = 0"))

(defparameter *example-2* '("mask = 000000000000000000000000000000X1001X"
                            "mem[42] = 100"
                            "mask = 00000000000000000000000000000000X0XX"
                            "mem[26] = 1"))

(defun execute-instructions (instructions &optional mask (mem (fset:empty-map)))
  (if (endp instructions)
      mem
      (if (stringp (car instructions))
          (execute-instructions (rest instructions) (list->mask (car instructions)) mem)
          (execute-instructions (rest instructions) mask (fset:with mem (caar instructions)
                                                                    (apply-mask mask (cadar instructions)))))))

(defun solve-1 (&optional (input (day-input-lines 14 2020)))
  (fset:reduce (lambda (sum key num)
                 (declare (ignore key))
                 (+ sum num))
               (execute-instructions (parseq:parseq 'initialization input))
               :initial-value 0))

(defun apply-mad (mad address)
  (loop :with addresses := (fset:set (loop :with num := address
                                           :for char :across mad
                                           :for index :downfrom (1- (length mad))
                                           :when (char= char #\1)
                                             :do (setf num (dpb 1 (byte 1 index) num))
                                           :finally (return num)))
        :for char :across mad

        :for index :downfrom (1- (length mad))
        :when (char= char #\X)
          :do (setf addresses (let ((news (fset:set (fset:$ addresses))))
                                (fset:do-set (num addresses)
                                  (fset:adjoinf news (dpb 1 (byte 1 index) num))
                                  (fset:adjoinf news (dpb 0 (byte 1 index) num)))
                                news))
        :finally (return addresses)))

(defun execute-mad-ins (instructions)
  (loop :with mem := (make-hash-table)
        :with mad
        :for ins :in instructions
        :if (stringp ins)
          :do (setf mad ins)
        :else
          :do (fset:image (lambda (address)
                            (setf (gethash address mem) (second ins)))
                          (apply-mad mad (car ins)))
        :finally (return mem)))

(defun solve-2 (&optional (input (day-input-lines 14 2020)))
  (let ((mem (execute-mad-ins (parseq:parseq 'initialization input))))
    (loop :for val :being :the :hash-values :of mem
          :summing val)))
