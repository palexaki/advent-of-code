(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :fset)
  (ql:quickload :advent-util))
(defpackage #:day-22
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-22)

(defparameter *example* '("Player 1:" "9" "2" "6" "3" "1"
                          ""
                          "Player 2:" "5" "8" "4" "7" "10"))

(defun parse-decks (input)
  (destructuring-bind (player-1 player-2)
      (split-sequence:split-sequence "" input :test #'equal)
    (list (mapcar #'parse-integer (rest player-1))
          (mapcar #'parse-integer (rest player-2)))))

(defun game-end? (player-1 player-2)
  (or (endp player-1) (endp player-2)))

(defun play-combat (player-1 player-2)
  (if (game-end? player-1 player-2)
      (or player-1 player-2)
      (let ((top-1 (first player-1))
            (top-2 (first player-2)))
        (play-combat (if (> top-1 top-2)
                         (append (rest player-1) (list top-1 top-2))
                         (rest player-1))
                     (if (> top-2 top-1)
                         (append (rest player-2) (list top-2 top-1))
                         (rest player-2))))))

(defun score (deck)
  (labels ((calculate-score (deck position acc)
             (if (endp deck)
                 acc
                 (calculate-score (rest deck) (1+ position) (+ acc (* position (first deck)))))))
    (calculate-score (reverse deck) 1 0)))

(defun solve-1 (&optional (input (day-input-lines 22 2020)))
  (score (apply #'play-combat (parse-decks input))))

(defun play-subgame? (player-1 player-2)
  (and (< (first player-1) (length player-1))
       (< (first player-2) (length player-2))))

(defun round-winner (player-1 player-2)
  (cond ((play-subgame? player-1 player-2) (nth-value 1
                                                      (play-recursive-combat (subseq (rest player-1)
                                                                                     0 (first player-1))
                                                                             (subseq (rest player-2)
                                                                                     0 (first player-2)))))
        ((> (first player-1) (first player-2)) :player-1)
        (t :player-2)))

(defun play-recursive-combat (player-1 player-2 &optional (history (make-hash-table :test #'equalp)))
  (cond ((endp player-1) (values player-2 :player-2 ))
        ((endp player-2) (values  player-1 :player-1))
        ((gethash (list player-1 player-2) history) (values player-1 :player-1))
        (t (let ((round-winner (round-winner player-1 player-2))
                 (top-1 (first player-1))
                 (top-2 (first player-2)))
             (setf (gethash (list player-1 player-2) history) t)
             (play-recursive-combat (if (eq round-winner :player-1)
                                        (append (rest player-1) (list top-1 top-2))
                                        (rest player-1))
                                    (if (eq round-winner :player-2)
                                        (append (rest player-2) (list top-2 top-1))
                                        (rest player-2))
                                    history)))))

(defun solve-2 (&optional (input (day-input-lines 22 2020)))
  (apply #'play-recursive-combat (parse-decks input)))
