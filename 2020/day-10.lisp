(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :fset)
  (ql:quickload :advent-util))
(defpackage #:day-10
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-10)

(defparameter *example* '( 16 10 15 5 1 11 7 19 6 12 4 ))

(defparameter *example-2* '(28 33 18 42 31 14 46 20 48 47 24 23 49 45 19 38 39
                            11 1 32 25 35 8 17 7 9 4 2 34 10 3 ))


(defun joltage-jumps (current joltages &optional (jumps (fset:empty-map 0)))
  (if (endp joltages)
      jumps
      (let ((jump (- (first joltages) current)))
        (when (<= 0 jump 3)
          (joltage-jumps (first joltages) (rest joltages)
                         (fset:with jumps jump (1+ (fset:lookup jumps jump))))))))


(defun solve-1 (&optional (input (day-input-lines 10 2020)))
  (let* ((joltages (mapcar #'parse-integer input))
         (device (+ 3 (apply #'max joltages))))
    (joltage-jumps 0 (sort (cons device (copy-list joltages))
                           #'<))))


(defun min-possible (joltages index)
  (let ((joltage (aref joltages index)))
    (loop :for i :from (1- index) :downto 0
          :while (<= (- joltage 3) (aref joltages i))
          :finally (return (1+ i)))))



(defun arrangements (joltages &optional (memo (fset:map (0 1) :default 0)))
  (flet ((previous ()
           (loop :repeat 3
                 :for joltage :downfrom (1- (first joltages))
                 :sum (fset:lookup memo joltage))))
    (if (endp (rest joltages))
        (previous)
        (arrangements (rest joltages) (fset:with memo
                                                 (first joltages)
                                                 (previous))))))

(defun solve-2 (&optional (input (day-input-lines 10 2020)))
  (let* ((joltages (mapcar #'parse-integer input))
         (device (+ 3 (apply #'max joltages))))
    (arrangements (sort (list* device (copy-list joltages)) #'<))))
