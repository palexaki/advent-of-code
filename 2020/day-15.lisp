(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-15
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-15)

(defparameter *example* '("0,3,6"))



(defun play-game (starting-numbers turns)
  (let ((spoken (make-array turns)))
    (loop :for num :in starting-numbers
          :for turn :from 1 :below (length starting-numbers)
          :do (setf (aref spoken num) turn))
    (loop :with previous := (car (last starting-numbers))
          :for turn :from (1+ (length starting-numbers)) :upto turns
                                        ;:do (print previous)
          :do (psetf previous (if (plusp (aref spoken previous))
                                  (- turn 1 (aref spoken previous))
                                  0)
                     (aref spoken previous) (1- turn))
          :finally (return previous))))

(defun solve-1 (&optional (input (day-input-lines 15 2020)))
  (let ((starting-numbers (ints (first input))))
    (play-game starting-numbers 2020)))

(defun solve-2 (&optional (input (day-input-lines 15 2020)))
  (let ((starting-numbers (ints (first input))))
    (play-game starting-numbers 30000000)))
