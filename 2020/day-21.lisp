(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :parseq)
  (ql:quickload :fset)
  (ql:quickload :advent-util))
(defpackage #:day-21
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-21)

(parseq:defrule foods ()
    (+ (string food))
  (:lambda (&rest foods) (mapcar #'first foods)))

(parseq:defrule food ()
    (and ingredients allergens))

(parseq:defrule ingredients ()
    (+ (and word " "))
  (:lambda (&rest ingredients) (reduce #'fset:with
                                       (mapcar #'first ingredients)
                                       :initial-value (fset:empty-set))))

(parseq:defrule allergens ()
    (and "(contains " (+ (and word (? ", "))) ")")
  (:choose 1)
  (:lambda (&rest allergens) (mapcar #'first allergens)))

(parseq:defrule word ()
    (+ alpha)
  (:string))

(defun allergen-map (foods)
  (let ((allergen-map (make-hash-table :test #'equal)))
    (mapcar (lambda (food)
              (mapcar (lambda (allergen)
                        (if (gethash allergen allergen-map)
                            (setf (gethash allergen allergen-map) (fset:intersection (gethash allergen allergen-map)
                                                                                     (first food)))
                            (setf (gethash allergen allergen-map) (first food))))
                      (second food)))
            foods)
    allergen-map))

(defun ingredient-possible-allergen (foods)
  (loop :for ingredients :being :the :hash-values :of (allergen-map foods)
        :for ingredients-all := ingredients :then (fset:union ingredients-all ingredients)
        :finally (return ingredients-all)))

(defun solve-1 (&optional (input (day-input-lines 21 2020)))
  (let* ((foods (parseq:parseq 'foods input))
         (all-ingredients (reduce #'fset:union foods :key #'first))
         (all-non-allergen-ingredients (fset:set-difference all-ingredients
                                                            (ingredient-possible-allergen foods))))
    (fset:size (reduce (lambda (bag ingredients)
                         (fset:bag-sum bag (fset:intersection all-non-allergen-ingredients
                                                              ingredients)))
                       foods :key #'first :initial-value (fset:empty-bag)))))

(defun solve-2 (&optional (input (day-input-lines 21 2020)))
  (let* ((foods (parseq:parseq 'foods input))
         (allergen-map (allergen-map foods))
         (all-allergen-ingredients (fset:convert 'list (ingredient-possible-allergen foods)))
         (reverse-map (make-hash-table :test #'equal)))
    (loop :with lst := (alexandria:hash-table-alist allergen-map)
          :while (some (lambda (acons)
                         (not (fset:empty? (cdr acons))))
                       lst)
          :do (destructuring-bind (allergen . set)
                  (find-if (lambda (ingredients)
                             (= 1 (fset:size ingredients)))
                           lst :key #'cdr)
                (setf (gethash (fset:arb set) reverse-map) allergen)
                (setf lst (mapcar (lambda (acons)
                                    (cons (car acons) (fset:less (cdr acons) (fset:arb set))))
                                  lst))))
    (format nil "~{~a~^,~}" (sort (copy-list all-allergen-ingredients) #'string< :key (alexandria:rcurry #'gethash reverse-map)))
    ))
