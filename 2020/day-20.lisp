(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :parseq)
  (ql:quickload :advent-util))
(defpackage #:day-20
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-20)

(defparameter *example* (with-open-file (stream #P"~/code/lisp/advent/2020/20.ex")
                          (read-stream-into-lines stream)))

(defstruct tile
  id
  content
  edges
  matches)

(parseq:defrule camera-array ()
    (+ (and tile (? "")))
  (:lambda (&rest tiles) (mapcar #'first tiles)))

(parseq:defrule tile ()
    (and tile-id tile-content)
  (:lambda (tile-id tile-content)
    (make-tile :id tile-id :content tile-content
               :edges (extract-tile-edges tile-content))))

(parseq:defrule tile-id ()
    (string (and "Tile " (+ digit) ":"))
  (:choose '(0 1))
  (:string)
  (:function #'parse-integer))

(parseq:defrule tile-content ()
    (+ (string (+ char)))
  (:lambda (&rest tile-row) (mapcar #'first tile-row)))

(defun extract-tile-edges (tile-content)
  (list (copy-list (first tile-content))
        (mapcar 'alexandria:lastcar tile-content)
        (reverse (alexandria:lastcar tile-content))
        (reverse (mapcar 'first tile-content))))

(defun find-matches (tile tiles)
  (let ((reversed-edges (mapcar #'reverse (tile-edges tile))))
    (mapcar #'tile-id (remove-if-not (lambda (match-tile)
                                       (and (/= (tile-id match-tile) (tile-id tile))
                                            (union (intersection (tile-edges tile) (tile-edges match-tile)
                                                                 :test #'equal)
                                                   (intersection reversed-edges (tile-edges match-tile)
                                                                 :test #'equal)
                                                   :test #'equal)))
                                     tiles))))

(defun solve-1 (&optional (input (day-input-lines 20 2020)))
  (let ((tiles (parseq:parseq 'camera-array input)))
    (mapcar (lambda (tile)
              (setf (tile-matches tile) (find-matches tile tiles)))
            tiles)
    (reduce #'* (remove-if-not (lambda (matches)
                                 (= 2 (length matches)))
                               tiles :key #'tile-matches)
            :key #'tile-id)))


(defun matching-edge (tile-a tile-b)
  (first (union (intersection (tile-edges tile-a) (tile-edges tile-b)
                              :test #'equal)
                (intersection (tile-edges tile-a) (mapcar #'reverse (tile-edges tile-b))
                              :test #'equal)
                :test #'equal)))

(defun should-flip (tile-a tile-b)
  (intersection (tile-edges tile-a) (tile-edges tile-b)
                :test #'equal))

(defun rotate-content (content)
  (mapcar (lambda (column)
            (reverse (mapcar (lambda (row)
                               (nth column row))
                             content)))
          (alexandria:iota (length (first content)))))


(defun rotate-tile (tile)
  (with-slots (id content matches)
      tile
    (let ((new-tile (make-tile :id id :content (rotate-content content) :matches matches)))
      (setf (tile-edges new-tile) (extract-tile-edges (tile-content new-tile)))
      new-tile)))

(defun flip-tile (tile)
  (let ((new-content (reverse (tile-content tile))))
    (make-tile :id (tile-id tile) :content new-content :matches (tile-matches tile)
               :edges (extract-tile-edges new-content))))

(defun mirror-tile (tile)
  (let ((new-content (mapcar #'reverse (tile-content tile))))
    (make-tile :id (tile-id tile) :content new-content :matches (tile-matches tile)
               :edges (extract-tile-edges new-content))))

(defun rotate-till-match-correct (tile match-tile edge)
  (loop :for ro-tile := tile :then (rotate-tile ro-tile)
        :when (= (matching-edge-dir ro-tile match-tile)
                  edge)
          :return (if (should-flip ro-tile match-tile)
                      (if (find edge '(0 2))
                          (mirror-tile ro-tile)
                          (flip-tile ro-tile))
                      ro-tile)))

(defun matching-edge-dir (tile-a tile-b)
  (position (matching-edge tile-a tile-b) (tile-edges tile-a)))

(defun rotate-fit-top-left-edge (tile match-a match-b)
  (loop :for ro-tile := tile :then (rotate-tile ro-tile)
        :for a-edge := (matching-edge-dir ro-tile match-a)
        :and b-edge := (matching-edge-dir ro-tile match-b)
        :when (or (and (= a-edge 1)
                       (= b-edge 2))

                  (and (= b-edge 1)
                       (= a-edge 2)))
          :return ro-tile))

(defun find-tile (id tiles)
  (find id tiles :key #'tile-id))

(defun rev-equal (a b)
  (or (equal a b)
      (equal (reverse a) b)))


(defun find-matching-tile (edge tiles)
  (let ((reversed (reverse edge)))
    (find-if (lambda (tile)
               (or (member edge (tile-edges tile) :test #'equal)
                   (member reversed (tile-edges tile) :test #'equal)))
             tiles)))


(defun arrange-image (tiles)
  (declare (optimize (debug 3)))
  (let* ((side-length (round (sqrt (length tiles))))
         (arrangement (make-array (list side-length side-length))))
    (setf (aref arrangement 0 0) (let ((top-left (find 2 tiles :key (alexandria:compose #'length
                                                                                        #'tile-matches))))
                                   (rotate-fit-top-left-edge top-left
                                                             (find-tile (first (tile-matches top-left)) tiles)
                                                             (find-tile (second (tile-matches top-left)) tiles))))
    (loop :for col :from 1 :below side-length
          :for prev := (aref arrangement 0 (1- col))
          :for next := (find-matching-tile (nth 1 (tile-edges prev))
                                           (mapcar (alexandria:rcurry 'find-tile tiles) (tile-matches prev)))
          :do (setf (aref arrangement 0 col) (rotate-till-match-correct next prev 3)))
    (loop :for row :from 1 :below side-length
          :do (loop :for col :from 0 :below side-length
                    :for prev := (aref arrangement (1- row) col)
                    :for next := (find-matching-tile (nth 2 (tile-edges prev))
                                                     (mapcar (alexandria:rcurry 'find-tile tiles) (tile-matches prev)))
                    :do (setf (aref arrangement row col) (rotate-till-match-correct next prev 0))))
    arrangement))

(defun remove-borders (content)
  (let ((len (length (first content))))
    (subseq (mapcar (lambda (row)
                      (subseq row 1 (1- len)))
                    content)
            1 (1- (length content)))))

(defun fill-image-part (image tile tile-row tile-col)
  (let ((content (remove-borders (tile-content tile))))
    (loop :for row :in content
          :for image-row :from (* 8 tile-row)
          :do (loop :for el :in row
                    :for image-col :from (* 8 tile-col)
                    :do (setf (aref image image-row image-col) el)))))

(defun stitch-image (arrangement)
  (let* ((tile-side (array-dimension arrangement 0))
         (image (make-array (list (* 8 tile-side) (* 8 tile-side)))))
    (dotimes (row-i tile-side)
      (dotimes (col-i tile-side)
        (fill-image-part image (aref arrangement row-i col-i) row-i col-i)))
    image))

(defun rotate-image (image)
  (let ((new-image (make-array (array-dimensions image))))
    (dotimes (row (array-dimension image 0))
      (dotimes (col (array-dimension image 1))
        (setf (aref new-image col (- (array-dimension image 0) row 1))
              (aref image row col))))
    new-image))

(defun flip-image (image)
  (let ((new-image (make-array (array-dimensions image))))
    (dotimes (row (array-dimension image 0))
      (dotimes (col (array-dimension image 1))
        (setf (aref new-image (- (array-dimension image 0) row 1) col)
              (aref image row col))))
    new-image))

(defparameter *monster-offsets* '((0 18) (1 0)  (1 5)  (1 6)  (1 11)
                                  (1 12) (1 17) (1 18) (1 19) (2 1)
                                  (2 4)  (2 7)  (2 10) (2 13) (2 16)))


(defun monster-at (image row col)
  (loop :for (off-row off-col) :in *monster-offsets*
        :never (char= #\. (aref image (+ off-row row) (+ off-col col)))))

(defun mark-monster-at (image row col)
  (loop :for (off-row off-col) :in *monster-offsets*
        :do (setf (aref image (+ off-row row) (+ off-col col)) #\O)))

(defun mark-monsters (image)
  (loop :for row :from 0 :upto (- (array-dimension image 0) 3)
        :do (loop :for col :from 0 :upto (- (array-dimension image 1) 20)
                  :when (monster-at image row col)
                    :do (mark-monster-at image row col)))
  image)

(defun count-sea (image)
  (count #\# (make-array (array-total-size image) :displaced-to image)))

(defun solve-2 (&optional (input (day-input-lines 20 2020)))
  (let ((tiles (parseq:parseq 'camera-array input)))
    (mapcar (lambda (tile)
              (setf (tile-matches tile) (find-matches tile tiles)))
            tiles)
    (loop :repeat 2
          :for image := (stitch-image (arrange-image tiles)) :then (flip-image image)
          :minimizing (loop :repeat 4
                            :for img := image :then (rotate-image img)
                            :do (mark-monsters img)
                            :minimizing (count-sea img)
                            :finally (setf image img)))))

(defun content-to-string (content)
  (format nil "~{~{~a~^ ~}~^~%~}" content))

(defun print-content (content)
  (format t "~{~{~a~^ ~}~^~%~}" content))

(defun pprint-tile (tile)
  (format t "Tile ~a:~%~{~{~a~^ ~}~%~}" (tile-id tile) (tile-content tile)))

(defun print-image (image)
  (dotimes (row (array-dimension image 0))
    (dotimes (col (array-dimension image 0))
      (format t "~a " (aref image row col)))
    (format t "~%")))
