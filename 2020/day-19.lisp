(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :fset)
  (ql:quickload :parseq)
  (ql:quickload :advent-util))
(defpackage #:day-19
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(defpackage #:day-19-rules)

(in-package #:day-19)

(defparameter *example* '("0: 1 2"
                          "1: \"a\""
                          "2: 1 3 | 3 1"
                          "3: \"b\""
                          ""
                          "string"
                          "aab"
                          "aba"
                          "aaa"))

(defparameter *example-2* '("0: 4 1 5"
                            "1: 2 3 | 3 2"
                            "2: 4 4 | 5 5"
                            "3: 4 5 | 5 4"
                            "4: \"a\""
                            "5: \"b\""
                            ""
                            "aaaabb"
                            "aaabab"
                            "aa"))



(defparameter *example-3* '("42: 9 14 | 10 1"
                            "9: 14 27 | 1 26"
                            "10: 23 14 | 28 1"
                            "1: \"a\""
                            "11: 42 31"
                            "5: 1 14 | 15 1"
                            "19: 14 1 | 14 14"
                            "12: 24 14 | 19 1"
                            "16: 15 1 | 14 14"
                            "31: 14 17 | 1 13"
                            "6: 14 14 | 1 14"
                            "2: 1 24 | 14 4"
                            "0: 8 11"
                            "13: 14 3 | 1 12"
                            "15: 1 | 14"
                            "17: 14 2 | 1 7"
                            "23: 25 1 | 22 14"
                            "28: 16 1"
                            "4: 1 1"
                            "20: 14 14 | 1 15"
                            "3: 5 14 | 16 1"
                            "27: 1 6 | 14 18"
                            "14: \"b\""
                            "21: 14 1 | 1 14"
                            "25: 1 1 | 1 14"
                            "22: 14 14"
                            "8: 42"
                            "26: 14 22 | 1 20"
                            "18: 15 15"
                            "7: 14 5 | 1 21"
                            "24: 14 1"
                            ""
                            "abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa"
                            "bbabbbbaabaabba"
                            "babbbbaabbbbbabbbbbbaabaaabaaa"
                            "aaabbbbbbaaaabaababaabababbabaaabbababababaaa"
                            "bbbbbbbaaaabbbbaaabbabaaa"
                            "bbbababbbbaaaaaaaabbababaaababaabab"
                            "ababaaaaaabaaab"
                            "ababaaaaabbbaba"
                            "baabbaaaabbaaaababbaababb"
                            "abbbbabbbbaaaababbbbbbaaaababb"
                            "aaaaabbaabaaaaababaa"
                            "aaaabbaaaabbaaa"
                            "aaaabbaabbaaaaaaabbbabbbaaabbaabaaa"
                            "babaaabbbaaabaababbaabababaaab"
                            "aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba"))

(parseq:defrule input ()
    (and (+ (string rule)) "" (+ string))
  (:choose 0 2)
  (:lambda (rules strings)
    (list (mapcar #'first rules)
          strings)))

(parseq:defrule rule ()
    (and rule-name ": " rule-body)
  (:choose 0 2))

(parseq:defrule rule-name ()
    (+ digit)
  (:string)
  (:function #'name-symbol))

(parseq:defrule rule-body ()
    (or rule-or rule-char))

(parseq:defrule rule-char ()
    (and #\" char #\")
  (:choose 1)
  (:string))

(parseq:defrule rule-or ()
    (+ (and rule-seq (? "| ")))
  (:lambda (&rest seqs)
    (mapcar #'first seqs)))

(parseq:defrule rule-seq ()
    (+ (and rule-name (? " ")))
  (:lambda (&rest rules)
    (mapcar #'first rules)))

(defun name-symbol (name)
  (intern name :day-19-rules))

(defun get-rule-body (rule-name rules)
  (second (assoc rule-name rules)))


(defun generate-for-rule (rule-name rules &optional (memo (make-hash-table)))
  (declare (optimize (debug 3)))
  (flet ((gen-for-seq (rule-seq)
           (apply #'alexandria:map-product
                  (alexandria:curry #'concatenate 'string)
                  (mapcar (alexandria:rcurry #'generate-for-rule rules memo)
                          rule-seq))))
    (cond ((gethash rule-name memo) (values (gethash rule-name memo)
                                            memo))
          ((stringp (get-rule-body rule-name rules)) (values (setf (gethash rule-name memo)
                                                                   (list (get-rule-body rule-name rules)))
                                                             memo))
          (t
           (let ((rule-body (get-rule-body rule-name rules)))
             (values (setf (gethash rule-name memo) (reduce #'union
                                                            (mapcar #'gen-for-seq rule-body)))
                     memo))))))


(defun solve-1 (&optional (input (day-input-lines 19 2020)))
  (destructuring-bind (rules strings)
      (parseq:parseq 'input input)
    (let ((all-possible (remove-if (alexandria:curry #'< (max-length-string strings))
                                   (generate-for-rule (name-symbol "0") rules)
                                   :key #'length)))
      (print (length all-possible))
      (length (intersection strings all-possible :test 'string=)))))

(defun solve-2 (&optional (input (day-input-lines 19 2020)))
  (destructuring-bind (rules strings)
      (parseq:parseq 'input input)
    (let* ((forty (generate-for-rule (name-symbol "42") rules))
           (thirty (generate-for-rule (name-symbol "31") rules)))
      (list forty thirty (eval `(parseq:with-local-rules
                                  (parseq:defrule top ()
                                      (and (+ forty-two)  (+ thirty-one))
                                    (:test (a b) (> (length a) (length b))))
                                  (parseq:defrule forty-two ()
                                      (or ,@forty))
                                  (parseq:defrule thirty-one ()
                                      (or ,@thirty))
                                  (count-if (lambda (string)
                                              (parseq:parseq 'top string))
                                            ',strings)))))))
