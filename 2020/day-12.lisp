(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :parseq)
  (ql:quickload :advent-util))
(defpackage #:day-12
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-12)

(defparameter *example* '("F10"
                          "N3"
                          "F7"
                          "R90"
                          "F11"))

(parseq:defrule instructions ()
    (+ (string instruction))
  (:lambda (&rest instructions) (mapcar #'first instructions)))

(parseq:defrule instruction ()
    (and (char "LRFNSEW") my-number)
  (:lambda (instruction argument)
    (case instruction
      (#\R (cons :turn-right (/ argument 90)))
      (#\L (cons :turn-left (/ argument 90)))
      (#\N (cons :up argument))
      (#\S (cons :down argument))
      (#\E (cons :right argument))
      (#\W (cons :left argument))
      (#\F (cons :forward argument)))))

(parseq:defrule my-number ()
    (+ digit)
  (:string)
  (:function 'parse-integer))

(defun pos-mul (pos x)
  (mapcar (lambda (coordinate)
            (* x coordinate))
          pos))

(defun turn (direction turn-dir amount)
  (when (eq turn-dir :turn-left)
    (setf amount (- 4 amount)))
  (loop :repeat amount
        :for dir := (next-direction direction) :then (next-direction dir)
        :finally (return dir)))

(defun follow-instructions (instructions &optional
                                           (position (make-pos 0 0))
                                           (direction :right))
  (if (endp instructions)
      position
      (case (caar instructions)
        ((:turn-right :turn-left) (follow-instructions (rest instructions)
                                                       position
                                                       (turn direction
                                                             (caar instructions)
                                                             (cdar instructions))))
        ((:up :down :left :right) (follow-instructions (rest instructions)
                                                       (pos-add position
                                                                (pos-mul (direction->delta (caar instructions))
                                                                         (cdar instructions)))
                                                       direction))
        (:forward (follow-instructions (rest instructions)
                                       (pos-add position
                                                (pos-mul (direction->delta direction)
                                                         (cdar instructions)))
                                       direction)))))

(defun solve-1 (&optional (input (day-input-lines 12 2020)))
  (let ((instructions (parseq:parseq 'instructions input)))
    (follow-instructions instructions)))


(defun turn-waypoint (waypoint direction amount)
  (when (eq direction :turn-left)
    (setf amount (- 4 amount)))
  (loop :repeat amount
        :for way := (* waypoint #C(0 1)) :then (* way #C(0 1))
        :finally (return way)))


(defun direction->complex (direction)
  (apply #'complex (direction->delta direction)))

(defun follow-waypoint (instructions &optional
                                       (position #C(0 0))
                                       (waypoint #C(10 -1)))
  (if (endp instructions)
      position
      (case (caar instructions)
        ((:turn-right :turn-left) (follow-waypoint (rest instructions)
                                                       position
                                                       (turn-waypoint waypoint
                                                                      (caar instructions)
                                                                      (cdar instructions))))
        ((:up :down :left :right) (follow-waypoint (rest instructions)
                                                       position
                                                       (+ waypoint
                                                          (* (cdar instructions)
                                                             (direction->complex (caar instructions))))))
        (:forward (follow-waypoint (rest instructions)
                                   (+ position
                                      (* waypoint
                                         (cdar instructions)))
                                   waypoint)))))

(defun solve-2 (&optional (input (day-input-lines 12 2020)))
  (let ((instructions (parseq:parseq 'instructions input)))
    (follow-waypoint instructions)))
