(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-10
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-10)

(defvar *test-1* '(".#..#..###"
                   "####.###.#"
                   "....###.#."
                   "..###.##.#"
                   "##.##.#.#."
                   "....###..#"
                   "..#.#..#.#"
                   "#..#.#.###"
                   ".##...##.#"
                   " .....#.#.."))

(defvar *test-2* '("#.#...#.#."
                   ".###....#."
                   ".#....#..."
                   "##.#.#.#.#"
                   "....#.#.#."
                   ".##..###.#"
                   "..#...##.."
                   "..##....##"
                   "......#..."
                   ".####.###."))

(defvar *test-3* '(".#..##.###...#######"
                   "##.############..##."
                   ".#.######.########.#"
                   ".###.#######.####.#."
                   "#####.##.#.##.###.##"
                   "..#####..#.#########"
                   "####################"
                   "#.####....###.#.#.##"
                   "##.#################"
                   "#####.##.###..####.."
                   "..######..##.#######"
                   "####.##.####...##..#"
                   ".#####..#.######.###"
                   "##...#.##########..."
                   "#.##########.#######"
                   ".####.#.###.###.#.##"
                   "....##.##.###..#####"
                   ".#.#.###########.###"
                   "#.#.#.#####.####.###"
                   "###.##.####.##.#..##"))

(defun parse-input (&optional (input (day-input-lines 10)))
  (let ((map (make-hash-table :test 'equal)))
    (loop :for y :from 0
          :for row :in input
          :do (loop :for x :from 0
                    :for space :across row
                    :do (when (char= #\# space)
                          (setf (map-position map (complex x y)) t))))
    map))

(defun complex-inbetween (a b c)
  (let ((b-sub (- b a))
        (c-sub (- c a)))
    (and (= (phase b-sub) (phase c-sub))
         (< (abs c-sub) (abs b-sub)))))

(defun detected-p (map monitor asteroid)
  (maphash (lambda (other v)
             (declare (ignore v))
             (when (and (/= monitor other)
                        (/= asteroid other)
                        (complex-inbetween monitor
                                           asteroid
                                           other))
               (return-from detected-p nil)))
           map)
  t)

(defun all-detected (map monitor)
  (map-hash-collect bag map (other v)
    (when (detected-p map monitor other)
      (push other bag))))

(defun total-detected (map monitor)
  (length (all-detected map monitor)))

(defun mphase (number)
  (let ((phase (phase number)))
    (if (plusp phase)
        phase
        (- pi phase))))

(defun vaporize (map monitor phase)
  (flet ((phaser (asteroid) (mphase (- asteroid monitor))))
    (let ((detected (sort (all-detected map monitor) #'< :key #'phaser)))
      (if (or (< (phaser (first (last detected))) phase)
              (< phase (phaser (first detected))))
          (car (last detected))
          (loop :for (asteroid next) :on detected
                :when (and (< (phaser asteroid) phase)
                           (<= phase (phaser next)))
                  :return asteroid)))))

(defun solve-1 (&optional (input (day-input-lines 10)))
  (let ((map (parse-input input)))
    (map-hash-collect bag map (monitor v)
      (push (cons monitor
                  (total-detected map monitor))
            bag))))

(defun solve-2 (&optional (input (day-input-lines 10)))
  (let* ((map (parse-input input))
         (solved (solve-1 input))
         (optimal (caar (sort solved #'> :key 'cdr))))
    (remhash optimal map)
    (loop :for x :below 200
          :for phase := (mphase (complex -0.01 -1.001)) :then (mphase (- vaporized
                                                              optimal))
          :for vaporized := (vaporize map optimal phase)
          :do (remhash vaporized map)
          :finally (return vaporized))))
