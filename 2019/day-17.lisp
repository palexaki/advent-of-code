(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-17
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-17)

(defun scaf-string-to-map (string)
  (let ((split (cl-ppcre:split "\\n" string)))
    (make-array (list (length split) (length (first split)))
                :initial-contents split)))


(defun intersection-p (board position)
  (let ((neighbors (pos-neighbors board position)))
    (and (= (length neighbors) 4)
         (char= #\# (board-position board position))
         (every (lambda (neighbor)
                  (char= #\# (board-position board neighbor)))
                neighbors))))

(defun total-sum-alignment-parameters (map)
  (let ((sum 0))
    (dotimes (y (array-dimension map 0))
      (dotimes (x (array-dimension map 1))
        (when (intersection-p map (make-pos x y))
          (incf sum (* x y)))))
    sum))

(defun find-robot (map)
  (dotimes (y (array-dimension map 0))
    (dotimes (x (array-dimension map 1))
      (when (find (aref map y x) '(#\> #\^ #\v #\<))
        (return-from find-robot (make-pos x y))))))

(defun input-to-map (input)
  (scaf-string-to-map
   (map 'string #'code-char
        (reverse (execute-process (start-process (parse-program input)))))))

(defun intersperse (list item &optional acc)
  (if (null list)
      (cdr (reverse acc))
      (intersperse (rest list)
                   item
                   (list* (first list) item acc))))

(defun robot-point (robot)
  (ecase robot
     (#\^ :up)
     (#\> :right)
     (#\v :down)
     (#\< :left)))

(defun next-turn (map position direction)
  (let* ((directions (list (next-direction direction)
                          (prev-direction direction)))
         (deltas (mapcar #'direction->delta directions))
         (neighbors (mapcar (alexandria:curry #'pos-add position)
                            deltas)))
    (if (and (advent-util::in-board-p map (first neighbors))
             (char= #\# (board-position map (first neighbors))))
        (values (first directions) #\R)
        (values (second directions) #\L))))

(defun segment-length (map start delta)
  (loop :for len :from 0
        :for prev := start :then pos
        :for pos := (pos-add start delta) :then (pos-add pos delta)
        :when (or (not (advent-util::in-board-p map pos))
                  (char/= #\# (board-position map pos)))
          :return (values len prev)))

(defun end-position-p (map position direction)
  (let ((next-position (pos-add position (direction->delta
                                          (next-turn map position direction)))))
    (or (not (advent-util::in-board-p map next-position))
        (char/= #\# (board-position map next-position)))))

(defun parse-path (map)
  (let* ((robot-pos (find-robot map))
         (start-direction (robot-point (board-position map robot-pos))))
    (loop :with direction := start-direction
          :with position := robot-pos
          :until (end-position-p map position direction)
          :collect (multiple-value-bind (next-dir turn)
                       (next-turn map position direction)
                     (multiple-value-bind (length next-position)
                         (segment-length map position (direction->delta next-dir))
                       (setf direction next-dir
                             position next-position)
                       (list turn length))))))

(defun remove-function (function path &optional (replacer #\#))
  (let ((funlen (length function)))
    (if (> funlen (length path))
        path
        (if (equal function (subseq path 0 funlen))
            (cons replacer (remove-function function (subseq path funlen) replacer))
            (cons (first path) (remove-function function (rest path) replacer))))))

(defun execute-robot (process path)
  (print path)
  (execute-process process))

(defun solve-1 (&optional (input (first (day-input-lines 17))))
  (total-sum-alignment-parameters (input-to-map input)))

(defparameter *path-segmented* '((#\A #\B #\A #\B #\A #\C #\B #\C #\A #\C)
                                 (#\L 10 #\L 12 #\R 6)
                                 (#\R 10 #\L 4 #\L 4 #\L 12)
                                 (#\L 10 #\R 10 #\R 6 #\L 4)))

(defun segmented-to-input (segmented)
  (alexandria:mappend (lambda (segment)
                        (append  (alexandria:mappend (lambda (item)
                                           (if (numberp item)
                                               (map 'list #'char-code
                                                    (format nil "~d" item))
                                               (list (char-code item))))
                                         (intersperse segment #\,))
                                 (list 10)))
                         segmented))

(defun solve-2 (&optional (input (first (day-input-lines 17))))
  (let* ((map (input-to-map input))
         (path (parse-path map))
         (program (parse-program input)))
    (setf (aref program 0) 2)
    (first (execute-robot (start-process program (append (segmented-to-input *path-segmented*)
                                                         (list (char-code #\n) 10)))
                          path))))
