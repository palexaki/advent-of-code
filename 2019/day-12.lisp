(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-12
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-12)

(defvar *test-1* '("<x=-1, y=0, z=2>"
                   "<x=2, y=-10, z=-7>"
                   "<x=4, y=-8, z=8>"
                   "<x=3, y=5, z=-1>"))

(defvar *test-2* '("<x=-8, y=-10, z=0>"
                   "<x=5, y=5, z=10>"
                   "<x=2, y=-7, z=3>"
                   "<x=9, y=-8, z=-3>"))


(defun moon-position (moon)
  (first moon))

(defun moon-velocity (moon)
  (second moon))


(defun velocity-diff (moon-a moon-b)
  (mapcar (lambda (a b)
            (cond ((< a b) 1)
                  ((> a b) -1)
                  (t 0)))
          moon-a moon-b))

(defun parse-moon (moon-string)
  (list (parse-groups "<x=(-?\\d+), y=(-?\\d+), z=(-?\\d+)>" moon-string
                      #'parse-integer #'parse-integer #'parse-integer)
        (list 0 0 0)))

(defun parse-input (input)
  (mapcar #'parse-moon input))

(defun update-velocities (moons)
  (mapcar (lambda (moon-data)
            (reduce (lambda+ ((moon velocity) (new-moon new-velocity))
                      (declare (ignore new-velocity))
                      (list moon
                            (pos-add velocity
                                     (velocity-diff moon new-moon))))
                    moons
                    :initial-value moon-data))
          moons))

(defun apply-velocities (moons)
  (mapcar (lambda+ ((moon velocity))
            (list (pos-add moon velocity)
                  velocity))
          moons))

(defun apply-step (moons)
  (apply-velocities (update-velocities moons)))

(defun total-energy (moons)
  (reduce #'+ moons :key (lambda+ ((moon velocity))
                           (* (reduce #'+ moon :key 'abs)
                              (reduce #'+ velocity :key 'abs)))))

(defun solve-1 (&optional (input (day-input-lines 12)) (times 1000))
  (loop :repeat (1+ times)
        :for moons := (parse-input input) :then (apply-step moons)
        :finally (return moons)))

(defun total-vel-diff (moons)
  (mapcar (lambda (moon)
            (reduce (lambda (vel-diff new-moon)
                      (+ vel-diff (vel-diff moon new-moon)))
                    moons :initial-value 0))
          moons))

(defun vel-diff (a b)
  (cond ((< a b) 1)
        ((> a b) -1)
        (t 0)))


(defun cycle (coordinates)
  (loop :with map := (make-hash-table :test 'equal)
        :for steps :from 0
        :for velocities := (make-list 4 :initial-element 0)
          :then (pos-add velocities
                         (total-vel-diff moons))
        :for moons := (copy-list coordinates) :then (pos-add velocities
                                                             moons)
        :when (gethash (list moons velocities) map)
          :return (values steps moons velocities)
        :do (setf (gethash (list moons velocities) map) t)))

(defun solve-2 (&optional (input (day-input-lines 12)))
  (let ((moons (mapcar 'first (parse-input input))))
    (apply 'lcm (apply 'mapcar (lambda (&rest moons-axis)
                                 (cycle moons-axis))
                       moons))))
