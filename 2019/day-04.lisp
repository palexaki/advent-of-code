(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-4
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))
(in-package #:day-4)

(defvar *low* 256310)
(defvar *high* 732736)

(defun digits (num)
  (map 'list #'digit-char-p (format nil "~a" num)))

(defun num-increasing (num)
  (apply #'<= (map 'list #'digit-char-p (format nil "~a" num))))

(defun double (num)
  (let* ((digits (digits num)))
    (some #'= digits (rest digits))))

(defun double-2 (num)
  (let* ((digits (digits num))
         (counts (mapcar (lambda (num)
                           (list num (count num digits)))
                         digits)))
    (find 2 counts :key 'second)))

(defun six (num)
  (= (length (digits num)) 6))

(defun valid (num &optional second)
  (and (<= *low* num *high*)
       (num-increasing num)
       (if second
           (double-2 num)
           (double num))
       (six num)))

(defun solve-1 (&optional second)
  (loop :for x :from *low* :upto *high*
        :count (valid x second)))

(defun solve-2 ()
  (solve-1 t))
