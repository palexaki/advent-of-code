(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-14
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-14)

(defparameter *test-1* '("10 ORE => 10 A"
                         "1 ORE => 1 B"
                         "7 A, 1 B => 1 C"
                         "7 A, 1 C => 1 D"
                         "7 A, 1 D => 1 E"
                         "7 A, 1 E => 1 FUEL"))

(defparameter *test-2* '("157 ORE => 5 NZVS"
                         "165 ORE => 6 DCFZ"
                         "44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL"
                         "12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ"
                         "179 ORE => 7 PSHF"
                         "177 ORE => 5 HKGWZ"
                         "7 DCFZ, 7 PSHF => 2 XJWVT"
                         "165 ORE => 2 GPVTF"
                         "3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT"))

(defun parse-chemical (chemical)
  (reverse (parse-groups "(\\d+) ([A-Z]+)" chemical #'parse-integer #'identity)))

(defun parse-reaction (reaction)
  (reverse (parse-groups "(.*) => (.*)" reaction
                         (lambda (chemicals)
                           (map 'list #'parse-chemical
                                (cl-ppcre:split ", " chemicals)))
                         #'parse-chemical)))

(defun parse-input (input)
  (mapcar #'parse-reaction input))

(defun find-leftover (chemical leftovers)
  (assoc chemical leftovers :test #'string=))

(defun find-reaction (chemical reactions)
  (assoc chemical reactions :test #'string= :key #'first))


(defun react (reactions chemical amount)
  (let* ((reaction (find-reaction chemical reactions))
         (reacted-amount (cadar reaction))
         (reaction-multiplier (ceiling amount reacted-amount))
         (leftover-amount (- (* reaction-multiplier reacted-amount) amount)))
    (values (mapcar (lambda+ ((input-chemical input-amount))
                      (list input-chemical
                            (* input-amount reaction-multiplier)))
                    (second reaction))
            (when (/= 0 leftover-amount)
              (list chemical leftover-amount)))))

(defun use-leftover (chemical amount leftovers)
  (destructuring-bind (chemical leftover-amount)
      (find-leftover chemical leftovers)
    (let ((used-amount (- leftover-amount amount)))
      (cond ((zerop used-amount) (values nil nil))
            ((plusp used-amount) (values (list chemical
                                               used-amount)
                                         nil))
            ((minusp used-amount) (values nil
                                          (list chemical
                                                (- used-amount))))))))

(defun fully-reactable (reactions chemical-amounts &optional leftovers)
  ;(print chemical-amounts)
  (when (null chemical-amounts)
    (return-from fully-reactable t))
  (destructuring-bind ((chemical amount) &rest rest-amounts) chemical-amounts
    (cond ((and (string= chemical "ORE")
                (not (find-leftover "ORE" leftovers))) (return-from fully-reactable nil))
          ((find-leftover chemical leftovers)
           (multiple-value-bind (leftover necessary)
               (use-leftover chemical amount leftovers)
             (fully-reactable reactions
                              (append (when necessary
                                        (list necessary))
                                      rest-amounts)
                              (append (when leftover
                                        (list leftover))
                                      (remove chemical leftovers
                                              :test #'string= :key #'first)))))
          (t (multiple-value-bind (necessary leftover)
                 (react reactions chemical amount)
               (fully-reactable reactions
                                (append necessary rest-amounts)
                                (append (when leftover
                                          (list leftover))
                                        leftovers)))))))

(defun required-ore (reactions chemical-amounts
                     &optional leftovers (ore-amount 0))
  ;(print chemical-amounts)
  (when (null chemical-amounts)
    (return-from required-ore ore-amount))
  (destructuring-bind ((chemical amount) &rest rest-amounts) chemical-amounts
    (cond ((string= chemical "ORE") (required-ore reactions rest-amounts
                                                  leftovers (+ ore-amount
                                                               amount)))
          ((find-leftover chemical leftovers)
           (multiple-value-bind (leftover necessary)
               (use-leftover chemical amount leftovers)
             (required-ore reactions
                           (append (when necessary
                                     (list necessary))
                                   rest-amounts)
                           (append (when leftover
                                     (list leftover))
                                   (remove chemical leftovers
                                           :test #'string= :key #'first))
                           ore-amount)))
          (t (multiple-value-bind (necessary leftover)
                 (react reactions chemical amount)
               (required-ore reactions
                             (append necessary rest-amounts)
                             (append (when leftover
                                       (list leftover))
                                     leftovers)
                             ore-amount))))))

(defun solve-1 (&optional (input (day-input-lines 14)))
  (required-ore (parse-input input) '(("FUEL" 1))))

(defun fuel-amounter (amount)
  `(("FUEL" ,amount)))

(defun solve-2 (&optional (input (day-input-lines 14)))
  (solve-2-recursive (parse-input input)))

(defun solve-2-recursive (reactions &optional (low 1) (high 1000000000000))
  (if (> low high)
      nil
      (let* ((ore '(("ORE" 1000000000000)))
             (average (floor (+ high low) 2))
             (reactable (fully-reactable reactions (fuel-amounter average)
                                         ore)))
        (cond ((and reactable
                    (not (fully-reactable reactions (fuel-amounter (1+ average))
                                          ore)))
               average)
              (reactable (solve-2-recursive reactions average high))
              (t (solve-2-recursive reactions low average))))))
