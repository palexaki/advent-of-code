(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-6
  (:use #:advent-util
        #:cl
        ;#:drakma
        #:cl-ppcre
        ;#:alexandria
        ))

(defpackage #:orbit)

(in-package #:day-6)

(defun parse-input (&optional (orbits (day-input-lines 6)))
  (let ((orbit-package (find-package :orbit)))
    (mapcar (lambda (orbit)
              (reverse (mapcar (lambda (object)
                                 (intern object orbit-package))
                               (split "\\)" orbit))))
            orbits)))

(defun get-orbit (object orbits)
  (second (assoc object orbits)))

(defun get-orbiters (object orbits)
  (mapcar 'first
          (remove-if-not (alexandria:curry 'eq object) orbits :key 'second)))

(defun all-orbits (orbits object &optional acc)
  (let ((around (get-orbit object orbits)))
    (if (null around)
        acc
        (all-orbits orbits around (cons around acc)))))

(defun solve-1 ()
  (let ((orbits (parse-input)))
    (reduce #'+ (mapcar (lambda (orbit)
                          (all-orbits orbits (first orbit)))
                        (remove-duplicates orbits :key 'first))
            :key #'length)))

(defun find-san (orbits current goal &optional visited)
  (cond ((eq current goal) (list t))
        ((or (find current visited) (null current))  nil)
        (t
         (let ((path-a (remove nil
                                (mapcar (lambda (object)
                                          (find-san orbits
                                                    object
                                                    goal
                                                    (cons current visited)))
                                        (get-orbiters current orbits))))
               (path-b (find-san orbits
                                 (get-orbit current orbits)
                                 goal
                                 (cons current
                                       visited))))

           (when (cdr path-a)
             (break))
           (if (first path-a)
               (cons current (first path-a))
               (if path-b
                   (cons current path-b)
                   nil))))))

(defun solve-2 ()
  (let ((orbits (parse-input)))
    (find-san orbits (get-orbit 'orbit::you orbits)
              (get-orbit 'orbit::san orbits))))
