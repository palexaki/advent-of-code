(ql:quickload :advent-util)
(defpackage #:day-2
  (:use #:cl
        #:advent-util
        ;#:drakma
        #:cl-ppcre
        ;#:alexandria
        ))

(in-package :day-2)
(defparameter *input-1* #(1 0 0 3 1 1 2 3 1 3 4 3 1 5 0 3 2 1 10 19 1 6 19 23 1
                          13 23 27 1 6 27 31 1 31 10 35 1 35 6 39 1 39 13 43 2
                          10 43 47 1 47 6 51 2 6 51 55 1 5 55 59 2 13 59 63 2 63
                          9 67 1 5 67 71 2 13 71 75 1 75 5 79 1 10 79 83 2 6 83
                          87 2 13 87 91 1 9 91 95 1 9 95 99 2 99 9 103 1 5 103
                          107 2 9 107 111 1 5 111 115 1 115 2 119 1 9 119 0 99
                          2 0 14 0))

(defun op-func (op)
  (cond ((= 1 op) #'+)
        ((= 2 op) #'*)
        (t (print op) #'-)))

(defun self-aref (program pos)
  (aref program (aref program pos)))

(defun (setf self-aref) (value program pos)
  (setf (aref program (aref program pos)) value))

(defun execute-op (program pc)
  (let ((opcode (aref program pc))
        (arg1 (self-aref program (+ pc 1)))
        (arg2 (self-aref program (+ pc 2))))
    (setf (self-aref program (+ pc 3)) (funcall (op-func opcode)
                                                arg1 arg2))))

(defun solve-1 (&optional (input *input-1*))
  (let ((program (copy-seq input)))
    (loop :initially (setf (aref program 1) 12
                             (aref program 2) 2)
          :for pc :from 0 :by 4
          :for opcode := (aref program pc)
          :while (/= opcode 99)
          :do (execute-op program pc)
          :finally (return program))))

(defun solve-2 (&optional (input *input-1*))
  (let ((wanted 19690720))
    (dotimes (noun 100)
      (dotimes (verb 100)
        (let ((program (copy-seq input)))
          (setf (aref program 1) noun
                (aref program 2) verb)
          (advent-util::execute-program program)
          (when (= (aref program 0) wanted)
            (return-from solve-2 (+ (* 100 noun) verb))))))))

(defun solve-2-utilized (&optional (input *input-1*))
  (let ((wanted 19690720))
    (dotimes (noun 100)
      (dotimes (verb 100)
        (let ((process (start-process input)))
          (with-slots ((program advent-util::program)) process
            (setf (aref program 1) noun
                  (aref program 2) verb)
            (advent-util::execute-process process)
            (when (= (aref program 0) wanted)
              (return-from solve-2-utilized (+ (* 100 noun) verb)))))))))

(defun solve-1-utilized (&optional (input *input-1*))
  (let ((program (copy-seq input)))
    (setf (aref program 1) 12
          (aref program 2) 2)
    (advent-util::execute-process (start-process program))))
