(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-3
  (:use #:cl
        #:advent-util
        ;#:drakma
        #:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-3)

(defmacro do-piece ((delta piece &optional return) &body body)
  (alexandria:with-gensyms (i)
    (alexandria:once-only (piece)
      `(let ((,delta (direction->delta (first ,piece))))
         (dotimes (,i (second piece) ,@(if return
                                           (list return)
                                           nil))
           ,@body)))))

(defun char->direction (char)
  (ecase char
    (#\R :right)
    (#\L :left)
    (#\U :up)
    (#\D :down)))

(defun string->direcion (string)
  (char->direction (char string 0)))

(defun parse-input (input)
  (labels ((parse-piece (piece)
             (parse-groups "([RULD])(\\d*)" piece
                           #'string->direcion
                           #'parse-integer))
           (parse-path (path-string)
             (let ((pieces (split "," path-string)))
               (mapcar #'parse-piece pieces))))
    (map 'list #'parse-path input)))

(defparameter *input* (parse-input (day-input-lines 3)))

(defparameter *test-1* (parse-input '("R75,D30,R83,U83,L12,D49,R71,U7,L72"
                                      "U62,R66,U55,R34,D71,R55,D58,R83")))
(defparameter *test-2* (parse-input '("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"
                                      "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7")))

(defun map-cross-p (map position self)
  (let ((value (map-position map position)))
    (if (or (and (not (eq self value))
                 (not (null value)))
            (crossed-p value))
        :cross
        self)))

(defun crossed-p (val)
  (eq val :cross))

(defun apply-piece (map self piece position)
  (do-piece (delta piece position)
    (pos-addf position delta)
    (setf (map-position map position) (map-cross-p map position self))))

(defun steps-till-position-piece (goal piece position steps)
  (do-piece (delta piece (values position steps nil))
    (pos-addf position delta)
    (incf steps)
    (when (position= goal position)
      (return (values position steps t)))))

(defun apply-path (map self path)
  (reduce (lambda (position piece)
            (apply-piece map self piece position))
          path
          :initial-value (make-pos 0 0)))

(defun steps-till-position (goal path)
  (reduce (lambda+ ((position steps) piece)
            (multiple-value-bind (position steps crossed)
                (steps-till-position-piece goal piece position steps)
              (if crossed
                  (return-from steps-till-position steps)
                  (list position steps))))
          path
          :initial-value (list (make-pos 0 0)
                               0)))

(defun closest-cross (map)
  (map-hash-collect closest map (position val)
    (when (crossed-p val)
      (setf closest (if closest
                        (shortest closest position)
                        position)))))

(defun crosses (map)
  (map-hash-collect bag map (position val)
    (when (crossed-p val)
      (push position bag))))

(defun solve-1 (&optional (input *input*))
  (let ((map (make-hash-table :test 'equal)))
    (apply-path map 0 (first input))
    (apply-path map 1 (second input))
    (closest-cross map)))

(defun solve-2 (&optional (input *input*))
  (let ((map (make-hash-table :test 'equal)))
    (apply-path map 0 (first input))
    (apply-path map 1 (second input))
    (reduce #'min (mapcar (lambda (position)
                            (+ (steps-till-position position (first input))
                               (steps-till-position position (second input))))
                          (crosses map)))))
