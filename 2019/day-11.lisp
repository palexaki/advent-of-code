(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util)
  (ql:quickload :png))
(defpackage #:day-11
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-11)

(defun execute-program (program &optional (initial-color 0))
  (let ((process (start-process program))
        (position (make-pos 0 0))
        (direction :up)
        (paint t)
        (map (make-hash-table :test 'equal)))
    (setf (gethash position map) initial-color)
    (flet ((input ()
             (gethash position map 0))
           (output (number)
             (if paint
                 (setf (gethash position map) number)
                 (setf direction (if (= number 0)
                                     (prev-direction direction)
                                     (next-direction direction))
                       position (pos-add (direction->delta direction)
                                         position)))
             (setf paint (not paint))))
      (add-operator process 3 #'input 0 1 2)
      (add-operator process 4 #'output 1 nil 2)
      (execute-process process)
      map)))

(defun solve-1 (&optional (input (first (day-input-lines 11))))
  (let ((map (execute-program (parse-program input))))
    (save-image (map->image map))
    map))

(defun all-positions-white (map)
  (map-hash-collect positions map (position color)
    (if (= color 1)
        (push position positions))))

(defun pos? (positions chooser)
  (reduce (lambda+ ((max-x max-y) (new-x new-y))
            (make-pos (funcall chooser max-x new-x)
                      (funcall chooser max-y new-y)))
          positions))

(defun max-pos (positions)
  (pos? positions #'max))

(defun min-pos (positions)
  (pos? positions #'min))

(defun map->image (map)
  (let* ((positions-white (all-positions-white map))
         (max-pos (max-pos positions-white))
         (min-pos (min-pos positions-white))
         (image (png:make-image (+ (abs (pos-y min-pos)) (pos-y max-pos) 1)
                                (+ (abs (pos-x min-pos)) (pos-x max-pos) 1)
                                1)))
    (mapcar (lambda+ ((pos-x pos-y))
              (setf (aref image
                          (+ (abs (pos-y min-pos)) pos-y)
                          (+ (abs (pos-x min-pos)) pos-x) 0) 255))
            positions-white)
    image))

(defun solve-2 (&optional (input (first (day-input-lines 11))))
  (save-image (map->image (execute-program (parse-program input) 1))))


(defun save-image (image &optional (file #P"~/day-11.png"))
  (with-open-file (stream file :direction :output
                               :element-type '(unsigned-byte 8)
                               :if-exists :supersede)
    (png:encode image stream)))
