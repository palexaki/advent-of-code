(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :alexandria)
  (ql:quickload :advent-util))
(defpackage #:day-24
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-24)

(defparameter *example* '("....#"
                          "#..#."
                          "#..##"
                          "..#.."
                          "#...."))

(defun count-neighbours (layout point)
  (count-if (lambda (neighbour)
              (char= #\#  (board-position layout neighbour)))
            (pos-neighbors layout point)))

;; (defun step-cycle (dimension)
;;   (loop :with next-dimension := (make-hash-table :test 'equal)
;;         :for point :being :the :hash-key :of dimension :using (:hash-value active)
;;         :for count := (count-neighbours dimension point)
;;         :when (or (and active (<= 2 count 3))
;;                   (and (not active) (= count 3)))
;;           :do (loop :initially (setf (gethash point next-dimension) t)
;;                     :for neighbour :in (neighbours point)
;;                     :do (setf (gethash neighbour next-dimension) (gethash neighbour next-dimension)))
;;         :finally (return next-dimension)))

(defun step-cycle (layout)
  (let ((next-layout (alexandria:copy-array layout)))
    (dotimes (row (array-dimension layout 0))
      (dotimes (col (array-dimension layout 1))
        (setf (aref next-layout row col) (case (aref layout row col)
                                           (#\# (if (= 1 (count-neighbours layout (make-pos col row)))
                                                    #\#
                                                    #\.))
                                           (#\. (if (<= 1 (count-neighbours layout (make-pos col row)) 2)
                                                    #\#
                                                    #\.))))))
    next-layout))

(defun biodiversity (layout)
  (let ((arr (coerce (make-array (array-total-size layout)
                                 :displaced-to (alexandria:copy-array layout))
                     'string)))
    (parse-integer (reverse (substitute #\0 #\. (substitute #\1 #\# arr)))
                   :radix 2)))

(defun parse-insects (input)
  (make-array '(5 5) :initial-contents input))

(defun solve-1 (&optional (input (day-input-lines 24 2019)))
  (loop :for layout := (parse-insects input) :then (step-cycle layout)
        :when (member layout history :test #'equalp)
          :return (values (biodiversity layout) layout)
        :collect layout :into history))

(defun solve-2 (&optional (input (day-input-lines 24 2019)))
  (declare (ignore input)))

(defun print-layout (layout &optional (destination t))
  (dotimes (row (array-dimension layout 0))
    (dotimes (col (array-dimension layout 1))
      (format destination "~a" (aref layout row col)))
    (format destination "~%"))
  layout)
