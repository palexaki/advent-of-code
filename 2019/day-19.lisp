(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-19
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-19)

(defun drone-pulled (program x y)
  (= 1 (first (execute-process (start-process program `(,x ,y))))))

(defun find-beam (program &optional (print nil))
  (let ((count 0))
    (dotimes (y 50)
      (dotimes (x 50)
        (if (= 1 (first (execute-process (start-process program `(,x ,y)))))
            (progn
              (incf count)
              (when print (format t "#")))
            (when print (format t "."))))
      (when print (format t "~%")))
    count))

(defun get-start (program starts y)
  (alexandria:if-let ((start (gethash y starts)))
    start
    (let* ((start (loop :for x :from (or (first (gethash (1- y) starts))
                                         0)
                        :when (drone-pulled program x y)
                          :return x))
           (length (loop :for x :from start
                         :while (drone-pulled program x y)
                         :counting t)))
      (setf (gethash y starts) (cons start length)))))

(defun can-fit (program size bottom starts)
  (loop :with (x) := (get-start program starts bottom)
        :for y :from bottom :above (- bottom size)
        :for (start . length) := (get-start program starts y)
        :always (>= (- length (- x start)) size)
        :finally (return (make-pos x (1+ y)))))

(defun find-square (program size)
  (loop :with thingos := (make-hash-table)
        :for y :from size
        :when (can-fit program size y thingos)
          :return it))

(defun solve-1 (&optional (input (first (day-input-lines 19))))
  (find-beam (parse-program input) t))

(defun solve-2 (&optional (input (first (day-input-lines 19))))
  (find-square (parse-program input) 100))
