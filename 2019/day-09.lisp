(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-9
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-9)

(defun solve-1 (&optional (program (parse-program (first (day-input-lines 9)))))
  (let ((program (start-process program '(1))))
    (execute-process program)))

(defun solve-2 (&optional (program (parse-program (first (day-input-lines 9)))))
  (let ((program (start-process program '(2))))
    (execute-process program)))
