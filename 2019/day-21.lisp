(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-21
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-21)

(defparameter *program* "NOT A J
NOT B T
OR T J
NOT C T
OR T J
AND D J
WALK
")

(defparameter *program-2* "NOT J T
AND A T
AND B T
AND C T
NOT T J
AND D J
NOT A T
OR H T
AND T J
RUN
")

(defun solve-1 ()
  (let ((process (start-process (parse-program (first (day-input-lines 21)))
                                (map 'list #'char-code *program*))))
    (execute-process process)))

(defun solve-2 ()
  (let ((process (start-process (parse-program (first (day-input-lines 21)))
                                (map 'list #'char-code *program-2*))))
    (execute-process process)))
