(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (find-package :advent-util)
    (ql:quickload :advent-util)))
(defpackage #:day-16
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-16)

(defun map-with-index (function list)
  (loop :for index :from 0
        :for element :in list
        :collect (funcall function index element)))

(defun pattern-number (n position)
  (let* ((modo (mod (1+ position) (* 4 n))))
    (case (floor modo n)
      (0 0)
      (1 1)
      (2 0)
      (3 -1))))

(defun first-digit (n)
  (mod (abs n) 10))

(defun calculate-digit (phase-input pos)
  (first-digit (reduce #'+ (map-with-index (lambda (i value)
                                             (* (pattern-number pos i)
                                                value))
                                           phase-input))))

(defun apply-phase (phase-input)
  (map-with-index (lambda (index el)
                    (declare (ignore el))
                    ;(print index)
                    (calculate-digit phase-input (1+ index)))
                  phase-input))

(defun parse-input (input)
  (map 'list #'digit-char-p input))

(defun take (list n)
  (loop :repeat n
        :for x :in list
        :collect x))

(defun solve-1 (&optional (input (first (day-input-lines 16))))
  (loop :repeat 101
        :for phase := (parse-input input) :then (apply-phase phase)
        :finally (return (take phase 8))))

(defparameter *multiplier* 10000)

(defun collect-input (input)
  (loop :repeat *multiplier*
        :appending (parse-input input)))

(defun mes-offset (input)
  (parse-integer (subseq input 0 7)))

(defun apply-phase-assumed (phase &optional (sum-until 0) acc)
  (if (endp phase)
      (reverse acc)
      (let ((new-sum (+ sum-until (first phase))))
        (apply-phase-assumed (rest phase)
                             new-sum
                             (cons (first-digit new-sum)
                                   acc)))))

(defun solve-2 (&optional (input (first (day-input-lines 16))))
  (let* ((message-offset (mes-offset input))
         (parsed (nthcdr message-offset (collect-input input))))
    (loop :repeat 101
          :for x :from 0
          :for phase := (reverse parsed) :then (apply-phase-assumed phase)
          :finally (return (values ;phase
                                   (take phase 8)
                                   (take (reverse phase) 8))))))
