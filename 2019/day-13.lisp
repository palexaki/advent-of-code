(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (find-package :advent-util)
    (ql:quickload :advent-util))
  (unless (find-package :png)
    (ql:quickload :png)))
(defpackage #:day-13
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-13)


(defun scaled (position)
  (let* ((scale 8)
         (scaled-position (mapcar (alexandria:curry #'* scale)
                                  position)))
    (mapcar (lambda (delta)
              (pos-add delta scaled-position))
            (loop :for x :below scale
                  :append (loop :for y :below scale
                                :collect (list x y))))))

(defun fill-image (tiles image min-tile)
  (mapcar (lambda+ ((pos . tile))
            (mapcar (lambda+ ((x y))
                      (setf (aref image y x 0)
                            (case tile
                              (0 0)
                              (1 30)
                              (2 170)
                              (3 200)
                              (4 255))))
                    (scaled (pos-sub pos min-tile))))
          tiles))

(defun map-to-pic (n map)
  (let* ((tiles (map-hash-collect bag map (pos type)
                  (push (cons pos type) bag)))
         (max-tile (max-tile tiles))
         (min-tile (min-tile tiles))
         (size (mapcar (alexandria:curry #'* 8)
                       (mapcar (alexandria:curry #'+ 1)
                               (pos-sub max-tile min-tile))))
         (image (png:make-image (pos-y size) (pos-x size) 1)))
    (fill-image tiles image min-tile)
    (with-open-file (stream (format nil "screen/~5,'0d.png" n) :direction :output
                                                        :element-type '(unsigned-byte 8)
                                                        :if-exists :supersede)
      (png:encode image stream))))

(defun max-tile (tiles)
  (reduce (lambda+ ((max-x max-y) ((new-x new-y) . tile-2))
            (declare (ignore tile-2))
            (list (max max-x new-x)
                  (max max-y new-y)))
          tiles
          :initial-value (caar tiles)))

(defun min-tile (tiles)
  (reduce (lambda+ ((max-x max-y) ((new-x new-y) . tile-2))
            (declare (ignore tile-2))
            (list (min max-x new-x)
                  (min max-y new-y)))
          tiles
          :initial-value (caar tiles)))

(defun fill-screen (tiles screen-array)
  (mapcar (lambda+ ((pos . tile))
            (setf (board-position screen-array pos) tile))
          tiles))

(defun print-screen-array (screen-array)
  (dotimes (y (array-dimension screen-array 0))
    (dotimes (x (array-dimension screen-array 1))
      (case (aref screen-array y x)
        (0 (format t " "))
        (1 (format t "W"))
        (2 (format t "B"))
        (3 (format t "-"))
        (4 (format t "*"))
        ((nil) (format t " "))))
    (format t "~%")))

(defun print-screen (screen)
  (let* ((tiles (map-hash-collect bag screen (pos tile)
                  (push (cons pos tile) bag)))
         (max-tile (max-tile tiles))
         (screen-array (make-array (reverse (mapcar #'1+ max-tile)))))
    (fill-screen tiles screen-array)
    (print-screen-array screen-array)
    (values)))

(defun go-to? (screen)
  (let* ((tiles (map-hash-collect bag screen (pos tile)
                   (when (or (= tile 3) (= tile 4))
                     (push (cons tile pos) bag))))
         (paddle-pos (second (assoc 3 tiles)))
         (ball-pos (second (assoc 4 tiles))))
    (cond ((< paddle-pos ball-pos) 1)
          ((= paddle-pos ball-pos) 0)
          ((> paddle-pos ball-pos) -1))))

(defun execute-program (program &optional coins images)
  (let ((process (start-process program))
        position
        (screen (make-hash-table :test 'equal))
        (n 0))
    (when coins
      (setf (aref (process-program process) 0) coins))
    (flet ((input ()
             (incf n)
             (when images
               (map-to-pic n screen))
             (go-to? screen))
           (output (number)
             (cond ((null position) (setf position (list number)))
                   ((= 1 (length position)) (setf position (append position
                                                                   (list number))))
                   ((equal position '(-1 0)) (setf position nil)
                    (print number))
                   (t (setf (gethash position screen) number
                               position nil)))))
      (add-operator process 3 #'input 0 1 2)
      (add-operator process 4 #'output 1 nil 2)
      (execute-process process)
      screen)))

(defun solve-1 (&optional (input (first (day-input-lines 13))))
  (let ((screen  (execute-program (parse-program input))))
    screen
    (length (map-hash-collect bag screen (position tile)
              (when (= tile 2)
                (push position bag))))))

(defun solve-2 (&optional (input (first (day-input-lines 13))))
  (execute-program (parse-program input) 2 t))
