(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util)
  (ql:quickload :png))
(defpackage #:day-8
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-8)

(defparameter *width* 25)
(defparameter *height* 6)
(defparameter *input* (first (day-input-lines 8)))

(defun split-into-layers (&optional (input *input*)
                            (width *width*) (height *height*)
                            (layer 0)
                            layers)
  (let ((start (* layer width height))
        (end (* (1+ layer) width height)))
    (cond ((>= start (length input)) (reverse layers))
          (t (split-into-layers input width height (1+ layer)
                                (cons (subseq input start end)
                                      layers))))))

(defun fill-image (layers &optional (image (make-list (length (first layers))
                                                      :initial-element #\2)))
  (if layers
      (fill-image (rest layers)
                  (map 'string
                       (lambda (layer-data image-data)
                         (cond ((char= #\2 image-data) layer-data)
                               (t image-data)))
                       (first layers)
                          image))
      image))

(defun solve-1 (&optional (input *input*)
                  (width *width*) (height *height*))
  (let ((min-layer (cdr (first (sort (mapcar (lambda (layer)
                                               (cons (count #\0
                                                            layer)
                                                     layer))
                                             (split-into-layers input width height))
                                     #'< :key #'car)))))
    (* (count #\1 min-layer) (count #\2 min-layer))))

(defun solve-2 ()
  (split-into-layers (fill-image (split-into-layers))
                     *width*
                     1))


(defun image-into-png (image width height)
  (let ((png (png:make-image height width 1)))
    (loop :for i :below height
          :for row :in image
          :do (loop :for j :below width
                    :for col :across row
                    :do (setf (aref png i j 0) (if (char= #\0 col)
                                                   0
                                                   255))))
    png))

(defun save-image (image &optional (file #P"~/day-8.png"))
  (with-open-file (stream file :direction :output
                               :element-type '(unsigned-byte 8)
                               :if-exists :supersede)
    (png:encode image stream)))
