(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util)
  (ql:quickload :queues)
  (ql:quickload :png)
  (ql:quickload :queues.simple-queue))
(defpackage #:day-15
  (:use #:advent-util
        #:cl
        #:queues
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-15)

(defun max-tile (tiles)
  (reduce (lambda+ ((max-x max-y) ((new-x new-y) . tile-2))
            (declare (ignore tile-2))
            (list (max max-x new-x)
                  (max max-y new-y)))
          tiles
          :initial-value (caar tiles)))

(defun min-tile (tiles)
  (reduce (lambda+ ((max-x max-y) ((new-x new-y) . tile-2))
            (declare (ignore tile-2))
            (list (min max-x new-x)
                  (min max-y new-y)))
          tiles
          :initial-value (caar tiles)))

(defun fill-screen (tiles screen-array min-tile)
  (mapcar (lambda+ ((pos . tile))
            (setf (board-position screen-array (pos-sub pos
                                                        min-tile))
                  tile))
          tiles))

(defun print-screen-array (screen-array)
  (dotimes (y (array-dimension screen-array 0))
    (dotimes (x (array-dimension screen-array 1))
      (if (and (= x 17)
               (= y 5)
               )
          (format t "0")
          (ecase (aref screen-array y x)
            (:wall (format t "#"))
            (:possible (format t "."))
            ((nil) (format t " "))
            (:oxygen (format t "O")))))
    (format t "~%")))


(defun print-screen (screen)
  (let* ((tiles (map-hash-collect bag screen (pos tile)
                  (push (cons pos tile) bag)))
         (max-tile (max-tile tiles))
         (min-tile (min-tile tiles))
         (screen-array (make-array (reverse (mapcar #'1+ (pos-sub max-tile
                                                                  min-tile)))
                                   :initial-element nil)))
    (fill-screen tiles screen-array min-tile)
    (print-screen-array screen-array)))

(defun pos-neigh (map position)
  (remove-if (lambda (neighbor)
               (eq :wall (gethash neighbor map)))
             (unsafe-pos-neighbors position)))

(defun undiscovered-pos-neighbors (map position)
  (remove-if (lambda (neighbor)
               (gethash neighbor map))
             (unsafe-pos-neighbors position)))

(defun undiscovered-map-neighbors (map)
  (map-hash-collect undiscovered map (position type)
    (when (eq type :possible)
      (mapcar (lambda (neighbor)
                (push neighbor undiscovered))
              (undiscovered-pos-neighbors map position)))))

(defun delta-to-direction (delta)
  (cond ((equal delta '(0 1)) 1)
        ((equal delta '(0 -1)) 2)
        ((equal delta '(1 0)) 4)
        ((equal delta '(-1 0)) 3)
        (t (error "No possible"))))

(defun find-next-move (predecessors goal start)
  (loop :with path
        :for node := goal :then prev
        :for prev := (gethash node predecessors)
        :until (equal start prev)
        :do (push (pos-sub node prev) path)
        :finally (return (cons (pos-sub node start) path))))

(defun bfs (map start goal)
  (declare (optimize (debug 3)))
  (when (or (null goal) (eq (gethash goal map) :wall)
            (equal start goal))
    (return-from bfs nil))
  (loop :with queue := (make-queue :simple-queue)
        :with visited := (make-hash-table :test 'equal)
        :with predecessors := (make-hash-table :test 'equal)
          :initially (qpush queue (list start 0))
        :for (node distance) := (qpop queue)
        :when (equal node goal)
          :return (values (find-next-move predecessors goal start)
                          distance)
        :do
           (setf (gethash node visited) t)
           (loop :for neighbor :in (pos-neigh map node)
                 :when (and (not (gethash neighbor visited))
                            (or (gethash neighbor map)
                                (equal neighbor goal)))
                    :do
                       (setf (gethash neighbor predecessors) node)
                       (qpush queue (list neighbor
                                          (1+ distance))))
        :while (/= 0 (qsize queue))))

(defun control-repair-droid (program &optional (find-oxygen t))
  (let* ((process (start-process program))
         (map (make-hash-table :test 'equal))
         (pos '(0 0))
         undiscovered-neighbors
         next-pos
         (next-goal pos)
         path)
    (setf (gethash pos map) :possible)
    (labels ((get-status (status-code)
               ;(print (list status-code next-pos))
               (case status-code
                 (0
                  (setf (gethash next-pos map) :wall)
                  (setf undiscovered-neighbors (undiscovered-map-neighbors map))
                  (next-undiscovered)
                  (setf path (bfs map pos next-goal)))
                 (1 (setf (gethash next-pos map) :possible
                          pos next-pos
                          next-pos nil))
                 (2 (if find-oxygen
                        (return-from control-repair-droid
                          (values (nth-value 1 (bfs map '(0 0)
                                                    next-pos))
                                  map
                                  next-pos))
                        (get-status 1))))
               (when (null (undiscovered-map-neighbors map))
                 (return-from control-repair-droid map)))
             (ensure-undiscovered ()
               (unless undiscovered-neighbors
                 (setf undiscovered-neighbors (undiscovered-map-neighbors map))))
             (next-undiscovered ()
               (let ((next (pop undiscovered-neighbors)))
                 (case (gethash next map)
                   ((nil) (setf next-goal next))
                   ((:wall :possible) (next-undiscovered)))))
             (next-movement ()
               (when (equal pos next-goal)
                 (next-undiscovered)
                 (setf path (bfs map pos next-goal)))
               (when (null next-goal)
                 (ensure-undiscovered)
                 (next-undiscovered)
                 (setf path (bfs map pos next-goal)))
               (let ((next-delta (pop path)))
                 (setf next-pos (pos-add pos next-delta))
                 (delta-to-direction next-delta))))
      (add-operator process 3 #'next-movement
                    0 1 2)
      (add-operator process 4 #'get-status
                    1 nil 2)
      (execute-process process))))

(defun unoxygened-pos-neighbors (map position)
  (remove-if-not (lambda (neighbor)
                   (eq (gethash neighbor map) :possible))
                 (unsafe-pos-neighbors position)))

(defun unoxygened-map-neighbors (map)
  (map-hash-collect undiscovered map (position type)
    (when (eq type :oxygen)
      (mapcar (lambda (neighbor)
                (push neighbor undiscovered))
              (unoxygened-pos-neighbors map position)))))

(defun flood-oxygen (map oxygen-system)
  (setf (gethash oxygen-system map) :oxygen)
  (loop :for x :from 0
        :for unoxygened := (unoxygened-map-neighbors map)
        :do (map-to-pic x map)
        :unless unoxygened
          :return x
        :do
           (mapcar (lambda (position)
                      (setf (gethash position map) :oxygen))
                   unoxygened)))

(defun scaled (position)
  (let* ((scale 8)
         (scaled-position (mapcar (alexandria:curry #'* scale)
                                  position)))
    (mapcar (lambda (delta)
              (pos-add delta scaled-position))
            (loop :for x :below scale
                  :append (loop :for y :below scale
                                :collect (list x y))))))

(defun fill-image (tiles image min-tile)
  (mapcar (lambda+ ((pos . tile))
            (mapcar (lambda+ ((x y))
                      (setf (aref image y x 0)
                            (case tile
                              (:possible 255)
                              (:wall 0)
                              (:oxygen 170)
                              )))
                    (scaled (pos-sub pos min-tile))))
          tiles))

(defun map-to-pic (n map)
  (let* ((tiles (map-hash-collect bag map (pos type)
                  (push (cons pos type) bag)))
         (max-tile (max-tile tiles))
         (min-tile (min-tile tiles))
         (size (mapcar (alexandria:curry #'* 8)
                       (mapcar (alexandria:curry #'+ 1)
                               (pos-sub max-tile min-tile))))
         (image (png:make-image (pos-y size) (pos-x size) 1)))
    (fill-image tiles image min-tile)
    (with-open-file (stream (format nil "map/~3,'0d.png" n) :direction :output
                                                        :element-type '(unsigned-byte 8)
                                                        :if-exists :supersede)
      (png:encode image stream))))

(defun solve-1 (&optional (input (first (day-input-lines 15))))
  (control-repair-droid (parse-program input)))

(defun solve-2 (&optional (input (first (day-input-lines 15))))
  (let ((map (control-repair-droid (parse-program input) nil))
        (oxygen-system (nth-value 2 (solve-1))))
    (flood-oxygen map oxygen-system)))
