(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-7
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        #:alexandria
        ))
(in-package #:day-7)


(defun parse-input (&optional (input (first (day-input-lines 7))))
  (map 'vector #'parse-integer (cl-ppcre:split "," input)))

(defun solve-1 (&optional (input (parse-input)))
  (flet ((reducer (output phase)
           (first
            (execute-process
             (start-process input (list phase output))))))
    (let (bag)
      (map-permutations (lambda (phase-sequence)
                          (push (cons (reduce #'reducer phase-sequence
                                              :initial-value 0)
                                      phase-sequence)
                                bag))
                        '(0 1 2 3 4))
      (reduce #'max bag :key #'first))))

(defun partially-execute (process input)
  (flet ((outputter (output)
           (incf (process-pc process) 2)
           (return-from partially-execute output)))
    (setf (process-input process) input)
    (add-operator process 4 #'outputter 1 nil 2)
    (execute-process process)
    (values)))

(defun amps (input)
  (let ((list (loop :repeat 5 :collect (advent-util::start-process input))))
    (setf (cdr (last list)) list)
    list))

(defun use-phase (phase-sequence input)
  (let ((amps (amps input)))
    (flet ((first-pass ()
             (reduce (lambda+ (output (phase amp-process))
                       (let ((res (partially-execute amp-process
                                                     (list phase output))))
                         res))
                     (mapcar #'list phase-sequence amps)
                     :initial-value 0)))
      (loop :for amp-process :in amps
            :for output := (first-pass) :then inter-out
            :for inter-out := (partially-execute amp-process
                                                 (list output))
            :do (if (not (numberp inter-out))
                    (return output))))))

(defun solve-2 (&optional (input (parse-input)))
  (let (bag)
    (map-permutations (lambda (phase-sequence)
                        (push (cons (use-phase phase-sequence (copy-seq input))
                                    phase-sequence)
                              bag))
                      '(5 6 7 8 9))
    (reduce #'max bag :key #'first)))
