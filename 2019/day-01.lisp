(cl:defpackage #:day-1
  (:use #:cl #:advent-util))

(in-package :day-1)

(defun fuel-for-mass (mass)
  (let ((fuel  (- (floor mass 3) 2)))
    (if (plusp fuel)
        fuel
        0)))

(defun total-fuel-requirements (mass)
  (loop :for fuel := (fuel-for-mass mass) :then (fuel-for-mass fuel)
        :while (plusp fuel)
        :summing fuel))

(defun solve-1 ()
  (with-open-file (stream #P"~/code/lisp/advent/2019/input/1")
    (loop :for mass := (read stream nil)
          :while mass
          :summing (fuel-for-mass mass))))

(defun solve-2 ()
  (with-open-file (stream #P"~/code/lisp/advent/2019/input/1")
    (loop :for mass := (read stream nil)
          :while mass
          :summing (total-fuel-requirements mass))))


(defun solve-2-recursive ()
  (labels ((fuel (mass &optional (acc 0))
             (let ((fuel (- (floor mass 3) 2)))
               (if (plusp fuel)
                   (fuel fuel (+ fuel acc))
                   acc))))
    (reduce #'+ (mapcar #'fuel (mapcar #'parse-integer (day-input-lines 1))))))
