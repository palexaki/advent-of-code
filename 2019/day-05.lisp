(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))

(defpackage #:day-5
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))
(in-package #:day-5)

(defparameter *test* (read-program "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99"))

(defun execute (prog-input &optional
                             (input (parse-program
                                     (first
                                      (day-input-lines 5)))))
  (let ((input (start-process input (list prog-input))))
    (execute-process input)))

(defun solve-1 ()
  (execute 1))

(defun solve-2 ()
  (execute 5))
