(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :parseq)
  (ql:quickload :advent-util))
(defpackage #:day-1
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-1)

(parseq:defrule directions ()
    (+ (and direction (? ", ")))
  (:lambda (&rest directions)
    (mapcar #'first directions)))

(parseq:defrule direction ()
    (and (or right left)
         (+ digit))
  (:lambda (dir amount) (list dir (parse-integer (coerce amount 'string)))))

(parseq:defrule right ()
    "R"
  (:constant #c(0 -1)))

(parseq:defrule left ()
    "L"
  (:constant #c(0 1)))

(defun solve-1 (&optional (input (day-input-lines 1 2016)))
  (reduce (lambda+ ((pos dir) (turn amount))
            (let ((new-dir (* dir turn)))
              (print pos)
              (list (+ pos (* new-dir amount))
                    new-dir)))
          (parseq:parseq 'directions (first input))
          :initial-value (list 0 1)))


(defun cramers-rule (a1 b1 c1 a2 b2 c2)
  ;(print (list a1 b1 c1))
  ;(print (list a2 b2 c2))
  (let ((inter (- (* a1 b2) (* b1 a2))))
    (if (= inter 0)
        nil
        (list (/ (- (* c1 b2) (* c2 b1)) inter)
              (/ (- (* a1 c2) (* a2 c1)) inter)))))

(defun intersect (a b c d)
  (destructuring-bind (&optional s r)
      (cramers-rule (- (realpart b) (realpart a))
                    (- (realpart c) (realpart d))
                    (- (realpart c) (realpart a))

                    (- (imagpart b) (imagpart a))
                    (- (imagpart c) (imagpart d))
                    (- (imagpart c) (imagpart a)))
    ;(print (list s r))
    (and s r
         (<= 0 s 1)
         (<= 0 r 1)
         (list s r))))

(defun previously-visited (position-a position-b positions)
  (declare (optimize (debug 3)))
  (when (not (endp (rest positions)))
    (alexandria:if-let ((intersect (intersect position-a position-b
                                              (first positions) (second positions))))
      (+ position-a (* (first intersect) (- position-b position-a)))
      (previously-visited position-a position-b (rest positions)))))

(defun solve-2 (&optional (input (day-input-lines 1 2016)))
  (labels ((first-location-twice (position direction instructions &optional visited)
             (when instructions
               (destructuring-bind (turn steps)
                   (first instructions)
                 (let* ((new-direction (* direction turn))
                        (new-position (+ position (* new-direction steps))))
                   (print position)
                   (alexandria:if-let ((pos (previously-visited position new-position visited)))
                       pos
                       (first-location-twice new-position new-direction (rest instructions)
                                             (cons position visited))))))))
    (first-location-twice 0 #c(0 1) (parseq:parseq 'directions (first input)))))
