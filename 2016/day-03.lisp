(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-3
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-3)

(defparameter *example* '((101 201 301) (102 202 302) (103 203 303)))


(defun groups-of (n list)
  (loop :for sub :on list :by (alexandria:curry #'nthcdr n)
        :collect (subseq sub 0 n)))


(defun valid-triangle (a b c)
  (and (< a (+ b c))
       (< b (+ a c))
       (< c (+ a b))))

(defun solve-1 (&optional (input (day-input-lines 3 2016)))
  (count-if (lambda (triangle)
              (apply #'valid-triangle triangle))
            (mapcar #'ints input)))

(defun solve-2 (&optional (input (day-input-lines 3 2016)))
  (let ((integered (mapcar #'ints input)))
    (count-if (lambda (triangle)
                (apply #'valid-triangle triangle))
              (groups-of 3
                         (nconc (mapcar #'first integered)
                                (mapcar #'second integered)
                                (mapcar #'third integered))))))
