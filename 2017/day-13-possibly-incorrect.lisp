(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :cl-ppcre))
(defpackage #:day-13
  (:use #:cl #:cl-ppcre))

(in-package #:day-13)

(defparameter *input-file* #P"./day-13-input.txt")
(defparameter *scanner* (create-scanner "([0-9]+): ([0-9]+)"))

(defstruct scanner
  (position 0 :type fixnum)
  (length (error "Provide a length for security scanner") :type fixnum)
  (direction :down)
  (id (error "Provide an id for security scanner")))

(defstruct scanner-c
  (position 0 :type fixnum)
  (length (error "Provide a length for security scanner") :type fixnum)
  (direction :down)
  (id (error "Provide an id for security scanner")))

(defun step-scanner (scanner)
  (with-slots (direction position length) scanner
    (if (eq direction :down)
        (incf position)
        (decf position))
    (when (< position 0)
      (setf position 1
            direction :down))
    (when (= position length)
      (setf position (- length 2)
            direction :up)))
  scanner)

(defun read-scanner (string)
  (let* ((scanner-info (nth-value 1 (scan-to-strings *scanner* string)))
         (id (parse-integer (aref scanner-info 0)))
         (length (parse-integer (aref scanner-info 1))))
    (make-scanner-c :length length :id id)))

(defun read-file (stream)
  (loop :for line := (read-line stream nil)
        :while line :collect line))
13507

(defun parse-input-file ()
  (let ((input (with-open-file (stream *input-file*)
                 (read-file stream))))
    (mapcar #'read-scanner input)))

(defun get-scanner (id scanners)
  (find-if (lambda (scanner)
             (eql (scanner-c-id scanner) id))
           scanners))

(defun max-scanner (scanners)
  (loop :for scanner :in scanners
        :maximizing (scanner-c-id scanner)))

(defun run-packet-through (scanners delay &optional break-p)
  (dotimes (i (mod delay (apply #'lcm (mapcar (lambda (scanner)
                                                (* (1- (scanner-c-length scanner))
                                                   2))
                                              scanners))))
    (mapcar #'step-scanner scanners))
  (let ((max (1+ (max-scanner scanners)))
        (severity 0))
    (dotimes (pos max)
      (when (and break-p
                 (plusp severity))
        (return-from run-packet-through severity))
      (let ((current-scanner (get-scanner pos scanners)))
        (when current-scanner
          (incf severity (* pos (scanner-c-length current-scanner)))))
      (mapcar #'step-scanner scanners))
    severity))

(defun reset-scanner (scanner)
  (setf (scanner-c-position scanner) 0
        (scanner-c-direction scanner) :down))

(defun solve-1 ()
  (let* ((scanners (parse-input-file))
         (max (1+ (max-scanner scanners)))
         (severity 0))
    (dotimes (pos max)
      (let ((current-scanner (get-scanner pos scanners)))
        (print pos)
        (print current-scanner)
        (when (and current-scanner
                   (= 0 (scanner-c-position current-scanner)))
          (incf severity (* pos (scanner-c-length current-scanner))))
        (mapcar #'step-scanner scanners)))
    severity
    (run-packet-through (parse-input-file) 0)))


(defun will-run-clean-scanner (scanner delay)
  (if (zerop (scanner-c-id scanner))
      t
      (not (zerop (mod (+ delay (scanner-c-id scanner))
                       (1- (* (scanner-c-length scanner) 2)))))))

(defun will-run-clean (scanners delay)
  (every (lambda (scanner)
           (will-run-clean-scanner scanner delay))
         scanners))

(defun xor (a b)
  (or (and (not a)
           b)
      (and a
           (not b))))

(defun solve-2 ()
  (let ((scanners (parse-input-file)))
    (loop :for delay :from 0
          :for severity := (run-packet-through scanners delay t)
          :do (mapcar #'reset-scanner scanners)
          ;:when (xor (zerop severity) (will-run-clean scanners delay))
          ;  :do (format t "Inconsistency in delay: ~a" delay)
                                        :when (= 0 severity) :do (return-from solve-2 delay)
          ;:do
                           #|(when (will-run-clean scanners delay)
                             (format t "Will run clean with delay: ~a~%" delay)
                             (return-from solve-2 delay))|#)))

(defun solve-3 ()
  (let ((scanners (parse-input-file)))
    (loop :for delay :upfrom 0
          :until (loop :for scanner :in scanners
                       :for position := (scanner-c-id scanner)
                       :for length := (scanner-c-length scanner)
                       :never (zerop (mod (+ position delay)
                                          (max 1
                                               (+ length length -2)))))
          :finally (return delay))))
