(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-17
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-17)

(defun spinlock (steps times)
  (let ((buf (list 0)))
    (setf (cdr buf) buf)
    (dotimes (i times)
      (setf buf (nthcdr steps buf))
      (setf (cdr buf) (cons (1+ i)
                            (cdr buf))
            buf (cdr buf)))
    buf))

(defun solve-1 (&optional (input (day-input-lines 17 2017)))
  (cadr (spinlock (parse-integer (first input)) 2017)))
(defun solve-2 (&optional (input (day-input-lines 17 2017)))
  (let ((steps (parse-integer (first input))
               ))
    (loop :with num-after-0 := 0
          :with pos := 0
          :for val :from 1 :upto 50000000
          :do (let ((new-pos (mod (+ pos steps) val)))
                ;(print new-pos)
                (when (= new-pos 0)
                  (setf num-after-0 val))
                (setf pos (1+ new-pos)))
              ;(print num-after-0)
          :finally (return num-after-0))))
