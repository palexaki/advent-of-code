#!/bin/sbcl --script

(defvar *avc*)
(defvar *steps* 0)

(defun read-file (file)
  (loop for line = (read-line file nil)
        while line
        collecting (parse-integer line)))

(defun perform-jump (list position)
  (incf *steps*)
  (let ((jump (aref list position))
        (delta 1))
    (when (>= jump 3)
      (setf delta -1))
    (incf (aref list position) delta)
    (+ position jump)))

(with-open-file (advent "./avc.oo")
  (let ((avclist (read-file advent)))
    (setq *avc* (make-array (length avclist) :initial-contents avclist))))


(loop with position = 0
      while (and (<= position position)
                 (< position (length *avc*)))
      do (setq position (perform-jump *avc* position)))

(format t "Amount of steps needed: ~a~%" *steps*)
(format t "~a~%" (length *avc*))
