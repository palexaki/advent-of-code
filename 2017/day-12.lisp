(defpackage #:day-12
  (:use #:cl #:queues))

(in-package #:day-12)

(defun read-record (stream map)
  (let ((record (read stream nil)))
    (when record
      (setf (gethash (first record) map) (rest record)))))


(defun read-connections (stream)
  (let ((hash-table (make-hash-table)))
    (loop while (read-record stream hash-table))
    hash-table))


(defun find-island-from (map start)
  (let ((queue (make-queue :simple-queue))
        (found (make-hash-table)))
    (qpush queue start)
    (loop for next = (qpop queue nil)
          for paths = (gethash next map)
          while next
          unless (gethash next found)
            do (loop for to in paths
                     do (qpush queue to))
               (setf (gethash next found) t))
    (loop for node being each hash-key in found
          collecting node)))

(defun day-12-1 (filespec)
  (let* ((map (with-open-file (stream filespec)
                (read-connections stream)))
         (ids (loop for key being each hash-key in map
                    collecting key)))
    (loop for islands from 0
          while ids
          do (setf ids (set-difference ids (find-island-from map (first ids))))
          finally (return islands))))
