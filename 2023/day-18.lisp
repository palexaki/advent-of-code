(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-18
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-18)

(defrule step ()
    (and (char "URDL") " " int  " (#" color ")")
  (:choose 0 2 4))

(defrule color ()
    (and (rep 5 alphanumeric) alphanumeric)
  (:lambda (steps dir)
    (list (case dir
            (#\0 #\R)
            (#\1 #\D)
            (#\2 #\L)
            (#\3 #\U))
          (parse-integer (coerce steps 'string) :radix 16))))

(defparameter +example-1+ '("R 6 (#70c710)"
                            "D 5 (#0dc571)"
                            "L 2 (#5713f0)"
                            "D 2 (#d2c081)"
                            "R 2 (#59c680)"
                            "D 2 (#411b91)"
                            "L 5 (#8ceee2)"
                            "U 2 (#caa173)"
                            "L 1 (#1b58a2)"
                            "U 2 (#caa171)"
                            "R 2 (#7807d2)"
                            "U 3 (#a77fa3)"
                            "L 2 (#015232)"
                            "U 2 (#7a21e3)"))

(defparameter +direction-map+ '((#\U . :up) (#\R . :right) (#\D . :down) (#\L . :left)))

(defun trench-points (steps)
  (iter (for (dir count color) in steps)
        (declare (ignorable color))
        (for delta = (direction->delta (cdr (assoc dir +direction-map+))))
        (for diff = (mapcar (curry #'* count) delta))
        (for point first diff then (pos-add point diff))
        (collect point into ps)
        (finally (return (cons '(0 0) ps)))))

(defun trench-area (points)
  (/ (abs (iter outer
                (for (x0 y0) in points)
                (for (x1 y1) in (cdr points))
                (sum (- (* x0 y1) (* x1 y0)))))
     2))

(defun solve-1 (&optional (input (day-input-lines 18 2023)))
  (let ((steps (mapcar (curry #'parseq 'step) input)))
    (+ (trench-area (trench-points steps))
       (/ (reduce #'+ steps :key #'second) 2)
       1)))

(defun solve-2 (&optional (input (day-input-lines 18 2023)))
  (let ((steps (mapcar (compose #'third (curry #'parseq 'step)) input)))
    (+ (trench-area (trench-points steps))
       (/ (reduce #'+ steps :key #'second) 2)
       1)))
