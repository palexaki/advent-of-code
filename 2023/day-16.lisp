(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload '(:queues :queues.simple-queue))
  (ql:quickload :advent-util))
(defpackage #:day-16
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-16)

(defparameter +example-1+ '(".|...\\...."
                            "|.-.\\....."
                            ".....|-..."
                            "........|."
                            ".........."
                            ".........\\"
                            "..../.\\\\.."
                            ".-.-/..|.."
                            ".|....-|.\\"
                            "..//.|...."))

(defparameter +element-behaviour+ '((#\. (:up :up) (:right :right) (:down :down) (:left :left))
                                    (#\/ (:up :right) (:right :up) (:down :left) (:left :down))
                                    (#\\ (:up :left) (:right :down) (:down :right) (:left :up))
                                    (#\- (:up :left :right) (:right :right) (:down :left :right) (:left :left))
                                    (#\| (:up :up) (:right :up :down) (:down :down) (:left :up :down))))

(defun propagate-beam (contraption start-position start-direction)
  (iter (with queue := (q:make-queue :simple-queue))
        (with beam := (make-hash-table :test 'equal))
        (initially (q:qpush queue (cons start-position start-direction))
                   (setf (gethash start-position beam) (list start-direction)))
        (while (> (q:qsize queue) 0))
        (for (position . direction) := (q:qpop queue))
        (for tile := (board-position contraption position))
        (for next-directions := (cdr (assoc direction (cdr (assoc tile +element-behaviour+)))))
        (iter (for next-direction in next-directions)
              (for next-position := (pos-add (direction->delta next-direction)
                                             position))
              (when (and (in-board-p contraption next-position)
                         (not (find next-direction (gethash next-position beam))))
                (q:qpush queue (cons next-position next-direction))
                (push next-direction (gethash next-position beam))))
        (finally (return (hash-table-count beam)))))

(defun parse-input (input)
  (make-array (list (length input) (length (first input)))
              :initial-contents input))

(defun solve-1 (&optional (input (day-input-lines 16 2023)))
  (propagate-beam
   (parse-input input) '(0 0) :right))

(defun solve-2 (&optional (input (day-input-lines 16 2023)))
  (let* ((contraption (parse-input input))
         (height (array-dimension contraption 0))
         (width (array-dimension contraption 1)))
    (max
     (iter (for x :from 0 :below width)
           (maximize (propagate-beam contraption (list x 0) :down))
           (maximize (propagate-beam contraption (list x (1- height)) :up)))
     (iter (for y :from 0 :below height)
           (maximize (propagate-beam contraption (list 0 y) :right))
           (maximize (propagate-beam contraption (list (1- width) y) :left))))))
