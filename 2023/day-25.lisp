(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :fset)
  (ql:quickload :cl-union-find)
  (ql:quickload :advent-util))
(defpackage #:day-25
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)
                    (#:u #:union-find)))

(in-package #:day-25)


(defparameter +example-1+ '("jqt: rhn xhk nvd"
                            "rsh: frs pzl lsr"
                            "xhk: hfx"
                            "cmg: qnr nvd lhk bvb"
                            "rhn: xhk bvb hfx"
                            "bvb: xhk hfx"
                            "pzl: lsr hfx nvd"
                            "qnr: nvd"
                            "ntq: jqt hfx bvb xhk"
                            "nvd: lhk"
                            "lsr: lhk"
                            "rzs: qnr cmg lsr rsh"
                            "frs: qnr lhk lsr"))

(defun collect-edges (input)
  (iter outer
        (for line in input)
        (for (name . rest) = (r:split ":? " line))
        (iter (for other in rest)
              (in outer (collect (list name other))))))

(defun find-components (edges items)
  (let ((p (u:make-partition)))
    (iter (for item in items)
          (u:make-set p item))
    (fset:image (lambda (edge)
                  (u:union p
                           (u:find-set p (car edge))
                           (u:find-set p (cadr edge))))
                edges)
    (iter (with sets = (make-hash-table :test 'equal))
          (for item in items)
          (for s = (u:find-set p item))
          (unless (gethash s sets)
            (setf (gethash s sets) t))
          (finally (when (= 2 (hash-table-count sets))
                     (return
                       (apply #'* (mapcar (compose #'list-length (curry #'u:collect-set p))
                                          (a:hash-table-keys sets)))))))))

(defun solve-1 (&optional (input (day-input-lines 25 2023)))
  ;;; What I actually did was output the edges into a dot file, then transform
  ;;; that into an svg file, identify the 3 bridges, remove them from the edge
  ;;; list and then manually called find-components on it
  (let* ((edges (collect-edges input))
         (edge-set (fset:convert 'fset:set edges))
         (items (remove-duplicates (a:flatten edges) :test 'string=)))
    (iter (for (e1 . r1) on edges)
          (for i from 1)
          (when (zerop (mod i 20))
            (print i))
          (iter (for (e2 . r2) on r1)
                (for j from 1)
                (iter (for e3 in r2)
                      (for components = (find-components (fset:less (fset:less (fset:less edge-set e1) e2) e3)
                                                         items))
                      (when components
                        (return-from solve-1 components)))))))

(defun solve-2 (&optional (input (day-input-lines 25 2023)))
  (declare (ignore input)))
