(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util)
  (ql:quickload :iterate))

(defpackage #:day-02
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:iterate
                #:iter)
  (:import-from #:parseq
                #:parseq
                #:defrule))

(in-package #:day-02)

(defparameter +test-1+ '("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"
                         "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue"
                         "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red"
                         "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red"
                         "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"))

(defrule line ()
    (and "Game " int ": " (+ set))
  (:choose 1 3))

(defrule set ()
    (and (+ color) (? "; "))
  (:choose 0))

(defrule color ()
    (and int " " color-name (? ", "))
  (:choose 2 0)
  (:lambda (color number)
    (cons color number)))

(defrule color-name ()
    (+ alpha)
  (:string))

(defun valid-game (game max-blocks)
  (notany (lambda (set)
            (some (lambda+ ((color . number))
                      (> number
                         (cdr (assoc color max-blocks :test 'equal))))
                    set)) game))

(defun solve-1 (&optional (input (day-input-lines 2 2023)))
  (let ((games (mapcar (curry #'parseq 'line) input)))
    (reduce #'+ (remove-if-not (alexandria:rcurry #'valid-game '(("red" . 12) ("green" . 13) ("blue" . 14)))
                               games :key #'second)
            :key #'first)))

(defun power (game)
  (let ((cubes (make-hash-table :test 'equal)))
    (mapcar (lambda (set)
              (mapcar (lambda+ ((color . number))
                        (setf (gethash color cubes) (max number
                                                         (gethash color cubes 0))))
                      set))
            game)
    (reduce #'* (alexandria:hash-table-values cubes))))

(defun solve-2 (&optional (input (day-input-lines 2 2023)))
  (reduce #'+ (mapcar (compose #'power #'second (curry #'parseq 'line)) input)))
