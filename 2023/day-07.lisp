(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :iterate)
  (ql:quickload :advent-util))
(defpackage #:day-7
  (:use #:advent-util
        #:cl
        #:iterate
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria)))

(in-package #:day-7)

(defparameter +example+ '("32T3K 765"
                          "T55J5 684"
                          "KK677 28"
                          "KTJJT 220"
                          "QQQJA 483"))

(defparameter +label-strength+ '(#\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9 #\T #\J #\Q #\K #\A))

(defun label-map (label)
  (case label
    ((#\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9) label)
    (#\T #\B)
    (#\J #\C)
    (#\Q #\D)
    (#\K #\E)
    (#\A #\F)))

(defun label-map-2 (label)
  (case label
    ((#\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9) label)
    (#\T #\B)
    (#\J #\0)
    (#\Q #\D)
    (#\K #\E)
    (#\A #\F)))


(defun hand-counts (hand)
  (let ((counts (make-hash-table)))
    (iter (for c in-sequence hand)
          (incf (gethash c counts 0)))
    (sort (a:hash-table-alist counts) #'< :key #'cdr)))

(defun counts-type (counts)
  (case (length counts)
    (1 7)                             ; Five of a kind
    (2 (if (= (cdar counts) 1)
           6                          ; Four of a kind
           5))                        ; Full house
    (3 (if (and (= (cdar counts) 1)
                (= (cdadr counts) 1))
           4                          ; Three of a kind
           3))                        ; Two pair
    (4 2)                             ; One pair
    (5 1)))                           ; High card

(defun hand-type (hand)
  (let ((counts (hand-counts hand)))
    (counts-type counts)))

(defun hand-type-2 (hand)
  (let* ((counts (hand-counts hand))
         (joker-count (cdr (assoc #\0 counts)))
         (counts (remove #\0 counts :key #'car)))
    (cond ((not joker-count) (counts-type counts))
          (counts (incf (cdar (last counts)) joker-count)
                  (counts-type counts))
          (t 7))))

(defun hand< (hand1 hand2)
  (let ((type1 (hand-type hand1))
        (type2 (hand-type hand2)))
    (if (eq type1 type2)
        (string< hand1 hand2)
        (< type1 type2))))

(defun hand<-2 (hand1 hand2)
  (let ((type1 (hand-type-2 hand1))
        (type2 (hand-type-2 hand2)))
    (if (eq type1 type2)
        (string< hand1 hand2)
        (< type1 type2))))

(defun parse-input (input &optional (label-mapper #'label-map))
  (iter (for line in input)
        (for (hand bid) = (ppcre:split " " line))
        (collect (cons (map 'string label-mapper hand) (parse-integer bid)))))

(defun solve-1 (&optional (input (day-input-lines 7 2023)))
  (let ((bids (parse-input input)))
    (iter (for (nil . amount) in (sort bids #'hand< :key #'car))
          (for rank from 1)
          (sum (* rank amount)))))

(defun solve-2 (&optional (input (day-input-lines 7 2023)))
  (let ((bids (parse-input input #'label-map-2)))
    (iter (for (nil . amount) in (sort bids #'hand<-2 :key #'car))
          (for rank from 1)
          (sum (* rank amount)))))
