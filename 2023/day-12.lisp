(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-12
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule))

(in-package #:day-12)

(defparameter +example-1+ '("???.### 1,1,3"
                            ".??..??...?##. 1,1,3"
                            "?#?#?#?#?#?#?#? 1,3,1,6"
                            "????.#...#... 4,1,1"
                            "????.######..#####. 1,6,5"
                            "?###???????? 3,2,1"))

(defun parse-line (line)
  (destructuring-bind (record runs)
      (cl-ppcre:split " " line)
    (list (coerce record 'list)
          (ints runs))))

(defun count-valid-arrangements (record contiguous-groups)
  (cond ((and (null contiguous-groups) (find #\# record)) 0)
        ((null contiguous-groups) 1)
        ((> (reduce (curry #'+ 1) contiguous-groups) (length record)) 0)
        ((char= #\. (car record)) (count-valid-arrangements (cdr record) contiguous-groups))
        ((char= #\# (car record))
         (let* ((group-size (car contiguous-groups))
                (record-length (length record))
                (impossible-p (or (> group-size record-length)
                                  (find #\. record :end group-size)
                                  (when (> record-length group-size)
                                    (char= #\# (nth group-size record)))
                                  (and (= record-length group-size) (cdr contiguous-groups)))))
           (if impossible-p
               0
               (count-valid-arrangements (subseq record
                                                 (min record-length (1+ group-size)))
                                         (cdr contiguous-groups)))))
        ((char= #\? (car record))
         (+ (count-valid-arrangements (cdr record) contiguous-groups)
            (count-valid-arrangements (cons #\# (cdr record)) contiguous-groups)))
        (t (error "Invalid condition ~a ~a" record contiguous-groups))))

(defun solve-1 (&optional (input (day-input-lines 12 2023)))
  (reduce #'+
          (mapcar (compose (curry #'apply #'count-valid-arrangements)
                           #'parse-line)
                  input)))

(defun memoize (function)
  (let ((cache (make-hash-table :test 'equal)))
    (lambda (&rest arguments)
      (multiple-value-bind (value exists)
          (gethash arguments cache)
        (if exists
            value
            (setf (gethash arguments cache)
                  (apply function arguments)))))))

(defun unfold (record &optional (between nil between-supplied-p))
  (iter (repeat 5)
        (when (and between-supplied-p
                   (not (first-iteration-p)))
          (collect between))
        (appending record)))

(defun solve-2 (&optional (input (day-input-lines 12 2023)))
  (let ((unmemoized #'count-valid-arrangements))
    (setf (fdefinition 'count-valid-arrangements) (memoize #'count-valid-arrangements))
    (prog1 (reduce #'+
                   (mapcar (compose (curry #'apply #'count-valid-arrangements)
                                    (lambda+ ((conditions contiguous-groups))
                                      (list (unfold conditions #\?)
                                            (unfold contiguous-groups)))

                                    #'parse-line)
                           input))
      (setf (fdefinition 'count-valid-arrangements) unmemoized))))
