(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :zpng)
  (ql:quickload :serapeum)
  (ql:quickload :advent-util))

(defpackage #:day-21
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre) (#:s #:serapeum)))

(in-package #:day-21)

(defparameter +example-1+ '("..........."
                            ".....###.#."
                            ".###.##..#."
                            "..#.#...#.."
                            "....#.#...."
                            ".##..S####."
                            ".##..#...#."
                            ".......##.."
                            ".##.#.####."
                            ".##..##.##."
                            "..........."))

(defun find-start (map)
  (iter (for y from 0 below (array-dimension map 0))
        (iter (for x from 0 below (array-dimension map 1))
              (when (char= #\S (aref map y x))
                (return-from find-start (list x y))))))

(defun infinity-board-position (map position)
  (destructuring-bind (x y)
      position
    (destructuring-bind (height width)
        (array-dimensions map)
      (aref map (mod y height)
            (mod x width)))))

(defun next-positions (map positions &optional previous pre-previous)
  ;; (format t "~a ~%" (a:hash-table-keys visited))
  (iter outer
        (with next-positions = (make-hash-table :test 'equal))
        (for (position nil) in-hashtable positions)
        (iter (for neighbour in (unsafe-pos-neighbors position))
              (unless (or (char= #\# (infinity-board-position map neighbour))
                          (and previous (gethash neighbour previous))
                          (and pre-previous (gethash neighbour pre-previous)))
                (setf (gethash neighbour next-positions) t)))
        (finally (return-from outer next-positions))))

(defun solve-1 (&optional (input (day-input-lines 21 2023)))
  (iter (with map = (make-2d-array input))
        (for i from 0 to 64)
        (for positions first (list (find-start map)) then (next-positions map positions))
        (finally (return (length positions)))))

(defun box (map visited)
  (iter (for (x y) in (list* (mapcar (curry #'* -1) (array-dimensions map))
                             (mapcar (curry #'* 2) (array-dimensions map))
                             (a:hash-table-keys visited)))
        (minimizing y into min-y)
        (maximizing y into max-y)
        (minimizing x into min-x)
        (maximizing x into max-x)
        (finally (return (list (list min-x min-y)
                               (list max-x max-y))))))

(defun write-world-png (iteration map visited)
  (with-open-file (stream (format nil "~~/gardener/~8,'0d.png" iteration) :direction :output :element-type '(unsigned-byte 8) :if-exists :overwrite :if-does-not-exist :create)
    (destructuring-bind ((min-x min-y) (max-x max-y))
        ;; (box map visited)
        '((-285 -285) (415 415))
      (let* ((expand 2)
             (png (make-instance 'zpng:pixel-streamed-png :width (* expand (- max-x min-x -1)) :height (* expand (- max-y min-y -1)) :color-type :truecolor)))
        (zpng:start-png png stream)
        (iter (with border = (array-dimension map 0))
              (for y from min-y to max-y)
              (iter (repeat expand)
                    (iter (for x from min-x to max-x)
                          (iter (repeat expand)
                                (cond ((gethash (list x y) visited) (zpng:write-pixel (list 0 255 0) png))
                                      ((char= #\# (infinity-board-position map (list x y))) (zpng:write-pixel (list 255 255 255) png))
                                      ((or (<= (mod (1+ x) border) 1)
                                           (<= (mod (1+ y) border) 1)) (zpng:write-pixel (list 70 70 70) png))
                                      (t (zpng:write-pixel (list 0 0 0) png)))))))
        (zpng:finish-png png)))))

;; 0 1
;; 1 3
;; 2 5

(defun central-diamond (map)
  (iter (with (s-x s-y) = (find-start map))
        (with map-c = (a:copy-array map))
        (for x from 0 below (array-dimension map 1))
        (iter (for y from 0 below (array-dimension map 0))
              (when (> (dist-man (list x y) (list s-x s-y)) s-x)
                (setf (aref map-c y x) #\#)))
        (finally (return map-c))))

(defun solve-2 (&optional (input (day-input-lines 21 2023)))
  (iter (with map = (make-2d-array input))
        ;; (with visited = (make-hash-table :test 'equal))
        ;; (with end = 26501365)
        (with end = (+ (* 10 131) 65))
        (for i from 0 to end)
        (for positions
             first (a:plist-hash-table (list (find-start map) t) :test 'equal)
             then (next-positions map positions (or previous-previous-visited (make-hash-table :test 'equal)) previous-visited))
        ;; (when (first-iteration-p)
        ;;   (setf (gethash (first positions) visited) t))
        (for previous-visited previous positions)
        (for previous-previous-visited previous previous-visited)
        ;; (format t "~a~%"  (a:hash-table-keys (s:merge-tables visited previous-visited)))

        ;; (format t "~a ~a~%" i positions)
        ;; (write-world-png i map positions)
                                        ;
        (when (oddp i)
          (sum (hash-table-count positions) into sum-odd))
        (when (evenp i)
          (sum (hash-table-count positions) into sum-even))
        (when (zerop (mod (- i 65) 131))
          (format t "~a: ~a~%" (floor (- i 65) 131) (if (evenp i)
                                                        sum-even
                                                        sum-odd))
          (collect (if (evenp i) sum-even sum-odd)))
        ;; (when (zerop (mod i 50))
        ;;   (format t "~a: ~a~%" i (box map positions)))
        ))

;; 0: 3720
;; 1: 33150 = #0 + 29430
;; 2: 91890 = #1 + 58740 = #0 + 29430 + 29310
;; 3: 179940 = #2 + 88050 = #0 + 29430 + 29310 + 29310
;; 4: 297300 = #3 + 117360
;;
;; 0    3720 = x0
;; 1   33150 = x0 +  29430
;; 2   91890 = x1 +  58740 = x0 + 29430 + 58740         = x0 + 29430 + (29430 + 29310 )
;; 3  179940 = x2 +  88050 = x0 + 29430 + 58740 + 88050 = x0 + 29430 + (29430 + 29310) + (29430 + 29310 + 29310 )
;; 4  297300 = x3 + 117360
;; 5  443970 = x4 + 146670
;; 6  619950 = x5 + 175980
;; 7  825240 = x6 + 205290
;; 8 1059840 = x7 + 234600
;; 9 1323750 = x8 + 263910
;;10 1616970 = x9 + 293220
