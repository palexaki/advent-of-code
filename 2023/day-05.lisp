(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util)
  (ql:quickload :iterate))
(defpackage #:day-5
  (:use #:advent-util
        #:cl
        #:iterate
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule))

(in-package #:day-5)

(defparameter +test+ '("seeds: 79 14 55 13" ""
                       "seed-to-soil map:" "50 98 2" "52 50 48" ""
                       "soil-to-fertilizer map:" "0 15 37" "37 52 2" "39 0 15" ""
                       "fertilizer-to-water map:" "49 53 8" "0 11 42" "42 0 7" "57 7 4" ""
                       "water-to-light map:" "88 18 7" "18 25 70" ""
                       "light-to-temperature map:" "45 77 23" "81 45 19" "68 64 13" ""
                       "temperature-to-humidity map:" "0 69 1" "1 0 69" ""
                       "humidity-to-location map:" "60 56 37" "56 93 4"))

(defrule almanac ()
    (and (string seeds)
         (+ map))
  (:lambda (seeds maps) (cons (first seeds) maps)))

(defrule seeds ()
    (and "seeds: " ints)
  (:choose 1))

(defrule map ()
    (and ""
         (string map-name)
         (+ (string ints)))
  (:choose 1 2)
  (:lambda (map-name ranges)
    (list* (caar map-name) (cadar map-name) (mapcar #'first ranges))))

(defrule map-name ()
    (and stage-name "-to-" stage-name " map:")
  (:choose 0 2))

(defrule stage-name ()
    (+ alpha)
  (:string))

(defrule ints ()
    (+ (and (? " ") int (? " ")))
  (:lambda (&rest rest) (mapcar #'second rest)))

(defun convert (number ranges)
  (iter (for (destination-start source-start length) in ranges)
        (when (<= source-start number (+ source-start length -1))
          (return (+ destination-start (- number source-start))))
        (finally (return number))))

(defun full-convert (number maps)
  (iter (for (nil nil . ranges) in maps)
        (for n initially number then (convert n ranges))
        (finally (return n))))

(defun solve-1 (&optional (input (day-input-lines 5 2023)))
  (destructuring-bind (seeds . maps)
      (parseq 'almanac input)
    (reduce #'min (mapcar (alexandria:rcurry #'full-convert maps) seeds))))


(defun convert-range (seed maps)
  ;; Taken from https://www.reddit.com/r/adventofcode/comments/18b4b0r/comment/kc3iefl/
  (iter (with (lo hi) = seed)
        (for (destination (source-lo source-hi)) in maps)
        (cond ((< hi source-lo) (leave (list seed)))
              ((and (<= source-lo lo source-hi)
                    (< source-hi hi))
               (collect (list (+ destination (- lo source-lo))
                              (+ destination (- source-hi source-lo)))
                 into dst-ranges)
               (setf lo (1+ source-hi)))
              ((<= source-lo lo source-hi)
               (leave (list* (list (+ destination (- lo source-lo))
                                   (+ destination (- hi source-lo)))
                             dst-ranges)))
              ((and (< lo source-lo)
                    (>= hi source-lo))
               (collect (list lo (1- source-lo)) into dst-ranges)
               (setf lo source-lo)
               (cond ((<= hi source-hi)
                      (leave (list* (list lo hi) dst-ranges)))
                     (t (collect (list lo source-hi))
                        (setf lo (1+ source-hi))))))
        (finally (return (if (< source-hi lo)
                             (list* (list lo hi) dst-ranges)
                             dst-ranges)))))

(defun full-convert-ranges (seeds maps)
  (iter (for map-ranges in maps)
        (for s initially seeds then (iter (for seed in s)
                                          (appending (convert-range seed map-ranges))))
        (finally (return s))))

(defun intervalify-maps (maps)
  (iter (for (nil nil . map) in maps)
        (collect (sort (iter (for (destination source length) in map)
                             (collect (list destination
                                            (list source (+ source length -1)))))
                       #'< :key #'caadr))))

(defun intervalify-seeds (seeds)
  (iter (for (start length) on seeds by #'cddr)
        (collect (list start (+ start length -1)))))

(defun solve-2 (&optional (input (day-input-lines 5 2023)))
  (destructuring-bind (seed-ranges . maps)
      (parseq 'almanac input)
    (let ((seeds (intervalify-seeds seed-ranges))
          (maps (intervalify-maps maps)))
      (reduce #'min (full-convert-ranges seeds maps) :key #'first))))
