(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload '(:queues :queues.priority-queue))
  (ql:quickload :advent-util))
(defpackage #:day-17
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-17)

(defparameter +example-1+ '("2413432311323"
                            "3215453535623"
                            "3255245654254"
                            "3446585845452"
                            "4546657867536"
                            "1438598798454"
                            "4457876987766"
                            "3637877979653"
                            "4654967986887"
                            "4564679986453"
                            "1224686865563"
                            "2546548887735"
                            "4322674655533"))

(defparameter +example-2+ '("111111111111"
                            "999999999991"
                            "999999999991"
                            "999999999991"
                            "999999999991"))

(defun neighbours (map position direction steps-in-current-direction)
  (iter (for (dir-changer changed-direction-p) in '((prev-direction t)
                                                    (identity nil)
                                                    (next-direction t)))
        (for next-direction = (funcall dir-changer direction))
        (for next-position = (pos-add position (direction->delta next-direction)))
        (for steps-in-direction = (if changed-direction-p 1 (1+ steps-in-current-direction)))
        (when (and (in-board-p map next-position)
                   (<= steps-in-direction 3))
          (collect (list next-position next-direction steps-in-direction)))))

(defun heat-loss-at (map position)
  (- (char-code (board-position map position))
     (char-code #\0)))

(defun ultra-neighbours (map position direction steps-in-current-direction)
  (iter (for (dir-changer changed-direction-p) in '((prev-direction t)
                                                    (identity nil)
                                                    (next-direction t)))
        (for next-direction = (funcall dir-changer direction))
        (for next-position = (pos-add position (direction->delta next-direction)))
        (for steps-in-direction = (if changed-direction-p 1 (1+ steps-in-current-direction)))
        (when (and (in-board-p map next-position)
                   (if changed-direction-p (>= steps-in-current-direction 4) t)
                   (<= steps-in-direction 10))
          (collect (list next-position next-direction steps-in-direction)))))

(defun shortest-path (map &optional (neighbour-func #'neighbours) ultra-p)
  (iter (with queue := (q:make-queue :priority-queue :compare (lambda (a b)
                                                                (< (fourth a)
                                                                   (fourth b)))))
        (with visited := (make-hash-table :test 'equal))
        (with end := (reverse (mapcar #'1- (array-dimensions map))))
        (initially (iter (for (p d s h) in `(((0 1) :down 1 ,(heat-loss-at map '(0 1)))
                                             ((1 0) :right 1 ,(heat-loss-at map '(1 0)))))
                         (q:qpush queue (list p d s h))
                         (setf (gethash (list p d s) visited) t)))
        (for (position direction steps-in-current-direction heat-loss) = (q:qpop queue))
        (iter (for (next-position next-direction next-steps) in (funcall neighbour-func map position direction steps-in-current-direction))
              (for next-loss = (+ heat-loss (- (char-code (board-position map next-position)) (char-code #\0))))
              (when (and (equal next-position end)
                         (if ultra-p (>= next-steps 4) t))
                (return-from shortest-path next-loss))
              (unless (gethash (list next-position next-direction next-steps) visited)
                (setf (gethash (list next-position next-direction next-steps) visited) t)
                (q:qpush queue (list next-position next-direction next-steps next-loss))))))

(defun solve-1 (&optional (input (day-input-lines 17 2023)))
  (shortest-path (make-2d-array input)))

(defun solve-2 (&optional (input (day-input-lines 17 2023)))
  (shortest-path (make-2d-array input) #'ultra-neighbours t))
