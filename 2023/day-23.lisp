(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload '(:queues :queues.simple-queue))
  (ql:quickload :fset)
  (ql:quickload :advent-util))
(defpackage #:day-23
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre) (#:f #:fset)))

(in-package #:day-23)

(defparameter +example-1+ '("#.#####################"
                            "#.......#########...###"
                            "#######.#########.#.###"
                            "###.....#.>.>.###.#.###"
                            "###v#####.#v#.###.#.###"
                            "###.>...#.#.#.....#...#"
                            "###v###.#.#.#########.#"
                            "###...#.#.#.......#...#"
                            "#####.#.#.#######.#.###"
                            "#.....#.#.#.......#...#"
                            "#.#####.#.#.#########v#"
                            "#.#...#...#...###...>.#"
                            "#.#.#v#######v###.###v#"
                            "#...#.>.#...>.>.#.###.#"
                            "#####v#.#.###v#.#.###.#"
                            "#.....#...#...#.#.#...#"
                            "#.#########.###.#.#.###"
                            "#...###...#...#...#.###"
                            "###.###.#.###v#####v###"
                            "#...#...#.#.>.>.#.>.###"
                            "#.###.###.#.###.#.#v###"
                            "#.....###...###...#...#"
                            "#####################.#"))

(defparameter +test-1+ '("#.##"
                         "#..#"
                         "##v#"
                         "##.#"))

(defparameter +test-2+ '("#.####"
                         "#.####"
                         "#....#"
                         "#.##.#"
                         "#..#.#"
                         "##.#.#"
                         "#..#.#"
                         "#.##.#"
                         "#....#"
                         "####.#"))

(defun slope-diff (slope)
  (case slope
    (#\> '(1 0))
    (#\v '(0 1))
    (#\< '(-1 0))
    (#\^ '(0 -1))))

(defun find-longest-path (map position end &optional (visited (f:empty-set)) (slipperyp t))
  (cond ((equal position end) 0)
        ((char= #\# (board-position map position)) nil)
        ((f:contains? visited position) nil)
        ((or (not slipperyp) (char= #\. (board-position map position)))
         (iter (for neighbour in (pos-neighbors map position))
               (a:when-let ((path (find-longest-path map neighbour end (f:with visited position) slipperyp)))
                 (maximize (1+ path)))))
        (t (a:when-let ((path (find-longest-path map
                                                 (pos-add position (slope-diff (board-position map position)))
                                                 end (f:with visited position)
                                                 slipperyp)))
             (1+ path)))))


(defun solve-1 (&optional (input (day-input-lines 23 2023)))
  (let* ((map (make-2d-array input))
         (dimensions (mapcar #'1- (array-dimensions map))))
    (find-longest-path map '(1 0) (list (1- (car dimensions)) (cadr dimensions)))))

(defun neighbours (map position)
  (remove-if (lambda (pos) (char= #\# (board-position map pos)))
             (pos-neighbors map position)))

(defun find-intersections (map)
  (iter (with intersections = (list '(1 0)
                                    (list (- (array-dimension map 1) 2)
                                          (1- (array-dimension map 0)))       ))
        (for y from 0 below (array-dimension map 0))
        (iter (for x from 0 below (array-dimension map 1))
              (for n-count = (length (neighbours map (make-pos x y))))
              (when (and (char/= #\# (aref map y x))
                         (> n-count 2))
                (push (make-pos x y) intersections)))
        (finally (return intersections))))

(defun find-shortest-path (map start end intersections)
  (iter (with distance = (make-hash-table :test 'equal))
        (with queue = (q:make-queue :simple-queue))
        (initially (q:qpush queue start)
                   (setf (gethash start distance) 0))
        (for p = (q:qpop queue))
        (for dist = (gethash p distance))
        (iter (for n in (neighbours map p))
              (when (equal n end)
                (return-from find-shortest-path (1+ dist)))
              (unless (gethash n distance)
                (q:qpush queue n)
                (setf (gethash n distance) (1+ dist))))))

(defun follow-hallway (map start next intersections)
  (iter (for prev previous p initially start)
        (for p first next then (first neighbours))
        (for neighbours = (remove prev (neighbours map p) :test 'equal))
        (for l from 1)
        (when (find p intersections :test 'equal)
          (leave (list p l)))))

(defun compress-map (map)
  (iter (with intersections = (find-intersections map))
        (with simple-map = (make-hash-table :test 'equal))
        (for p in intersections)
        (iter (for next in (neighbours map p))
              (for (pos dist) = (follow-hallway map p next intersections))
              (a:when-let ((c (assoc pos (gethash p simple-map) :test 'equal)))
                (setf (cdr c) (max dist (cdr c)))
                (next-iteration))
              (unless (equal p next)
                (push (cons pos dist) (gethash p simple-map))))
        (finally (return simple-map))))

(defun make-end (map)
  (list (- (array-dimension map 1) 2)
        (1- (array-dimension map 0))))

(defun find-longest-path-2 (map start end &optional (visited (f:empty-set)))
  (iter (for (neighbour . distance) in (gethash start map))
        (when (equal neighbour end)
          (return-from find-longest-path-2 distance))
        (unless (f:contains? visited neighbour)
          (a:when-let ((path (find-longest-path-2 map neighbour end (f:with visited start))))
            (maximize (+ path
                         distance))))))

(defun solve-2 (&optional (input (day-input-lines 23 2023)))
  (let* ((map (make-2d-array input))
         (compressed (compress-map map))
         (start '(1 0))
         (end (make-end map)))
    (find-longest-path-2 compressed start end)))
