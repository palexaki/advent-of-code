(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util)
  (ql:quickload :iterate))
(defpackage #:day-4
  (:use #:advent-util
        #:cl
        #:iterate
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule))

(in-package #:day-4)

(defparameter +test-1+ '("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53"
                         "Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19"
                         "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1"
                         "Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83"
                         "Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36"
                         "Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"))

(defrule card ()
    (and "Card" (+ " ") int ":" numbers " |" numbers)
  (:choose 2 4 6))

(defrule numbers ()
    (+ (and (+ " ") int))
  (:lambda (&rest rest)
    (mapcar #'second rest)))

(defun solve-1 (&optional (input (day-input-lines 4 2023)))
  (let ((cards (mapcar (curry #'parseq 'card) input)))
    (iter (for (nil winning-numbers my-numbers) in cards)
          (for my-winning-numbers = (length (intersection winning-numbers my-numbers)))
          (when (plusp my-winning-numbers)
            (sum (expt 2 (1- my-winning-numbers)))))))

(defun solve-2 (&optional (input (day-input-lines 4 2023)))
  (let ((cards (mapcar (curry #'parseq 'card) input)))
    (iter (with max-card = (reduce #'max cards :key #'car))
          (with copies = (make-hash-table))
          (for (i winning-numbers my-numbers) in cards)
          (for my-winning-numbers = (length (intersection winning-numbers my-numbers)))
          (for current-copies = (gethash i copies 1))
          (when (plusp my-winning-numbers)
            (iter (for x from i to (min (+ i my-winning-numbers) max-card))
                  (incf (gethash x copies 1) current-copies)))
          (sum current-copies))))
