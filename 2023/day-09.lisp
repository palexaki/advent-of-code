(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))

(defpackage #:day-9
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule))

(in-package #:day-9)

(defparameter +example+ '("0 3 6 9 12 15"
                          "1 3 6 10 15 21"
                          "10 13 16 21 30 45"))

(defun predict-next-value (sequence)
  (iter (for differences initially sequence then (mapcar #'- (rest differences) differences))
        (for i from 0)
        (until (every #'zerop differences))
        (sum (car (last differences)))))

(defun predict-previous-value (sequence)
  (reduce #'- (iter (for differences initially sequence then (mapcar #'- (rest differences) differences))
                    (for i from 0)
                    (until (every #'zerop differences))
                    (collect (car differences)))
          :from-end t))

(defun solve-1 (&optional (input (day-input-lines 9 2023)))
  (reduce #'+ (mapcar (compose #'predict-next-value #'ints) input)))

(defun solve-2 (&optional (input (day-input-lines 9 2023)))
  (reduce #'+ (mapcar (compose #'predict-previous-value #'ints) input)))
