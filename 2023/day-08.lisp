(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-8
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule))

(in-package #:day-8)

(defparameter +example-1+ '("RL"
                            ""
                            "AAA = (BBB, CCC)"
                            "BBB = (DDD, EEE)"
                            "CCC = (ZZZ, GGG)"
                            "DDD = (DDD, DDD)"
                            "EEE = (EEE, EEE)"
                            "GGG = (GGG, GGG)"
                            "ZZZ = (ZZZ, ZZZ)"))

(defparameter +example-2+ '("LLR"
                            ""
                            "AAA = (BBB, BBB)"
                            "BBB = (AAA, ZZZ)"
                            "ZZZ = (ZZZ, ZZZ)"))

(defparameter +example-3+ '("LR"
                            ""
                            "11A = (11B, XXX)"
                            "11B = (XXX, 11Z)"
                            "11Z = (11B, XXX)"
                            "22A = (22B, XXX)"
                            "22B = (22C, 22C)"
                            "22C = (22Z, 22Z)"
                            "22Z = (22B, 22B)"
                            "XXX = (XXX, XXX)"))

(defun parse-input (input)
  (flet ((parse-map-line (line)
           (coerce (nth-value 1
                              (ppcre:scan-to-strings "([1-9A-Z]+) = \\(([1-9A-Z]+), ([1-9A-Z]+)\\)"
                                                     line))
                   'list)))
    (values (first input)
            (alexandria:alist-hash-table (mapcar #'parse-map-line (cddr input))
                                         :test 'equal))))

(defun walk (instructions map &optional (start "AAA") ghost-p)
  (iter (with i-length = (length instructions))
        (for steps from 0)
        (for i initially 0 then (mod (1+ i) i-length))
        (for instruction = (if (char= #\L (aref instructions i)) 0 1))
        (for location initially start then (nth instruction (gethash location map)))
        (when (if ghost-p
                  (char= #\Z (aref location 2))
                  (string= "ZZZ" location))
          (leave steps))))

(defun solve-1 (&optional (input (day-input-lines 8 2023)))
  (multiple-value-bind (instructions map)
      (parse-input input)
    (walk instructions map)))

(defun solve-2 (&optional (input (day-input-lines 8 2023)))
  (multiple-value-bind (instructions map)
      (parse-input input)
    (apply #'lcm (mapcar (lambda (l) (walk instructions map l t))
                         (remove-if-not (lambda (str) (char= #\A (aref str 2)))
                                        (alexandria:hash-table-keys map))))))
