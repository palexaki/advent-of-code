(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload '(:queues :queues.simple-queue))
  (ql:quickload :advent-util))

(defpackage #:day-20
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-20)

(defparameter +example-1+ '("broadcaster -> a, b, c"
                            "%a -> b"
                            "%b -> c"
                            "%c -> inv"
                            "&inv -> a"))

(defparameter +example-2+ '("broadcaster -> a"
                            "%a -> inv, con"
                            "&inv -> b"
                            "%b -> con"
                            "&con -> output"))


(defclass module ()
  ((destinations :initarg :destinations
                 :initform nil
                 :accessor destinations))
  (:documentation "A module gets input processes it
  and sends the result to its destination modules, the basic module just sends
  its input as output without processing"))

(defclass flip-flop (module)
  ((state :initform nil
          :accessor state)))

(defclass conjunction (module)
  ((source-memory :initform (make-hash-table :test 'equal)
                  :accessor source-memory)))

(defmethod print-object ((module module) stream)
  (print-unreadable-object (module stream :type t :identity t)
    (format stream "destinations ~a" (destinations module))
    (when (typep module 'flip-flop)
      (format stream ", state ~:[l~;h~]" (state module)))
    (when (typep module 'conjunction)
      (format stream ", memory {~{~a: ~:[l~;h~]~^, ~}}" (a:hash-table-plist (source-memory module))))))

(defmethod handle-pulse (module pulse source)
  (declare (ignore source))
  (mapcar (a:rcurry #'cons pulse) (destinations module)))

(defmethod handle-pulse ((module flip-flop) pulse source)
  (declare (ignore source))
  (unless pulse
    (setf (state module) (not (state module)))
    (call-next-method module (state module))))

(defmethod handle-pulse ((module conjunction) pulse source)
  (let ((source-memory (source-memory module)))
    (setf (gethash source source-memory) pulse)
    (call-next-method module (some #'null (a:hash-table-values source-memory)))))

(defun add-source (module source-name)
  (when (typep module 'conjunction)
    (setf (gethash source-name (source-memory module)) nil)))

(defrule input ()
    (+ (string module))
  (:lambda (&rest modules)
    (let ((module-map (a:alist-hash-table (mapcar #'first modules)
                                          :test 'equal)))
      (iter (for (source-name module) in-hashtable module-map)
            (iter (for destination-name in (destinations module))
                  (add-source (gethash destination-name module-map) source-name)))
      module-map)))

(defrule module ()
    (and module-type name " ->" (+ (and " " name (? ","))))
  (:choose 0 1 3)
  (:lambda (module-type name destinations)
    (cons name
          (make-instance module-type
                         :destinations (mapcar #'second destinations)))))

(defrule module-type ()
    (? (char "%&"))
  (:lambda (&optional type-name)
    (case type-name
      ((nil) 'module)
      (#\% 'flip-flop)
      (#\& 'conjunction))))

(defrule name ()
    (+ alpha)
  (:string))

(defun process-button (module-map &optional register-source register-dest signal-wait)
  (iter (with signals = (q:make-queue :simple-queue))
        (initially (q:qpush signals '("button" "broadcaster" nil)))
        (for (source destination pulse) = (q:qpop signals))
        (for destination-module = (gethash destination module-map))
        ; (format t "~a -~:[low~;high~]-> ~a~%" source pulse destination)
        (counting pulse into highs)
        (counting (null pulse) into lows)
        (when (and register-source (string= source register-source) (string= destination register-dest) (eq pulse signal-wait))
          (leave t))
        (when destination-module
          (iter (for (next-destination . next-pulse) in (handle-pulse destination-module pulse source))
                (q:qpush signals (list destination next-destination next-pulse))))
        (when (zerop (q:qsize signals))
          (leave (if register-source
                     nil
                     (list highs lows))))))

(defun solve-1 (&optional (input (day-input-lines 20 2023)))
  (iter (with module-map = (parseq 'input input))
        (repeat 1000)
        (for (hs ls) = (process-button module-map))
        (sum hs into highs)
        (sum ls into lows)
        (finally (return (* highs lows)))))

(defun solve-2 (&optional (input (day-input-lines 20 2023)))
  (apply #'lcm
         (iter (with (destination sources) = (let* ((modules (a:hash-table-alist (parseq 'input input)))
                                                    (rx-source (find-if (lambda (m)
                                                                          (find "rx" (destinations (cdr m)) :test #'string=))
                                                                        modules)))
                                               (list (car rx-source)
                                                     (a:hash-table-keys (source-memory (cdr rx-source))))))
               (for module-name in sources)
               (collect
                   (iter inner
                         (with module-map = (parseq 'input input))
                         (for i from 1)
                         (when (process-button module-map module-name destination t)
                           (leave i)))))))
