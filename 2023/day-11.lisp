(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-11
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule))

(in-package #:day-11)

(defparameter +example-1+ '("...#......"
                            ".......#.."
                            "#........."
                            ".........."
                            "......#..."
                            ".#........"
                            ".........#"
                            ".........."
                            ".......#.."
                            "#...#....."))



(defun expand-observation (observation)
  (flet ((expand-horizontally (observation)
           (iter (for row in observation)
                 (collect row)
                 (when (every (curry #'char= #\.) row)
                   (collect row))))
         (transpose (observation)
           (apply #'map 'list #'list observation)))
    (transpose (expand-horizontally (transpose (expand-horizontally observation))))))

(defun print-observation-list (observation)
  (format t "~{~{~a~}~%~}~%" observation))

(defun locate-galaxies (observation)
  (iter outer
        (for row in observation)
        (for y from 0)
        (iter (for pixel in row)
              (for x from 0)
              (when (char= #\# pixel)
                (in outer (collect (list x y)))))))

(defun solve-1 (&optional (input (day-input-lines 11 2023)))
  (let ((galaxies (locate-galaxies (expand-observation input)))
        (total-distance 0))
    (alexandria:map-combinations (lambda+ ((start end)) (incf total-distance (dist-man start end)))
                                 galaxies :length 2)
    total-distance))

(defun find-empty-rows (observation)
  (iter (for row in observation)
        (for i from 0)
        (if (every (curry #'char= #\.) row)
            (collect t)
            (collect nil))))

(defun solve-2 (&optional (input (day-input-lines 11 2023)) (empty-space-increase 1000000))
  (let ((empty-rows (find-empty-rows input))
        (empty-columns (find-empty-rows (apply #'map 'list #'list input))))
    (iter outer
          (for row in input)
          (for row-empty-p in empty-rows)
          (counting row-empty-p into empty-row-count)
          (for y from 0)
          (iter (for pixel in-string row)
                (for column-empty-p in empty-columns)
                (counting column-empty-p into empty-column-count)
                (for x from 0)
                (when (char= pixel #\#)
                  (in outer (collect (list (+ (* (1- empty-space-increase) empty-column-count) x)
                                           (+ (* (1- empty-space-increase) empty-row-count) y))
                              into galaxies))))
          (finally (let ((total-distance 0))
                     (alexandria:map-combinations (lambda+ ((start end)) (incf total-distance (dist-man start end)))
                                                  galaxies :length 2)
                     (return-from outer total-distance))))))
