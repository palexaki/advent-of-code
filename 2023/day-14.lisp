(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-14
  (:use #:advent-util
        #:cl
        #:iterate)
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-14)

(defparameter +example-1+ '("O....#...."
                            "O.OO#....#"
                            ".....##..."
                            "OO.#O....O"
                            ".O.....O#."
                            "O.#..O.#.#"
                            "..O..#O..O"
                            ".......O.."
                            "#....###.."
                            "#OO..#...."))

(defun roll-rocks-northmost (column)
  (iter (for i from 0)
        (for char in-string column)
        (case char
          (#\# (setf free-spaces nil))
          (#\. (collect i into free-spaces))
          (#\O (when free-spaces
                 (setf (char column (pop free-spaces)) #\O
                       (char column i) #\.)
                 (collect i into free-spaces)))))
  column)

(defun column-load (column)
  (iter (for i downfrom (length column))
        (for char in-string column)
        (when (char= char #\O)
          (sum i))))

(defun solve-1 (&optional (input (day-input-lines 14 2023)))
  (reduce #'+
          (mapcar (compose #'column-load #'roll-rocks-northmost)
                  (apply #'map 'list (lambda (&rest items) (coerce items 'string)) input))))

(defparameter +direction-map+ '((:up    . (1 0 nil))
                                (:right . (0 1 t))
                                (:down  . (1 0 t))
                                (:left  . (0 1 nil)))
  "An association list of directions the rocks roll to with (STABLE-AXIS
  CHANGING-AXIS FROM-END)")

(defun roll-rocks (platform &optional (direction :up) copy)
  (let ((platform-copy (if copy (a:copy-array platform) platform))
        (properties (assoc direction +direction-map+)))
    (destructuring-bind (stable-axis changing-axis from-end)
        (cdr properties)
      (iter (repeat (array-dimension platform-copy stable-axis))
            (for i from 0)
            (iter (repeat (array-dimension platform-copy changing-axis))
                  (for j
                       from (if from-end (1- (array-dimension platform-copy changing-axis)) 0)
                       by (if from-end -1 1))
                  (for x = (if (= stable-axis 1) i j))
                  (for y = (if (= stable-axis 0) i j))
                  (for char = (aref platform-copy y x))
                  (case char
                    (#\# (setf free-spaces nil))
                    (#\. (collect (list y x) into free-spaces))
                    (#\O (when free-spaces
                           (setf (apply #'aref platform-copy (pop free-spaces)) #\O
                                 (aref platform-copy y x) #\.)
                           (collect (list y x) into free-spaces)))))))
    platform-copy))

(defun northern-load (platform)
  (iter outer
        (for x from 0 below (array-dimension platform 1))
        (iter (for y from 0 below (array-dimension platform 0))
              (for load downfrom (array-dimension platform 0))
              (when (char= #\O (aref platform y x))
                (in outer (sum load))))))

(defun parse-platform (input)
  (make-array (list (length input) (length (first input))) :initial-contents input))

(defun cycle (platform)
  (let ((platform-copy (a:copy-array platform)))
    (roll-rocks (roll-rocks (roll-rocks (roll-rocks platform-copy :up)
                                        :left)
                            :down)
                :right)))

(defun solve-2 (&optional (input (day-input-lines 14 2023)))
  (iter (with configurations = (make-hash-table :test 'equalp))
        (with configuration-at = (make-hash-table))
        (with iterations = 1000000000)
        (for i from 0 to iterations)
        (for platform initially (parse-platform input) then (cycle platform))
        (a:when-let* ((cycle-start (gethash platform configurations)))
          (leave (northern-load (gethash (+ cycle-start (mod (- iterations cycle-start)
                                                             (- i cycle-start)))
                                         configuration-at))))
        (setf (gethash platform configurations) i
              (gethash i configuration-at) platform)
        (finally (return (northern-load platform)))))
