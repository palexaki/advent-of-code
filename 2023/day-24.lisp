(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-24
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-24)

(defparameter +example-1+ '("19, 13, 30 @ -2,  1, -2"
                            "18, 19, 22 @ -1, -1, -2"
                            "20, 25, 34 @ -2, -2, -4"
                            "12, 31, 28 @ -1, -2, -1"
                            "20, 19, 15 @  1, -5, -3"))

(defun solve-for-s (x0 y0 a0 b0 x1 y1 a1 b1)
  (/ (- (* a0 (- y0 y1))
        (* b0 (- x0 x1)))
     (- (* a0 b1) (* a1 b0))))

(defun solve-for-t (x0 y0 a0 b0 x1 y1 a1 b1)
  (/ (- (* a1 (- y0 y1))
        (* b1 (- x0 x1)))
     (- (* a0 b1) (* a1 b0))))

(defun hailstones-intersect-p (hailstone-0 hailstone-1 min max)
  (destructuring-bind ((x0 y0 z0 a0 b0 c0) (x1 y1 z1 a1 b1 c1))
      (list hailstone-0 hailstone-1)
    (declare (ignore z0 z1 c0 c1))
    (when (or (= (* a1 b0) (* a0 b1)) (zerop b0))
      (return-from hailstones-intersect-p nil))
    (let* ((t-i (solve-for-t x0 y0 a0 b0 x1 y1 a1 b1))
           (s (solve-for-s x0 y0 a0 b0 x1 y1 a1 b1))
           (x-h0 (+ x0 (* a0 t-i)))
           (y-h0 (+ y0 (* b0 t-i)))
           (x-h1 (+ x1 (* a1 s)))
           (y-h1 (+ y1 (* b1 s))))
      (assert (and (= x-h0 x-h1) (= y-h0 y-h1)))
      (and (>= t-i 0) (>= s 0)
           (<= min x-h0 max)
           (<= min y-h0 max)))))


(defun solve-1 (&optional (input (day-input-lines 24 2023)))
  (let ((hail (mapcar #'ints input))
        (min 200000000000000)
        (max 400000000000000))
    (iter outer
          (for (h1 . rest) on hail)
          (iter (for h2 in rest)
                (in outer (counting (hailstones-intersect-p h1 h2 min max)))))))

(defun solve-2 (&optional (input (day-input-lines 24 2023)))
  ;; Feed the equations into sympy and you get your result
  (iter (for line in input)
        (for (x y z a b c) = (ints line))
        (for time in '("t" "u" "v"))
        (format t "Eq(x + (a * ~a), ~a + (~a * ~a)),~%" time x a time)
        (format t "Eq(y + (b * ~a), ~a + (~a * ~a)),~%" time y b time)
        (format t "Eq(z + (c * ~a), ~a + (~a * ~a)),~%" time z c time)))
