(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util)
  (ql:quickload '(:queues :queues.simple-queue)))
(defpackage #:day-10
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule))

(in-package #:day-10)

(defparameter +example-1+ '("....."
                            ".S-7."
                            ".|.|."
                            ".L-J."
                            "....."))

(defparameter +example-1-complicated+ '("-L|F7"
                                        "7S-7|"
                                        "L|7||"
                                        "-L-J|"
                                        "L|-JF"))

(defparameter +example-2+ '("..F7."
                            ".FJ|."
                            "SJ.L7"
                            "|F--J"
                            "LJ..."))

(defparameter +example-2-complicated+ '("7-F7-"
                                        ".FJ|7"
                                        "SJLL7"
                                        "|F--J"
                                        "LJ.LJ"))

(defparameter +example-3+ '("..........."
                            ".S-------7."
                            ".|F-----7|."
                            ".||.....||."
                            ".||.....||."
                            ".|L-7.F-J|."
                            ".|..|.|..|."
                            ".L--J.L--J."
                            "..........."))

(defparameter +example-4+ '(".F----7F7F7F7F-7...."
                            ".|F--7||||||||FJ...."
                            ".||.FJ||||||||L7...."
                            "FJL7L7LJLJ||LJ.L-7.."
                            "L--J.L7...LJS7F-7L7."
                            "....F-J..F7FJ|L7L7L7"
                            "....L7.F7||L7|.L7L7|"
                            ".....|FJLJ|FJ|F7|.LJ"
                            "....FJL-7.||.||||..."
                            "....L---J.LJ.LJLJ..."))

(defparameter +example-5+ '("FF7FSF7F7F7F7F7F---7"
                            "L|LJ||||||||||||F--J"
                            "FL-7LJLJ||||||LJL-77"
                            "F--JF--7||LJLJIF7FJ-"
                            "L---JF-JLJIIIIFJLJJ7"
                            "|F|F-JF---7IIIL7L|7|"
                            "|FFJF7L7F-JF7IIL---7"
                            "7-L-JL7||F7|L7F-7F7|"
                            "L.L7LFJ|||||FJL7||LJ"
                            "L7JLJL-JLJLJL--JLJ.L"))

(defparameter +neighbours+ '((#\| . ((0 1) (0 -1)))
                             (#\- . ((1 0) (-1 0)))
                             (#\L . ((0 -1) (1 0)))
                             (#\J . ((0 -1) (-1 0)))
                             (#\7 . ((0 1) (-1 0)))
                             (#\F . ((0 1) (1 0)))
                             (#\. . nil)
                             (#\S . ((1 0) (-1 0) (0 1) (0 -1)))))

(defun board-subscripts (board row-major-index)
  (list (floor row-major-index (array-dimension board 0))
        (mod row-major-index (array-dimension board 0))))

(defun find-start (map)
  (iter (for i from 0 below (array-total-size map))
        (when (char= #\S (row-major-aref map i))
          (leave (board-subscripts map i)))))

(defun neighbours (map position)
  (let* ((tile (board-position map position))
         (deltas (cdr (assoc tile +neighbours+)))
         (neighbours (remove-if-not (alexandria:curry #'in-board-p map)
                                    (mapcar (curry #'pos-add position) deltas))))
    (if (char= tile #\S)
        (remove-if-not (lambda (neighbour) (find position (neighbours map neighbour) :test 'equal))
                       neighbours)
        neighbours)))

(defun parse-input (input)
  (make-array (list (length input) (length (car input))) :initial-contents input))

(defun loop-tiles (map)
  (iter (with queue = (q:make-queue :simple-queue))
        (with visited = (make-hash-table :test 'equal))
        (initially (q:qpush queue (cons (find-start map) 0)))
        (for (position . distance) = (q:qpop queue))
        (for neighbours := (neighbours map position))
        (setf (gethash position visited) distance)
        (iter (for n in neighbours)
              (unless (gethash n visited)
                (q:qpush queue (cons n (1+ distance)))))
        (until (zerop (q:qsize queue)))
        (finally (return visited))))

(defun solve-1 (&optional (input (day-input-lines 10 2023)))
  (reduce #'max (alexandria:hash-table-values (loop-tiles (parse-input input)))))

(defparameter +between-neighbour-checks+ '(((-1 0) . ((0 0) (0 1)))
                                           ((1 0)  . ((1 0) (1 1)))
                                           ((0 -1) . ((0 0) (1 0)))
                                           ((0 1)  . ((0 1) (1 1)))))

(defun non-loop-section (map loop-tiles start)
  (iter (with queue = (q:make-queue :simple-queue))
        (with visited = (make-hash-table :test 'equal))
        (initially (q:qpush queue start))
        (for position = (q:qpop queue))
        (for neighbours = (pos-neighbors map position))
        (setf (gethash position visited) t)
        (iter (for n in neighbours)
              (unless (or (gethash n visited) (gethash n loop-tiles))
                (setf (gethash n visited) t)
                (q:qpush queue n)))
        (until (zerop (q:qsize queue)))
        (finally (return visited))))

(defun non-loop-islands (map loop-tiles)
  (iter (with visited = (alexandria:copy-hash-table loop-tiles))
        (for i from 0 below (array-total-size map))
        (for position = (board-subscripts map i))
        (unless (gethash position visited)
          (let ((island (non-loop-section map loop-tiles position)))
            (unless (zerop (hash-table-count island))
              (collect island)
              (iter (for (k nil) in-hashtable island)
                    (setf (gethash k visited) t)))))))

(defun hash-table-random-key (hash-table)
  (iter (for (k nil) in-hashtable hash-table)
        (leave k)))

(defun squeezed-neighbours (map loop-tiles position)
  (iter (for (delta . (check-delta-1 check-delta-2)) in +between-neighbour-checks+)
        (for check-1 = (pos-add position check-delta-1))
        (for check-2 = (pos-add position check-delta-2))
        (unless (and (gethash check-1 loop-tiles)
                     (gethash check-2 loop-tiles)
                     (find check-1 (neighbours map check-2) :test 'equal)
                     (find check-2 (neighbours map check-1) :test 'equal))
          (collect (pos-add delta position)))))

(defun edgep (map position)
  (or (< (pos-x position) 0)
      (< (pos-y position) 0)
      (>= (1+ (pos-x position)) (array-dimension map 1))
      (>= (1+ (pos-y position)) (array-dimension map 0))))

(defun island-enclosed-p (map loop-tiles island)
  (iter (with visited = (make-hash-table :test 'equal))
        (with queue = (q:make-queue :simple-queue))
        (initially (q:qpush queue (hash-table-random-key island)))
        (for x from 0)
        (for position = (q:qpop queue))
        (when (edgep map position)
          (leave nil))
        (for neighbours = (squeezed-neighbours map loop-tiles position))
        (iter (for n in neighbours)
              (unless (gethash n visited)
                (setf (gethash n visited) t)
                (q:qpush queue n)))
        (until (zerop (q:qsize queue)))
        (finally (return t))))

(defun solve-2 (&optional (input (day-input-lines 10 2023)))
  (let* ((map (parse-input input))
         (loop-tiles (loop-tiles map))
         (islands (non-loop-islands map loop-tiles)))
    (iter (for island in islands)
          (when (island-enclosed-p map loop-tiles island)
            (sum (hash-table-count island))))))
