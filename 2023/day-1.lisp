(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :cl-ppcre)
  (ql:quickload :advent-util))
(defpackage #:day-01
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule))


(in-package #:day-01)

(defparameter +digits+ '("zero|0"
                        "one|1"
                        "two|2"
                        "three|3"
                        "four|4"
                        "five|5"
                        "six|6"
                        "seven|7"
                        "eight|8"
                        "nine|9"))

(defun digits-to-calibration-value (digits)
  (parse-integer (concatenate 'string
                              (list (char digits 0)
                                    (char digits (1- (length digits)))))))

(defun all-matches-starts (regex string)
  (loop :for m :in (cl-ppcre:all-matches regex string)
        :and i :from 0
        :when (evenp i)
          :collect m))

(defun find-word-digits (line)
  (loop :with min-digit
        :and max-digit
        :and min-pos := (length line)
        :and max-pos := -1
        :for digit-regex :in +digits+
        :and i :from 0
        :for matches := (all-matches-starts digit-regex line)
        :do (mapcar (lambda (pos)
                      (when (< pos min-pos)
                        (setf min-pos pos
                              min-digit i))
                      (when (> pos max-pos)
                        (setf max-pos pos
                              max-digit i))) matches)
        :finally (return (+ (* 10 min-digit) max-digit))))

(defun solve-1 (&optional (input (day-input-lines 1 2023)))
  (reduce #'+ (mapcar (compose #'digits-to-calibration-value (curry #'remove-if #'alpha-char-p)) input)))

(defun solve-2 (&optional (input (day-input-lines 1 2023)))
  (reduce #'+ (mapcar #'find-word-digits input)))
