(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-13
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule))

(in-package #:day-13)

(defparameter +example-1+ '("#.##..##."
                            "..#.##.#."
                            "##......#"
                            "##......#"
                            "..#.##.#."
                            "..##..##."
                            "#.#.##.#."
                            ""
                            "#...##..#"
                            "#....#..#"
                            "..##..###"
                            "#####.##."
                            "#####.##."
                            "..##..###"
                            "#....#..#"))

(defun find-horizontal-mirror (pattern test)
  (iter (for above-line on pattern)
        (for row from 1)
        (collect (car above-line) at beginning into below-line)
        (while (cdr above-line))
        (when (funcall test below-line (cdr above-line))
          (leave row))))

(defun find-vertical-mirror (pattern test)
  (find-horizontal-mirror (apply #'map 'list (lambda (&rest rows) (coerce rows 'string)) pattern)
                          test))

(defun score-mirror (pattern &key (test (curry #'every #'string=)))
  (alexandria:if-let ((horizontal-mirror (find-horizontal-mirror pattern test)))
    (* 100 horizontal-mirror)
    (find-vertical-mirror pattern test)))

(defun solve-1 (&optional (input (day-input-lines 13 2023)))
  (reduce #'+
          (mapcar #'score-mirror (split-sequence:split-sequence "" input :test #'equal))))

(defun count-differences (string-1 string-2)
  (iter (for char-1 in-string string-1)
        (for char-2 in-string string-2)
        (counting (char/= char-1 char-2))))

(defun smudged-mirror-p (above below)
  (iter (for a in above)
        (for b in below)
        (sum (count-differences a b) into differences)
        (finally (return (= differences 1)))))

(defun solve-2 (&optional (input (day-input-lines 13 2023)))
  (reduce #'+
          (mapcar (alexandria:rcurry #'score-mirror :test #'smudged-mirror-p)
                  (split-sequence:split-sequence "" input :test #'equal))))
