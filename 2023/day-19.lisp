(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-19
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-19)

(defparameter +example-1+ '("px{a<2006:qkq,m>2090:A,rfg}"
                            "pv{a>1716:R,A}"
                            "lnx{m>1548:A,A}"
                            "rfg{s<537:gd,x>2440:R,A}"
                            "qs{s>3448:A,lnx}"
                            "qkq{x<1416:A,crn}"
                            "crn{x>2662:A,R}"
                            "in{s<1351:px,qqz}"
                            "qqz{s>2770:qs,m<1801:hdj,R}"
                            "gd{a>3333:R,R}"
                            "hdj{m>838:A,pv}"
                            ""
                            "{x=787,m=2655,a=1222,s=2876}"
                            "{x=1679,m=44,a=2067,s=496}"
                            "{x=2036,m=264,a=79,s=2244}"
                            "{x=2461,m=1339,a=466,s=291}"
                            "{x=2127,m=1623,a=2188,s=1013}"))

(defrule input ()
    (and (+ (string workflow)) "" (+ (string part)))
  (:choose 0 2)
  (:lambda (workflows parts)
    (list (mapcar #'first workflows)
          (mapcar #'first parts))))

(defrule part ()
    (and "{" (+ rating) "}")
  (:choose 1))

(defrule rating ()
    (and name "=" int (? ","))
  (:choose 0 2)
  (:function #'cons))

(defrule workflow ()
    (and name "{" (+ rule) "}")
  (:choose 0 2)
  (:function #'cons))

(defrule rule ()
    (and (? condition) (or (char "AR") name) (? ","))
  (:choose 0 1)
  (:lambda (condition name)
    (cons condition
          (cond ((eql #\A name) t)
                ((eql #\R name) nil)
                (t name)))))

(defrule condition ()
    (and name constraint int ":")
  (:choose 0 1 2))

(defrule constraint ()
    (char "<>"))

(defrule name ()
    (+ alpha)
  (:string))

(defun workflow-result (workflow part)
  (iter (for ((category constraint amount) . result) in workflow)
        (cond ((null constraint) (leave result))
              ((funcall (case constraint
                          (#\< #'<)
                          (#\> #'>))
                        (cdr (assoc category part :test #'string=))
                        amount)
               (leave result)))))

(defun process-part (workflows part)
  (iter (for workflow-name initially "in" then result)
        (for workflow = (cdr (assoc workflow-name workflows :test #'string=)))
        (for result = (workflow-result workflow part))
        (unless (stringp result)
          (leave result))))

(defun solve-1 (&optional (input (day-input-lines 19 2023)))
  (destructuring-bind (workflows parts)
      (parseq 'input input)
    (reduce #'+ (mapcar (lambda (part) (reduce #'+ part :key #'cdr))
                        (remove-if-not (curry #'process-part workflows) parts)))))

(defun complement-constraint (constraint-type)
  (case constraint-type
    (#\< #\≥)
    (#\> #\≤)))

(defun list-constraints (workflows workflow-name)
  (let ((workflow (cdr (assoc workflow-name workflows :test #'equal))))
    (iter (for ((category constraint-type amount) . result) in workflow)
          (for gathered-constraints = (if category
                                          (append complements (list (list category constraint-type amount)))
                                          complements))
          (cond ((eql result t) (collect (copy-list gathered-constraints) into accepting))
                (t (appending (mapcar (curry #'append gathered-constraints)
                                      (list-constraints workflows
                                                        result))
                              into accepting)))

          (collect (list category (complement-constraint constraint-type) amount) into complements)
          (finally (return accepting)))))

(defun convert-constraint (constraint)
  (destructuring-bind (category type amount)
      constraint
    (case type
      (#\< (list category
                 #\≤
                 (1- amount)))
      (#\> (list category
                 #\≥
                 (1+ amount)))
      (t constraint))))

(defun merge-constraints (constraints)
  (iter (with range-map = (make-hash-table :test 'equal))
        (initially (iter (for category in '("x" "m" "a" "s"))
                         (setf (gethash category range-map) '(1 4000))))
        (for (category constraint-type amount) in constraints)
        (for (category-min category-max) = (gethash category range-map))
        (case constraint-type
          (#\≤ (setf category-max (min category-max amount)))
          (#\≥ (setf category-min (max category-min amount))))
        (setf (gethash category range-map) (list category-min category-max))
        (finally (return (a:hash-table-alist range-map)))))

(defun ranges-possible-combinations (ranges)
  (iter (for (nil low high) in ranges)
        (multiply (1+ (- high low)))))

(defun solve-2 (&optional (input (day-input-lines 19 2023)))
  (reduce #'+
          (mapcar (compose #'ranges-possible-combinations
                           #'merge-constraints
                           (curry #'mapcar #'convert-constraint))
                  (list-constraints (first (parseq 'input input)) "in"))))
