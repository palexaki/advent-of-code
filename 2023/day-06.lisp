(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :iterate)
  (ql:quickload :advent-util))
(defpackage #:day-6
  (:use #:advent-util
        #:cl
        #:iterate
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule))

(in-package #:day-6)

(defparameter +example+ '("Time:      7  15   30"
                          "Distance:  9  40  200"))

(defun calculate (times distances)
  (iter (for time in times)
        (for distance in distances)
        (multiply (iter (for ms from 1 below time)
                        (counting (< distance (* ms (- time ms))))))))

(defun solve-1 (&optional (input (day-input-lines 6 2023)))
  (apply #'calculate (mapcar #'ints input)))

(defun solve-2 (&optional (input (day-input-lines 6 2023)))
  (apply #'calculate (mapcar (compose #'ints
                                      (lambda (line) (remove-if-not #'digit-char-p line)))
                             input)))
