(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-15
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-15)

(defparameter +example-1+ '("rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7"))

(defun hash-string (string)
  (iter (for c in-string string)
        (for current-value initially 0 then (mod (* (+ current-value (char-code c)) 17) 256))
        (finally (return current-value))))

(defun solve-1 (&optional (input (day-input-lines 15 2023)))
  (reduce #'+ (mapcar #'hash-string (r:split "," (first input)))))

(defun hash-operation (map instruction)
  (let* ((operation-position (position-if-not #'alpha-char-p instruction))
         (label (subseq instruction 0 operation-position))
         (hash (hash-string label))
         (operation-type (char instruction operation-position))
         (focal-length (when (char= operation-type #\=)
                         (- (char-code (char instruction (1+ operation-position)))
                            (char-code #\0)))))
    (case operation-type
      (#\- (a:removef (aref map hash) label :key #'first :test #'string=))
      (#\= (a:if-let ((pair (assoc label (aref map hash) :test #'string=)))
             (setf (cdr pair) focal-length)
             (a:appendf (aref map hash) (list (cons label focal-length))))))
    map))

(defun focusing-power (configuration)
  (iter outer
        (for box from 1)
        (for box-contents in-vector configuration)
        (iter (for slot from 1)
              (for (nil . focal-length) in box-contents)
              (in outer
                  (sum (* box slot focal-length))))))

(defun solve-2 (&optional (input (day-input-lines 15 2023)))
  (focusing-power (reduce #'hash-operation
                          (r:split "," (first input))
                          :initial-value (make-array 256 :initial-element nil))))
