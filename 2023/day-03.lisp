(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util)
  (ql:quickload :iterate))
(defpackage #:day-3
  (:use #:advent-util
        #:cl
        #:iterate
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule))

(in-package #:day-3)

(defvar *test* '("467..114.."
                 "...*......"
                 "..35..633."
                 "......#..."
                 "617*......"
                 ".....+.58."
                 "..592....."
                 "......755."
                 "...$.*...."
                 ".664.598.."))

(defun engine-symbol-p (char)
  (not (or (digit-char-p char)
           (char= char #\.))))

(defun part-number-p (start end row map)
  (iter (for y from (1- row) to (1+ row))
        (appending (iter (for x from (1- start) to end)
                         (when (and (in-board-p map (make-pos x y))
                                    (engine-symbol-p (aref map y x)))
                           (collect (list (aref map y x)
                                          (list x y))))))))

(defun part-numbers (list-map)
  (iter (with array-map = (make-array (list (length (first list-map))
                                            (length list-map))
                                      :initial-contents list-map))
        (for row in list-map)
        (for y from 0)
        (ppcre:do-matches (start end "\\d+" row)
          (alexandria:when-let ((symbols (part-number-p start end y array-map)))
            (collect (list (parse-integer row :start start :end end)
                           symbols))))))



(defun solve-1 (&optional (input (day-input-lines 3 2023)))
  (reduce #'+ (part-numbers input) :key #'first))

(defun solve-2 (&optional (input (day-input-lines 3 2023)))
  (let ((part-numbers (part-numbers input))
        (gears (make-hash-table :test 'equal)))
    (iter (for (part-number parts) in part-numbers)
          (iter (for (symbol position) in parts)
                (when (char= symbol #\*)
                  (push part-number (gethash position gears)))))
    (iter (for (nil numbers) in-hashtable gears)
          (when (= 2 (length numbers))
            (sum (apply #'* numbers))))))
