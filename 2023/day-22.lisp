(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-22
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-22)

(defparameter +example-1+ '("1,0,1~1,2,1"
                            "0,0,2~2,0,2"
                            "0,2,3~2,2,3"
                            "0,0,4~0,2,4"
                            "2,0,5~2,2,5"
                            "0,1,6~2,1,6"
                            "1,1,8~1,1,9"))

(defun parse-input (line)
  (let ((nums (ints line)))
    (mapcar (lambda (min max)
              (if (= min max)
                  min
                  (progn
                    (assert (< min max))
                    (list min max))))
            nums (cdddr nums))))

(defun overlap (a b)
  (cond ((and (listp a) (listp b))
         (<= (max (car a)
                  (car b))
             (min (cadr a)
                  (cadr b))))
        ((and (numberp a) (listp b))
         (overlap b a))
        ((and (listp a) (numberp b))
         (<= (car a) b (cadr a)))
        (t (= a b))))

(defun bricks-overlap-p (brick1 brick2)
  (let ((x1 (car brick1))
        (y1 (cadr brick1))
        (x2 (car brick2))
        (y2 (cadr brick2)))
    (and (overlap x1 x2)
         (overlap y1 y2))))

(defun largest (a)
  (apply #'max 0 (a:ensure-list a)))

(defun lowest (a)
  (apply #'min (a:ensure-list a)))

(defun drop-bricks (bricks)
  (iter (for brick in (sort (copy-tree bricks) #'< :key (compose #'a:ensure-car #'third)))
        (for (x y z) = brick)
        (for floor = (1+ (reduce #'max (remove-if-not (curry #'bricks-overlap-p brick) dropped-bricks) :key (compose #'largest #'third)
                                                                                                       :initial-value 0)))
        (collect (list x y (if (listp z)
                               (list floor (+ floor (- (cadr z) (car z))))
                               floor))
          into dropped-bricks at beginning)
        (counting (/= (a:ensure-car z)
                      floor)
                  into dropped-count)
        (finally (return (values dropped-bricks
                                 dropped-count)))))

(defun supports (brick supported)
  (when (bricks-overlap-p brick supported)
    (= (1+ (largest (third brick)))
       (lowest (third supported)))))

(defun determine-supports (bricks)
  (iter (with supporting-map = (make-hash-table :test 'equal))
        (with supporting-count = (make-hash-table :test 'equal))
        (for brick in bricks)
        (setf (gethash brick supporting-map) nil)
        (iter (for supported-brick in bricks)
              (when (supports brick supported-brick)
                (push supported-brick (gethash brick supporting-map))
                (incf (gethash supported-brick supporting-count 0))))
        (finally (return (values supporting-map supporting-count)))))


(defun solve-1 (&optional (input (day-input-lines 22 2023)))
  (multiple-value-bind (map count)
      (determine-supports (drop-bricks (mapcar #'parse-input input)))
    (iter (for (brick supported) in-hashtable map)
          (for dropping-bricks = (remove-if-not (lambda (b)
                                                  (= 1 (gethash b count)))
                                                supported))
          (counting (null dropping-bricks) into disintegratable-count)
          (when dropping-bricks
            (collect brick into chain-bricks))
          (finally (return (values disintegratable-count chain-bricks))))))

(defun solve-2 (&optional (input (day-input-lines 22 2023)))
  (let ((dropped-bricks (reverse (drop-bricks (mapcar #'parse-input input)))))
    (iter (for brick in (nth-value 1 (solve-1 input)))
          (sum (nth-value 1 (drop-bricks (remove brick dropped-bricks :test 'equal)))))))
