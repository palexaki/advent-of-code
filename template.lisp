(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-${1:`(format-time-string "%d")`}
  (:use #:advent-util
        #:cl
        #:iterate
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-$1)

$0

(defun solve-1 (&optional (input (day-input-lines $1 ${2:`(format-time-string "%Y")`})))
  (declare (ignore input)))

(defun solve-2 (&optional (input (day-input-lines $1 $2)))
  (declare (ignore input)))
