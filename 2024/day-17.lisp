(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-17
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-17)

(defparameter +test-1+ '("Register A: 729"
                         "Register B: 0"
                         "Register C: 0"
                         ""
                         "Program: 0,1,5,4,3,0"))

(defparameter +test-2+ '("Register A: 2024"
                         "Register B: 0"
                         "Register C: 0"
                         ""
                         "Program: 0,3,5,4,3,0"))

(defun parse-input (input)
  (list (make-array 3 :initial-contents (mapcar (lambda (s) (first (ints s))) (subseq input 0 3)))
        (coerce (ints (nth 4 input)) '(simple-vector *))))

(defun operand-value (registers operand)
  (ccase operand
    ((0 1 2 3) operand)
    ((4 5 6) (aref registers (- operand 4)))))

(defun perform-instruction (registers instruction operand)
  (let ((value-a (aref registers 0))
        (value-b (aref registers 1))
        (value-c (aref registers 2)))
    (a:ensure-list
     (case instruction
       (0 (setf (aref registers 0) (ash value-a (- (operand-value registers operand)))))
       (1 (setf (aref registers 1) (logxor value-b operand)))
       (2 (setf (aref registers 1) (mod (operand-value registers operand) 8)))
       (3 (unless (zerop value-a) (list :pc operand)))
       (4 (setf (aref registers 1) (logxor value-b value-c)))
       (5 (list :out (mod (operand-value registers operand) 8)))
       (6 (setf (aref registers 1) (ash value-a (- (operand-value registers operand)))))
       (7 (setf (aref registers 2) (ash value-a (- (operand-value registers operand)))))))))

(defun execute-program (registers program)
  (iter (with pc = 0)
        (for instruction = (aref program pc))
        (for operand = (aref program (1+ pc)))
        (for (op value) = (perform-instruction registers instruction operand))
        (when (eq op :out)
          (collect value into values))
        (if (eq op :pc)
            (setf pc value)
            (incf pc 2))
        (until (>= pc (array-dimension program 0)))
        (finally (return (values values registers)))))

(defun perform-instruction-tentative (registers instruction operand outputs)
  (let ((value-a (aref registers 0))
        (value-b (aref registers 1))
        (value-c (aref registers 2)))
    (case instruction
      (0 (setf (aref registers 0) `(bvlshr ,value-a ,(operand-value registers operand))))
      (1 (setf (aref registers 1) `(bvxor ,value-b ,operand)))
      (2 (setf (aref registers 1) `(bvsmod ,(operand-value registers operand) 8)))
      (3 nil)
      (4 (setf (aref registers 1) `(bvxor ,value-b ,value-c)))
      (5 `(assert (= ,(vector-pop outputs) (bvsmod ,(operand-value registers operand) 8))))
      (6 (setf (aref registers 1) `(bvlshr ,value-a ,(operand-value registers operand))))
      (7 (setf (aref registers 2) `(bvlshr ,value-a ,(operand-value registers operand)))))))

(defun final-assertion (a)
  (print
   (list 'assert (list '= 0 a))))

(defun execute-program-tentative (program)
  (iter (with pc = 0)
        (with registers = (a:copy-array #('a 0 0)))
        (with outputs = (a:copy-array (reverse program) :fill-pointer t))
        (for instruction = (aref program pc))
        (for operand = (aref program (1+ pc)))
        (for result = (perform-instruction-tentative registers instruction operand outputs))
        (when (eq (car result) 'assert)
          (collect result into assertions))
        (cond ((and (= instruction 3) (> (fill-pointer outputs) 0)) (setf pc operand))
              ((and (= instruction 3) (zerop (fill-pointer outputs)))
               (return-from execute-program-tentative (append (list `(assert (= 0 ,(aref registers 0)))) assertions `((check-sat) (get-model)))))
              (t (incf pc 2)))))

(defun solve-1 (&optional (input (day-input-lines 17 2024)))
  (format nil "~{~a~^,~}" (apply #'execute-program (parse-input input))))

(defun solve-2 (&optional (input (day-input-lines 17 2024)))
  (concatenate 'string
               "(declare-const a (_ BitVec 64))
"
               (remove #\'
                       (r:regex-replace-all "\\d+"
                                            (string-downcase (format nil "~{~a~%~}"
                                                                     (execute-program-tentative (second (parse-input input)))))
                                            "(_ bv\\& 64)"))))
