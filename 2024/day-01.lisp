(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-01
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-01)

(defparameter +test+ '("3   4"
                       "4   3"
                       "2   5"
                       "1   3"
                       "3   9"
                       "3   3"))

(defun parse-input (input)
  (iter (for line in input)
        (for (left right) = (r:split "\\s+" line))
        (collect (parse-integer left) into left-list)
        (collect (parse-integer right) into right-list)
        (finally (return (values left-list right-list)))))

(defun solve-1 (&optional (input (day-input-lines 01 2024)))
  (multiple-value-bind (left-list right-list)
      (parse-input input)
    (reduce #'+ (mapcar (lambda (a b) (abs (- a b))) (sort left-list #'<) (sort right-list #'<)))))

(defun solve-2 (&optional (input (day-input-lines 01 2024)))
  (multiple-value-bind (left-list right-list)
      (parse-input input)
    (reduce #'+ (mapcar (lambda (l) (* l (count l right-list))) left-list))))
