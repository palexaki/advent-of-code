(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-09
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-09)

(defparameter +test-1+ '("2333133121414131402"))

(defun parse-input (line)
  (iter (with id = -1)
        (for c in-string line)
        (for size = (- (char-code c) 48))
        (for free first nil then (not free))
        (collect (if free
                     size
                     (cons (incf id) size)))))

(defun find-free (disk)
  (iter (for rest on disk)
        (when (atom (car rest))
          (return rest))))

(defun move-file (disk)
  (let* ((free-rest (find-free disk))
         (end (last disk 2))
         (free-size (car free-rest))
         (end-block (cadr end))
         (end-id (when (consp end-block) (car end-block)))
         (end-size (when (consp end-block) (cdr end-block))))
    (cond
      ((null free-rest) nil)
      ((atom end-block)
       (setf (cdr end) nil)
       disk)
      ((= end-size free-size)
       (setf (car free-rest) (cons end-id end-size)
             (cdr end) nil)
       disk)
      ((> end-size free-size)
       (setf (car free-rest) (cons end-id free-size)
             (cdr end-block) (- end-size free-size))
       disk)
      ((< end-size free-size)
       (setf (cdr end) nil
             (car free-rest) end-block
             (cdr free-rest) (cons (- free-size end-size) (cdr free-rest)))
       disk)
      (t (assert nil)))))

(defun calculate-score (disk)
  (iter outer
        (with position = -1)
        (for block in disk)
        (if (atom block)
            (incf position block)
            (iter (with (id . size) = block)
                  (repeat size)
                  (in outer (summing (* (incf position) id)))))))

(defun print-disk (disk)
  (format t "~{~a~}~%"
          (iter outer
                (for block in disk)
                (if (atom block)
                    (iter (repeat block)
                          (in outer (collect #\.)))
                    (iter (with (id . size) = block)
                          (repeat size)
                          (in outer (collect id)))))))

(defun compact-disk (disk)
  (let ((disk (copy-tree disk)))
    (iter (while (move-file disk)))
    disk))

(defun max-id (disk)
  (let ((end (last disk 2)))
    (if (atom (car end))
        (caadr end)
        (caar end))))

(defun compact-disk-in-order (disk)
  (let ((disk (copy-tree disk)))
    (iter (for id from (max-id disk) downto 1)
          (for pseudo-end = (iter (for rest on disk)
                                  (for to-find = (cadr rest))
                                  (when (and (consp to-find) (= (car to-find) id))
                                    (return rest))))
          (for (nil . size) = (cadr pseudo-end))
          (for free-rest = (iter (for rest on disk)
                                 (when (and (atom (car rest))
                                            (>= (car rest) size))
                                   (return rest))
                                 (never (eq rest pseudo-end))))
          (when free-rest
            (when (> (car free-rest) size)
              (setf (cdr free-rest) (cons (- (car free-rest) size) (cdr free-rest))))
            (setf (car free-rest) (cons id size)
                  (cadr pseudo-end) size)))
    disk))

(defun solve-1 (&optional (input (day-input-lines 09 2024)))
  (calculate-score
   (compact-disk (parse-input (car input)))))

(defun solve-2 (&optional (input (day-input-lines 09 2024)))
  (calculate-score (compact-disk-in-order (parse-input (car input)))))
