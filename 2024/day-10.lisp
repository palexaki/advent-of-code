(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util)
  (ql:quickload :queues.simple-queue))
(defpackage #:day-10
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-10)

(defparameter +test-1+ '("0123"
                         "1234"
                         "8765"
                         "9876"))

(defparameter +test-2+ '("89010123"
                         "78121874"
                         "87430965"
                         "96549874"
                         "45678903"
                         "32019012"
                         "01329801"
                         "10456732"))

(defun parse-input (input)
  (make-2d-array (mapcar (lambda (line)
                           (map 'list (lambda (char) (if (char= char #\.)
                                                         100
                                                         (- (char-code char) (char-code #\0))))
                                line))
                         input)))

(defun score-trailhead (position map &optional distinct)
  (let ((visited (make-hash-table :test 'equal))
        (queue (q:make-queue :simple-queue))
        (distinct-score 0))
    (q:qpush queue position)
    (iter (until (zerop (q:qsize queue)))
          (for position = (q:qpop queue))
          (for height = (board-position map position))
          (cond
            ((= 9 height)
             (setf (gethash position visited) t)
             (incf distinct-score))
            (t (iter (for neighbor in (pos-neighbors map position))
                     (when (= (1+ height) (board-position map neighbor))
                        (q:qpush queue neighbor))))))
    (if distinct
        distinct-score
        (hash-table-count visited))))


(defun score-map (map &optional distinct)
  (iter outer
        (for x from 0 below (array-dimension map 1))
        (iter (for y from 0 below (array-dimension map 0))
              (when (zerop (aref map y x))
                (in outer (summing (score-trailhead (make-pos x y) map distinct)))))))

(defun solve-1 (&optional (input (day-input-lines 10 2024)))
  (score-map (parse-input input)))

(defun solve-2 (&optional (input (day-input-lines 10 2024)))
  (score-map (parse-input input) t))
