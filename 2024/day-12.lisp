(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util)
  (ql:quickload :queues.simple-queue))

(defpackage #:day-12
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-12)

(defparameter +test-1+ '("AAAA"
                         "BBCD"
                         "BBCC"
                         "EEEC"))

(defparameter +test-2+ '("OOOOO"
                         "OXOXO"
                         "OOOOO"
                         "OXOXO"
                         "OOOOO"))

(defparameter +test-3+ '("RRRRIICCFF"
                         "RRRRIICCCF"
                         "VVRRRCCFFF"
                         "VVRCCCJFFF"
                         "VVVVCJJCFE"
                         "VVIVCCJJEE"
                         "VVIIICJJEE"
                         "MIIIIIJJEE"
                         "MIIISIJEEE"
                         "MMMISSJEEE"))

(defun determine-region (map start)
  (iter (with visited = (make-hash-table :test 'equal))
        (with queue = (q:make-queue :simple-queue))
        (with region-plant = (board-position map start))
        (initially
         (q:qpush queue start)
         (setf (gethash start visited) t))
        (until (zerop (q:qsize queue)))
        (for pos = (q:qpop queue))
        (iter (for neighbor in (pos-neighbors map pos))
              (for neighbor-plant = (board-position map neighbor))
              (when (and (char= region-plant neighbor-plant)
                         (not (gethash neighbor visited)))
                (q:qpush queue neighbor)
                (setf (gethash neighbor visited) t)))
        (finally (return (a:hash-table-keys visited)))))

(defun region-perimeter (map region)
  (iter outer
        (with region-plant = (board-position map (first region)))
        (for position in region)
        (iter (for neighbor in (unsafe-pos-neighbors position))
              (for neighbor-plant = (safe-board-position map neighbor))
              (in outer (counting (or (not neighbor-plant) (char/= region-plant neighbor-plant)))))))

(defun safe-aref (array &rest subscripts)
  (when (apply #'array-in-bounds-p array subscripts)
    (apply #'aref array subscripts)))

(defun region-sides (map region)
  (let ((sides 0)
        (width (array-dimension map 1))
        (height (array-dimension map 0)))
    (iter (for y below height)
          (iter (with side-up = nil)
                (with side-down = nil)
                (for x below width)
                (for plant = (aref map y x))
                (for plant-up = (safe-aref map (1- y) x))
                (for plant-down = (safe-aref map (1+ y) x))
                (unless (find (make-pos x y) region :test 'equal)
                  (setf side-down nil
                        side-up nil)
                  (next-iteration))
                (when (eq plant plant-up)
                  (setf side-up nil))
                (when (eq plant plant-down)
                  (setf side-down nil))
                (when (and (not side-up)
                           (not (eq plant plant-up)))
                  (incf sides)
                  (setf side-up t))
                (when (and (not side-down)
                           (not (eq plant plant-down)))
                  (incf sides)
                  (setf side-down t))))
    (iter (for x below height)
          (iter (with side-left = nil)
                (with side-right = nil)
                (for y below width)
                (for plant = (aref map y x))
                (for plant-left = (safe-aref map y (1- x)))
                (for plant-right = (safe-aref map y (1+ x)))
                (unless (find (make-pos x y) region :test 'equal)
                  (setf side-right nil
                        side-left nil)
                  (next-iteration))
                (when (eq plant plant-left)
                  (setf side-left nil))
                (when (eq plant plant-right)
                  (setf side-right nil))
                (when (and (not side-left)
                           (not (eq plant plant-left)))
                  (incf sides)
                  (setf side-left t))
                (when (and (not side-right)
                           (not (eq plant plant-right)))
                  (incf sides)
                  (setf side-right t))))
    sides))

(defun all-map-regions (map)
  (iter outer
        (with visited = (make-hash-table :test 'equal))
        (for x below (array-dimension map 1))
        (iter (for y below (array-dimension map 0))
              (for pos = (make-pos x y))
              (when (gethash pos visited)
                (next-iteration))
              (for region = (determine-region map pos))
              (iter (for p in region)
                    (setf (gethash p visited) t))
              (in outer (collect region)))))

(defun map-total-price (map &optional sides)
  (iter (for region in (all-map-regions map))
        (summing (* (length region)
                    (if sides
                        (region-sides map region)
                        (region-perimeter map region))))))

(defun solve-1 (&optional (input (day-input-lines 12 2024)))
  (map-total-price (make-2d-array input)))

(defun solve-2 (&optional (input (day-input-lines 12 2024)))
  (map-total-price (make-2d-array input) t))
