(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))

(defpackage #:day-08
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-08)

(defun frequency-positions (map)
  (iter (with frequencies = (make-hash-table))
        (for x below (array-dimension map 1))
        (iter (for y below (array-dimension map 0))
              (for frequency = (aref map y x))
              (unless (char= #\. frequency)
                (push (list x y) (gethash frequency frequencies))))
        (finally (return frequencies))))

(defun antinodes-from-pair (a b &optional map)
  (let ((diff (pos-sub a b)))
    (if (null map)
        (list (pos-add a diff)
              (pos-sub b diff))
        (concatenate 'list
                     (iter (for node first a then (pos-add node diff))
                           (while (in-board-p map node))
                           (collect node))
                     (iter (for node first b then (pos-sub node diff))
                           (while (in-board-p map node))
                           (collect node))
                     ))))


(defun antinodes (map &optional provide-map-p)
  (iter (with antinodes = (make-hash-table :test 'equal))
        (for (nil positions) in-hashtable (frequency-positions map))
        (iter (for (a . rest) on positions)
              (iter (for b in rest)
                    (iter (for antinode in (antinodes-from-pair a b (when provide-map-p map)))
                          (when (in-board-p map antinode)
                            (setf (gethash antinode antinodes) t)))))
        (finally (return antinodes))))

(defun solve-1 (&optional (input (day-input-lines 08 2024)))
  (hash-table-count (antinodes (make-2d-array input))))

(defun solve-2 (&optional (input (day-input-lines 08 2024)))
  (hash-table-count (antinodes (make-2d-array input) t)))
