(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-07
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-07)

(defparameter +test-1+ '("190: 10 19"
                         "3267: 81 40 27"
                         "83: 17 5"
                         "156: 15 6"
                         "7290: 6 8 6 15"
                         "161011: 16 10 13"
                         "192: 17 8 14"
                         "21037: 9 7 18 13"
                         "292: 11 6 16 20"))

(defun concat (a b)
  (+ (* a (expt 10 (iter (for x from 1)
                         (for c first b then (floor c 10))
                         (when (< c 10)
                           (return x)))))
     b))

(defun possible-results (first rest &optional concat-p)
  (let ((first (a:ensure-list first)))
    (cond ((null rest) first)
          (t (possible-results (remove-duplicates (iter (with m = (car rest))
                                                        (for n in first)
                                                        (collect (* m n))
                                                        (collect (+ m n))
                                                        (when concat-p
                                                          (collect
                                                              (concat n m)))))
                               (cdr rest)
                               concat-p)))))

(defun solve-1 (&optional (input (day-input-lines 07 2024)))
  (iter (for line in input)
        (for (value first . rest) = (ints line))
        (when (find value (possible-results first rest))
          (summing value))))

(defun solve-2 (&optional (input (day-input-lines 07 2024)))
  (iter (for line in input)
        (for (value first . rest) = (ints line))
        (when (find value (possible-results first rest t))
          (summing value))))
