(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-04
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-04)

(defparameter +test-1+ '("MMMSXXMASM"
                         "MSAMXMSMSA"
                         "AMXSXMAAMM"
                         "MSAMASMSMX"
                         "XMASAMXAMM"
                         "XXAMMXXAMA"
                         "SMSMSASXSS"
                         "SAXAMASAAA"
                         "MAMMMXMMMM"
                         "MXMXAXMASX"))

(defun calc-directions ()
  (iter outer
        (for x from -1 to 1)
        (iter (for y from -1 to 1)
              (unless (= x y 0)
                (in outer (collect (make-pos x y)))))))

(defun has-xmas (grid position direction)
  (iter (for pos first position then (pos-add pos direction))
        (for char-to-find in-string "XMAS")
        (for char = (when (in-board-p grid pos) (board-position grid pos)))
        (always (and char (char= char char-to-find)))))

(defun safe-board-position (board position)
  (when (in-board-p board position)
    (board-position board position)))

(defun has-x-mas (grid position)
  (flet ((m-or-s-p (c) (or (char= #\M c) (char= #\S c))))
    (let ((top-left (safe-board-position grid (pos-add position '(-1 -1))))
          (top-right (safe-board-position grid (pos-add position '(1 -1))))
          (bottom-left (safe-board-position grid (pos-add position '(-1 1))))
          (bottom-right (safe-board-position grid (pos-add position '(1 1)))))
      (and top-left top-right bottom-left bottom-right
           (char= #\A (board-position grid position))
           (m-or-s-p top-left) (m-or-s-p top-right) (m-or-s-p bottom-left) (m-or-s-p bottom-right)
           (char/= top-left bottom-right)
           (char/= top-right bottom-left)))))

(defun count-xmas (grid)
  (iter outer
        (with directions := (calc-directions))
        (for x from 0 below (array-dimension grid 1))
        (iter (for y from 0 below (array-dimension grid 0))
              (iter (for direction in directions)
                    (in outer (counting (has-xmas grid (make-pos x y) direction)))))))

(defun count-x-mas (grid)
  (iter outer
        (for x from 0 below (array-dimension grid 1))
        (iter (for y from 0 below (array-dimension grid 0))
              (in outer (counting (has-x-mas grid (make-pos x y)))))))

(defun solve-1 (&optional (input (day-input-lines 04 2024)))
  (count-xmas (make-2d-array input)))

(defun solve-2 (&optional (input (day-input-lines 04 2024)))
  (count-x-mas (make-2d-array input)))
