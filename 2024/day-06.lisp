(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-06
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-06)

(defparameter +test-1+ '("....#....."
                         ".........#"
                         ".........."
                         "..#......."
                         ".......#.."
                         ".........."
                         ".#..^....."
                         "........#."
                         "#........."
                         "......#..."))

(defun find-start (map)
  (iter (for x from 0 below (array-dimension map 1))
        (iter (for y from 0 below (array-dimension map 0))
              (when (char= (aref map y x) #\^)
                (return-from find-start (list x y))))))

(defun move (map position direction)
  (let* ((delta (direction->delta direction))
         (next-position (pos-add position delta))
         (next-tile (safe-board-position map next-position)))
    (cond ((null next-tile) nil)
          ((char/= next-tile #\#) (values next-position direction))
          (t (values position (next-direction direction))))))


(defun solve-1 (&optional (input (day-input-lines 06 2024)))
  (iter (with map = (make-2d-array input))
        (with visited = (make-hash-table :test 'equal))
        (for (position direction) first (list (find-start map) :up)
             then (multiple-value-list (move map position direction)))
        (unless position
          (return (hash-table-count visited)))
        (setf (gethash position visited) t)))

(defun obstructs (map start obstruction)
  (when (equal start obstruction)
    (return-from obstructs nil))
  (iter (with map = (let ((map-copy (a:copy-array map)))
                      (setf (board-position map-copy obstruction) #\#)
                      map-copy))
        (with visited = (make-hash-table :test 'equal))
        (for (position direction) first (list start :up)
             then (multiple-value-list (move map position direction)))
        (unless position
          (return nil))
        (when (gethash (cons position direction) visited)
          (return t))
        (setf (gethash (cons position direction) visited) t)))

(defun solve-2 (&optional (input (day-input-lines 06 2024)))
  (iter outer
        (with map = (make-2d-array input))
        (with start = (find-start map))
        (for x below (array-dimension map 1))
        (iter (for y below (array-dimension map 0))
              (in outer (counting (obstructs map start (make-pos x y)))))))
