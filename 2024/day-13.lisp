(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-13
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-13)

(defparameter +test-1+ '("Button A: X+94, Y+34"
                         "Button B: X+22, Y+67"
                         "Prize: X=8400, Y=5400"
                         ""
                         "Button A: X+26, Y+66"
                         "Button B: X+67, Y+21"
                         "Prize: X=12748, Y=12176"
                         ""
                         "Button A: X+17, Y+86"
                         "Button B: X+84, Y+37"
                         "Prize: X=7870, Y=6450"
                         ""
                         "Button A: X+69, Y+23"
                         "Button B: X+27, Y+71"
                         "Prize: X=18641, Y=10279"))

(defun parse-input (input &optional (prize-correction 0))
  (iter (for (button-a button-b prize) on input by (a:curry #'nthcdr 4))
        (for (ax ay) = (ints button-a))
        (for (bx by) = (ints button-b))
        (for (gx gy) = (ints prize))
        (collect (list :ax ax :ay ay
                       :bx bx :by by
                       :gx (+ prize-correction gx) :gy (+ prize-correction gy)))))

(defun solve-machine (machine)
  (destructuring-bind (&key (ax 0) (ay 0) (bx 0) (by 0) (gx 0) (gy 0))
      machine
    (let* ((b-denominator (- (* ax by) (* ay bx)))
           (b-numerator (- (* ax gy) (* ay gx)))
           (b (unless (zerop b-denominator) (/ b-numerator b-denominator)))
           (a (when b (/ (- gx (* b bx)) ax))))
      (when (and (integerp a) (integerp b))
        (list :a a
              :b b)))))

(defun machine-price (machine)
  (destructuring-bind (&key a b)
      (solve-machine machine)
    (when (and a b)
      (+ (* a 3) b))))

(defun solve-1 (&optional (input (day-input-lines 13 2024)))
  (reduce #'+ (remove nil (mapcar #'machine-price (parse-input input)))))

(defun solve-2 (&optional (input (day-input-lines 13 2024)))
  (reduce #'+ (remove nil (mapcar #'machine-price (parse-input input 10000000000000)))))
