(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util)
  (ql:quickload :queues.simple-queue))

(defpackage #:day-18
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-18)

(defparameter *map-dimensions* 70)

(defparameter +test-1+ '("5,4" "4,2" "4,5" "3,0" "2,1"
                         "6,3" "2,4" "1,5" "0,6" "3,3"
                         "2,6" "5,1" "1,2" "5,5" "2,5"
                         "6,5" "1,4" "0,4" "6,4" "1,1"
                         "6,1" "1,0" "0,5" "1,6" "2,0"))

(defun make-board-partial (positions &optional (bytes-to-drop 1024))
  (iter (with map = (make-array (list (1+ *map-dimensions*) (1+ *map-dimensions*))
                                :initial-element nil))
        (repeat bytes-to-drop)
        (for position in positions)
        (setf (board-position map (ints position)) t)
        (finally (return map))))

(defun shortest-path (map)
  (iter (with queue = (q:make-queue :simple-queue))
        (with visited = (make-hash-table :test 'equal))
        (initially (q:qpush queue '((0 0) . 0)))
        (while (plusp (q:qsize queue)))
        (for (position . distance) = (q:qpop queue))
        (when (= (first position) (second position) *map-dimensions*)
          (return distance))
        (iter (for neighbor in (pos-neighbors map position))
              (unless (or (gethash neighbor visited)
                          (board-position map neighbor))
                (setf (gethash neighbor visited) t)
                (q:qpush queue (cons neighbor (1+ distance)))))))

(defun solve-1 (&optional (input (day-input-lines 18 2024)))
  (shortest-path (make-board-partial input)))

(defun solve-2 (&optional (input (day-input-lines 18 2024)))
  (labels ((find-first-blocker (lo hi)
             (let ((center (floor (+ lo hi) 2)))
               (cond ((= lo hi)
                      (nth (1- lo) input))
                     ((shortest-path (make-board-partial input center))
                      (find-first-blocker (1+ center) hi))
                     (t (find-first-blocker lo center))))))
    (find-first-blocker 1 (length input))))
