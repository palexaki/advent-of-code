(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-03
  (:use #:advent-util
        #:cl
        #:iterate
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-03)

(defparameter +test-1+ "xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))")

(defparameter +test-2+ "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))")

(defparameter +instruction-regex+ "mul\\(\\d+,\\d+\\)")

(defparameter +instruction-regex-conditionals+ "mul\\(\\d+,\\d+\\)|do\\(\\)|don't\\(\\)")

(defun execute-mul (instruction)
  (apply #'* (ints instruction)))

(defun find-all-instructions (instructions &optional include-conditionals-p)
  (iter (for line in instructions)
        (appending (r:all-matches-as-strings (if include-conditionals-p
                                                 +instruction-regex-conditionals+
                                                 +instruction-regex+)
                                             line))))

(defun perform-instructions (instructions)
  (iter (with do = t)
        (for instruction in instructions)
        (cond ((string= instruction "do()") (setf do t))
              ((string= instruction "don't()") (setf do nil))
              (do (sum (execute-mul instruction))))))


(defun solve-1 (&optional (input (day-input-lines 3 2024)))
  (reduce #'+ (mapcar #'execute-mul (find-all-instructions (a:ensure-list input)))))

(defun solve-2 (&optional (input (day-input-lines 3 2024)))
  (perform-instructions (find-all-instructions (a:ensure-list input) t)))
