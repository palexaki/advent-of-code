(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-05
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-05)

(defparameter +test-1+ '("47|53"
                         "97|13"
                         "97|61"
                         "97|47"
                         "75|29"
                         "61|13"
                         "75|53"
                         "29|13"
                         "97|29"
                         "53|29"
                         "61|53"
                         "97|53"
                         "61|29"
                         "47|13"
                         "75|47"
                         "97|75"
                         "47|61"
                         "75|61"
                         "47|29"
                         "75|13"
                         "53|13"
                         ""
                         "75,47,61,53,29"
                         "97,61,53,29,13"
                         "75,29,13"
                         "75,97,47,61,53"
                         "61,13,29"
                         "97,13,75,29,47"))

(defun parse-input (input)
  (let* ((parsed (mapcar #'ints input))
         (boundary (position nil parsed)))
    (values (subseq parsed 0 boundary)
            (subseq parsed (1+ boundary)))))

(defun rules-hash-map (rules-list)
  (let ((map (make-hash-table)))
    (iter (for (a b) in rules-list)
          (push a (gethash b map nil)))
    map))

(defun ordered-p (update rules)
  (labels ((check-order (conditions)
             (cond
               ((null conditions) (ordered-p (rest update) rules))
               ((find (car conditions) (cdr update)) nil)
               (t (check-order (rest conditions))))))
    (if (cdr update)
        (check-order (gethash (car update) rules))
        t)))

(defun middle-elt (list)
  (elt list (floor (length list) 2)))

(defun solve-1 (&optional (input (day-input-lines 05 2024)))
  (multiple-value-bind (rules updates)
      (parse-input input)
    (let ((rules-mapped (rules-hash-map rules)))
      (reduce #'+ (remove-if-not (a:rcurry #'ordered-p rules-mapped) updates) :key #'middle-elt))))

(defun order-update (update rules)
  (sort (copy-list update)
        (lambda (a b)
          (let ((a-preceding (gethash a rules))
                (b-preceding (gethash b rules)))
            (cond
              ((find b a-preceding) nil)
              ((find a b-preceding) t)
              (t (break)))))))

(defun solve-2 (&optional (input (day-input-lines 05 2024)))
  (multiple-value-bind (rules updates)
      (parse-input input)
    (let* ((rules-mapped (rules-hash-map rules))
           (unordered-updates (remove-if (a:rcurry #'ordered-p rules-mapped) updates)))
      (reduce #'+ unordered-updates :key (lambda (update) (middle-elt (order-update update rules-mapped)))))))
