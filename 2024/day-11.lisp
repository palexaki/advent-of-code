(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-11
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-11)

(defparameter +test-1+ '("0 1 10 99 999"))

(defparameter +test-2+ '("125 17"))

(defun even-digits-p (n)
  (evenp (ceiling (log (1+ n) 10))))

(defun split-number (n)
  (let ((divisor (expt 10 (/ (ceiling (log (1+ n) 10)) 2))))
    (list (floor n divisor)
          (mod n divisor))))

(defun blink (stones)
  (labels ((blink (stones next-stones)
             (if (null stones)
                 next-stones
                 (blink (cdr stones)
                        (transform-stone (car stones) next-stones))))
           (transform-stone (stone next-stones)
             (cond ((zerop stone) (cons 1 next-stones))
                   ((even-digits-p stone) (append (reverse (split-number stone))
                                                  next-stones))
                   (t (cons (* stone 2024) next-stones)))))
    (nreverse (blink stones nil))))

(defun blink-map (stones-map)
  (let ((next-stones-map (make-hash-table)))
    (iter (for (stone frequency) in-hashtable stones-map)
          (for next-stones = (cond ((zerop stone) 1)
                                   ((even-digits-p stone) (split-number stone))
                                   (t (* stone 2024))))
          (iter (for next-stone in (a:ensure-list next-stones))
                (incf (gethash next-stone next-stones-map 0) frequency)))
    next-stones-map))

(defun blink-times (stones-list times)
  (iter (repeat (1+ times))
        (for stones first (list-to-count-map stones-list) then (blink-map stones))
        (finally (return (iter (for (nil v) in-hashtable stones)
                               (summing v))))))

(defun solve-1 (&optional (input (day-input-lines 11 2024)))
  (iter (repeat 26)
        (for stones first (ints (first input)) then (blink stones))
        (finally (return (length stones)))))

(defun list-to-count-map (list)
  (iter (with count-map = (make-hash-table))
        (for elt in list)
        (incf (gethash elt count-map 0))
        (finally (return count-map))))

(defun solve-2 (&optional (input (day-input-lines 11 2024)))
  (blink-times (ints (first input)) 75))
