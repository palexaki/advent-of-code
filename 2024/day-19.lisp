(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-19
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-19)

(defparameter +test-1+ '("r, wr, b, g, bwu, rb, gb, br"
                         ""
                         "brwrr"
                         "bggr"
                         "gbbr"
                         "rrbgbr"
                         "ubwu"
                         "bwurrg"
                         "brgr"
                         "bbrgwb"))

(defun parse-input (input)
  (values (mapcar (a:curry #'map 'list #'identity)
                  (r:split ", " (first input)))
          (mapcar (a:curry #'map 'list #'identity)
                  (cddr input))))

(defun design-possible (available-patterns design)
  (let ((table (make-hash-table :test 'equal)))
    (labels ((possible (design-partial)
               (multiple-value-bind (value has-value)
                   (gethash design-partial table)
                 (cond (has-value value)
                       ((not design-partial) 1)
                       (t
                        (setf (gethash design-partial table)
                              (iter (with design-length = (length design-partial))
                                    (for pattern in available-patterns)
                                    (for pattern-length = (length pattern))
                                    (unless (and (every #'char= pattern design-partial)
                                               (>= design-length pattern-length))
                                      (next-iteration))
                                    (for possible = (possible (subseq design-partial pattern-length)))
                                    (when possible
                                      (summing possible into possibilities))
                                    (finally (return (and (plusp possibilities) possibilities))))))))))
      (possible design))))


(defun solve-1 (&optional (input (day-input-lines 19 2024)))
  (multiple-value-bind (available-patterns designs)
      (parse-input input)
    (count-if (a:curry #'design-possible available-patterns) designs)))

(defun solve-2 (&optional (input (day-input-lines 19 2024)))
  (multiple-value-bind (available-patterns designs)
      (parse-input input)
    (reduce #'+ (remove nil (mapcar (a:curry #'design-possible available-patterns) designs)))))
