(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util)
  (ql:quickload :queues.priority-queue))

(defpackage #:day-16
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-16)

(defparameter +test-1+ '("###############"
                         "#.......#....E#"
                         "#.#.###.#.###.#"
                         "#.....#.#...#.#"
                         "#.###.#####.#.#"
                         "#.#.#.......#.#"
                         "#.#.#####.###.#"
                         "#...........#.#"
                         "###.#.#####.#.#"
                         "#...#.....#.#.#"
                         "#.#.#.###.#.#.#"
                         "#.....#...#.#.#"
                         "#.###.#.#.#.#.#"
                         "#S..#.....#...#"
                         "###############"))

(defparameter +test-2+ '("#################"
                         "#...#...#...#..E#"
                         "#.#.#.#.#.#.#.#.#"
                         "#.#.#.#...#...#.#"
                         "#.#.#.#.###.#.#.#"
                         "#...#.#.#.....#.#"
                         "#.#.#.#.#.#####.#"
                         "#.#...#.#.#.....#"
                         "#.#.#####.#.###.#"
                         "#.#.#.......#...#"
                         "#.#.###.#####.###"
                         "#.#.#...#.....#.#"
                         "#.#.#.#####.###.#"
                         "#.#.#.........#.#"
                         "#.#.#.#########.#"
                         "#S#.............#"
                         "#################"))

(defstruct (state (:type list)
                  (:constructor make-state (position direction score)))
  position
  direction
  score)

(defun state-< (state1 state2)
  (< (state-score state1)
     (state-score state2)))

(defun state-neighbors (map state)
  (destructuring-bind (position direction score)
      state
    (let* (neighbors
           (step-position (pos-add position (direction->delta direction)))
           (next-tile (board-position map step-position)))
      (when (char/= next-tile #\#)
        (push (make-state step-position direction (1+ score)) neighbors))
      (push (make-state position (next-direction direction) (+ score 1000)) neighbors)
      (push (make-state position (prev-direction direction) (+ score 1000)) neighbors)
      neighbors)))

(defun visit-state (visited state)
  (setf (gethash (subseq state 0 2) visited) (state-score state)))

(defun state-visited-p (visited state)
  (gethash (subseq state 0 2) visited))

(defun starting-state (map)
  (iter (with (y-size x-size) = (array-dimensions map))
        (for x below x-size)
        (iter (for y below y-size)
              (for tile = (aref map y x))
              (when (char= tile #\S)
                (return-from starting-state (make-state (make-pos x y) :right 0))))))

(defun find-end-position (map)
  (iter (with (y-size x-size) = (array-dimensions map))
        (for x below x-size)
        (iter (for y below y-size)
              (for tile = (aref map y x))
              (when (char= tile #\E)
                (return-from find-end-position (make-pos x y))))))

(defun end-states-lowest-score (map lowest-score-map)
  (iter (with end-position = (find-end-position map))
        (with states)
        (for direction in '(:up :down :left :right))
        (for state = (list end-position direction))
        (for score = (gethash state lowest-score-map))
        (cond ((or (first-iteration-p)
                   (= score lowest-score))
               (push state states))
              ((< score lowest-score) (setf states (list state))))
        (minimizing score into lowest-score)
        (finally (return states))))

(defun solve-maze (map)
  (iter (with visited = (make-hash-table :test 'equal))
        (with queue = (q:make-queue :priority-queue :compare #'state-<))
        (initially (q:qpush queue (starting-state map)))
        (for state = (q:qpop queue))
        (when (char= #\E (board-position map (state-position state)))
          (return-from solve-maze (state-score state)))
        (unless (state-visited-p visited state)
          (iter (for neighbor in (state-neighbors map state))
                (q:qpush queue neighbor)))
        (visit-state visited state)))

(defun traverse-predecessors-count-tiles (end-states predecessors)
  (iter (with stack = end-states)
        (with visited-pos = (make-hash-table :test 'equal))
        (with visited-state = (make-hash-table :test 'equal))
        (for state = (pop stack))
        (unless state
          (return-from traverse-predecessors-count-tiles (hash-table-count visited-pos)))
        (for unvisited = (remove-if (a:rcurry #'gethash visited-state) (gethash state predecessors)))
        (setf stack (append unvisited stack)
              (gethash (car state) visited-pos) t
              (gethash state visited-state) t)))

(defun tiles-on-shortest-paths (map)
  (declare (optimize (debug 3)))
  (iter (with visited = (make-hash-table :test 'equal))
        (with queue = (q:make-queue :priority-queue :compare #'state-<))
        (with predecessors = (make-hash-table :test 'equal))
        (with position-lowest-score = (make-hash-table :test 'equal))
        (initially (let ((starting-state (starting-state map)))
                     (q:qpush queue starting-state)
                     (visit-state position-lowest-score starting-state)))
        (for state = (q:qpop queue))
        (while state)
        (unless (state-visited-p visited state)
          (iter (for neighbor in (state-neighbors map state))
                (for score = (state-score neighbor))
                (for lowest-score-till-now = (state-visited-p position-lowest-score neighbor))
                (when (or (not lowest-score-till-now) (< score lowest-score-till-now))
                  (visit-state position-lowest-score neighbor)
                  (setf (gethash (subseq neighbor 0 2) predecessors) (list (subseq state 0 2))))
                (when (and lowest-score-till-now (= score lowest-score-till-now))
                  (push (subseq state 0 2) (gethash (subseq neighbor 0 2) predecessors)))
                (q:qpush queue neighbor))
          (visit-state visited state))
        (finally (return-from tiles-on-shortest-paths (traverse-predecessors-count-tiles (end-states-lowest-score map position-lowest-score) predecessors)))))

(defun solve-1 (&optional (input (day-input-lines 16 2024)))
  (solve-maze (make-2d-array input)))

(defun solve-2 (&optional (input (day-input-lines 16 2024)))
  (tiles-on-shortest-paths (make-2d-array input)))

(defun print-map (map &optional shortest-paths)
  (iter (with (n m) = (array-dimensions map))
        (for y below n)
        (iter (for x below m)
              (if (and shortest-paths
                       (gethash (make-pos x y) shortest-paths))
                  (write-char #\O)
                  (write-char (aref map y x))))
        (write-char #\Newline)))
