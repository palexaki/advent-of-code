(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-02
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-02)

(defparameter +test+ '("7 6 4 2 1"
                       "1 2 7 8 9"
                       "9 7 6 2 1"
                       "1 3 2 4 5"
                       "8 6 4 4 1"
                       "1 3 6 7 9"))

(defun safe-p (report)
  (let ((comparator (if (< (first report) (second report))
                        #'<
                        #'>)))
    (iter (for (a b) on report)
          (when b
            (always (and (funcall comparator a b)
                         (<= 1 (abs (- a b)) 3)))))))

(defun remove-nth (n list)
  (iter (for i from 0)
        (for e in list)
        (unless (= i n)
          (collect e))))

(defun safe-dampened-p (report)
  (iter (for i from 0 below (length report))
        (thereis (safe-p (remove-nth i report)))))

(defun solve-1 (&optional (input (day-input-lines 02 2024)))
  (count t input :key (alexandria:compose #'safe-p #'ints)))

(defun solve-2 (&optional (input (day-input-lines 02 2024)))
  (count t input :key (alexandria:compose #'safe-dampened-p #'ints)))
