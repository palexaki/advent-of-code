(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-15
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-15)

(defparameter +test-1+ '("########"
                         "#..O.O.#"
                         "##@.O..#"
                         "#...O..#"
                         "#.#.O..#"
                         "#...O..#"
                         "#......#"
                         "########"
                         ""
                         "<^^>>>vv<v>>v<<"))

(defparameter +test-2+ '("##########"
                         "#..O..O.O#"
                         "#......O.#"
                         "#.OO..O.O#"
                         "#..O@..O.#"
                         "#O#..O...#"
                         "#O..O..O.#"
                         "#.OO.O.OO#"
                         "#....O...#"
                         "##########"
                         ""
                         "<vv>^<v^>v>^vv^v>v<>v^v<v<^vv<<<^><<><>>v<vvv<>^v^>^<<<><<v<<<v^vv^v>^"
                         "vvv<<^>^v^^><<>>><>^<<><^vv^^<>vvv<>><^^v>^>vv<>v<<<<v<^v>^<^^>>>^<v<v"
                         "><>vv>v^v^<>><>>>><^^>vv>v<^^^>>v^v^<^^>v^^>v^<^v>v<>>v^v^<v>v^^<^^vv<"
                         "<<v<^>>^^^^>>>v^<>vvv^><v<<<>^^^vv^<vvv>^>v<^^^^v<>^>vvvv><>>v^<<^^^^^"
                         "^><^><>>><>^^<<^^v>>><^<v>^<vv>>v>>>^v><>^v><<<<v>>v<v<v>vvv>^<><<>^><"
                         "^>><>^v<><^vvv<^^<><v<<<<<><^v<<<><<<^^<v<^^^><^>>^<v^><<<^>>^v<v^v<v^"
                         ">^>>^v>vv>^<<^v<>><<><<v<<v><>v<^vv<<<>^^v^>^^>>><<^v>>v^v><^^>>^<>vv^"
                         "<><^^>^^^<><vvvvv^v<v<<>^v<v>v<<^><<><<><<<^^<<<^<<>><<><^^^>^^<>^>v<>"
                         "^^>vv<^v^v<vv>^<><v<^v>^^^>>>^^vvv^>vvv<>>>^<^>>>>>^<<^v>^vvv<>^<><<v>"
                         "v^^>>><<^^<>>^v^<v^vv<>v^<<>^<^v^v><^<<<><<^<v><v<>vv>>v><v^<vv<>v^<<^"))

(defun parse-input (input &optional widen)
  (let* ((split-position (position "" input :test #'string=))
         (map (subseq input 0 split-position))
         (instructions (subseq input (1+ split-position))))
    (values (make-2d-array (if widen
                               (mapcar #'widen-row map)
                               map))
            (apply #'concatenate 'string instructions))))

(defun widen-row (row)
  (iter (for item in-string row)
        (appending (case item
                     (#\# '(#\# #\#))
                     (#\. '(#\. #\.))
                     (#\O '(#\[ #\]))
                     (#\@ '(#\@ #\.))))))

(defun move-item (map position direction &optional secondary-p)
  (let* ((item (board-position map position))
         (neighbor (pos-add position direction))
         (neighbor-item (board-position map neighbor)))
    (flet ((shift ()
             (unless secondary-p
               (setf (board-position map neighbor) item
                     (board-position map position) #\.))
             t))
      (assert (char/= neighbor-item #\@))
      (cond ((and (char= neighbor-item #\[)
                  (zerop (pos-x direction)))
             (when (and (move-item map neighbor direction t)
                        (move-item map (pos-add neighbor '(1 0)) direction t))
               (unless secondary-p
                 (move-item map neighbor direction)
                 (move-item map (pos-add neighbor '(1 0)) direction))
               (shift)))
            ((and (char= neighbor-item #\])
                  (zerop (pos-x direction)))
             (when (and (move-item map neighbor direction t)
                        (move-item map (pos-add neighbor '(-1 0)) direction t))
               (unless secondary-p
                 (move-item map neighbor direction)
                 (move-item map (pos-add neighbor '(-1 0)) direction))
               (shift)))
            ((and (char= neighbor-item #\[)
                  (zerop (pos-y direction))
                  (move-item map (pos-add '(1 0) neighbor) direction secondary-p))
             (setf (board-position map (pos-add '(1 0) neighbor)) #\[)
             (shift))
            ((and (char= neighbor-item #\])
                  (zerop (pos-y direction))
                  (move-item map (pos-add '(-1 0) neighbor) direction secondary-p))
             (setf (board-position map (pos-add '(-1 0) neighbor)) #\])
             (shift))
            ((char= neighbor-item #\.) (shift))
            ((char= neighbor-item #\#) nil)
            ((and (char= neighbor-item #\O)
                  (move-item map neighbor direction secondary-p))
             (shift))))))

(defun instruction-direction (instruction)
  (case instruction
    (#\> '(1 0))
    (#\v '(0 1))
    (#\< '(-1 0))
    (#\^ '(0 -1))))

(defun find-robot (map)
  (iter (for x below (array-dimension map 1))
        (iter (for y below (array-dimension map 0))
              (for item = (aref map y x))
              (when (char= item #\@)
                (return-from find-robot (make-pos x y))))))

(defun box-gps-sum (map)
  (iter outer
        (for x below (array-dimension map 1))
        (iter (for y below (array-dimension map 0))
              (when (find (aref map y x) '(#\O #\[ ) :test #'char=)

                (in outer (summing (+ x (* y 100))))))))

(defun print-map (map)
  (iter (for y below (array-dimension map 0))
        (format t "~{~a~}~%"
                (iter (for x below (array-dimension map 1))
                      (collect (aref map y x))))))

(defun follow-instructions (map instructions)
  (iter (with robot-position = (find-robot map))
        (for instruction in-string instructions)
        (for direction = (instruction-direction instruction))
        (for moved-p = (move-item map robot-position direction))
        (when moved-p
          (pos-addf robot-position direction))
        (finally (return (box-gps-sum map)))))

(defun solve-1 (&optional (input (day-input-lines 15 2024)))
  (multiple-value-call #'follow-instructions (parse-input input)))

(defun solve-2 (&optional (input (day-input-lines 15 2024)))
  (multiple-value-call #'follow-instructions (parse-input input t)))
