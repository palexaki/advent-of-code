(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util)
  (ql:quickload :zpng))
(defpackage #:day-14
  (:use #:advent-util
        #:cl
        #:iterate
                                        ;#:drakma
                                        ;#:cl-ppcre
                                        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:local-nicknames (#:a #:alexandria) (#:r #:cl-ppcre)))

(in-package #:day-14)

(defparameter +test-1+ '("p=0,4 v=3,-3"
                         "p=6,3 v=-1,-3"
                         "p=10,3 v=-1,2"
                         "p=2,0 v=2,-1"
                         "p=0,0 v=1,3"
                         "p=3,0 v=-2,-2"
                         "p=7,6 v=-1,-3"
                         "p=3,0 v=-1,-2"
                         "p=9,3 v=2,3"
                         "p=7,3 v=-1,2"
                         "p=2,4 v=2,-3"
                         "p=9,5 v=-3,-3"))

(defparameter *space-width* 101)
(defparameter *space-height* 103)

(defun move-robot (initial-state &optional (steps 100) (width *space-width*) (height *space-height*))
  (destructuring-bind (px py vx vy)
      initial-state
    (list (mod (+ px (* vx steps)) width)
          (mod (+ py (* vy steps)) height))))

(defun in-quadrant-p (position &optional (width *space-width*) (height *space-height*))
  (not (and (= (pos-x position) (floor width 2))
            (= (pos-y position) (floor height 2)))))

(defun quadrant-number (position &optional (width *space-width*) (height *space-height*))
  (let ((x (pos-x position))
        (y (pos-y position))
        (mx (floor width 2))
        (my (floor height 2)))
    (cond ((and (< x mx) (< y my)) 2)
          ((and (> x mx) (< y my)) 1)
          ((and (< x mx) (> y my)) 3)
          ((and (> x mx) (> y my)) 4))))

(defun safety-factor (robots &optional (steps 100) (width *space-width*) (height *space-height*))
  (let ((*space-width* width)
        (*space-height* height))
    (iter (with quadrant-count = (make-hash-table))
          (for robot in robots)
          (for quadrant = (quadrant-number (move-robot robot steps)))
          (when quadrant
            (incf (gethash quadrant quadrant-count 0)))
          (finally (return (reduce #'* (a:hash-table-values quadrant-count)))))))

(defun solve-1 (&optional (input (day-input-lines 14 2024)))
  (safety-factor (mapcar #'ints input)))

(defun print-robots (robots &optional (steps 100) (width *space-width*) (height *space-height*))
  (let ((*space-width* width)
        (*space-height* height)
        (robots (mapcar (lambda (robot) (move-robot robot steps width height)) robots)))
    (format t "Configuration at step ~a:~%" steps)
    (iter (for y below height)
          (format t "~{~a~}~%"
                  (iter (for x below width)
                        (collect (if (find (list x y) robots :test 'equal)
                                     #\#
                                     #\.)))))
    (format t "~%")))

(defun save-robots-to-image (robots &optional (steps 100) (width *space-width*) (height *space-height*))
  (let ((*space-width* width)
        (*space-height* height)
        (robots (mapcar (lambda (robot) (move-robot robot steps width height)) robots))
        (path (merge-pathnames (format nil "14/~4,'0d.png" steps)))
        (image (make-instance 'zpng:png
                              :color-type :grayscale
                              :width width
                              :height height))
        data)
    (setf data (zpng:data-array image))
    (iter (for y below height)
          (iter (for x below width)
                (setf (aref data y x 0)
                      (if (find (list x y) robots :test 'equal)
                          255
                          0))))
    (zpng:write-png image path)))

(defun solve-2 (&optional (input (day-input-lines 14 2024)))
  (let ((robots (mapcar #'ints input)))
    (iter (for steps from 0 below 9999)
          (save-robots-to-image robots steps))))
