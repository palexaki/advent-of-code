(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-5
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-5)

(defparameter *test* '("0,9 -> 5,9"
                       "8,0 -> 0,8"
                       "9,4 -> 3,4"
                       "2,2 -> 2,1"
                       "7,0 -> 7,4"
                       "6,4 -> 2,0"
                       "0,9 -> 2,9"
                       "3,4 -> 1,4"
                       "0,0 -> 8,8"
                       "5,5 -> 8,2"))

(parseq:defrule input ()
    (+ (string line))
  (:lambda (&rest things)
    (mapcar #'first things)))

(parseq:defrule line ()
    (and int "," int " -> " int "," int)
  (:lambda (&rest items)
    (remove-if-not #'numberp items))
  (:lambda (x1 y1 x2 y2)
    (list (list x1 y1)
          (list x2 y2))))

(defun line-straight-p (line)
  (destructuring-bind ((x1 y1) (x2 y2))
      line
    (or (= x1 x2)
        (= y1 y2))))

(defun fill-straight (map line)
  (destructuring-bind ((x1 y1) (x2 y2))
      line
    (let ((d1 (if (= x1 x2) (min y1 y2) (min x1 x2)))
          (d2 (if (= x1 x2) (max y1 y2) (max x1 x2)))
          (s (if (= x1 x2) x1 y1))
          (dd (if (= x1 x2) :x :y)))
      (loop :for d :from d1 :upto d2
            :do (incf (gethash (if (eq :x dd)
                                   (list s d)
                                   (list d s))
                               map 0)))
      map)))

(defun fill-line (map line)
  (if (line-straight-p line)
      (fill-straight map line)
      (loop :with ((x1 y1) (x2 y2)) := line
            :with xmin := (min x1 x2)
            :and  ymin := (min y1 y2)
            :and  ymax := (max y1 y2)
            :for x :from 0 :upto (abs (- x1 x2))
            :for y :from 0 :upto (abs (- x1 x2))
            :do (incf (gethash (if (or (and (< x1 x2)
                                            (< y1 y2))
                                       (and (> x1 x2)
                                            (> y1 y2)))
                                   (list (+ xmin x) (+ ymin y))
                                   (list (+ xmin x) (- ymax y))) map 0))
            :finally (return map))))

(defun count-intersections (map)
  (map-hash-collect (intersections 0) map
      (_ val)
    (if (> val 1) (incf intersections))))

(defun solve-1 (&optional (input (day-input-lines 5 2021)))
  (let ((map (make-hash-table :test 'equal)))
    (mapcar (alexandria:curry #'fill-straight map)
            (remove-if-not #'line-straight-p (parseq:parseq 'input input)))
    (count-intersections map)))

(defun solve-2 (&optional (input (day-input-lines 5 2021)))
  (let ((map (make-hash-table :test 'equal)))
    (mapcar (alexandria:curry #'fill-line map)
            (parseq:parseq 'input input))
    (count-intersections map)))
