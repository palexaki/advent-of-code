(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-3
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-3)

(defparameter *test-1* '("00100"
                         "11110"
                         "10110"
                         "10111"
                         "10101"
                         "01111"
                         "00111"
                         "11100"
                         "10000"
                         "11001"
                         "00010"
                         "01010"))

(defun count-zero (bit numbers)
  (count-if (lambda (num)
              (char= #\0 (char num bit)))
            numbers))

(defun solve-1 (&optional (input (day-input-lines 3 2021)))
  (loop :with total-count := (/ (length input) 2)
        :and input := (mapcar #'reverse input)
        :and gamma := 0
        :and epsilon := 0
        :for bit :from 0 :below (length (first input))
        :for bit-mask := (expt 2 bit)
        :for zeros := (count-zero bit input)
        :do (if (> zeros total-count)
                (setf epsilon (logior bit-mask epsilon))
                (setf gamma (logior bit-mask gamma)))
            ;(format t "zeros: ~a~%gamma:    ~5,'0b~%epsilon:  ~5,'0b~%bit-mask: ~5,'0b~%~%" zeros gamma epsilon bit-mask)
        :finally (return (values (* gamma epsilon)
                                 gamma
                                 epsilon
                                 total-count))))

(defun rating (numbers bit most-common)
  (when (= bit (length (first numbers)))
    (return-from rating (values (first numbers) bit)))
  (let ((len (length numbers))
        (zero-count (count-zero bit numbers)))
    (if (= len 1)
        (first numbers)
        (let ((common-bit (cond ((= zero-count (/ len 2)) (if most-common #\1 #\0))
                                ((< zero-count (/ len 2)) (if most-common #\1 #\0))
                                (t (if most-common #\0 #\1)))))
          (rating (remove-if-not (lambda (number)
                                   (char= common-bit (char number bit)))
                                 numbers)
                  (1+ bit)
                  most-common)))))

(defun solve-2 (&optional (input (day-input-lines 3 2021)))
  (* (parse-integer (rating input 0 t) :radix 2)
     (parse-integer (rating input 0 nil) :radix 2)))
