(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-21
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule))

(in-package #:day-21)

(defun shifted-mod (number divisor)
  (let ((modular (mod number divisor)))
    (if (zerop modular)
        divisor
        modular)))

(defun parse-input (position-line)
  (parse-integer position-line :start (1+ (position #\: position-line))
                               :junk-allowed t))

(defun throw-die (start)
  (loop :for n := start :then (shifted-mod (1+ n) 100)
        :repeat 3
        :summing n :into sum
        :finally (return (values sum n))))

(defun play-game (players)
  (loop :with die := 1
        :with player-positions := players
        :with player-scores := (list 0 0)
        :for throws :from 0 :by 3
        :for player := 0 :then (mod (1+ player) 2)
        :do (when (>= (apply #'max player-scores) 1000)
              (return (* throws (apply #'min player-scores))))
        :do (multiple-value-bind (die-value next-die) (throw-die die)
              (setf die next-die)
              (setf (nth player player-positions) (shifted-mod (+ (nth player player-positions)
                                                                  die-value)
                                                               10))
              (incf (nth player player-scores) (nth player player-positions)))))


(defun solve-1 (&optional (input (day-input-lines 21 2021)))
  (play-game (mapcar #'parse-input input)))

(defun throw-quantum-die ()
  (let ((value-frequencies (make-hash-table)))
    (dotimes (i 3)
      (dotimes (j 3)
        (dotimes (k 3)
          (incf (gethash (+ i j k 3) value-frequencies 0)))))
    (alexandria:hash-table-alist value-frequencies)))

(defun initial-state (starts)
  (let ((states (make-hash-table :test #'equal)))
    (setf (gethash (list starts (list 0 0)) states) 1)
    states))

(defun with (list value position)
  (fill (copy-list list) value :start position :end (1+ position)))

(defun quantum-turn (states player)
  (let ((die-frequencies (throw-quantum-die))
        (next-states (make-hash-table :test #'equal)))
    (loop :for (positions scores) :being :each :hash-key :in states :using (:hash-value frequency)
          :do (loop :for (value . value-frequency) :in die-frequencies
                    :as new-pos := (shifted-mod (+ (nth player positions) value) 10)
                    :as new-score := (+ new-pos (nth player scores))
                    :do (incf (gethash (list (with positions new-pos player)
                                             (with scores new-score player))
                                       next-states 0)
                              (* frequency value-frequency))))
    next-states))

(defun remove-player-wins (states player)
  (loop :for state :being :each :hash-key :in states :using (:hash-value times)
        :when (>= (nth player (second state)) 21)
          :do (remhash state states) :and :sum times))

(defun play-quantum-game (starts)
  (let ((wins (list 0 0)))
    (loop :as player := 1 :then (mod (1+ player) 2)
          :as states := (initial-state starts) :then (quantum-turn states player)
          :do (incf (nth player wins) (remove-player-wins states player))
          :when (zerop (hash-table-count states))
            :do (return wins))))

(defun solve-2 (&optional (input (day-input-lines 21 2021)))
  (play-quantum-game (mapcar #'parse-input input)))
