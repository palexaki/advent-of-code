(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-6
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-6)

(defparameter *test* '("3,4,3,1,2"))

(parseq:defrule input ()
    (+ (and int (? ",")))
  (:lambda (&rest nums)
    (mapcar #'first nums))
  (:lambda (&rest nums)
    (let ((fish (make-array 9)))
      (mapc (lambda (n) (incf (aref fish n))) nums)
      fish)))

(defun evolve-day (fish)
  (let ((new-fish (make-array 9)))
    (dotimes (i 9)
      (if (= i 0)
          (setf (aref new-fish 6) (aref fish 0)
                (aref new-fish 8) (aref fish 0))
          (incf (aref new-fish (1- i)) (aref fish i))))
    new-fish))

(defun evolve-x-days (first-fish days)
  (loop :repeat days
        :for fish := (evolve-day first-fish) :then (evolve-day fish)
        :finally (return (values (reduce #'+ fish)
                                 fish))))

(defun solve-1 (&optional (input (day-input-lines 6 2021)))
  (evolve-x-days (parseq:parseq 'input (first input)) 80))

(defun solve-2 (&optional (input (day-input-lines 6 2021)))
  (evolve-x-days (parseq:parseq 'input (first input)) 256))
