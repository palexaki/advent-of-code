(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util)
  (ql:quickload :parseq))
(defpackage #:day-2
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-2)

(parseq:defrule commands ()
    (+ (string (or forward down up)))
  (:flatten))

(parseq:defrule forward ()
    (and "forward " num)
  (:lambda (c i)
    (declare (ignore c))
    (complex i)))

(parseq:defrule down ()
    (and "down " num)
  (:lambda (c i)
    (declare (ignore c))
    (complex 0 i)))


(parseq:defrule up ()
    (and "up " num)
  (:lambda (c i)
    (declare (ignore c))
    (complex 0 (- i))))

(parseq:defrule num ()
    (+ digit)
  (:string)
  (:lambda (x) (parse-integer x)))




(defun solve-1 (&optional (input (day-input-lines 2 2021)))
  (let ((final-pos (reduce #'+
                           (parseq:parseq 'commands input))))
    (values (* (realpart final-pos)
               (imagpart final-pos))
            final-pos)))



(defun solve-2 (&optional (input (day-input-lines 2 2021)))
  (let ((final-state (reduce
                      (lambda+ ((h d a) command)
                        (let ((r (realpart command))
                              (i (imagpart command)))
                          (cond
                            ((zerop i) (list (+ h r)
                                             (+ (* a r) d)
                                             a))
                            (t (list h d (+ a i))))))
                      (parseq:parseq 'commands input)
                      :initial-value '(0 0 0))))
    (values (* (first final-state)
               (second final-state))
            final-state)))
