(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-4
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-4)

(parseq:defrule input ()
    (and (string numbers)
         (+ board))
  (:lambda (numbers boards)
    (list (first numbers)
          boards)))

(parseq:defrule numbers ()
    (+ (and int (? ",")))
  (:lambda (&rest numbers) (mapcar #'first numbers)))

(parseq:defrule board ()
    (and "" (rep 5 (string (rep 5 (and (* " ") int)))))
  (:lambda (&rest board)
    (make-array '(5 5)
                :initial-contents  (mapcar (lambda (row)
                                             (mapcar (alexandria:compose (lambda (i)
                                                                           (list i :unmarked))
                                                                         #'second)
                                                     (first row)))
                                           (second board)))))


(defun board-win-p (board)
  (loop :for x :from 0 :upto 4
          :thereis (or (loop :for y :from 0 :upto 4
                             :always (eq :marked (second (aref board x y))))
                       (loop :for y :from 0 :upto 4
                             :always (eq :marked (second (aref board y x)))))))

(defun mark (number board)
  (loop :with board-copy := (alexandria:copy-array board)
        :for x :from 0 :upto 4
        :do (loop :for y :from 0 :upto 4
                  :when (= number (first (aref board x y)))
                    :do (setf (aref board-copy x y) (list number :marked)))
        :finally (return board-copy)))

(defun score-winner (board last-num)
  (* last-num
     (loop :for x :from 0 :upto 4
           :sum (loop :for y :from 0 :upto 4
                      :when (eq :unmarked (second (aref board x y)))
                      :sum (first (aref board x y))))))

(defun determine-winner (numbers boards)
  (let* ((number (first numbers))
         (boards (mapcar (alexandria:curry #'mark number) boards))
         (winner (find-if #'board-win-p boards)))
    (if winner
        (score-winner winner number)
        (determine-winner (rest numbers) boards))))

(defun determine-last-winner (numbers boards)
  (let* ((number (first numbers))
         (boards (mapcar (alexandria:curry #'mark number) boards))
         (losers (remove-if #'board-win-p boards)))
    (if (and boards (not losers))
        (score-winner (first boards) number)
        (determine-last-winner (rest numbers) losers))))

(defun solve-1 (&optional (input (day-input-lines 4 2021)))
  (let ((input (parseq:parseq 'input input)))
    (apply #'determine-winner input)))

(defun solve-2 (&optional (input (day-input-lines 4 2021)))
  (let ((input (parseq:parseq 'input input)))
    (apply #'determine-last-winner input)))
