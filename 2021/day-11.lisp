(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-11
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-11)

(defparameter *test* '("5483143223"
                       "2745854711"
                       "5264556173"
                       "6141336146"
                       "6357385478"
                       "4167524645"
                       "2176841721"
                       "6882881134"
                       "4846848554"
                       "5283751526"))

(parseq:defrule input ()
  (+ line)
  (:lambda (&rest rows)
    (make-array (list (length rows)
                      (length (first rows)))
                :initial-contents rows)))

(parseq:defrule line ()
  (string (+ digit))
  (:choose 0)
  (:lambda (&rest digits)
    (mapcar (lambda (digit)
              (- (char-code digit) 48))
            digits)))

(defun step-octos (octopuses)
  ;(declare (optimize (debug 3)))
  (let ((flashes 0)
        (flash-locations nil)
        (octo-copy (alexandria:copy-array octopuses)))
    (loop :for x :from 0 :upto 9
          :do (loop :for y :from 0 :upto 9
                    :do (when (= (incf (aref octo-copy y x)) 10)
                          (push (make-pos x y) flash-locations)
                          (incf flashes))))
    (loop :with flash-l := (copy-list flash-locations)
          :while flash-l
          :for (x y) := (pop flash-l)
          :do (loop :for x1 :from (max 0 (- x 1)) :upto (min 9 (+ x 1))
                    :do (loop :for y1 :from (max 0 (- y 1)) :upto (min 9 (+ y 1))
                              :do (when (= (incf (aref octo-copy y1 x1)) 10)
                                    (push (make-pos x1 y1) flash-locations)
                                    (push (make-pos x1 y1) flash-l)
                                    (incf flashes)))))
    (loop :for flash :in flash-locations
          :do (setf (board-position octo-copy flash) 0))
    (assert (= flashes (length flash-locations)))
    (values octo-copy
            flashes)))

(defun solve-1 (&optional (input (day-input-lines 11 2021)))
  (let ((octos (parseq:parseq 'input input)))
    (loop :repeat 100
          :for (octoss flashes) := (multiple-value-list (step-octos octos)) :then (multiple-value-list (step-octos octoss))
          :sum flashes)))

(defun solve-2 (&optional (input (day-input-lines 11 2021)))
  (let ((octos (parseq:parseq 'input input)))
    (loop :for step :upfrom 1
          :for (octoss flashes) := (multiple-value-list (step-octos octos)) :then (multiple-value-list (step-octos octoss))
          :when (= flashes 100)
            :do (return step))))
