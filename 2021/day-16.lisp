(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-16
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule))
(in-package #:day-16)

(defun read-literal-packet (&optional (stream *standard-input*))
  (let ((digits (make-array 16 :element-type 'character
                               :adjustable t :fill-pointer 4))
        (dim 16)
        (fill 0))
    (loop :for more := (read-char stream)
          :while (char= more #\1)
          :do (read-sequence digits stream :start fill)
              (incf fill 4)
              (adjust-array digits (max 16 (+ fill 4)) :fill-pointer (+ fill 4))
              (setf dim (array-dimension digits 0))
          :finally (read-sequence digits stream :start fill))
    (values (parse-integer digits :radix 2)
            (+ (/ fill 4)
               fill
               5
               6))))

(defun read-operator-packet (&optional (stream *standard-input*))
  (let* ((length-type (if (char= #\0 (read-char stream))
                         'total-length
                         'number))
         (length-string (make-array (case length-type
                                      (total-length 15)
                                      (number 11))
                                    :element-type 'character))
         (length-limit (progn (read-sequence length-string stream)
                              (parse-integer length-string :radix 2))))
    (loop :for (sub-packet length) := (multiple-value-list (read-packet stream))
          :for packet-count :upfrom 1
          :collect sub-packet :into packets
          :sum length :into sub-length
          :when (case length-type
                  (total-length (>= sub-length length-limit))
                  (number (>= packet-count length-limit)))
            :return (values packets
                            (+ 1
                               (case length-type
                                 (total-length 15)
                                 (number 11))
                               sub-length
                               6)))))

(defun read-packet (&optional (stream *standard-input*))
  (let ((version (make-array 3 :element-type 'character))
        (type-id (make-array 3 :element-type 'character)))
    (read-sequence version stream)
    (read-sequence type-id stream)
    (setf version (parse-integer version :radix 2)
          type-id (parse-integer type-id :radix 2))
    (multiple-value-bind (packet length)
        (cond ((= 4 type-id) (read-literal-packet stream))
              (t (read-operator-packet stream)))
      (values (list version type-id packet)
              length))))

(defun hexa-to-binary (hex)
  (apply #'concatenate 'string
         (loop :for d :across hex
               :collect (format nil "~4,'0b" (digit-char-p d 16)))))

(defun parse-packet (hex)
  (with-input-from-string (stream (hexa-to-binary hex))
    (read-packet stream)))

(defun sum-version-numbers (packet)
  (+ (first packet)
     (if (numberp (third packet))
         0
         (reduce #'+ (mapcar #'sum-version-numbers (third packet))))))

(defun solve-1 (&optional (input (day-input-lines 16 2021)))
  (sum-version-numbers (parse-packet (first input))))

(defun eval-packet (packet)
  (destructuring-bind (version type value) packet
    (declare (ignore version))
    (cond
      ((= type 4) value)
      ((= type 0) (reduce #'+ (mapcar #'eval-packet value)))
      ((= type 1) (reduce #'* (mapcar #'eval-packet value)))
      ((= type 2) (reduce #'min (mapcar #'eval-packet value)))
      ((= type 3) (reduce #'max (mapcar #'eval-packet value)))
      ((= type 5) (if (> (eval-packet (first value))
                         (eval-packet (second value)))
                      1 0))
      ((= type 6) (if (< (eval-packet (first value))
                         (eval-packet (second value)))
                      1 0))
      ((= type 7) (if (= (eval-packet (first value))
                         (eval-packet (second value)))
                      1 0)))))

(defun solve-2 (&optional (input (day-input-lines 16 2021)))
  (eval-packet (parse-packet (first input))))
