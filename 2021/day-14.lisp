(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-14
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule))

(in-package #:day-14)

(defparameter *test* '("NNCB"
                       ""
                       "CH -> B"
                       "HH -> N"
                       "CB -> H"
                       "NH -> C"
                       "HB -> C"
                       "HC -> B"
                       "HN -> C"
                       "NN -> C"
                       "BH -> H"
                       "NC -> B"
                       "NB -> B"
                       "BN -> B"
                       "BB -> N"
                       "BC -> B"
                       "CC -> N"
                       "CN -> C"))

(defrule input ()
    (and (string template) "" (+ (string rule)))
  (:choose '(0 0) 2)
  (:lambda (template rules)
    (list template
          (mapcar #'first rules))))

(defrule template ()
    (+ alpha)
  (:lambda (&rest letters)
    (let ((polymer (make-hash-table :test 'equal)))
      (mapc (compose (lambda (l)
                       (incf (gethash l polymer 0)))
                     #'list)
            letters (rest letters))
      polymer)))

(defrule rule ()
    (and (and alpha alpha) " -> " alpha)
  (:choose 0 2))

(defun count-polymer (polymer)
  (let ((char-count (make-hash-table)))
    (maphash (lambda+ ((char-a char-b) count)
               (incf (gethash char-a char-count 0) count)
               (incf (gethash char-b char-count 0) count))
             polymer)
    (map-hash-collect l char-count (char count)
      (push (list char (ceiling count 2)) l))))


(defun apply-rules (polymer rules)
  (let ((new-polymer (make-hash-table :test 'equal)))
    (maphash (lambda (pat count)
               (alexandria:if-let (rule (find pat rules :key #'first :test 'equal))
                 (progn (incf (gethash (list (first pat) (second rule))
                                         new-polymer 0)
                                count)
                          (incf (gethash (list (second rule) (second pat))
                                         new-polymer 0)
                                count))
                 (incf (gethash pat new-polymer 0) count)))
             polymer)
    new-polymer))

(defun score-polymer (polymer)
  (let ((counts (mapcar #'second (count-polymer polymer))))
    (- (reduce #'max counts)
       (reduce #'min counts))))

(defun pair-insert-n (template rules n)
  (loop :repeat (1+ n)
        :for polymer := template :then (apply-rules polymer rules)
        :finally (return polymer)))

(defun solve-1 (&optional (input (day-input-lines 14 2021)))
  (destructuring-bind (template rules) (parseq 'input input)
    (score-polymer (pair-insert-n template rules 10))))

(defun solve-2 (&optional (input (day-input-lines 14 2021)))
  (destructuring-bind (template rules) (parseq 'input input)
    (score-polymer (pair-insert-n template rules 40))))
