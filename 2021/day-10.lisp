(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-10
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-10)

(defparameter *test* '("[({(<(())[]>[[{[]{<()<>>"
                       "[(()[<>])]({[<{<<[]>>("
                       "(((({<>}<{<{<>}{[]{[]{}"
                       "{<[[]]>}<{[{[{[]{()[[[]"
                       "<{([{{}}[<[[[<>{}]]]>[]]"))


(defun char-score (char)
  (cdr (assoc char '((#\) . 3)
                     (#\] . 57)
                     (#\} . 1197)
                     (#\> . 25137)))))


(defun char-autocomplete-score (char)
  (let ((pos (position char '(#\( #\[ #\{ #\<))))
    (if pos
        (1+ pos)
        (error "should be a closing bracket: ~a" char))))

(defun opening-p (char)
  (find char '(#\( #\[ #\{ #\<)))

(defun match (char-open char-close)
  (char= char-close (cdr (assoc char-open '((#\( . #\))
                                            (#\[ . #\])
                                            (#\{ . #\})
                                            (#\< . #\>))))))

(defun line-score (line)
  (loop :with stack
        :for char :across line
        :do (if (opening-p char)
                (push char stack)
                (unless (match (pop stack) char)
                  (return-from line-score (char-score char)))))
  0)

(defun solve-1 (&optional (input (day-input-lines 10 2021)))
  (reduce #'+ (mapcar #'line-score input)))

(defun line-autocomplete (line)
  (loop :with stack
        :for char :across line
        :do (if (opening-p char)
                (push char stack)
                (pop stack))
        :finally (return stack)))

(defun autocomplete-score (stack)
  (reduce (lambda (s c)
            (+ (* s 5) (char-autocomplete-score c)))
          stack
          :initial-value 0))

(defun solve-2 (&optional (input (day-input-lines 10 2021)))
  (alexandria:median (mapcar (alexandria:compose #'autocomplete-score #'line-autocomplete) (remove-if-not #'zerop input :key #'line-score))))
