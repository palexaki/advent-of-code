(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-8
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        )
  (:import-from #:alexandria #:curry))

(in-package #:day-8)

(parseq:defrule line ()
    (and (+ (and signals " ")) "| " (+ (and signals (? " "))))
  (:lambda (ps sep out)
    (declare (ignore sep))
    (list (mapcar #'first ps)
          (mapcar #'first out))))

(parseq:defrule signals ()
    (+ alpha))

(defun length-unique (len)
  (or (<= 2 len 4)
      (= len 7)))

(defun count-uniques (line)
  (count-if #'length-unique (second line) :key #'length))

(defun total-uniques (lines)
  (reduce (lambda (u line) (+ u (count-uniques line))) lines :initial-value 0))

(defun solve-1 (&optional (input (day-input-lines 8 2021)))
  (total-uniques (mapcar (curry #'parseq:parseq 'line) input)))

(defun set-equal (set-1 set-2)
  (null (set-exclusive-or set-1 set-2)))

(defun deduce-numbers (line)
  (let* ((line (list (mapcar (lambda (seq)
                               (sort seq #'char<))
                             (first line))
                     (mapcar (lambda (seq)
                               (sort seq #'char<))
                             (second line))))
         (pats (first line))
         (one (find 2 pats :key #'length))
         (seven (find 3 pats :key #'length))
         (four (find 4 pats :key #'length))
         (eight (find 7 pats :key #'length))
         (six-nine-zero (remove-if-not (curry #'= 6) pats :key #'length))
         (nine-zero (remove-if-not (curry #'subsetp one) six-nine-zero))
         (six (first (set-difference six-nine-zero nine-zero)))
         (nine (find-if (curry #'subsetp four) nine-zero))
         (zero (first (remove nine nine-zero)))
         (two-three-five (remove-if-not (curry #'= 5) pats :key #'length))
         (three (find-if (curry #'subsetp one) two-three-five))
         (left (first (set-difference four three)))
         (right (first (set-difference seven six)))
         (two (find-if (curry #'find right) (remove three two-three-five)))
         (five (find-if (curry #'find left) two-three-five))
         (numbers (list zero one two three four five six seven eight nine)))
    (calc-result (mapcar (lambda (thing)
                           (position thing
                                     numbers
                                     :test #'equal))
                         (second line)))))

(defun calc-result (nums)
  (destructuring-bind (a b c d)
      nums
    (+ (* 1000 a)
       (* 100  b)
       (* 10   c)
       d)))

(defun solve-2 (&optional (input (day-input-lines 8 2021)))
  (let ((lines (mapcar (curry #'parseq:parseq 'line) input)))
    (reduce #'+ (mapcar #'deduce-numbers lines))))
