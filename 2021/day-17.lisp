(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-17
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule))

(in-package #:day-17)

(defparameter *test* '("target area: x=20..30, y=-10..-5"))

(defrule input ()
    (and "target area: x=" (? "-") int ".." (? "-") int ", y=" (? "-") int ".." (? "-") int)
  (:choose 1 2 4 5 7 8 10 11)
  (:lambda (nx1 x1 nx2 x2 ny1 y1 ny2 y2)
    (list (list (if nx1 (- x1) x1)
                (if nx2 (- x2) x2))
          (list (if ny1 (- y1) y1)
                (if ny2 (- y2) y2)))))

(defun triangle-number (n)
  (/ (* n (1+ n)) 2))

(defun find-hitting-speed (min max)
  (loop :for x :upfrom 1
        :for trn := (triangle-number x)
        :when (<= min trn max)
          :do (return x)
        :when (> trn max)
          :do (return nil)))

(defun calc-trajectory (y-v y-min y-max)
  (let* ((height (triangle-number y-v))
         (max-fall (- height y-min))
         (min-fall (- height y-max)))
    (if (find-hitting-speed min-fall max-fall)
        height
        nil)))

(defun calc-max-y (y-min y-max)
  (loop :for y-v :upfrom 0 :below 1000
        :when (calc-trajectory y-v y-min y-max)
          :maximize :it))

(defun solve-1 (&optional (input (day-input-lines 17 2021)))
  (let ((area (parseq 'input (first input))))
    (apply #'calc-max-y (second area))))

(defun configuration-hits (x-v y-v x-min x-max y-min y-max)
  (loop :for xv := x-v :then (max 0 (- xv 1))
        :for x := xv :then (+ xv x)
        :for yv := y-v :then (- yv 1)
        :for y := yv :then (+ y yv)
        :when (and (<= x-min x x-max)
                   (<= y-min y y-max))
          :do (return t)
        :when (or (> x x-max)
                  (< y y-min))
          :do (return nil)))

(defun find-all-configurations (x-min x-max y-min y-max)
  (loop :for x-v :from (find-hitting-speed x-min x-max) :upto x-max
        :nconc (loop :for y-v :from y-min :upto (1- (- y-min))
                     :when (configuration-hits x-v y-v
                                               x-min x-max
                                               y-min y-max)
                       :collect (list x-v y-v))))

(defun solve-2 (&optional (input (day-input-lines 17 2021)))
  (let ((area (parseq 'input (first input))))
    (length (apply #'find-all-configurations (alexandria:flatten area)))))
