(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-13
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule))

(in-package #:day-13)

(defvar *test* '("6,10"
                 "0,14"
                 "9,10"
                 "0,3"
                 "10,4"
                 "4,11"
                 "6,0"
                 "6,12"
                 "4,1"
                 "0,13"
                 "10,12"
                 "3,4"
                 "3,0"
                 "8,4"
                 "1,10"
                 "2,14"
                 "8,10"
                 "9,0"
                 ""
                 "fold along y=7"
                 "fold along x=5"))

(defrule input ()
    (and (+ (string coordinate))
         ""
         (+ (string folding)))
  (:choose 0 2)
  (:lambda (ps ls)
    (list (mapcar #'first ps)
          (mapcar #'first ls))))

(defrule coordinate ()
    (and int "," int)
  (:choose 0 2))

(defrule folding ()
    (and "fold along " (or "y" "x") "=" int)
  (:choose 1 3)
  (:lambda (v n) (list (intern (string-upcase v)) n)))

(defun fold-point (var num point)
  (destructuring-bind (x y) point
    (list (if (and (eq var 'x)
                   (> x num))
              (+ num num (- x))
              x)
          (if (and (eq var 'y)
                   (> y num))
              (+ num num (- y))
              y))))

(defun fold-along (points line)
  (remove-duplicates (mapcar (curry #'fold-point (first line) (second line))
                             points)
                     :test 'equal))

(defun solve-1 (&optional (input (day-input-lines 13 2021)))
  (destructuring-bind (points lines) (parseq 'input input)
    (length (fold-along points (first lines)))))

(defun solve-2 (&optional (input (day-input-lines 13 2021)))
  (destructuring-bind (points lines) (parseq 'input input)
    (print-points (reduce (lambda (ps line)
                            (fold-along ps line))
                          lines
                          :initial-value points))))

(defun print-points (points)
  (let ((min-x (reduce #'min points :key #'first))
        (max-x (reduce #'max points :key #'first))
        (min-y (reduce #'min points :key #'second))
        (max-y (reduce #'max points :key #'second)))
    (loop :for y :from min-y :upto max-y
          :do (loop :for x :from min-x :upto max-x
                    :do (if (find (list x y) points :test 'equal)
                            (format t "#")
                            (format t " ")))
              (format t "~%"))))
