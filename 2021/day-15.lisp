(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util)
  (ql:quickload :queues.priority-queue))
(defpackage #:day-15
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule)
  (:import-from #:pqueue
                #:make-pqueue
                #:pqueue-push
                #:pqueue-pop
                #:pqueue-empty-p))

(in-package #:day-15)

(defparameter *test* '("1163751742"
                       "1381373672"
                       "2136511328"
                       "3694931569"
                       "7463417111"
                       "1319128137"
                       "1359912421"
                       "3125421639"
                       "1293138521"
                       "2311944581"))

(defrule map ()
    (+ (string line))
  (:lambda (&rest ls)
    (let ((contents (mapcar #'first ls)))
      (make-array (list (length contents)
                        (length (first contents)))
                  :initial-contents contents))))

(defrule line ()
  (+ dig))

(defrule dig ()
  digit
  (:string)
  (:lambda (n) (parse-integer n)))

(defun pos-tile-neighbors (map pos multiplier)
  (let ((dimensions (mapcar (compose #'1- (curry #'* multiplier))
                            (array-dimensions map))))
    (remove-if-not (lambda (n)
                     (every #'<= '(0 0) n dimensions))
               (advent-util::unsafe-pos-neighbors pos))))

(defun position-risk (map position)
  (let* ((dimensions (array-dimensions map))
         (simple-coord (mapcar #'mod position dimensions))
         (additions (mapcar #'floor position dimensions))
         (simple-risk (apply #'aref map simple-coord))
         (risk (reduce #'+ additions :initial-value simple-risk)))
    (when (some (curry #'< 4) additions)
      (break))
    (if (> risk 9)
        (mod risk 9)
        risk)))


(defun risks-path (risks &optional (end-multiplier 1))
  (loop :with q := (make-pqueue #'<)
        :with end := (mapcar (compose #'1- (curry #'* end-multiplier))
                             (array-dimensions risks))
        :with paths := (make-hash-table :test 'equal)
          :initially (pqueue-push '(0 0) 0 q)
        :for (cur cost) := (multiple-value-list (pqueue-pop q))
        :until (gethash end paths)
        :do (unless (gethash cur paths)
              (setf (gethash cur paths) cost)
              (loop :for n :in (pos-tile-neighbors risks cur end-multiplier)
                    :for r := (position-risk risks n)
                    :do (pqueue-push n (+ cost r) q)))
        :finally (return (gethash end paths))))

(defun solve-1 (&optional (input (day-input-lines 15 2021)))
  (risks-path (parseq 'map input)))

(defun solve-2 (&optional (input (day-input-lines 15 2021)))
  (risks-path (parseq 'map input) 5))
