(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-1
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-1)

(defun parse-input (input)
  (mapcar #'parse-integer input))

(defun solve-1 (&optional (input (day-input-lines 1 2021)))
  (loop :with in := (parse-input input)
        :for fst :in in
        :and snd :in (rest in)
        :count (< fst snd)))

(defun solve-1-functional (&optional (input (day-input-lines 1 2021)))
  (let ((in (parse-input input)))
    (count 't (mapcar #'< in (rest in)))))

(defun slide-window (input)
  (loop :repeat 3
        :for x :in input
        :sum x))

(defun solve-2 (&optional (input (day-input-lines 1 2021)))
  (loop :with in := (parse-input input)
        :for fstl :on in
        :and sndl :on (rest in)
        :while (<= 3 (length sndl))
        :count (< (slide-window fstl)
                  (slide-window sndl))))

(defun windowize (list)
  (mapcar #'+ list (cdr list) (cddr list)))

(defun solve-2-functional (&optional (input (day-input-lines 1 2021)))
  (let ((in (windowize (parse-input input))))
    (count 't (mapcar #'< in (rest in)))))
