(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-12
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        )
  (:import-from #:alexandria #:curry #:compose))

(in-package #:day-12)

(defparameter *test-1* '("start-A"
                         "start-b"
                         "A-c"
                         "A-b"
                         "b-d"
                         "A-end"
                         "b-end"))

(defparameter *test-2* '("dc-end"
                         "HN-start"
                         "start-kj"
                         "dc-start"
                         "dc-HN"
                         "LN-dc"
                         "HN-end"
                         "kj-sa"
                         "kj-HN"
                         "kj-dc"))

(defparameter *test-3* '("fs-end"
                         "he-DX"
                         "fs-he"
                         "start-DX"
                         "pj-DX"
                         "end-zg"
                         "zg-sl"
                         "zg-pj"
                         "pj-he"
                         "RW-he"
                         "fs-DX"
                         "pj-RW"
                         "zg-RW"
                         "start-pj"
                         "he-WI"
                         "zg-he"
                         "pj-fs"
                         "start-RW"))


(defun parse-input (input)
  (let ((connections (mapcar (curry #'parseq:parseq 'connection) input))
        (map (make-hash-table :test 'equal)))
    (loop :for (a b) :in connections
          :do (when (string/= a "start") (push a (gethash b map)))
              (when (string/= b "start") (push b (gethash a map))))
    map))

(parseq:defrule connection ()
    (and node "-" node)
  (:choose 0 2))

(parseq:defrule node ()
  (+ alpha)
  (:string))

(defun big-cave-p (cave)
  (upper-case-p (char cave 0)))

(defun small-twice-possible-p (cave)
  (and (not (big-cave-p cave))
       (string/= cave "end")
       (string/= cave "start")))

(defun paths-to-end (map &optional (current "start") visited)
  (when (string= current "end")
    (return-from paths-to-end (list (list "end"))))
  (let ((next (set-difference (gethash current map) visited :test 'equal)))
    (loop :for n :in next
          :nconc (mapcar (curry #'cons current)
                         (paths-to-end map n (if (big-cave-p current)
                                                 visited
                                                 (cons current visited)))))))

(defun paths-to-end-small-twice (map &optional (current "start") visited)
  (when (string= current "end")
    (return-from paths-to-end-small-twice (list (list "end"))))
  (let ((next (gethash current map)))
    (if (and (not (big-cave-p current))
             (= 1 (count current visited :test 'equal)))
        (paths-to-end map current visited)
        (loop :for n :in next
              :nconc (mapcar (curry #'cons current)
                             (paths-to-end-small-twice map n (if (big-cave-p current)
                                                                 visited
                                                                 (cons current visited))))))))


(defun solve-1 (&optional (input (day-input-lines 12 2021)))
  (length (paths-to-end (parse-input input))))

(defun solve-2 (&optional (input (day-input-lines 12 2021)))
  (length (paths-to-end-small-twice (parse-input input))))
