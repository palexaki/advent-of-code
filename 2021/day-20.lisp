(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-20
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule))

(in-package #:day-20)

(defparameter *test* '("..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#"
                       ""
                       "#..#."
                       "#...."
                       "##..#"
                       "..#.."
                       "..###"))

(defrule input ()
    (and enhancement "" image)
  (:choose 0 2))

(defrule enhancement ()
    (string (+ bit))
  (:choose 0)
  (:lambda (&rest bs)
    (make-array (length bs) :element-type 'bit
                            :initial-contents bs)))

(defrule image ()
    (+ row)
  (:lambda (&rest rows)
    (make-array (list (length rows)
                      (length (first rows)))
                :element-type 'bit
                :initial-contents rows)))

(defrule row ()
    (string (+ bit))
  (:choose 0))

(defrule bit ()
    (or #\# #\.)
  (:lambda (bit) (bit-value bit)))

(defun pad-image (image padding)
  (let ((padded (make-array (list (+ (* 2 padding)
                                     (array-dimension image 0))
                                  (+ (* 2 padding)
                                     (array-dimension image 1)))
                            :element-type 'bit)))
    (loop :for y :from 0 :below (array-dimension image 0)
          :do (loop :for x :from 0 :below (array-dimension image 1)
                    :do (setf (aref padded (+ y padding) (+ x padding))
                              (aref image y x))))
    padded))

(defun safe-aref (image y x)
  (aref image
        (max 0 (min (1- (array-dimension image 0)) y))
        (max 0 (min (1- (array-dimension image 1)) x))))

(defun binary-list-value (list)
  (loop :for e := 1 :then (* e 2)
        :for i :in (reverse list)
        :sum (* e i)))

(defun new-pixel-index (image y x)
  (binary-list-value
   (loop :for ya :from (1- y) :upto (1+ y)
         :nconc (loop :for xa :from (1- x) :upto (1+ x)
                      :collect (safe-aref image ya xa)))))

(defun enhance (image algorithm)
  (let ((new-image (alexandria:copy-array image)))
    (loop :for y :from 0 :below (array-dimension image 0)
          :do (loop :for x :from 0 :below (array-dimension image 1)
                    :do (setf (aref new-image y x) (aref algorithm
                                                         (new-pixel-index image y x)))))
    new-image))

(defun bit-value (char)
  (position char '(#\. #\#)))

(defun print-image (image)
  (dotimes (y (array-dimension image 0))
    (dotimes (x (array-dimension image 1))
      (format t "~[.~;#~]" (aref image y x)))
    (format t "~%"))
  (format t "~%"))

(defun count-light (image)
  (let ((count 0))
    (dotimes (y (array-dimension image 0))
      (dotimes (x (array-dimension image 1))
        (incf count (aref image y x))))
    count))

(defun solve-1 (&optional (input (day-input-lines 20 2021)))
  (destructuring-bind (algorithm image) (parseq 'input input)
    (count-light
     (enhance (enhance (pad-image image 6) algorithm)
              algorithm))))

(defun solve-2 (&optional (input (day-input-lines 20 2021)))
  (destructuring-bind (algorithm image) (parseq 'input input)
    (loop :repeat 51
          :for enhanced := (pad-image image 50) :then (enhance enhanced algorithm)
          :finally (return (count-light enhanced)))))
