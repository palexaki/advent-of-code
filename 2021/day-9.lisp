(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :queues.simple-queue)
  (ql:quickload :queues)
  (ql:quickload :advent-util))
(defpackage #:day-9
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-9)

(defparameter *test* '("2199943210"
                       "3987894921"
                       "9856789892"
                       "8767896789"
                       "9899965678"))

(parseq:defrule input ()
    (+ line)
  (:lambda (&rest rows)
    (make-array (list (length rows)
                      (length (first rows)))
                :initial-contents rows)))

(parseq:defrule line ()
    (string (+ digit))
  (:choose 0)
  (:lambda (&rest digits)
    (mapcar (lambda (digit)
              (- (char-code digit) 48))
            digits)))


(defun minimum-positions (map)
  (loop :for x :from 0 :below (array-dimension map 1)
        :nconc (loop
                 :for y :from 0 :below (array-dimension map 0)
                 :for n := (aref map y x)
                 :when (every (alexandria:compose (alexandria:curry #'< n)
                                                  (alexandria:curry #'board-position map))

                              (pos-neighbors map (make-pos x y)))
                   :collect (list n (make-pos x y)))))

(defun solve-1 (&optional (input (day-input-lines 9 2021)))
  (let ((map (parseq:parseq 'input input)))
    (reduce #'+ (mapcar (alexandria:compose #'1+ #'first)
                        (minimum-positions map)))))

(defun basin-size (map low-point)
  (declare (optimize (debug 3)))
  (loop :with q := (queues:make-queue :simple-queue)
          :initially (queues:qpush q low-point)
        :with passed := (make-hash-table :test 'equal)
        :with size := 0
        :while (plusp (queues:qsize q))
        :for pos := (queues:qpop q)
        :for val := (board-position map pos)
        :when (and (/= val 9)
                   (not (gethash pos passed)))
          :do (progn (mapcar (lambda (neighbor)
                               (unless (gethash neighbor passed)
                                 (queues:qpush q neighbor)))
                             (pos-neighbors map pos))
                     (setf (gethash pos passed) t)
                     ;(format t "~a:~a~%" pos val)
                     (incf size))
       :finally (return size)))

(defun solve-2 (&optional (input (day-input-lines 9 2021)))
  (let ((map (parseq:parseq 'input input)))
    (reduce #'* (sort (mapcar (alexandria:compose (alexandria:curry #'basin-size map)
                                                  #'second)
                              (minimum-positions map))
                      #'>)
            :end 3)))
