(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-7
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        ))

(in-package #:day-7)

(defparameter *test* '("16,1,2,0,4,2,7,1,2,14"))

(parseq:defrule input ()
  (+ (and int (? ",")))
  (:lambda (&rest nums)
    (mapcar #'first nums)))

(defun diff (x nums)
  (reduce #'+ (mapcar (lambda (y) (abs (- y x))) nums)))

(defun diff-g (x nums)
  (reduce #'+ (mapcar (lambda (y) (let ((d (abs (- y x))))
                                    (/ (* d (1+ d)) 2)))
                      nums)))

(defun find-min-diff (nums diff-func)
  (loop :with save-x := (first nums)
        :with min-diff := most-positive-fixnum
        :for x :in nums
        :for diff := (funcall diff-func x nums)
        :when (< diff min-diff)
          :do (setf save-x x
                    min-diff diff)
        :finally (return (values save-x
                                 min-diff))))

(defun solve-1 (&optional (input (day-input-lines 7 2021)))
  (let ((input (parseq:parseq 'input (first input))))
    (find-min-diff input #'diff)))

(defun solve-2 (&optional (input (day-input-lines 7 2021)))
  (let ((input (parseq:parseq 'input (first input))))
    (find-min-diff input #'diff-g)))
