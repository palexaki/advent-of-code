(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-19
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose)
  (:import-from #:parseq
                #:parseq
                #:defrule))

(in-package #:day-19)

(defun read-file-into-lines (file)
  (with-open-file (stream file)
    (loop :for line := (read-line stream nil)
          :while line
          :collect line)))

(defparameter *test-1* (read-file-into-lines #P"./19-test-1.txt"))

(defparameter *test-2* (read-file-into-lines #P "./19-test-2.txt"))

;;

(defrule input ()
    (+ scanner))

(defrule scanner ()
    (and scanner-tag
         (+ (string coordinate))
         (? ""))
  (:choose 0 1)
  (:lambda (tag coordinates)
    (list tag (mapcar #'first coordinates))))

(defrule scanner-tag ()
    (string (and "--- scanner " int " ---"))
  (:choose '(0 1)))

(defrule coordinate ()
    (and num "," num "," num)
  (:choose 0 2 4))

(defrule num ()
    (and (? "-") int)
  (:lambda (neg num)
    (if neg
        (- num)
        num)))

(defun apply-permutation (permutation coords)
  (destructuring-bind (p-x p-y p-z) permutation
    (list (nth p-x coords)
          (nth p-y coords)
          (nth p-z coords))))

(defun apply-negation (negations coords)
  (mapcar (lambda (neg coord) (if neg (- coord) coord))
          negations
          coords))

(defun bl ()
  (case f-axis
    (0 (case up-axis
         (1)
         (2)))
    (1)
    (2)))

;;; We deduce the "left" side by taking the remaining axis and then
;;; taking the left-direction to be f-dir <=> up-dir, this is because
;;; if f-dir and up-dir are the same l-dir is positive, if they are
;;; different, l-dir is negative
(defun deduce-left (f-axis f-dir up-axis up-dir)
  (flet ((l-dir ()
           (case f-axis
             (0 (case up-axis
                  (1 (not (alexandria:xor f-dir up-dir)))
                  (2 (alexandria:xor f-dir up-dir))))
             (1 (case up-axis
                  (0 (alexandria:xor f-dir up-dir))
                  (2 (not (alexandria:xor f-dir up-dir)))))
             (2 (case up-axis
                  (0 (not (alexandria:xor f-dir up-dir)))
                  (1 (alexandria:xor f-dir up-dir)))))))
    (let ((l-axis (first (set-difference '(0 1 2) (list f-axis up-axis)))))
      (list l-axis
            (l-dir)))))

(defun generate-rotations ()
  (loop :for (f-axis f-dir) :in (alexandria:map-product 'list '(0 1 2) '(nil t))
        :nconc (loop :for (up-axis up-dir) :in (alexandria:map-product 'list
                                                                       (remove f-axis '(0 1 2))
                                                                       '(nil t))
                     :for (l-axis l-dir) := (deduce-left f-axis f-dir
                                                         up-axis up-dir)
                     :collect (list (list f-axis l-axis up-axis)
                                    (list f-dir l-dir up-dir)))))

(defun apply-rotation (rotation coords)
  (destructuring-bind (permutation negation) rotation
    ;(apply-negation negation (apply-permutation permutation coords))
    (apply-permutation permutation (apply-negation negation coords))
    ))

(defun generate-all-rotations (coords)
  (mapcar (lambda (rotation)
            (mapcar (curry #'apply-rotation rotation) coords))
          (generate-rotations)))

(defun differences-overlap (diff-a diff-b)
  (let ((len (length (intersection diff-a diff-b :test #'equal))))
    (>= len
        12)))

(defun coords-match (as bs)
  (loop :for b-a :in as
        :for diff-a := (mapcar (alexandria:rcurry #'pos-sub b-a) as)
          :thereis (loop :for b-b :in bs
                         :for diff-b := (mapcar (alexandria:rcurry #'pos-sub b-b)
                                                bs)
                         :when (differences-overlap diff-a diff-b)
                           :return (list (mapcar (curry #'pos-add b-a)
                                                   diff-b)
                                           (pos-sub b-a b-b)))))

(defun match-coords (base other)
  (let ((all-rotations (generate-all-rotations other)))
    (some (lambda (rotated)
            (coords-match base rotated))
          all-rotations)))

(defun solve-1 (&optional (input (day-input-lines 19 2021)))
  (let* ((scanners (mapcar #'second (parseq 'input input)))
         (correct (list (first scanners)))
         (scanners-pos '((0 0 0))))
    (pop scanners)
    (loop :while scanners
          :do (loop :for scanner :in scanners
                    :for overlapped := (loop :for base :in correct
                                               :thereis (match-coords base
                                                                      scanner))
                    :when overlapped
                      :do (push (first overlapped) correct)
                          (push (second overlapped) scanners-pos)
                          (setf scanners (remove scanner scanners))
                          (format t "Found: ~a" (length correct))
                          (return)))
    (values (length (remove-duplicates (apply #'append correct)
                                       :test 'equal))
            scanners-pos)))

(defun distance (a b)
  (reduce #'+ (mapcar (compose #'abs #'-) a b)))

(defun solve-2 (&optional (input (day-input-lines 19 2021)))
  (multiple-value-bind (total-beacons scanner-positions)
      (solve-1 input)
    (declare (ignore total-beacons))
    (loop :for x :in scanner-positions
          :maximize (loop :for y :in scanner-positions
                          :maximize (distance x y)))))
