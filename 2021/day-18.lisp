(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :advent-util))
(defpackage #:day-18
  (:use #:advent-util
        #:cl
        ;#:drakma
        ;#:cl-ppcre
        ;#:alexandria
        )
  (:import-from #:alexandria
                #:curry #:compose #:if-let)
  (:import-from #:parseq
                #:parseq
                #:defrule))

(in-package #:day-18)

(defparameter *test* '("[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]"
                       "[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]"
                       "[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]"
                       "[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]"
                       "[7,[5,[[3,8],[1,4]]]]"
                       "[[2,[2,2]],[8,[8,1]]]"
                       "[2,9]"
                       "[1,[[[9,3],9],[[9,0],[0,7]]]]"
                       "[[[5,[7,4]],7],1]"
                       "[[[[4,2],2],6],[8,7]]"))

(defparameter *test-2* '("[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]"
                         "[[[5,[2,8]],4],[5,[[9,9],0]]]"
                         "[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]"
                         "[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]"
                         "[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]"
                         "[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]"
                         "[[[[5,4],[7,7]],8],[[8,3],8]]"
                         "[[9,3],[[9,9],[6,[4,9]]]]"
                         "[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]"
                         "[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]"))

(defun parse-snail-number (string)
  (read-from-string (map 'string #'identity
                         (sublis '((#\[ . #\() (#\] . #\)) (#\, . #\Space))
                                 (map 'list #'identity string)))))

(defun explode (node &optional (depth 0))
  (labels ((add-to (direction number node)
             (cond
               ((= 0 number) node)
               ((numberp node) (+ number node))
               ((eq direction 'right)
                (list (add-to 'right number (first node))
                      (second node)))
               ((eq direction 'left)
                (list (first node)
                      (add-to 'left number (second node))))
               (t (error "Invalid input, direction: ~a" direction)))))
    (if (numberp node)
        (values)
        (if (= depth 4)
            (values 0 (first node) (second node))
            (if-let (left-exploded-p (multiple-value-list (explode (first node)
                                                                   (1+ depth))))
              (values (list (first left-exploded-p)
                            (add-to 'right
                                    (third left-exploded-p)
                                    (second node)))
                      (second left-exploded-p)
                      0)
              (if-let (right-exploded-p (multiple-value-list (explode (second node)
                                                                      (1+ depth))))
                (values (list (add-to 'left
                                      (second right-exploded-p)
                                      (first node))
                              (first right-exploded-p))
                        0
                        (third right-exploded-p))
                (values)))))))

(defun split (node)
  (cond ((and (numberp node) (> node 9))
         (list (floor node 2)
               (ceiling node 2)))
        ((numberp node) nil)
        (t (if-let (left-split (split (first node)))
             (list left-split (second node))
             (if-let (right-split (split (second node)))
               (list (first node) right-split))))))

(defun reduce-snail (snail-num)
  (if-let (exploded (explode snail-num))
    (reduce-snail exploded)
    (if-let (split (split snail-num))
      (reduce-snail split)
      snail-num)))

(defun add-snail (a b)
  (reduce-snail (list a b)))

(defun magnitude (snail-num)
  (if (numberp snail-num)
      snail-num
      (values (+ (* 3 (magnitude (first snail-num)))
                 (* 2 (magnitude (second snail-num))))
              snail-num)))

(defun solve-1 (&optional (input (day-input-lines 18 2021)))
  (magnitude (reduce #'add-snail (mapcar #'parse-snail-number input))))

(defun solve-2 (&optional (input (day-input-lines 18 2021)))
  (let ((nums (mapcar #'parse-snail-number input)))
    (loop :for x :in nums
          :maximize (loop :for y :in (remove x nums :test #'equal)
                          :maximize (magnitude (add-snail x y))))))
